\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}The installation process}{1}
\contentsline {section}{\numberline {3}Program running}{2}
\contentsline {subsection}{\numberline {3.1}Tune bash shell}{2}
\contentsline {subsection}{\numberline {3.2}Start program}{3}
\contentsline {section}{\numberline {4}Program manager description (control bar)}{3}
\contentsline {subsection}{\numberline {4.1}Standard browser button}{4}
\contentsline {subsection}{\numberline {4.2}Select configuration file button}{5}
\contentsline {subsection}{\numberline {4.3}Select tif file button}{6}
\contentsline {subsection}{\numberline {4.4}Read tif file button}{7}
\contentsline {subsection}{\numberline {4.5}Do pedestals (baseline) button}{7}
\contentsline {subsection}{\numberline {4.6}Do cluster finder button}{7}
\contentsline {subsection}{\numberline {4.7}Save SDManger}{8}
\contentsline {section}{\numberline {A}ROOT installation instruction for UBUNTU}{9}
\contentsline {section}{\numberline {B}Brief XEmacs Command Summary}{10}
