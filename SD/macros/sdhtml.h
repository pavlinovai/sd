/*
 * sdhtml.h
 *
 *  Created on: Aug 10, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDHTML_H_
#define SDHTML_H_

bool Load();
void DocClass();
void dc();
void DocMacros(int im=-1);
void dm(int im=-1);
void docAll();
void HtmlDocForClasses();
void InitHtml();
void HtmlDocForClasses();
void HtmlDocForMacros(int im=-1);
void HtmlIndex();

#endif /* SDHTML_H_ */
