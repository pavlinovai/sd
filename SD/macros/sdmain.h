// Macro for running sdmain staff : does not compile now
class SDManager;
class SDSimulationManager;

void sdmainOne(int var=0, int bar=0);
void gui(Int_t toBrowser=1);

void initSD();
void loadAliasLib();
void testUtils();
//
//void readDirectory(const Char_t* dir="/Users/pavlinov/tmp/SD/root/");
void readDirectory(const Char_t* dir="/Users/pavlinov/wrk/sdoutput/");
SDManager* readFileFromDirectory(const Char_t* dir="/Users/pavlinov/wrk/sdoutput/");
void picts(const char* dir="");
// XML staff
void testXML();

// Simulation
void testSim(Int_t nev=100, Double_t h=1., Double_t a=10., Double_t b=10.);
void varSim(const Int_t var);
void pictsSim(const char* dir="");

SDSimulationManager* readSimManagerFromDirectory(const Char_t* dir="/Users/pavlinov/wrk/sdoutput/SIMULATION");
