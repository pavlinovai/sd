#ifndef includeForPlainRoot_ROOT
#define includeForPlainRoot_ROOT
//
// 28-aug-2003 - just for convenience; Jan 26,2006; May 23, 2007
//
#include <stdio.h>
#include <math.h>
// --
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
// --
#include <TROOT.h>
#include <TSystem.h>
#include <TClassTable.h>
#include <TDirectory.h>
#include <TStyle.h>
#include <TString.h>
#include <TStringLong.h> // Jul 14,09
#include <TTree.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TFile.h>
#include <TArrayD.h>
#include <TMarker.h>
#include <TLatex.h>
#include <TLine.h>
#include <TPaveText.h>
#include <TBrowser.h>
#include <TMath.h>
#include <TRandom.h>
#include <TStopwatch.h>
#include <TTime.h>
#include <TGraphErrors.h>
#include <TVector3.h>
#include <TParticle.h>
#include <TPDGCode.h>
#include <TParticlePDG.h>
#include <TDatabasePDG.h>
#include <TArrow.h>
#include <TKey.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TChain.h>
#include <TMap.h>   // May 23, 2007
#include <TStopwatch.h> // Jun 08, 2007
#include <TDatime.h>
#include <TGeoManager.h> // Jan 29,08
//
#include <TNtuple.h>

#include <Gtypes.h> // color, line style and so on

#endif
