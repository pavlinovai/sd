/*
 * SDClusterFinder2DOne.h
 *
 *  Created on: May 20, 2012
 *      Author: pavlinov
 */

#ifndef SDCLUSTERFINDER2DONE_H_
#define SDCLUSTERFINDER2DONE_H_

#include "SDClusterFinderStdOne.h"

class TDataSet;

class SDClusterFinder2DOne: public SDClusterFinderStdOne {
public:
	SDClusterFinder2DOne();
	SDClusterFinder2DOne(const char* name);
	virtual ~SDClusterFinder2DOne();
	//
	TDataSet* GetCLFPool() {return FindByName(gkCLFPool);}
	SDClusterFinderStdOne* GetCLF(Int_t ind);
	//
	virtual Int_t FindClusters();
	virtual Int_t FindClsutersWithThreads();      // Apr 14,2013
	virtual Int_t FindClsutersWithPosixThreads(); // Apr 20,2013

	static void* FindClustersStatic(void* arg);
//
	static const char* gkCLFPool;     // name of cluster finder pool

	ClassDef(SDClusterFinder2DOne,1)
};

#endif /* SDCLUSTERFINDER2DONE_H_ */
