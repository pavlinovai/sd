/*
 * SDUtil.h
 *
 *  Created on: Apr 17, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDUTIL_H_
#define SDUTIL_H_

#include <TDataSet.h>
#include <TThread.h>
#include "SDManager.h"
#include "SDImageAnalysis.h"

#include <deque>
#include <list>
#include <vector>

#include "sdpixel.h"

class TF1;
class TH1;
class TGFrame;
class TCanvas;

using namespace std;

typedef deque<pixel> clusterSimple; // first variant
typedef list<clusterSimple *> clusterContainerSimple;
typedef unsigned char byte;

class compPixels {
public:
	bool operator() (pixel c1, pixel c2) {return (c1.amp>c2.amp);}
};

class SDManager;
class SDCluster;
class SDClusterBase;
class SDClusterFinderBase;
class SDClusterFinderStdOne;
class SDPedestals;
class parsFillHists;
class parsClusterFinder;
class parsInputOutput;
class parsPedestals;
class sdClusterChar;
class SDTiffFileInfo;

class SDSimulationManager;
class SimResponseFunction;

class TH2;
class TF1;
class TDataSet;
class TGProgressBar;
class TImagePalette;

//class pthread_t;
//class pthread_attr_t;

class SDUtils: public TDataSet {
public:
	SDUtils(const char* name="SDUtils");
	virtual ~SDUtils();
    //
	static SDManager*           GetSDManager(TDataSet* dsChild, int pri=0);
	static SDTiffFileInfo*      GetTiffFileInfo(TDataSet* dsChild);
	static SDImageAnalysis*     GetImageAnalysis(TDataSet* dsChild);
	static SDPedestals*         GetPedestals(TDataSet* dsChild);
	static SDPedestals*         GetPedestalsParent(TDataSet* dsChild);
	static SDClusterFinderBase* GetClusterFinder(TDataSet* dsChild, const Int_t ind);
	static SDClusterFinderBase* GetClusterFinderParent(TDataSet* dsChild); // Nearest parent
	static parsClusterFinder*   GetParsClusterFinder(TDataSet* dsChild, int pri=0);
	static parsInputOutput*     GetParsOutput(TDataSet* dsChild);
	static parsInputOutput*     GetParsInput(TDataSet* dsChild);
	static parsFillHists*       GetParsFillHists(TDataSet* dsChild);
	static parsPedestals*       GetParsPedestals(TDataSet* dsChild);
	//
	template<class T>
	static T* GetParentTempl(TDataSet* child)
	{
		// Get nearest parent of T : works well!
		TDataSet *parent = 0;
		T* t = 0;
		while ((parent = child->GetParent())) {
			t = dynamic_cast<T *>(parent);
			if (t == 0) child = parent;
			else        break;
		}
		return t;
	}
	// Some useful methods
	static Double_t RelError(Double_t a1, Double_t a2);
	//
	static Bool_t IsPixelNeighbourToCluster(const pixel &p, clusterSimple* vp, Bool_t (*fun)(const pixel &p1, const pixel &p2));
	static Bool_t IsPixelNeighbourToCluster(const pixel &p, SDCluster* clst, Bool_t (*fun)(const pixel &p1, const pixel &p2));
	static TImagePalette *GetGrayPallete(Int_t npoints=256, Int_t nbits=8, Int_t pri=0);
	static void PrintPallete(const TImagePalette* pal);
	//
    static void FillPixel(TH2* hin, int binx, int biny, pixel &p, int reset=1);
    // static void FillPixelWithNeighbors(const TH2* hin, int binx, int biny, pixel &p);
    //
    static void FillCluster(const TH2* hin, const Double_t th, const Double_t ped, clusterSimple& vp, TH2* h2th);
	static void FillCluster2d(const TH2* hin, const Double_t th, clusterContainerSimple& vp, TH2* h2th);
	// Apr 20,2013 - transition from (col,row) <--> (binx,biny)
	// shift is different for x(y) axis
	static UShort_t GetColRowFromBin(UShort_t binxy, UShort_t shift)  {return (binxy  - shift);}
	static UShort_t GetBinFromColRow(UShort_t colrow, UShort_t shift) {return (colrow + shift);}
    //
	static void FillClusterFromHist(const TH2* hin, clusterSimple &cl, Bool_t sort=kTRUE);
//	static void FillClusterFromHist(const TH2* hin, SDCluster &cl);
	static void FillClusterFromHist(const TH2* hin, SDClusterBase &cl);
	// from clusters to hists
	static TH2* FillHistFromCluster(clusterSimple* clust, Int_t nclust);
	// Read sd or simulation manager
	static TDataSet* ReadDirectory(const char* dir="/Users/pavlinov/tmp/SD/root/");
//	template<typename T>
//	static T* ReadManager(const char *nf="");
//	template<typename T>
//    static T* ReadManagerFromDirectory(const char* dir=0);

	// Service methods
	static void FillFitInfoToHist(TH2 *h2, Double_t sigma, Option_t *option,
			     Double_t threshold, Double_t thAbs, const char *name = "");
	static TH2*  CloneHist(TH2* hin, const char* addName="", const char* tit="", int reset=1);
	static void  AddCorrValueToTitle(TH2* h2);
    static TList* BookAndFill1DHistsFromClusterHist(TH2* h2, const char* opt, Int_t scaleProj=0, TH1* hamp=0);
    // Jun 26,2012
	static SDClusterFinderBase* CreateClusterFinder(parsClusterFinder &pcf, Int_t pri=0);
	static SDClusterBase* CreateCluster(const char* name, const UInt_t nmax, const UInt_t key=0);
    static TF1* Expo(const char *addName, double xmi, double xma, const char* name0="A_{0}",
    		const char* name1="B", Double_t val0=20., Double_t val1=2.e-5);
	static void PrintChar(char ch);
	static void PrintBlock(byte* vch, Int_t start, Int_t end, const Char_t *tit="");
	static void PrintBlock(vector<char> memblock, Int_t start, Int_t end, const Char_t *tit="");
	static void PrintBlock(UShort_t* vshort, Int_t start, Int_t end, const Char_t *tit="");
	static void DeleteVectorMember(vector<TString> &v, TString &m, int pri=0);
	// Tif staff
	static UShort_t GetUShort(byte* vch, const UInt_t ind, Bool_t switchByte=kFALSE);
	static UInt_t   GetUInt(byte* vch, const UInt_t ind, Bool_t switchByte=kFALSE);
	static Float_t  GetFloat(byte* vch, const UInt_t ind, Bool_t switchByte=kFALSE);
	// GUI & Parser
	static Long_t   GetPointer(void* p, Long_t offset);
	static Float_t  GetPointerFloat(void* p, Long_t offset);
	static Int_t    GetPointerInt(void* p, Long_t offset);
	static TString  GetPointerTString(void* p, Long_t offset);
	template<class T> static T GetPointer(void* p, Long_t offset);
	static TGFrame* GetFrame(TList *l,Int_t i);
	// Hist. staff
    static Bool_t   GetFWHM(TH1* h, Double_t &xhm1, Double_t &xhm2);
    static Bool_t   GetFWHM(TH1* h, Double_t &fwhm);
    static void     GetFWHMs(TH1* hx, TH1* hy, sdClusterChar& sdCh);
    // Resolve linear equation between two points: (x1,y1) and (x2,y2)
    static Double_t GetX(Double_t y, Double_t x1,Double_t y1, Double_t x2,Double_t y2);
    static TH1*     RebinAndReplace(TList *l, Int_t ind, TH1* hin, Int_t nrebin);
    static void     CopyHistContent(TH2* hin, TH2* hout);
    static TList*   CalculateHistCharacteristics(TH2* h2, sdClusterChar &sdChar);
    //
    //            F i l t e r s
    //      Median filters
    static TH2* MedianFilter(TH2* hin, UInt_t window=1, TH2* hout=0); // 1,2,3 and ..
    static TH2* MedianFilter(TH2* hin, TString sopt, TH2* hout=0);    // "m1","m2" and so on
    //     Mean filter
    static TH2* MeanFilter(TH2* hin,UInt_t wx=1,UInt_t wy=1, TH2* hout=0);
    static TH2* MeanFilter(TH2* hin,TString sopt, TH2* hout=0);
    static TH1* MeanFilter(TH1* hin,UInt_t w=1, TH1* hout=0, Int_t pri=0);
    // Select and apply corresponding filter
    static TH2* Smooth(TH2* hin, TString sopt, TH2* hout=0);
//    void Rebin2D(int idiv = 1, int ngr = 2, const char *name = ""); // *MENU*
    // CVS
    static void PrintCvsInfo();  // Version control information
    static TString GetVersion();
    //   S I M U L A T I O N
    static TGProgressBar *GetHProgressBar(Int_t hsize=400);
    static void UpdateProgressBar(TGProgressBar *pb, Int_t nevCurrent, Int_t nev);
    static TList* BookHistsPars1VsPars2(const char* name, const char* opt="Ana vs TH2", Int_t pri=0);
	static void FillHistsPars1VsPars2(TList* l, const sdClusterChar& par1, const sdClusterChar& par2);

    static SDClusterFinderStdOne* InitClusterFinder(parsClusterFinder *pcf, TH2* hin, Double_t sigF0Unit=0.07);
    //
    // Posix Thread
    //
#ifndef WIN32
    static size_t SetPThreadStacksize(size_t newStackSize, Int_t pri=0);
#endif
//    static Int_t  InitThread(pthread_attr_t& attr, pthread_t& thread, Int_t detach=0);
    static void  DrawHistInThreads(TH1* h, TCanvas* c);
    static void* DrawHist(void* arg);
	// May 7,2013 - for sdExe.cpp
	static void LoadAliasLib();

 	ClassDef(SDUtils,1)
};

typedef SDUtils sdu;

#endif /* SDUTIL_H_ */
