//
// Nov 13,2012 - move from $HOME/macros
//
#ifndef SDALIASES_H_
#define SDALIASES_H_
//
// You can use the next notation sda::nameOfFunction(.. )
// instead of SDAliases::nameOfFunction(.. )
//

#include "sdOpt.h"

#include <TNamed.h>
#include <TString.h>
#include <TMath.h>

class TCanvas; 
class TPad;
class TGraph;
class TGraphErrors;
class TLatex;
class TLegend;
class TPaveText; 
class TList; 
class TH1;
class TH2;
class TH1F;
class TProfile;
class TF1;
class TF2;
class TH1D;
class TH2F;
class TNtuple;
class TVector3;
class TLorentzVector;
class TArrayD;
class TFile;
class Pypars_t;
class TSeqCollection;
class TMap;
class TMinuit;

//class SD_DLLEXPORT SDAliases : public TNamed{
class SDAliases : public TNamed{
 public:
  SDAliases(const Char_t *name="AliasPAI",const Char_t *title="Set of methods for convenience");
  virtual ~SDAliases() { }

  void Help();           // *MENU* 

  static void GetEnvironment(const Char_t* envName, TString &sEnvValue, int print=1); 
  static TList* FindList(TList *l, const char* name);
  static TList* GetList(TList *l, UInt_t ind);
  static TH1* GetH1(TList *l, UInt_t ind);
  static TH2* GetH2(TList *l, UInt_t ind);
  static TF1* GetF1(TList *l, UInt_t ind);
  static TF2* GetF2(TList *l, UInt_t ind);
  static TProfile* GetProfile(TList *l, UInt_t ind);
  static void ResetListOfHists(TList *l);
  static void ClearListOfHists(TList *l);
  static TList*   ReadAll(const char *fname, const char *fnl="list", int pri=0); // *MENU*
  static TObject* ReadFirstObject(const char *fname);
  static TObject* ReadFirstObjectFromDirectory(const char* dir=0);

  static Int_t FillH2byTF2(TH2* h2, TF2 *f2, Double_t th=0.0, Double_t epsil=0.0); // Return number of entries (pixels)

  static void stat(const int istat=1);
  // Read && Write methods
  static TH1* hid(const char *fname, const char *nid);                     // *MENU* 
  static TH1* hid(const char *fname, const char *nid, const char *adName); // *MENU* 
  static int  hrin(const char* file="", const char *opt="hist",Int_t pri=0); // *MENU* 
  static TList*  hrinToList(const char* file="",const char *ln="liOfHis",const bool putToBrowser=kFALSE, Int_t pri=0); // *MENU* 
  static void rf(const char *fname, const char *nid);                      // *MENU* 
  static void rf(const char *fname, const int id);                         // *MENU*   
  static TNtuple *readNtuple(const char *fname, const char *fnuple);       // *MENU* 
  static int saveListOfHists(TList *list=0, const char* name="test", Bool_t kSingleKey=kFALSE, 
  const char* opt="RECREATE");
  static void printFileRootVer(const TFile *f);

  static TList* moveHistsToList(const char* name="ListHist1", const bool putToBrowser=kFALSE,
  int pri=0); // *MENU*
  static void AddToName(TH1* hid=0, const char *addText="");
  static void AddToTitle(TH1* hid=0, const char *addText="");
  static void AddToNameAndTitle(TH1   *h=0, const char *name="", const char *title="");
  static void AddToNameAndTitleToList(TList *l=0,const char *name="",const char *title="");
 // Sum. of hists (first for HF) - 05-06-04
  static TList *SumHists(const char* ln="HF", 
  const char* lfs="/home/pavlinov/STAR/SIMULATION/PYTHIA/MuDstLists/2003/all-1.lis", 
  const char* nout="/data/r18a/data0/pavlinov/EXPDATA/2003/elMuDstPpSum.root",
  const char* opt="RECREATE");
  static int add(TList *l1, TList *l2);
  static int addOld(TList *l1, TList *l2);
  static void CompareHistsParameters(TH1 *h1, TH1 *h2);
  static TF1* GetFunction(TH1 *h=0, const int ind=0);

  // Drawing hists ..
  static void DrawHist(TH1* hid=0, int lineWidth=1, int lineColor=1, const char* opt="", int lineStyle=1);// *MENU* 
  static void draw(TH1* hid=0, int lineWidth=1, int lineColor=1, const char* opt="", int lineStyle=1)
    {DrawHist(hid, lineWidth, lineColor,opt, lineStyle);}                // *MENU* 
  static TCanvas* DrawListOfHist(TList *list, int nx, int ny, int nmax=0, int istart=0);
  static void DrawTwoListsOfHists(TList *li1=0, TList *li2=0, int nx=0, int ny=0,
		  const char *tit1="", const char *tit2="", int col1=1, int col2=2,
		  const char *globalTitle="", Int_t sameMax=0);
  static void dispLine(double xm=0, double ym=0, Short_t color=1, const char* text="",
  double dx=5, int lineStyle=1, int lineWidth=2, double dxShift=0, double dyShift=0, float txtSize=0.12);
  static TLatex *lat(const char *text="", Float_t x=0.0,Float_t y=0.0, Int_t align=12, Float_t tsize=0.05, short tcolor = 1); 
  static void Titles(TH1 *hid=0, const char *titx="",const char *tity="");
  static void PrintHist(TH1 *hid=0, int n1=0, int n2=0);
  static void DrawProjectionX(TH2F *hid=0, const char *tit="e/pi", int ix1=1, int ix2=2, int nx=1, int ny=2, TPad *p=0);
  static TList* CreateProjection(TH2 *h2=0, const char *name="", const char* opt="Y");
  static void FillProfiles(TList *l=0, const Int_t ind=15);
  // comparing with drawing
  static TCanvas* compareWithDrawing(TH1F* h1=0, TH1F* h2=0, char *tit="comparing");
  // Drawing function ...
  static TH1* drawF(TF1* f=0, int lineWidth=1, int lineColor=1, const char* opt="", int lineStyle=1); // *MENU* 

  static TCanvas* Canvas(const char *name=0,const char *tit=0, 
			 int wtopx=0, int wtopy=25, int ww=600, int wh=700, int clear=1); // *MENU* 
  static TCanvas* c();                        // *MENU* 
  static TPad*    pad(const char *name="", TCanvas *c=0, double ymax = 0.94);
  static TPad* getMainPad(TCanvas *c=0);
  static TPad* getTitPad(TCanvas *c=0);
  static TLatex *getTitText(TCanvas *c=0);
  static TLegend *getLegend(TCanvas *c=0);
  static void PrintList(TList *l);
  static void   savePict(const int save=0, TCanvas* c=0, const char *dir="RES/PICT/");
  // 15-apr-2003 - from hc.h 
  // http://root.cern.ch/root/html/TGraph.html#TGraph:PaintGraph
  static TGraph *drawGraph(Int_t n=4, Double_t *x=0, Double_t *y=0, Int_t markerColor=4,  
  Int_t markerStyle=4, const char* opt="", const char* tit="", const char* xTit="  jet E_{t}  ",
  const char* yTit="", Int_t ifun=0, const char *optFit="W+", const char *fun="");
  static TGraphErrors *drawGraphErrors(const Int_t n=4,Double_t *x=0,Double_t *y=0,Double_t *ex=0, 
  Double_t *ey=0, Int_t markerColor=4,Int_t markerStyle=4, const char* opt="", 
  const char* tit="", const char* xTit="  jet E_{t}  ",
  const char* yTit="", Int_t ifun=0, const char *optFit="W+", const char *fun="");
  static void print(Int_t n=0,Double_t *x=0,Double_t *y=0,Double_t *ex=0,Double_t *ey=0, const char* tit="");
  // Hists service - can I use the templates
  static TH1F *create(TH1F* hid,const Char_t *nam,const Char_t *tit);
  static TH1F *copy(TH1F* hid, const Char_t *nam, const Char_t *tit,Int_t modeCopy);
  static TH1D *create(TH1D* hid=0,const Char_t *nam="hout",const Char_t *tit="htit");
  static TH1D *copy(TH1D* hid=0, const Char_t *nam="hout", const Char_t *tit="hout tit",Int_t modeCopy=0);
  // set grapics attrubute - 25-jan-04
  static void setGraphAttr(TGraph *gr,int lCol=1,int lWidth=1,int mkStyle=21, int mkCol=1,int mkSize=1);
  // Fill with list
  static void FillH1(TList *l=0, int ind=0, double x=-99999., double w=1., double error=0.0);
  static void FillH2(TList *l=0, int ind=0, double x=-99999., double y=-99999., double w=1.);
  static void FillHprof(TList *l=0, int ind=0, double x=-99999., double y=-99999., double w=1.);
  static void FillHnSparse(TList *l=0, Int_t ind=0, Double_t* x=0, Double_t w=1.);
  // get method
  static TObject *FindObjectByPartName(TSeqCollection *col,const char* partName);
  static TH1* findHistByName(TList *list, const char* name);
  // File Map - see rHits.C, method getFileMap(const char* fn, const char *dir)
  static TMap* getFileMap(const char* fn, const char *dir, const int pri=1);
  static TMap* GetFileMap(const char* fn, const char *dir, const int pri=1) 
  {return getFileMap(fn,dir,pri);}
  static TMap* initFilesMap(const char* fn, const Int_t pri=0);

  static TString getFileName(const TMap *MAP, const int var);
  static TString GetFileName(const TMap *MAP, const int var) {return getFileName(MAP, var);}
  static Double_t* ArrLogAxis(const Double_t xmax=100., const Int_t nbin=100, const Double_t xminLog=-6., const Int_t pri=0); 
  // calculaiton ... 6-jan-2003 
  static TH1F* efficiencyFactor(TH1F* hid=0, TString opt="", Int_t norm=0);  // opt is empty or "r"
  static TH1F* suppressionFactor(TH1F* hid=0,TString opt="", Int_t norm=0); 
  static void probAndErr(int m, int n, double &prob, double& errProb, int print=0);
  static void suppressionAndErr(int m, int n, double &sup, double& errSup, int print=0);
  // --
  static void divide(TArrayD *ar, const double c);
  static void multiply(TArrayD *ar, const double c);
  static void multiply(int n, double *ar, const double c);
  // FIT Jan 27, 2004
  static TF1* expo(const char *addName, double xmi, double xma);
  static TF1* gausi(const char *addName, double xmi, double xma, double N, double mean, double sig, 
  double width);
  static TF1* gausi(const char *addName, double xmi, double xma, TH1 *h);
  static Double_t gi(Double_t *x, Double_t *par);
  // Landau convoluted with gauss - Mar 09,2006; 
  static TF1 *fitLandauWithGaus(TH1 *h, const char *addName, double xmi,double xma,double width, double MP,
                 double area, double gSigma, double binWidth, int setParLimits=0);
  static TF1 *landauWithGaus(const char *addName, double xmi, double xma, double width, double MP,
		 double area, double gSigma, double binWidth, int setParLimits=0);
  static Double_t langaufun(Double_t *x, Double_t *par); // c-function
  // Landau convoluted with gauss + bg3(4)
  static TF1 *landauWithGausBg(const char *addName, double xmi, double xma, double width, double MP, 
			       double area, double gSigma, double binWidth, int setParLimits=0, int keyBg=3);
  // langaufun + bg3(4)
  static Double_t langaufunPlusBg3(Double_t *x, Double_t *par);
  static Double_t langaufunPlusBg4(Double_t *x, Double_t *par);
  static Double_t langaufunPlusBg(Double_t *x, Double_t *par) {return langaufunPlusBg3(x,par);} // for compatibility
  // landau with gaus + bg + gaus(electron) - Apr 21, 2006
  static TF1 *landauWithGausBgGaus(const char *addName, double xmi, double xma, double width, double MP, 
				   double area, double gSigma, double binWidth, double pbeam, double sig, 
                                   int setParLimits=0);
  static Double_t langaufunPlusBgPlusGaus(Double_t *x, Double_t *par);
  // Background function - May 25, 2006
  static Double_t bg1(Double_t *x, Double_t *par); // 3 pars; pol2(0)
  static Double_t bg2(Double_t *x, Double_t *par); // 3 pars; exp(pol2(0))
  static Double_t bg3(Double_t *x, Double_t *par); // 5 pars;   pol2(0) * expo(3)
  static Double_t bg4(Double_t *x, Double_t *par); // exp(pol3);
  //
  static TF1* fbg2(const char *addName, double xmi, double xma); // see bg2
 // loading libraries
  static void loadPythiaStaff();
  static void loadLibraries(const char *opt="", const char *libList="jets.so,StPicoInfoManager",
  const char* sep=",");
  static void loadAliceAnalysisLibraries();
  // Service routine 
  static TVector3* vecFromMagEtaPhi(TVector3* v, double mag, double eta, double phi);
  static double  thetaToEta(double theta, int mode=0);
  static double  etaToTheta(double eta,   int mode=0);
  static int ParseString(const TString &tChain, TObjArray &Opt); // see StBFChain 
  static int parseString(const TString &tChain, TObjArray &Opt) {return ParseString(tChain,Opt);}  
  static void  DrawKickAngle();
  static void  DrawElectronPositronAngle();
  static void cbar(); // 25-feb-2004
  // Fo DB business : see /home/pavlinov/STAR/ANALYSIS/PED/StRoot/StTestPedMaker/DA_EMC_pedestals
  static Int_t  getDateStamp(int day=41, int year=2003);
  // ALICE
  // Correction to rec.energy - Jul 15, 2007
  static Double_t EnergyCorrectionPol2(Double_t eRec);
  //
  // HTML - Aug 24,09 : look to HypPicturesOfMasses
  // 
  static void  AddTextFromFunction(TString &text, TF1 *f, const Char_t* name);
  static void  CreateHtmlHead(FILE *io, const Char_t *title="title");
  static void  CreateHtmlEnd(FILE *io);
  static void  CreateLink(FILE *io, const Char_t *name, const Char_t *text=0, const int br=2);
  static void  CreatePictureLink(FILE *io, const Char_t *pictName, const Char_t *pictText=0);
  // Table
  static void  CreateTableHead(FILE *io, const Char_t* textAlign="center");
  static void  CreateTableEnd(FILE *io, const Char_t* textFoot=0);
  static void  CreateTableTd(FILE *io, const Char_t* textAlign="center", 
			     const Char_t* href="href.html", const Char_t* text="text");

   ClassDef(SDAliases,0) //Set of methods for convenience
};

typedef SDAliases sda;

#endif
