/*
 * SDCluster.h
 *
 *  Created on: May 20, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDCLUSTER_H_
#define SDCLUSTER_H_
#include "SDClusterBase.h"
#include <TTable.h>
#include "sdpixel.h"
#include "sdClusterChar.h"

#include <iostream>
using namespace std;

class SDClusterInfo;

class SDCluster :  public TTable,  public SDClusterBase{
public:
	virtual SDClusterInfo *GetClusterInfo() const; // Parent of SDCluster
	virtual UInt_t GetClusterSize() const {return static_cast<UInt_t>(GetNRows());}
	virtual void   AddPixel(pixel &c);
	virtual pixel* GetPixel(UInt_t ind);
	virtual pixel* GetPixel(UInt_t col, UInt_t row);
	virtual void   PrintClusterInfo(const int pri=0) const;
	virtual Int_t  Compare(const SDClusterBase* clb) const;
	virtual Bool_t  IsNormalCluster() const;
	// Drawing
	virtual TPolyLine* CreatePolyline();

	virtual TPolyLine* GetPolyline() {return fPolyLine;}
protected:
	TPolyLine* fPolyLine; //!

	ClassDefTable(SDCluster,pixel)
	ClassDef(SDCluster, 1)  // SD cluster from pixels
};

#endif /* SDCLUSTER_H_ */
