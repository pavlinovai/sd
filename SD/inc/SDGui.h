/*
 * SDGui.h
 *
 *  Created on: Aug 29, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDGUI_H_
#define SDGUI_H_

#include "SDManager.h"
#include "SDConfigurationInfo.h"
#include <TGFrame.h>
#include <TGButton.h>

#include <iostream>
#include <vector>

class TGTab;
class TVirtualPad;
class TCanvas;
class TGLabel;
class TGComboBox;
class TGTextEntry;
//class TGNumberEntry;
class TGNumberEntryField;
class TGDoubleHSlider;
class TGNumberEntryField;
class TGStatusBar;
class TGTab;
class TGCompositeFrame;
class TBrowser;
class TClass;
class TGTextEdit;
class TBrowser;
class TDataMember;
class TClass;
class TPair;

class SDImageAnalysis;

struct tabRec
{
  tabRec() {compFrame=0; groupFrame=0; textEdit=0;}
  TGCompositeFrame* compFrame;
  TGGroupFrame*     groupFrame;
  TGTextEdit*       textEdit;
};
ostream& operator<<(ostream& os, const tabRec& r);

typedef vector<tabRec> vecTabs;

class SDGui : public TGMainFrame {
public:
	static SDGui* GetInstance(SDManager* sdman);
	virtual ~SDGui();
	//   Slots
	// Manage of program
	void SelectXMLFileDialog();
	void SelectTifFileDialog();
	void ReadTIF();
	void Fill2DHists();
	void CalculateF0();
	void FindClusters();
	void ScanClusters();
	void SaveSD();
	// Service methods for manage buttons - 2013 : June 15,23
	void RGBnoImages();
	void RGBImagesMoreThanOne(Int_t pri=0);
	void CreateOutputTextFiles(UInt_t indCurrent, UInt_t scan);
	void ResetGUI();
	// Check Buttons staff
	void SetCheckButtonsAfterReadTIF();
	void SetCheckButtonsAfterFill2DHists();

	void StartTBrowser();
	void ExitFromRoot();

    // Update configuration values : from GUI to configure object
	void UpdateTifDir(const char  *newtext);
	void UpdateTifName(const char *newtext);
	void UpdateRGBSelection(const char *newtext);
	// Transformation
	void UpdateTransformationPars();
	void UpdateThreadsPars(const char  *newtext);
	void UpdateScanPars(const char  *newtext);
	void UpdateThreadsParsChk();  // Obsolete but useful
    // F0
	void UpdateF0Algo(const char *newtext);
	void UpdateFilter(const char *newtext);

	void UpdateChi2Cut(const char *newtext);
	void UpdateSet(const char *newtext);
	void UpdateSigCut(const char *newtext);
	void UpdateNsig(const char *newtext);
	// CF
	void UpdateCFName(const char* newtext);
	void UpdateSeed(const char *newtext);
	void UpdateTimeScale(const char *newtext);
	void UpdateSpaceScale(const char *newtext);
	void UpdateMinPixels(const char *newtext);
	void UpdateMinSize(const char *newtext);
	void UpdateMinDuration(const char *newtext);
	void UpdateDrawing(const char *newtext);
    // Out
	void UpdateOutputDir(const char  *newtext);
	void UpdateOutputName(const char  *newtext);

	void FontDialog();

	void UpdateButtonsState();
	// Service methods
	void PrintInfo();  // *MENU*

protected:
   SDGui(SDManager *sdman=0);

   void CreateMainGroup3x2(Int_t &id);
   void CreateButtonsGroup4x2(Int_t &id);
   void CreateServiceGroup(Int_t &id);
   void CreateStatusBar();
   // Tabs
   void CreateConfigurationTab();
   void CreateOutFrame(TGGroupFrame *gf, parsInputOutput& parsOut, const char* tit,
		   TGTextEntry *arr[3]);
   void SetEnabledTifFrame(Bool_t flag, Int_t indMax=2);
   void SetEnabledOutFrame(Bool_t flag);
   void SetEnabledTList(TList *l, Bool_t flag);
   void CreateFillHistFrame(TGGroupFrame *gf, parsFillHists& pars, const char* tit="Fill 2d hist");
   // Apr 23-26, 2013
   void CreateThreadsFrame(TGGroupFrame *gf);
   //
   void CreatePedestalsFrame(TGGroupFrame *gf, parsPedestals& pars, const char* tit="Pars for F0 calculation", int ng=2);
   TGFrame* CreateSmoothComboBox(TGHorizontalFrame *hf);
   void     AllignSmoothComboBox(TString st);
   TGFrame* CreateAlgoComboBox(TGHorizontalFrame *hf);
   void CreateClusterFinderFrame(TGGroupFrame *gf, parsClusterFinder& pars, const char* tit="Pars for cluster finder");
   // Mar 30,2013
   void CreateTransformationAndRgbFrame(TGGroupFrame *gf);
   // June 13, 2013
   void CreateScanFrame(TGGroupFrame *gf);
   //
   // Update configuration tab : from configure object to GUI
   //
   void UpdateTabConfigurationInfo(SDConfigurationInfo* conf);
   void UpdateNumbersEntries(TClass *cl,void* p, TList *l, Int_t pri=0);
//   void UpdateCheckButtons(TClass *cl,void* p, TList *l, Int_t pri=0);

   // Result tabs
   void CreateResultTabs(const char* color, UInt_t &indCurrent);
   void CreateResultTabsOld(); // Before Mar 13,2013;
   //

   void UpdateInput(parsInputOutput *pi);
   // Utils
   void CreateFrameTitle(TGGroupFrame *gf, const char* tit="frame title");
   void CreateFrame(TGGroupFrame *gf, TClass *cl, void* pars, UInt_t ind1, UInt_t ind2, TList *l, Int_t pri=0);
   void CreateFrameText(TGGroupFrame *gf, TClass *cl, void* pars, UInt_t ind1, UInt_t ind2, TGTextEntry** entries);
   TDataMember* FindIndexOfDataMember(TPair* pair, TClass *cl);
   void UpdateF0andCFandScanButtons(const char* newtext); // Mar 10;

   // Connect slots
   void ConnectControlBarsSlotsOne();
   void ConnectControlBarsSlotsTwo();
   void ConnectConfigurationSlots(Int_t pri=0);


   // Working variables : for convenience
   SDManager*           fSdManager;          //!
   SDTiffFileInfo*      fTifInfo;            //!
   SDImageAnalysis*     fImgAna;             //!
   SDPedestals*         fPed;
   SDClusterFinderBase* fCF;                 //!
   SDConfigurationInfo* fConfInfo;           //! configuration information from parser

   TGLabel             *fTiffLabel;
   TGComboBox          *fTiffData;

   // Main frame
   // List of buttons
   TGTextButton        *fSelectXMLButton;    // Select configuration XML file
   TGTextButton        *fSelectTIFButton;    // Select TIF file
   TGTextButton        *fReadTIFButton;      // Read TIF file
   TGTextButton        *fFill2DHists;        // Fill 2D hists for analysis
   TGTextButton        *fCalculateF0Button;  // Calculate F0
   TGTextButton        *fFindClustsButton;   // Find clusters
   TGTextButton        *fClustersScanButton; // Scan clusters
   TGTextButton        *fSaveButton;         // Save files

   // Service Frame
   TGTextButton        *fBrowserButton;      // Start browser
   TGTextButton        *fExitButton;         // Exit from ROOT
   TGTextButton        *fSelectFont;         // Select font for SD GUI
   //
   TGStatusBar         *fStatusBar;          // Status bar widget
   //
   TGTab               *fTab;                // tab widget holding the configuration
   TGCompositeFrame    *fTabContainer;       // main tab container
   UInt_t               fNShift;             // number of output text file for each color: 2 or 4(with scan)
   TGCompositeFrame    *fConfTab;            // configuration tab
   TGGroupFrame        *fConfFrame;
   // Tif input
   TGTextEntry         *fTEofTifDir;         // Repository directory of tif files
   TGTextEntry         *fTEofTifName;        // name of tif files
   TGComboBox          *fRGBSelection;       // Filter entry
   vector<TString>      fRGBStrings;         // working variable
   TString             fStringSelected;
  // Transformation
   TList                fButtonsGf;
   TList                fTransformChksList;      // TGCheckButton

   TGNumberEntryField  *fScaleEntry;         // Scale  entry
   TGComboBox          *fFilterEntry;        // Filter entry
   // F0
   Int_t                fNF0Entries;         // number of number entries for F0
   TList*               fF0Entries;         // F0 entries => struct parsPedestals
   //
   // Cluster Finder (CF)
   //
   Int_t                fNCFEntries;         // number of entries for CF
   TList*               fCFEntries;          // CF entries => struct parsClusterFinder
   // Threads - Apr 23, 2013
   TGGroupFrame*        fThreadsFrame;
   TList                fThreadsList;  //
   // Scan    - June14,2013
   TGGroupFrame*        fScanFrame;
   TList                fScanList;  //
   // Out
   TGTextEntry         *fTEofOutDir;         // Directory of output files
   TGTextEntry         *fTEofOutName;        // Name of output file
   //
   // Result tab
   //
   UInt_t              fNResTabs;            // number of results tabs
   TGCompositeFrame*   fResultTab[2];        //
   TGGroupFrame*       fResultFrame[2];
   TGTextEdit*         fEdit[2];
   vecTabs             fResultTabs;
   // Font
   const TGFont        *fFont;               //

   // Transformation & color (duplicate);
   // Switch on/of
   Int_t fTransColor;

   static SDGui* fgSDGuiDialog;
protected:
   TBrowser*           fBrowser; //! working variables
   virtual Bool_t IsFolder() const {return kTRUE;}
   //
   // obsolete
   //
	void CalculateF0Old();
	void DoClustersFinder();

   ClassDef(SDGui,0)
};

#endif /* SDGUI_H_ */
