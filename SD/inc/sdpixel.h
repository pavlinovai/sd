/*
 * sdpixel.h
 *
 *  Created on: May 2, 2012; May 25,2013
 *      Author: Alexei Pavlinov
 */

#ifndef SDPIXEL_H_
#define SDPIXEL_H_
#include "Rtypes.h"
#include <iostream>

// #define NEIGHBORS

using namespace std;

// May 23, 2013. Introduced the number of neighbors - change from 0 to 8.
// The border pixel could has at least two neighbors.
// The pixel is not join to the cluster if his neighbor is border pixels.
class pixel
{ // see SDCluster
public:
  Float_t  amp;
  UShort_t col;
  UShort_t row;
#ifndef NEIGHBORS
  pixel() : amp(0.0), col(0), row(0) {;}
#else
  pixel() : amp(0.0), col(0), row(0), neighbors(0) {;}
  UShort_t neighbors; // number of neighbors (from 0 to 8)
#endif
};

ostream& operator<<(ostream &os, const pixel &p);

#endif /* SDPIXEL_H_ */
