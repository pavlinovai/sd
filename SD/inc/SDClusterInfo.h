/*
 * SDClusterInfo.h
 *
 *  Created on: May 21, 2012;
 *  2013: Jan  7;
 *      Author: Alexei Pavlinov
 */

#ifndef SDCLUSTERINFO_H_
#define SDCLUSTERINFO_H_

#include "SDObjectSet.h"
#include "sdClusterChar.h"
#include "SDConfigurationInfo.h"

class SDClusterFinderBase;
class SDCluster;
class SDClusterBase;
class SDManager;
class SDClusterScan;
class pixel;

class SDClusterInfo: public SDObjectSet {
public:
	SDClusterInfo();
	SDClusterInfo(const char* name);
	SDClusterInfo(const int clustNum);
	virtual ~SDClusterInfo();
	void Init(TString &clustName, const int nmaxRow = 6000);

	TList* Get2DHists() {
		return GetListHists(gk2DHists);
	}
	TList* GetProjHists(Int_t mode=0) {
		TString snam(gkProjHists);
		if(mode!=0) snam += "NoCuts";
		return GetListHists(snam.Data());
	}
	sdClusterChar& GetClstCharFromF0()   {return fClstCharFromF0;}
	sdClusterChar& GetClstCharFromZero() {return fClstCharFromZero;}

	SDCluster* GetClusterTable(const char* name = "") const;
	SDClusterBase* GetCluster(const char* name = "") const;
	SDClusterFinderBase* GetClusterFinder();
	void AddPixel(pixel &c);

	void ChangeName(const Int_t ncl);
	void Create2DHists(int force = 0);
	void CreateProjHists(Int_t ind=0);
	// Drawing
	void Draw2DHists();               // *MENU*
	void DrawProjHists(Int_t mode=0); // *MENU*
    // For sorting of cluster container
	virtual Int_t Compare(const TObject* obj) const;
	// Splitting
	Int_t SearchTH1(Int_t proxy=0, Double_t sigma = 1, Option_t* option = "nobackground", Double_t threshold = 0.25, int pri=1); // *MENU*
	Int_t SearchTH2(Double_t sigma = 5, Option_t* option = "nobackground nomarkov", Double_t threshold = 0.4, Int_t w=3, int pri=1); // *MENU*
	Bool_t SplitClusterByLineParallelYcoord(Double_t x0);           // *MENU*
	Bool_t SplitClusterByPCA();           // *MENU*
	// Scan
	SDClusterScan* GetClusterScan();
	Bool_t ScanTest(Int_t nScan=5);  // *MENU*
	Bool_t Scan(const parsSdScan& pars);
private:
	// Cluster characteristics
	sdClusterChar fClstCharFromF0; // Previous fClstChar
	sdClusterChar fClstCharFromZero;

	static const char* gk2DHists;   // list name of 2D hists
	static const char* gkProjHists; // base list name of projection

    ClassDef(SDClusterInfo,1) // Class for keeping information about cluster
};

#endif /* SDCLUSTERINFO_H_ */
