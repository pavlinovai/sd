/*
 * SDCluster2D.h
 *
 *  Created on: Nov 15, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDCLUSTER2D_H_
#define SDCLUSTER2D_H_

#include "TDataSet.h"
#include "SDClusterBase.h"
#include "sdpixel.h"
#include <set>
#include <vector>

class TBrowser;

class pixelColumnCompare { // growing order
public:
	Bool_t operator() (const pixel& p1, const pixel& p2) const {
        return p1.col<p2.col;
    }
};
typedef set<pixel, pixelColumnCompare> setpx; // set of ordered pixels
typedef vector<setpx*>  vrowpx;                // vector of setpx

class SDCluster2D: public TDataSet , public SDClusterBase
{
public:
	SDCluster2D();
	SDCluster2D(const char* name, const UInt_t nrow=512, const char* tit=0);
	virtual ~SDCluster2D();
	virtual SDClusterInfo *GetClusterInfo() const; // Parent of SDCluster
	virtual UInt_t  GetClusterSize() const {return fNClusterSize;}
	virtual void    AddPixel(pixel &p);
	virtual pixel*  GetPixel(UInt_t ind);
	virtual pixel*  GetPixel(UInt_t col, UInt_t row) ;
	virtual void    PrintClusterInfo(const int pri=2) const;  // *MENU*
	virtual Int_t   Compare(const SDClusterBase* clb) const;
	virtual Bool_t  IsNormalCluster() const;
	//
	vrowpx& GetRows() {return fRows;}

	virtual TPolyLine* CreatePolyline();
	virtual TPolyLine* GetPolyline() {return fPolyLine;}
    virtual Bool_t IsFolder() const {return kTRUE;}
    virtual void Browse(TBrowser *b);
    void    SetDebug(const Int_t deb) {fDebug=deb;}
protected:
	UInt_t fNClusterSize; // number of pixels
	vrowpx fRows;         //
	TPolyLine* fPolyLine; //!
	Int_t fDebug;

	ClassDef(SDCluster2D,1)
};

#endif /* SDCLUSTER2D_H_ */
