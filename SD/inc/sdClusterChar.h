/*
 * sdClusterChar.h
 *
 *  Created on: May 2, 2012
 *  Revised   : June 18; Nov 1;
 *      Author: Alexe Pavlinov
 */

#ifndef SDCLUSTERCHAR_H_
#define SDCLUSTERCHAR_H_

#include "Rtypes.h"
#include "sdpixel.h"

#include <iostream>
using namespace std;

struct typeDecomp {
  unsigned border : 1;   // 1-border cluster; 0 - internal cluster
  unsigned scan   : 1;   // 1-cluster will be scaned; 0 - no scan
};

union clusterType {
	// 2013: June 22;
	UInt_t     type;
	typeDecomp tdecomp;
	void Print();
};

class sdClusterChar
{ // Cluster characteristics
public:
	sdClusterChar();
	Bool_t IsNormalCluster() const;

	UInt_t   numpixels; // Number of pixels
	Double_t maxamp;    // Maximum amplitude
	Double_t aveamp;    // Average amplitude = volume/#pixels
	Double_t duration;  // Time duration at half amplitude = xhm2 - xhm1
	Double_t size;      // Spatial size  at half amplitude = yhm2 - yhm1
	// sigma ??
    //
	// Working variable  (in pixels)
	// for cluster finder
	UInt_t    colmin;
	UInt_t    colmax;
	UInt_t    rowmin;
	UInt_t    rowmax;
	// for projection drawing
	Double_t xhm1; // Time
	Double_t xhm2;
	Double_t yhm1; // Space
	Double_t yhm2;
	// May 11,2013 : Victor request
	UInt_t   type; // description in union clusterType
	void SetType(const clusterType& clt) {type = clt.type;}
	void SetBorder(UInt_t var) {clusterType clt; clt.tdecomp.border = var==0?0:1; type = clt.type;}
	void SetScan(UInt_t var) {clusterType clt; clt.tdecomp.scan = var==0?0:1; type = clt.type;}
	UInt_t GetBorder() const {clusterType clt; clt.type = type; return UInt_t(clt.tdecomp.border);}
	UInt_t GetScan()   const {clusterType clt; clt.type = type; return UInt_t(clt.tdecomp.scan);}
};

ostream& operator<<(ostream &os, const sdClusterChar &c);

#endif /* SDCLUSTERCHAR_H_ */
