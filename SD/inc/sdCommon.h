/*
 * sdCommon.h
 *
 *    Created on: Jan 5, 2013
 * Copyright (C) 2012-2013, Alexei Pavlinov
 * All rights reserved.
 *  Look to the sdCommon.cpp for implementation
 *          Singleton
 */

#ifndef SDCOMMON_H_
#define SDCOMMON_H_
#include "versionUpdate.h"
#include <TString.h>

#include <vector>
#include <utility>
#include <string>

using namespace std;

typedef vector<string> vecStr;
typedef pair<string, vecStr> funPair; // name of function and vector of parameters names
typedef vector<funPair> vecFuns;

struct sdCommon {
private:
	sdCommon();
public:
	static sdCommon* GetSdCommon();
	virtual ~sdCommon() {;}
	// Common services
	Bool_t IsThisWindows();
	//
	void PrintNameOfAnaFuns();
  // Name of implemented pedestals(F0) reconstruction
  vecStr nameOfPeds;
  // Name of implemented cluster finder
  vecStr nameOfClfs;
  // Name of implemented response function
 // vecStr nameOfFuns;
  vecFuns nameOfFuns;
  // Name of implemented F0(pedestals) function
  vecStr nameOfF0;
  // Trial option
  TString sdTrial;
  // Copyright string
   TString CPAI;
   TString CPAI2;
   // CVS info
   TString rcsid;
   // Tiff staff
   TString leadingTiffName;
   TString leadingPedName;
   TString leadingPCFName;
   // Split
   TString leadingNameSplitAlgo1;
   // Scan
   TString leadNameScan;
   TString nameCLFContainer;

   vecStr  tiffRgb;
   char    fSlash; // '/' for Unix or '\\' or Windows
protected:
  static sdCommon* fgSdCommon;
};

typedef sdCommon sd;

#endif /* SDCOMMON_H_ */
