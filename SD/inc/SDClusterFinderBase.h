/*
 * SDClusterFinderBase.h
 *
 *  Created on: May 17, 2012
 *      Author: pavlinov
 */

#ifndef SDCLUSTERFINDERBASE_H_
#define SDCLUSTERFINDERBASE_H_
#include <Rtypes.h>
#include "SDConfigurationInfo.h"

class TDataSet;
class TList;
class TH2;
class TCanvas;
class SDClusterInfo;
class SDCluster;
class SDClusterBase;
class pixel;
class parsClusterFinder;

class SDClusterFinderBase {
public:
	enum ECLFThreadsStatus { // cluster finder threads status : Apr 21, 2013
	    kNoThreads = 0,
	    kTThreads,      // ROOT threads
	    kPosixThreads,
	    kWindowsThreads // for future
	};

	SDClusterFinderBase() {;}
	virtual ~SDClusterFinderBase() {;}
	virtual void   Init() = 0;
	virtual Int_t  FindClusters() = 0;
	virtual Int_t  FindClsutersWithThreads();      // Apr 14,2013
#ifndef WIN32
	virtual Int_t  FindClsutersWithPosixThreads(); // Apr 20,2013
#endif
	// Get methods
	virtual Int_t   GetOnlineDrawing() const = 0;
	virtual void    SetOnlineDrawing(Int_t var) = 0;
	virtual Int_t   GetThreadsStatus() const = 0;
	virtual void    SetThreadsStatus(Int_t var) = 0;
	virtual Bool_t  IsThreadsAvailable() = 0;
	virtual TDataSet* GetClusters() = 0;
	virtual SDClusterInfo* GetClusterInfo(const int ind) = 0;
	virtual TList* GetHists() = 0; // List of objects (hists or some TObject's)
    virtual TList* GetInputHists() = 0;
	virtual TList* GetMainHists() = 0;    // Return list of main hists
	virtual TList* GetSummaryHists() = 0; // Return list of summary hists
	virtual void SetInputHist(TH2* hin) = 0;
	virtual void SortAndRenameClusters(Int_t mode) = 0;
	virtual TList* BookSummaryHists(const char* opt="") = 0;
	virtual void FillSummaryHists() = 0;
	virtual void SetCFName(const char* name) = 0;
	virtual void SetCFPars(parsClusterFinder pars) = 0;
    virtual void SetSeed(const Double_t seed) = 0;
	virtual Double_t GetSeed() const = 0;
    virtual parsClusterFinder GetPars() const = 0;
//
	virtual Bool_t ArePixelsNeighbours(const pixel &p1, const pixel &p2);
	virtual Bool_t IsPixelNeighbourToCluster(const pixel &p, SDClusterBase* clb);
	virtual void   CalculateClusterCharachteristics(SDClusterInfo &clinfo) = 0;
	virtual Bool_t IsThisClusterReal(SDClusterInfo &clinfo, parsClusterFinder *pcf) = 0;
	// Threads : Apr 21, 2013
	virtual Int_t MergeClusterFinders(TDataSet* dsIn) = 0; // return number of clusters(sparks)
	// Drawing
	virtual void DrawCFSummary() = 0;
	virtual void DrawInputHistInCanvasWithClusters(Int_t redraw=0) = 0;
    virtual void DrawBackgroundColumns(Int_t var=0) = 0;
	virtual TCanvas* DrawInputHistInCanvas(TH2* h, TCanvas* c=0) = 0;
	virtual void DrawClusterBox(TCanvas* c, SDClusterInfo* cli, Int_t ic) = 0;
	virtual void DrawFinalMessage(TCanvas* c, Int_t clstsReal, Int_t clstsAll) = 0;
	virtual void ClearClusterFinder() = 0;
	virtual TString SaveTxtFile(const char csv, UInt_t scan) = 0;
	virtual Double_t GetAmpCut() const = 0;
	virtual void     SetAmpCut(Double_t ampCut)  = 0;


	ClassDef(SDClusterFinderBase,1)
};

#endif /* SDCLUSTERFINDERBASE_H_ */
