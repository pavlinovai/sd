/*
 * SDClusterFinder2DTwo.h
 *
 *  Created on: Jul 26, 2012
 *      Author: pavlinov
 */

#ifndef SDCLUSTERFINDER2DTWO_H_
#define SDCLUSTERFINDER2DTWO_H_

#include "SDClusterFinder2DOne.h"

class SDClusterFinder2DTwo: public SDClusterFinder2DOne {
public:
	SDClusterFinder2DTwo();
	SDClusterFinder2DTwo(const char* name);
	virtual ~SDClusterFinder2DTwo();
    // Pixels should have common side
	//virtual Int_t FindClusters();
	virtual Bool_t ArePixelsNeighbours(const pixel &p1, const pixel &p2);

	ClassDef(SDClusterFinder2DTwo,1)
};

#endif /* SDCLUSTERFINDER2DTWO_H_ */
