/*
 * SDImageAnalysis.h
 *
 *  Created on: Mar 9, 2013
 *      Author: Alexei Pavlinov
 */

#ifndef SDIMAGEANALYSIS_H_
#define SDIMAGEANALYSIS_H_

#include "SDObjectSet.h"
#include "SDTiffFileInfo.h"
#include "SDConfigurationInfo.h"

class SDPedestals;
class SDManager;
class SDClusterFinderStdOne;

class SDImageAnalysis: public SDObjectSet {
public:
	SDImageAnalysis();
	SDImageAnalysis(imageStatus imgStat);
	virtual ~SDImageAnalysis();
	//
	SDTiffFileInfo* GetTiffFileInfo();
	imageStatus     GetImageStatus() const {return fImageStatus;}
	TString         GetSignature();
	TString         GetSignatureForCanvas();
	SDPedestals*    GetPedestals();            // one pedestal object
	SDClusterFinderStdOne* GetClusterFinder(); // one cluster finder object
	void SetImageStatus(int var) {fImageStatus.status = var;}

	const parsSdScan& GetParsScan() const {return fParsScan;}
	void SetParsScan(const parsSdScan& parsScan) {fParsScan = parsScan;}

	void ClustersScan(UInt_t pri=0);
protected:
	// CF: could be call only from GetClusterFinder()
	SDClusterFinderStdOne* InitClusterFinder();

protected:
	imageStatus fImageStatus;
	TString     fSig;
	TString     fSig2;
	parsSdScan  fParsScan;
	SDClusterFinderStdOne* fCF; //! working variables - 1 June, 2013

	ClassDef(SDImageAnalysis,1)
};

#endif /* SDIMAGEANALYSIS_H_ */
