/*
 * SDConfigurationInfo.h
 *
 *  Created on: May 26, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDCONFIGURATIONINFO_H_
#define SDCONFIGURATIONINFO_H_

#include "SDObjectSet.h"
#include <TSystem.h>
#include <TDataSet.h>
#include <TString.h>

class parsInputOutput {
public:
	parsInputOutput();
	TString dir;
	TString name;
	TString GetAbsoluteName()
	{
		TString st = dir;
		if(st[st.Length()-1] != '/') st += "/";
		st += name;
		return st;
	}
};

class parsFillHists {
public:
	parsFillHists();
	virtual ~parsFillHists() {};
	Float_t scale;  // scaling factor for transition from image to histogram
	Int_t   ndiv;   // number of division of image (for testing) - unused now
	ClassDef(parsFillHists,1)
};
// Jun 29,2013
class parsTransformation
{
public:
	parsTransformation(const UInt_t flip=1) : fRotate90(1),fFlipVert(flip),fCombine(1) {};
	virtual ~parsTransformation() {};

Int_t fRotate90;
Int_t fFlipVert;
Int_t fCombine;
////	Int_t fFlipHor;
//Int_t fReadInfo;
//// Color
//Int_t fGrey;
//Int_t fRed;
//Int_t fGreen;
//Int_t fBlue;
//Int_t fSplit;
//// Combine panel
//Int_t fNpannels;
//
Int_t GetRotate90() const {return fRotate90;}
void  SetRotate90(Int_t v) {fRotate90 = v;}
Int_t GetFlipVert() const {return fFlipVert;}
void  SetFlipVert(Int_t v) {fFlipVert = v;}
Int_t GetCombine() const {return fCombine;}
void  SetCombine(Int_t v) {fCombine = v;}
//int   GetReadInfo() const {return fReadInfo;}
//void  SetReadInfo(Int_t v) {fReadInfo = v;}
//int   GetRed() const {return fRed;}
//void  SetRed(Int_t v) {fRed = v;}
//int   GetGreen() const {return fGreen;}
//void  SetGreen(Int_t v) {fGreen = v;}
//int   GetBlue() const {return fBlue;}
//void  SetBlue(Int_t v) {fBlue = v;}
//int   GetSplit() const {return fSplit;}
//void  SetSplit(Int_t v) {fSplit = v;}

ClassDef(parsTransformation,1)
};

class parsPedestals {
public:
	parsPedestals();
	virtual ~parsPedestals() {};
    TString algo;   // Algorithm of correction (bleaching and baseline-F0)
	TString smooth; // Filter  - 'r5a','r5b' or r3a as in ROOT (median algorithm) or m1,m2,m3 - median filter

	Float_t chi2cut;
	Float_t sigcut;
	Int_t   set;
	Float_t nsig;

	ClassDef(parsPedestals,1)
};

class parsClusterFinder {
public:
    // Jan 19,2013
	// The member order is important for GUI !
	parsClusterFinder();
	virtual ~parsClusterFinder(){};
	void CalculateSizesInPixels();

	TString name;              // name of cluster funder : should be last

	Float_t seed;              // seed of cluster
	Float_t timescale;         // 2.09 ms : default
	Float_t spacescale;        // 0.05 um = 50nm: default

	Int_t   minpixels;         // minimal number of pixel in cluster
	Float_t minsize;           // minimal size of cluster (~0.8um or 16 pixels)
	Float_t minduration;       // minimal duration        (10ms or or 5 pixels - just my guess )

	Int_t   drawing;           // switch of interactive drawing

	Int_t   minsizeInPxs;      // minimal size of cluster in pixels (5 pixels)  Y coord
	Int_t   mindurationInPxs;  // minimal duration  in pixels      (3 pixels)   X coord

	ClassDef(parsClusterFinder,1)
};

// Scan
class parsSdScan
{
	// 2013: June 9,20;
public:
	parsSdScan();
	virtual ~parsSdScan() {};
	//
	Int_t   fNpScan;     // Number of points for scan; <=1 no scan
	Float_t fMinStep;    // Minimal step in amplitude cut
	Float_t fMaxRelAmp;  // Max relative amplitude for scan
	Int_t   fMaxNClusts; // Max number of clusters after scan
	// Get/Set
	void    SetNpScan(Int_t var)    {fNpScan = var;}
	Int_t   GetNpScan() const       {return fNpScan;}
	void    SetMinStep(Float_t var) {fMinStep = var;}
	Float_t GetMinStep() const      {return fMinStep;}
	Int_t   GetMaxNClusts() const {return fMaxNClusts;}
	void    SetMaxNClusts(Int_t maxNClusts) {fMaxNClusts = maxNClusts;}
	Float_t GetMaxRelAmp() const {return fMaxRelAmp;}
	void    SetMaxRelAmp(Float_t maxRelAmp) {fMaxRelAmp = maxRelAmp;}
	// May be something else like first point and last point
	ClassDef(parsSdScan,1)
};

class parsSdThreads{
	// Apr 13-23, 2013 - for threading
public:
	parsSdThreads();
	virtual ~parsSdThreads() {};
	Bool_t IsMultiThreadingPossible();
	// Get/Set
	Int_t GetMaxThreads() const {return fMaxThreads;}
	void  SetMaxThreads(Int_t var) {fMaxThreads = var;}
	Int_t GetKeep() const {return fKeep;}
	void  SetKeep(Int_t var) {fKeep = var;}
	Int_t GetNcpus() const {return fNcpus;}

protected:
	// To do: add the thread type selection (TThread, posix or windows)
	Int_t fMaxThreads;   // max number of threads
	Int_t fKeep;         // Keep splitted histograms or not
	Int_t fNcpus;        // number of CPU - just for information -> should be discard from here

	SysInfo_t  fSysInfo; // system info

	ClassDef(parsSdThreads,1);
};

class TGMainFrame;
class SDGuiTwoStrings;
class SDConfigurationParser;

class SDConfigurationInfo: public TDataSet {
public:
	SDConfigurationInfo(const char* name="SdConf");
	// Copy constructor
	SDConfigurationInfo(const SDConfigurationInfo& conf);
	virtual ~SDConfigurationInfo();
	//
    SDManager*             GetManager();
 	SDConfigurationParser* GetConfigurationParser();

	parsInputOutput& GetParsInput(){return fParsInput;}

    parsFillHists&   GetParsFillHists() {return fParsFillHists;}

    parsTransformation&  GetParsTranformation() {return  fParsTransformation;}

    parsPedestals& GetParsPedestals() {return fParsPedestals;}

    parsClusterFinder& GetParsClusterFinder() {return fParsClusterFinder;}

    parsInputOutput& GetParsOutput() {return fParsOutput;}

    parsSdScan&      GetParsSdScan() {return fParsScan;}

    parsSdThreads&   GetParsThreads() {return fParsThreads;}

    void PrintInfo(); // *MENU*
    void SetConfigurationStd(const char* indir); //*MENU* *ARGS={indir=>fParsInput.dir}

    void SetConfiguration(); // *MENU*
    void InputDialog(TGMainFrame *mf);

   parsInputOutput   fParsInput;
   parsFillHists     fParsFillHists;
   parsTransformation fParsTransformation;
   parsPedestals     fParsPedestals;
   parsClusterFinder fParsClusterFinder;
   parsInputOutput   fParsOutput;
   parsSdThreads     fParsThreads;
   parsSdScan        fParsScan;

protected:
// GUI staff
   TGMainFrame *fMain;         //! main frame
   SDGuiTwoStrings *fGuiInput; //! Do I need this ?

   ClassDef(SDConfigurationInfo, 1)
};
ostream& operator<<(ostream &os, const parsInputOutput &pio);
ostream& operator<<(ostream &os, const parsFillHists &pfh);
ostream& operator<<(ostream &os, const parsTransformation& trans);
ostream& operator<<(ostream &os, const parsPedestals &peds);
ostream& operator<<(ostream &os, const parsClusterFinder &cf);
ostream& operator<<(ostream &os, const parsSdThreads &th);
ostream& operator<<(ostream &os, const parsSdScan &sc);

ostream& operator<<(ostream &os, const SDConfigurationInfo &ci);

#endif /* SDCONFIGURATIONINFO_H_ */
