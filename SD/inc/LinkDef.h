/*
 * LinkDef.h for cint dictionary
 * Don't forget to change MakeDict and CMakeLists.txt
 *
 *  Created on: Apr 12, 2012
 *  2013 : 23,Jan
 *      Author: pavlinov
 */

//#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class SDObjectSet+;
#pragma link C++ class SDManager+;
#pragma link C++ class SDTiffFileInfo+;
#pragma link C++ class tifHeader+;
#pragma link C++ class tag866c+;
#pragma link C++ class imageStatus+;
#pragma link C++ class SDImagesPool+;
#pragma link C++ class SDImageAnalysis+;
#pragma link C++ class SDTransformation+;
#pragma link C++ class SDControlBar+;
#pragma link C++ class SDConfigurationParser+;
#pragma link C++ class SDConfigurationInfo+;
#pragma link C++ class SDPedestals+;
#pragma link C++ class SDUtils+;
#pragma link C++ class SDAliases+;
#pragma link C++ class sdClusterChar+;
#pragma link C++ class SDClusterInfo+;
#pragma link C++ class pixel+;
#pragma link C++ class SDClusterBase+;
#pragma link C++ class SDCluster-;
#pragma link C++ class SDCluster2D+;
// parameters info
#pragma link C++ class parsInputOutput+;
#pragma link C++ class parsFillHists+;
#pragma link C++ class parsTransformation+;
#pragma link C++ class parsPedestals+;
#pragma link C++ class parsClusterFinder+;
#pragma link C++ class parsSdThreads+;
#pragma link C++ class parsSdScan+;
#pragma link C++ class SDF0Base+;
//#pragma link C++ class +;
#pragma link C++ class SDClusterFinderBase+;
#pragma link C++ class SDClusterFinderStdOne+;
#pragma link C++ class SDClusterFinderStdTwo+;
#pragma link C++ class SDClusterFinder2DOne+;
#pragma link C++ class SDClusterFinder2DTwo+;
#pragma link C++ class SDClusterFinder2DThree+;
//#pragma link C++ class
#pragma link C++ class SDGui;
#pragma link C++ class SDPictures+;
#pragma link C++ class SDTrialVersion+;
// Splitting : June 4,2013 & Scanning June 9,2013
#pragma link C++ class SDClusterSplittingManager+;
#pragma link C++ class SDSplittingAlgoBase+;
#pragma link C++ class SDSplittingAlgoWithTwoMaximumOnX+;
#pragma link C++ class SDClusterScan+;
// Simulation Staff
#pragma link C++ class SimResponseFunction+;
#pragma link C++ class SimSimpleGauss+;
#pragma link C++ class SDEllipsoid+;
#pragma link C++ class SDF0Uniform+;
#pragma link C++ class SDSimulationManager+;
#pragma link C++ class sdSimPars+;
#pragma link C++ class SDSimPictures;
//#endif
