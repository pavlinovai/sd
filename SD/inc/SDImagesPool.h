/*
 * SDImagesPool.h
 *
 *  Created on: Mar 9, 2013
 *      Author: Alexei Pavlinov
 *      Images pool(container) for tif file - could be up to 3 images
 */

#ifndef SDPICTURESPOOL_H_
#define SDPICTURESPOOL_H_

#include <TDataSet.h>

class SDImageAnalysis;

class SDImagesPool: public TDataSet {
public:
	SDImagesPool();
	SDImagesPool(const char* name);
	virtual ~SDImagesPool();
	//
	SDImageAnalysis *GetSDImageAnalysis(const char* partName);
	//
	ClassDef(SDImagesPool,1)
};

#endif /* SDPICTURESPOOL_H_ */
