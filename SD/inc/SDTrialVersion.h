#pragma once
#define STR(x)   #x
#define SHOW_DEFINE(x) printf("%s=%s\n", #x, STR(x))

#include <TDataSet.h>
#include <TTimeStamp.h>
#include <fstream>

//#include "SDSystem.h"
// #define PROTLINKAPI

class  SDTrialVersion : public TDataSet
{
public:
	SDTrialVersion(void);
	SDTrialVersion(const char* name, const char* title, Int_t year, Int_t month, Int_t day, Int_t nm=1, Int_t nd=0);
	virtual  ~SDTrialVersion(void);
	//
	TTimeStamp GetStartDate() const {return fStartDate;}
	TTimeStamp GetEndDate()   const {return fStartDate;}
	TString    GetSignature() const {return fSignature;}
	void SetDebug(Int_t deb)  {fDebug = deb;}
	//
	void CreateSignature();
	Bool_t CheckValidation();

//	void PrintLicenseCondition(ofstream fout) const;
protected:
	TTimeStamp fStartDate;
	TTimeStamp fEndDate;
	TString    fSignature;
	Int_t      fDebug;
	//
	static const char* gkFileName;

	ClassDef(SDTrialVersion,1)
};

