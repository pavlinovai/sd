/*
 * SDClusterFinderStdTwo.h
 *
 *  Created on: May 20, 2012
 *      Author: pavlinov
 */

#ifndef SDCLUSTERFINDERSTDTWO_H_
#define SDCLUSTERFINDERSTDTWO_H_

#include "SDClusterFinderStdOne.h"

class SDClusterFinderStdTwo: public SDClusterFinderStdOne {
public:
	SDClusterFinderStdTwo();
	SDClusterFinderStdTwo(const char* name);
	virtual ~SDClusterFinderStdTwo();
	// Neighbors should have common side
	virtual Bool_t ArePixelsNeighbours(const pixel &p1, const pixel &p2);

	ClassDef(SDClusterFinderStdTwo,1)
};

#endif /* SDCLUSTERFINDERSTDTWO_H_ */
