/*
 * sdOpt.h
 *
 *  Created on: Dec 2, 2012
 *      Author: Alexei Pavlinov
 *      Contain common option for whole project
 */

#ifndef SDOPT_H_
#define SDOPT_H_

#ifdef WIN32
#define SD_DLLEXPORT __declspec(dllexport)
#else
#define SD_DLLEXPORT
#endif
// June 2, 2013 - test
// July 1, 2013 - switch off the trial checking
// #define TRIAL
#ifdef TRIAL
#define t_year 2013
#define t_month 7
#define t_day  30
#endif

#endif /* SDOPT_H_ */
