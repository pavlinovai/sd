/*
 * SDPictures.h
 *
 *  Created on: Nov 9, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDPICTURES_H_
#define SDPICTURES_H_

#include "SDObjectSet.h"

class SDManager;
class TDataSet;
class TCanvas;

class SDPictures: public SDObjectSet {
public:
	SDPictures(const char* dir="");
	virtual ~SDPictures();
	//
	void SetDir(const char* dir) {fDir = dir;}
	SDManager* GetSDMan() const {return fSDMan;}
	TDataSet*  GetPool()  const {return fSDPool;}
    //
	SDManager* ReadSdObject();  // *MENU*
	//
    TCanvas*   DrawF0PictOne(); // *MENU*
    TCanvas*   DrawF0PictTwo(); // *MENU*
    TCanvas*   DrawF0PictThree(); // *MENU*
protected:
	TString    fDir;
	TDataSet*  fSDPool;
	SDManager* fSDMan;

	ClassDef(SDPictures,1)
};

#endif /* SDPICTURES_H_ */
