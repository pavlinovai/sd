/*
 * SDObjectSet.h
 *
 *  Created on: May 14, 2012
 *      Author: Alexei Pavlinov
 */

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// SDObjectSet is the main class of SD software. This class due to      //
// inheritance from TObjectSet gives a possibility to build             //
// the directory-like data structures. The type of embedded object is   //
// TList with name "hists".                                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef SDOBJECTSET_H_
#define SDOBJECTSET_H_

#include <TObjectSet.h>

class SDManager;
class SDImageAnalysis;

class SDObjectSet: public TObjectSet {
public:
	SDObjectSet();
	SDObjectSet(const char* name, const char* title="");
	virtual ~SDObjectSet();
	virtual void ClearSDObject();  // *MENU*

	TList* InitHists();

    void   SetDebug(const Int_t deb) {fDebug = deb;}
    Int_t  GetDebug() {return fDebug;}

	TList *GetList();
    TList *GetListHists(int ind);
    TList *GetListHists(const char *name);
	virtual SDManager* GetSDManager();
	virtual SDImageAnalysis* GetImageAnalysis();
    virtual TString  GetSignature();

//private:
//    void operator=(const TObjectSet &set) {};
    Int_t fDebug;   // =0 no print

    ClassDef(SDObjectSet,1)  // The main class of SD software
};

#endif /* SDOBJECTSET_H_ */
