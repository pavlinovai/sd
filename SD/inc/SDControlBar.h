/*
 * SDControlBar.h
 *
 *  Created on: May 29, 2012
 *      Author: pavlinov
 */

#ifndef SDCONTROLBAR_H_
#define SDCONTROLBAR_H_

#include <SDObjectSet.h>
#include <TDataSet.h>

class TControlBar;

class SDControlBar: public TDataSet {
public:
	SDControlBar();
	SDControlBar(const char* name);
	void Init(Int_t brief=1);
	virtual ~SDControlBar();
// Action
	static void SelectXMLFileDialog();
	static void SelectTifFileDialog();
	static void InitConfiguration();
	static void ReadTif();

	static void InitPedestals();
	static void DoPedestals();

	static void InitClusterFinder();

	static void SaveSD();
protected:
	TControlBar* fBar;
	ClassDef(SDControlBar,0)
};

#endif /* SDCONTROLBAR_H_ */
