/*
 * SDTransformation.h
 *
 *  Created on: Mar 28, 2013
 *      Author: pavlinov
 */

#ifndef SDTRANSFORMATION_H_
#define SDTRANSFORMATION_H_

#include "SDObjectSet.h"
#include "SDConfigurationInfo.h"
#include <iostream>

using namespace std;

class SDTransformation: public SDObjectSet {
public:
	SDTransformation();
	SDTransformation(Int_t set);
	virtual ~SDTransformation();

	void InitOne();

    parsTransformation& GetPars() {return fPars;}
    void SetPars(const parsTransformation& pars) {fPars = pars;}

    void Transform(Int_t col,Int_t row, Int_t &colNew,Int_t &rowNew);
	void TransformRotate90(Int_t col, Int_t row, Int_t &colNew, Int_t &rowNew);
	void TransformFlipVert(Int_t col,Int_t row, Int_t &colNew, Int_t &rowNew);
	void TransformCombine(Int_t col,Int_t row, Int_t &colNew,Int_t &rowNew);
	void TransformFlipHor(Int_t col,Int_t row, Int_t &colNew, Int_t &rowNew);
    virtual TString  GetSignature();

protected:
    parsTransformation fPars;

	ClassDef(SDTransformation,1);
};

#endif /* SDTRANSFORMATION_H_ */
