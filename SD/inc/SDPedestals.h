/*
 * SDPedestals.h
 *
 *  Created on: May 14, 2012
 *      Author: pavlinov
 */

#ifndef SDPEDESTALS_H_
#define SDPEDESTALS_H_

#include "SDObjectSet.h"
#include "SDConfigurationInfo.h"
#include "SDAliases.h"
#include "SDUtils.h"

#include <TH1.h>
#include <TMath.h>

#include "Fit/BinData.h"
#include "Fit/UnBinData.h"
#include "HFitInterface.h"
#include "Fit/Fitter.h"

#include <iostream>
using namespace std;

class TH1D;
class TH2;
class TList;
class TF1;

class SDPedestals: public SDObjectSet {
public:
	  enum ESet {
	      kFromMean = 0,
	      kFromFit,
	      kFromFitWithCuts
	  };
	SDPedestals();
	SDPedestals(const char* name);
	virtual ~SDPedestals();
	// Get methods
	Float_t  GetFoMax() const {return fF0Max;}
    TList* GetListWorkHists() {return GetListHists(gkNameWork);}
    TList* GetListAmplitudesHists() {return GetListHists(gkNameAmp);}
    TList* GetListAmplitudesHists(Int_t ind)
    {return dynamic_cast<TList *>(GetListHists(gkNameAmp)->At(ind));}
    TList* GetListAmplitudesAllHists() {return GetListAmplitudesHists(0);}
    TList* GetListAmplitudesCutsHists() {return GetListAmplitudesHists(1);}
    TList* GetListAmplitudesFitQAHists() {return GetListAmplitudesHists(2);}
    TList* GetListSummaryHists()    {return GetListAmplitudesHists(3);}
    TString GetSignature();

    TList* GetListProjections()     {return GetListHists(gkNameProj);}
    TList* GetListF0Hists()         {return GetListHists(gkNameF0);}
    TList* GetListF0Hists(Int_t ind)
    {return dynamic_cast<TList *>(GetListHists(gkNameF0)->At(ind));}
    TList* GetListF0WorkHists() {return GetListF0Hists(0);}
    TList* GetListF0SumHists()  {return GetListF0Hists(1);}
    TList* GetListF0QaHists()   {return GetListF0Hists(2);}
    TList* GetListF0SmoothHists()   {return GetListF0Hists(3);}
	TList* GetSplittedHists()   {return sda::FindList(GetListWorkHists(),gkSplittedHists);}
    TH2*   GetFoverF0Hist()     {return sda::GetH2(GetListWorkHists(), fNworkHists-2);}
    TH2*   GetFoverF0CutHist()  {return sda::GetH2(GetListWorkHists(), fNworkHists-1);}
    TH2*   GetHistForClusterFinder() {return GetFoverF0CutHist();}
    Int_t  fNworkHists;  // number of hists in list with name gkNameWork : 4 or 5
     //
    TF1* GetPedestalFun(int indh=1);
    TF1* GetPedestalRmsFun(int indh=1);
    void GetPedestalFunctions(int set, TF1 *funs[2]);
    TH1* GetHistWithLastPedFunction();
    // F0(x,y) : x,y are indexes: start from 0
    // biny - bin of hist, start from 1
    Double_t GetF0X(const Double_t x) const
    {return TMath::Exp(fBexp*x);}
    Double_t GetF0Y(const Int_t biny) const
    {return fF0Y->GetBinContent(biny);}
    Double_t GetF0Y(const Double_t y) const
    {return fF0Y->Interpolate(y);}
    Double_t GetF0XY(const Double_t x, const Double_t y) const
    {return GetF0X(x)*GetF0Y(y);}
    Double_t GetF0XY(const Double_t x, const Int_t biny) const
    {return GetF0X(x)*GetF0Y(biny);}
    parsPedestals& GetPedestalsPars() {return fPars;} // May 29,2013
	Int_t  GetMaxGoodBGCoord() const {return fMaxGoodBGCoord;}
	vector<pair<float,float> > GetGoodBgCoord ()  const {return fGoodBgCoords;}
	vector<pair<float,float> > GetEmptyBgCoords() const {return fEmptyBgCoords;}
   // Set
    SDPedestals& SetInput2DHist(TH2* h2raw/*, UInt_t set */);
    Double_t GetSigF0Unit() const {return fSigF0Unit;}
    void     SetPedestalsPars(parsPedestals pars) {fPars = pars;}
	void     SetMaxGoodBGCoord(Int_t var) {fMaxGoodBGCoord=var;}
	Double_t GetAmpCut() const {return (fPars.nsig * fSigF0Unit);} // June 12,2013

    // Book hists
    TList* BookSummaryHists();
    TList* BookF0Hists();
    TList* BookFitQAHists(TList *lqa);
    // Calculations
    bool FillInitialAmplitudesDistribution();
    bool FillInitialAmplitudesDistributionFrom2dHist(TH2* h2, TList *lall);
    static TH1* FillAndFit1dAmplitudeDistribution(TH1D *hd, const char* fitOpt="RQ0");
    void FillAmpDistWithCutsQaAndSum();
    void FitWithPointsRejection(TH1 *h, TF1* f, Int_t niter=2, Double_t nSigCut=3., Double_t chi2OverNdfOut=2.);
    TH1* RejectPoints(TH1* h, TF1* f, Double_t nSigCut=3., Int_t it=0);
    TH2* DeductPedestal(int set, int nsig);
    void CalculateF0(TH2* himg, TH1* hPedNoise);
    void FillH2FoverF0(TH2* hin, TH2* h2FOverF0);

    static void FillH2WithTimeCorrection (const TH2 *hin, const TF1* fcorr, TH2* hout); // Bleaching(time correction)
    static void FillH2WithSpaceCorrection(const TH2 *hin, TH1* hcorr, TH2* hout); // Blurring (space correction)
    static void FillFOverF0MinusOneWithCut(const TH2 *hin, const Double_t ampCut, TH2* hout); // Hist for cluster finder
    static void FillH2WithCut(const TH2 *hin, const Double_t ampCut, TH2* hout); // Apply cut for hist
    static void FillF0SmoothForSigma(TH2* hf0smooth, TH1* hreper, TList* lout);
    static void FillEmptyTimeZone(TH1* h, vector<pair<float,float> >&  v);
    static void Split2DHist(TH2* hin, vector<pair<float,float> > vcoords, Int_t maxThreads, TList *lout);
    //
    TH2* DoAll();
    TH2* SDAlgorithm();
    TH2* SDAlgorithm(int set, int nsig);
    TH2* IdlLikeAlgorithm();
    // Drawing
    void DrawPedestalsFunctions();  //   *MENU*
    void DrawPedExamples();         //   *MENU*
    void DrawChi2AndSigma();        //   *MENU*
    void DrawAmpAndSig(int ind=0);  //   *MENU*
    void DrawF0YFitQA();            //   *MENU*
    void DrawF0XorYvsProections(Int_t ind=0);   //   *MENU*   0-x; other-y
    void Draw2DRatioFoverF0();      // *MENU*
    void DrawFoverF0(UInt_t createProjs); //   *MENU*
    void PrintSplittingLines();     //   *MENU*
    // Service
    void DeleteListAmplitudesHists();
    TH1* CreateChi2List(const char* opt="goodChi2", const double chi2Cut=1.); // *MENU*
    static ROOT::Fit::Fitter* InitNewFitter(const char* name="Migrad"); // Fumili
    void FitExpoByMinuit2(TH1* hexp, TF1 *fexp, Double_t slope, Int_t pri=0);
	static void FitExpoAnalytically(TH1* hexp, TF1* fexp, Double_t slope, int pri=0);

protected:
    parsPedestals fPars; // copy of the parsPedestals at SDConfigurationInfo
    // F0(x,y) = F0(x)*F0(y); x-time; y-space
	Double_t fBexp;      // F0(x)~exp(-fBexp*x)
	Double_t fSigF0Unit; // Sigma of F0 for cuts
	TH1*     fF0Y;       // begin of X(time)
	TH1*     fF0YC;      // center of X(time)
	Float_t  fF0Max;     // max of (F/F0-1)
	// For histogram dividing before threading
	Int_t    fMaxGoodBGCoord;
	Double_t fMaxChi2;
	vector<pair<float,float> >  fGoodBgCoords;
	vector<pair<float,float> >  fEmptyBgCoords;
	// Working variable(s)
	TString  fFitOpt;    // fitting option: Feb 03,2013
	TString  fSignature; // Pedestal signature
	ROOT::Fit::Fitter*   fFitter; //!
public:
    static const char *gkNameWork;
	static const char *gkNameAmp;
	static const char *gkNameSum;
	static const char *gkNameProj;
	static const char *gkNameF0;
	static const char *gkSplittedHists;

	ClassDef(SDPedestals,1)
};

#endif /* SDPEDESTALS_H_ */
