/*
 * SDManager.h
 *
 *  Created on: Apr 12, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDMANAGER_H_
#define SDMANAGER_H_

#include "SDObjectSet.h"
#include "sdOpt.h"
#include "SDUtils.h"

#include <TList.h>
#include <TObjString.h>
#include <deque>
#include <list>

class SDPedestals;
class SDConfigurationParser;
class SDClusterFinderBase;
class SDControlBar;
class SDTiffFileInfo;
class SDTrialVersion;

class TImage;
class TBrowser;
class TH1;
class TH1D;
class TH2;
class TDatime;

class SDManager: public SDObjectSet {
public:
	  enum EStatusManager {
	      kBegin = 0,           // object created
	      kInitMain,            // object initialized (InitMain() was called)
	      kSelectXmlFile,       // Select XML file
	      kInitConfiguration,   // initialize the configuration
	      kReadTiff,            // read tif file, create a hist(s) for cluster finder(s)
	      kFill2DHists,         // create a hist(s) for cluster finder(s)
	      kInitPedestals,       // initialize the pedestal
	      kDoPedestals,         // do pedestals calculation, create hist with ped subtraction for cluster finder(s)
	      kInitClusterFunding,  // initialize clusters finder
	      kDoClusterFunding
	  };
    // do clusters finder
    SDManager(); // For reading
    SDManager(const char *name);
    virtual ~SDManager();
    void ClearEverything();  // *MENU*

    void SetConfigDir(TString configDir)   {fConfigDir.SetString(configDir.Data());}
    void SetConfigName(TString configName) {fConfigName.SetString(configName.Data());}
    TString GetConfigDir()  const {return fConfigDir.GetString();}
    TString GetConfigName() const {return fConfigName.GetString();}
    Int_t   GetStatus() const {return fStatus;}
    //
    TList* GetListHistsInput();

    TH2*    GetRawInputHist();
    TH2*    GetImageHistForCF(const int ind = 0);

    SDConfigurationParser* GetConfigurationParser() const;
    SDPedestals*           GetPedestals() const;
    SDClusterFinderBase*   GetClusterFinder(const int ind) const;
    SDControlBar*          GetControlBar();
    SDTiffFileInfo*        GetTiffFileInfo();

    void InitMain();
    void InitControlBar();
    void InitDefaultConfiguration();
    void InitConfiguration();
#ifdef TRIAL
    Bool_t InitTrial();
    SDTrialVersion*   GetTrialVersion();
#endif
	Bool_t SelectXMLFileDialog();
	Bool_t SelectTifFileDialog();
    SDTiffFileInfo* ReadTif();
//    Bool_t GetSpaceAndTimeScales(TString fname);
    TList *BookAndFillHistsIn(TImage *img, const char *name = "Hists test", Float_t scale = 100., Int_t ndiv = 16);
    void UpdateName(TString stmp);
    void InitPedestals();
    void DoPedestals();
    void InitClustersFinder();
    TList *CreateListHistsInputForCF();
    Int_t DoAll();
    //
    void DeleteControlBar();
    TString GetFileNameWithoutExtension(Int_t pri=0); // *MENU*
    void    SetFileNameWithoutExtension(TString nf);  // *MENU*
    void SaveSD(Int_t pri=0);         // *MENU*
    TString SaveTxtFileAsCSV(char symbol=',');
    TString SaveTxtFileTwo(); // *MENU*
    //
    void TestOne();
    static void TestTwo();
    //
    void PrintInfo();   // *MENU*
    void PrintVersion(FILE* fout);

    virtual Bool_t IsFolder() const {return kTRUE;}
    virtual void Browse(TBrowser *b);
protected:
    TObjString fConfigDir;         // Directory where configuration XML file is
    TObjString fConfigName;        // Name of configuration XML file (should be absolute)
    TObjString fConfigNameDefault; // Name of configuration XML file (should be absolute)
    Int_t      fStatus;            // Status of SDManager
    TDatime*   fDateStart;
    TDatime*   fDateStop;
    TString    fOutputFileName;    // Absolute output file name without extension like /home/wrk/output
public:
    static const char *gkNameHistInForCF;
    static const char *gkPeds;
    static const char *gkConfParser;
    static const char *gkBarName;

    ClassDef(SDManager,1)
};

#endif
