/*
 * SDTiffFileInfo.h
 *
 *  Created on: Jul 2, 2012
 * Copyright (C) 2012-2013, Alexei Pavlinov
 * All rights reserved.
 */

#ifndef SDTIFFFILEINFO_H_
#define SDTIFFFILEINFO_H_

#include "SDObjectSet.h"
#include "TObjString.h"
#include <vector>

using namespace std;

class TImage;
class TBrowser;
class TH2;
class SDImagesPool;
struct tiff;
// typedef tiff TIFF
class SDTransformation;

class tifHeader {
public:
	tifHeader() :
			byte0_1(-1), byte2_3(0), byte4_7(0), switchByte(0), numberOfDirectoryEntries(
					0), imageWidth(0), imageLength(0), samplesPerPixel(0), bitsPerSample(
					0), scanLine(0), fmt(0), maxSampleValue(0) {
		;
	}
	UShort_t byte0_1; // "II" or "MM"
	UShort_t byte2_3; // Should be 42 for tiff
	UInt_t byte4_7; // The offset (in bytes) of the first IFD
	Bool_t switchByte;
	UInt_t numberOfDirectoryEntries;
	UInt_t imageWidth;
	UInt_t imageLength;
	UInt_t samplesPerPixel;  // The number of components per pixel.
	UInt_t bitsPerSample;    // Number of bits per component.
	UInt_t scanLine;
	UInt_t fmt;
	UInt_t maxSampleValue;
};
ostream& operator<<(ostream& os, const tifHeader& h);

struct tag866c {
	tag866c() :
			timeScale(-1.), spaceScale(-1.) {
		;
	}
	// Instrumental unit
	// ms/line, ms = milli sec = 10^(-3) sec
	// Typical value is 2.08 ms, so 2.09*2048 ~ 4.28 sec
	Double_t timeScale;   // x
	// um - micro meter; 1um = 10^-6 meter
	// Typical value is 0.05 (um/pixel), so 0.05um*512 = 25.6um = 0.0256mm
	Double_t spaceScale;  // y
};
ostream& operator<<(ostream& os, const tag866c& t);

// Look to the http://root.cern.ch/root/html534/src/TASImage.cxx.html
#define __ARGB__
#ifndef __ARGB__
union pix32
{
	UInt_t ui32;
	struct {
		unsigned char alpha;  // 0-7   bits
		unsigned char blue;// 8-15  bits
		unsigned char green;// 16-23 bits
		unsigned char red;// 24-31 bits - empty
	};
	unsigned char charArr[4];
	void Print();
};
#else
union pix32 {
	UInt_t ui32;
	struct {
		unsigned char blue;   // 0-7   bits
		unsigned char green;  // 8-15  bits
		unsigned char red;    // 16-23 bits
		unsigned char alpha;  // 24-31 bits - empty
	};
	unsigned char charArr[4];
	void Print();
};
#endif
//ostream& operator<<(ostream& os, const pic32argb& p);

class imageStatus {
public:
	imageStatus() : color("grey"), status(0) {;}
	TString color;  // Byte selection from argb array
	UInt_t status; // status of picture as defined in EImagesStatus
//	ClassDef(imageStatus,1)
};
ostream& operator<<(ostream& os, const imageStatus& pr);

typedef vector<imageStatus> imgsStatusVector;

class SDTiffFileInfo: public SDObjectSet {
public:
	enum EImagesStatus {
		kBad = 0,         // bad
		kAvailable,       // Available for analysis
		kF0Done,          // F0 done
		kCFDone,          // Cluster Finder done
		kCSDone           // Cluster Scan   done
	};

	SDTiffFileInfo();
	SDTiffFileInfo(const char* name, const char* tifName);
	SDImagesPool *CreateImagesPool(const char* opt);
	virtual ~SDTiffFileInfo();
	//
	TString GetNameOfTiffReader() const {
		return fTiffReader;
	}
	TImage* GetImage() {
		return fImage;
	}
	SDImagesPool *GetImagesPool();

	TList* GetListHistsInput(const char* color = "Red");
	TList* GetListHistsInputProj() {
		return GetListHists(Form("%sProj", gkNameHistInput));
	} // ?

	Bool_t IsThisLittleEndian();
	Bool_t IsThisBigEndian() {
		return !IsThisLittleEndian();
	}
	TH2* GetTifAs2DHist(const char* color = "Red");
	tag866c GetTag866c() const {
		return fTag866c;
	}
	Double_t GetTimeScale() const {
		return fTag866c.timeScale;
	}
	Double_t GetSpaceScale() const {
		return fTag866c.spaceScale;
	}
	tifHeader GetTifHeader() const {
		return fTifHeader;
	}
	tifHeader GetTifHeader2() const {
		return fTifHeader2;
	}
	UInt_t GetMaxNumberOfPictures() const {
		return fTifHeader.samplesPerPixel;
	}
	imgsStatusVector& GetImagesStatus() {
		return fImagesStatus;
	}
	SDTransformation* GetSDTransformation() {
		return fTransform;
	}
//    UInt_t   GetGoodImagesCandidate();
	//
	Bool_t DecodeTiffFile();
	Bool_t ReadTiffFile();
	Bool_t ReadTiffFileDirectly(const char* fname);
	void ReadTagsByLibTiff(tiff *tiffInput);
	Bool_t Fill2DHists();

	TList* BookHistsIn(const char *name = "Hists test",
			const char* titRgb = "");
	void FillHistsInOne(TImage *img, TList *l, Int_t pict = -1);
	void FillHistsInTwo(TImage *img, TList *l, Int_t pict = -1);
	void GetNewColRowIfLeftPartIsFirst(Int_t col, Int_t row, Int_t &colnew,
			Int_t &rownew);
	void GetNewColRowIfRightPartIsFirst(Int_t col, Int_t row, Int_t &colnew,
			Int_t &rownew);
	void CheckRealityOfPicture(TList *lproj, imageStatus &pictRec);
	void PrintPictsStatus(imgsStatusVector &pv);
	void PrintTags(const char* tit = ""); // *MENU*
	void CheckTags(); // Mar 8,2013
	//    D r a w
	void DrawProjsNofilterVsFilter();  // *MENU*
	// Test tiff reader
	void TestUnionpix32();       // *MENU*
	void TestRaster(UInt_t n = 3, UInt_t start = 0); // *MENU*
	//
	virtual Bool_t IsFolder() const {
		return kTRUE;
	}
	virtual void Browse(TBrowser *b);
protected:
	TString fTiffReader;
	TImage* fImage;    //! Input image
	tiff* fTiff;     //! Tiff image as in libtif
	SDTransformation *fTransform; //! SD Transformation
	// Tiff decoding
	UInt_t fSize;       // size of file
	tifHeader fTifHeader;
	tifHeader fTifHeader2;
	tag866c fTag866c;
	//
	UInt_t fGoodImagesInput;
	imgsStatusVector fImagesStatus; //! images status

public:
	static const char* gkName;
	static const char *gkNameHistInput;

ClassDef(SDTiffFileInfo,1)
	;
};

#endif /* SDTIFFFILEINFO_H_ */
