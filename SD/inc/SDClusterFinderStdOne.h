/*
 * SDClusterFinderStdOne.h
 *
 *  Created on: May 17, 2012
 *      Author: pavlinov
 */

#ifndef SDCLUSTERFINDERSTDONE_H_
#define SDCLUSTERFINDERSTDONE_H_

#include "SDClusterFinderBase.h"
#include "SDObjectSet.h"
#include "SDUtils.h"

class TH2;
class TDataSet;
class TCanvas;
class SDManager;

class SDImageAnalysis;
class SDPedestals;
class SDClusterScan;

class SDClusterFinderStdOne: public SDClusterFinderBase, public SDObjectSet {
public:
	SDClusterFinderStdOne();
	SDClusterFinderStdOne(const char* name);
	virtual ~SDClusterFinderStdOne();
	void ClearClusterFinder();  // *MENU*

	virtual void  Init();
	virtual Int_t FindClusters();
	virtual Int_t GetOnlineDrawing() const    {return fOnlineDrawing;}
	virtual void  SetOnlineDrawing(Int_t var) {fOnlineDrawing = var;}
	virtual Int_t GetThreadsStatus() const    {return fThreadsStatus;}
	virtual void  SetThreadsStatus(Int_t var) {fThreadsStatus = var;}
	virtual Bool_t  IsThreadsAvailable() {return (fThreadsStatus != kNoThreads);}
	// Get methods
	SDImageAnalysis* GetImageAnalysis() {return sdu::GetImageAnalysis(this);}
	SDPedestals*     GetPedestal();
	virtual TDataSet* GetClusters();
	virtual SDClusterInfo* GetClusterInfo(const int ind);
	virtual TList* GetHists();        // List of objects (hists or some TObject's)
 	TList*  GetListHistsMain() {return GetListHists(gkNameHistsMain);}
	TList*  GetListHistsSum() {return GetListHists(gkNameHistsCFSum);}
	TList*  GetListOfBgLines() {return GetListHists(gkBgLines);}
	UInt_t  GetNClstsAll() const {return fNClstsAll;}
	TCanvas* GetCanvas() {return fC;}
    virtual parsClusterFinder GetPars() const {return fPars;}
	virtual Double_t GetSeed() const {return fPars.seed;}
    virtual TString GetSignature();

	virtual void SetCFName(const char* name)  {SetName(name);}
	virtual void SetCFPars(parsClusterFinder pars) {fPars = pars;}
    virtual void SetSeed(const Double_t seed) {fPars.seed = seed;}

	virtual void SetInputHist(TH2* hin);
	virtual void SortAndRenameClusters(Int_t mode);
	virtual TList* GetInputHists() {return GetListHists(gkNameInputHists);}
	virtual TList* GetMainHists();    // Return list of main hists
	virtual TList* GetSummaryHists(); // Return list of summary hists
	virtual TList* BookSummaryHists(const char* opt="");
	virtual void   FillSummaryHists();
	virtual void   CalculateClusterCharachteristics(SDClusterInfo &clinfo);
	virtual Bool_t IsThisClusterReal(SDClusterInfo &clinfo, parsClusterFinder *pcf);
	// Threads : Apr 21, 2013
	virtual Int_t MergeClusterFinders(TDataSet* dsIn);
	// Drawing
	virtual void DrawCFSummary();  // *MENU*
	virtual void DrawInputHistInCanvasWithClusters(Int_t redraw=0); // *MENU*
	virtual void DrawBackgroundColumns(Int_t var=0);                // *MENU*
	virtual TCanvas* DrawInputHistInCanvas(TH2* h, TCanvas* c=0);
	virtual void DrawClusterBox(TCanvas* c, SDClusterInfo* cli, Int_t ic);
	virtual void DrawFinalMessage(TCanvas* c, Int_t clstsReal, Int_t clstsAll);
	virtual TString SaveTxtFile(const char csv, UInt_t scan=0);
	static void PrintRecord(FILE* fout, SDClusterInfo* cli, UInt_t i);
	static void PrintRecordCSV(FILE* fout, SDClusterInfo* cli, UInt_t i, const char csv);
	static void PrintClusterScan(FILE* fout, SDClusterInfo* cli);
	static void PrintClusterScanCSV(FILE* fout, SDClusterInfo* cli, const char csv);
	//
	TH2*   FillRemnantHist(TH2* hin, clusterSimple *tab);
	virtual Double_t GetAmpCut() const {return fAmpCut;}
	virtual void     SetAmpCut(Double_t ampCut) {fAmpCut = ampCut;}
protected:
    parsClusterFinder fPars;
    Int_t fOnlineDrawing;
    Int_t fThreadsStatus;    // Run in threads or not : Apr 21, 2013
    Double_t fAmpCut;        // Amplitude cut for creating input hists : June 12,2013
	// Working variable
	UInt_t   fNClstsAll;     // Number of all clusters
	TCanvas *fC;             //! Canvas for online drawing
	TH1* fHInCopy;           //! Copy of input histogram
    TString fSignature;

    static const char *gkNameInputHists;
	static const char *gkNameHistsMain;
	static const char *gkNameHistsCFSum;
	static const char *gkBgLines;

	ClassDef(SDClusterFinderStdOne,1)

};

#endif /* SDCLUSTERFINDERSTDONE_H_ */
