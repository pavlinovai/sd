/*
 * SDF0Base.h
 *
 *  Created on: Dec 16, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDF0BASE_H_
#define SDF0BASE_H_

#include <SDObjectSet.h>
class TH1;

// We have one byte adc - from 0 to 255
const Double_t f0Max=4.0;
const Int_t    maxAdc = 255;

class SDF0Base : public SDObjectSet {
public:
	// Interface
	// Parameterization as in tiff file : for transition from F0 unit to tiff unit
	SDF0Base();
	SDF0Base(const Char_t* name );
	virtual void Init() = 0;
	virtual Double_t GetF0X(Double_t x) const = 0;
	virtual Double_t GetF0Y(Double_t y) const = 0;
	// F0 unit
	virtual void     SetSigF0Unit(Double_t f0Sig) = 0;
	virtual Double_t GetSigF0Unit() const = 0;
	virtual TH1*     GetErrorHist() = 0;
	// Digitization
	virtual Double_t GetDigitizationScale() {return f0Max/maxAdc;}

	ClassDef(SDF0Base,1)
};

#endif /* SDF0BASE_H_ */
