/*
 * SDClusterFinder2DThree.h
 *
 *  Created on: May 25, 2013
 *      Author: Alexei Pavlinov
 */

#ifndef SDCLUSTERFINDER2DTHREE_H_
#define SDCLUSTERFINDER2DTHREE_H_

#include "SDClusterFinder2DOne.h"

class SDClusterFinder2DThree: public SDClusterFinder2DOne {
public:
	SDClusterFinder2DThree();
	SDClusterFinder2DThree(const char* name);
	virtual ~SDClusterFinder2DThree();

	//virtual Int_t FindClusters();    // June 2
    // Pixels should have common side
	static void* FindClustersStatic(void* arg);
	virtual Bool_t ArePixelsNeighbours(const pixel &p1, const pixel &p2);

	ClassDef(SDClusterFinder2DThree,1)
};

#endif /* SDCLUSTERFINDER2DTWO_H_ */
