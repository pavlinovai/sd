/*
 * SDConfigurationParser.h
 *
 *  Created on: May 26, 2012
 *      Author: pavlinov
 */

#ifndef SDCONFIGURATIONPARSER_H_
#define SDCONFIGURATIONPARSER_H_

#include <TDataSet.h>

class SDConfigurationInfo;
class SDTransformation;
class TBrowser;
class TDOMParser;
class TXMLDocument;
class TXMLNode;

class SDConfigurationParser: public TDataSet {
public:
	SDConfigurationParser(const char* name="SD configuration");
	virtual ~SDConfigurationParser();

    void  SetDebug(const Int_t deb) {fDebug = deb;}
    Int_t  GetDebug() {return fDebug;}

    SDConfigurationInfo *GetConfigurationInfo(Int_t ind=0);
	SDConfigurationInfo *GetLastConfigurationInfo();
	SDConfigurationInfo *GetDefaultConfigurationInfo()
	{return GetConfigurationInfo(0);}

	void ParseFile(const char* name);
	void ParseElements(SDConfigurationInfo* ci);
	void ParseInput(TXMLNode* node, SDConfigurationInfo* ci);
	void ParseFillHists(TXMLNode* node, SDConfigurationInfo* ci);
	void ParseThreads(TXMLNode* node, SDConfigurationInfo* ci);
	void ParseTransformation(TXMLNode* node, SDConfigurationInfo* ci);
	void ParsePedestals(TXMLNode* node, SDConfigurationInfo* ci);
	void ParseClusterFinder(TXMLNode* node, SDConfigurationInfo* ci);
	void ParseOutput(TXMLNode* node, SDConfigurationInfo* ci);
	void ParseParsScan(TXMLNode* node, SDConfigurationInfo* ci);
	void GetDirectoryName(TString &stmp, TString envString);
	void CheckNameOfOutputFile();
	Bool_t SaveToRootFile();

	void CheckFloat(TString &s);
	void CheckDigit(TString &s);

	void PrintInfoXml(int key=0);  // *MENU*
    static SDConfigurationInfo *CreateDefaultConfiguration(
    		const char* defName="$(SDMain)/data/DefaultConf.xml");

protected:
	TDOMParser* fParser;         //!
	Int_t fDebug;

	ClassDef(SDConfigurationParser,1)
};

#endif /* SDCONFIGURATION_H_ */
