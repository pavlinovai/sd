/*
 * SDClusterBase.h
 *
 *  Created on: Nov 15, 2012
 *      Author: Alexei Pavlinov
 *      Base (super) class for cluster
 */

#ifndef SDCLUSTERBASE_H_
#define SDCLUSTERBASE_H_

#include "SDClusterInfo.h"

class pixel;
class TPolyLine;

class SDClusterBase {
public:
	virtual ~SDClusterBase() {;}
	virtual SDClusterInfo *GetClusterInfo() const = 0; // Parent of SDCluster
	virtual UInt_t     GetClusterSize() const = 0;
	virtual void       AddPixel(pixel &c) = 0;
	virtual pixel*     GetPixel(UInt_t ind) = 0;
	virtual pixel*     GetPixel(UInt_t col, UInt_t row) = 0;
	virtual void       PrintClusterInfo(const int pri=0) const = 0;
	virtual Int_t      Compare(const SDClusterBase* clb) const = 0;
	virtual Bool_t     IsNormalCluster() const {return 0;} // normal on default
	// Drawing
	virtual TPolyLine* CreatePolyline() = 0;
	virtual TPolyLine* GetPolyline() = 0;

	ClassDef(SDClusterBase,1)
};

#endif /* SDCLUSTERBASE_H_ */
