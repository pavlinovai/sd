// June 16,2013 : versionUpdate.h
#ifndef _SDVERSION_H_
#define _SDVERSION_H_
#define  VERSION_MAJOR      1
#define  VERSION_MINOR     42
#define  VERSION_BUILD_DATE	__DATE__ __TIME__
#define	 VERSION_DEBUG      0
#endif
