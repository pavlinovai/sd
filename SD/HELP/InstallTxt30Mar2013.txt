 mar 31
 ======
    Hi Syevda and Victor,
  
  I did the new version with custom selection of 
  transformation. With complete transformation 
  (rotate to righ on 90 degree, vertical flip and
  combine bottom/top) the comparing with Victor's
  program (tif file 3_ca50.tif) by yes looks good
  for me.
  
  I put to the attachment two files: installSdFromTar.sh and 
  SD03_31_2013.tar.
  
    Installation procedure is the same.
    
    Regards,
    Alexei.
