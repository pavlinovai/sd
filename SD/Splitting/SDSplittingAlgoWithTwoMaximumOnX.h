/*
 * SDSplittingAlgoWithTwoMaximumOnX.h
 *
 *  Created on: May 24, 2013
 *      Author: pavlinov
 */

#ifndef SDSPLITTINGALGOWITHTWOMAXIMUMONX_H_
#define SDSPLITTINGALGOWITHTWOMAXIMUMONX_H_

#include "SDSplittingAlgoBase.h"

class TH2;

class SDSplittingAlgoWithTwoMaximumOnX: public SDSplittingAlgoBase {
public:
	SDSplittingAlgoWithTwoMaximumOnX();
	SDSplittingAlgoWithTwoMaximumOnX(const Char_t* name,
			const Char_t* tit="splitting algo with 2 maximum on X axis");
	virtual ~SDSplittingAlgoWithTwoMaximumOnX();
	//
	virtual void MakeClusterSplitting();
	void CalculateEigenValues(TH2* h2, Int_t pri=1);

	ClassDef(SDSplittingAlgoWithTwoMaximumOnX,1)
};

#endif /* SDSPLITTINGALGOWITHTWOMAXIMUMONX_H_ */
