/*
 * SDSplittingAlgoBase.h
 *
 *  Created on: May 24, 2013
 *      Author: Alexei Pavlinov
 *      Base class for splitting algorithms
 */

#ifndef SDSPLITTINGALGOBASE_H_
#define SDSPLITTINGALGOBASE_H_

#include "SDObjectSet.h"
#include "SDClusterInfo.h"
#include "SDAliases.h"

class SDSplittingAlgoBase: public SDObjectSet {
public:
	SDSplittingAlgoBase();
	SDSplittingAlgoBase(const Char_t* name, const Char_t* tit="Base class for splitting algorithms");
	virtual ~SDSplittingAlgoBase();
	//
	void           SetInPutCluster(SDClusterInfo* cli);
	SDClusterInfo* GetInputCluster()         {return fInputCli;}
	TDataSet*      GetOutputClusters()       {return FindByName(gkDSOut);}
	//
//	TList* BookHists(); // do we need hist here
	virtual void MakeClusterSplitting();
	//
protected:
	SDClusterInfo* fInputCli;  //!

	static const Char_t* gkDSOut;

	ClassDef(SDSplittingAlgoBase,1)
};

#endif /* SDSPLITTINGALGOBASE_H_ */
