/*
 * SDClusterSplittingManager.cpp
 *
 *  Created on: May 23, 2013
 *      Author: Alexei Pavlinov
 */

#include "SDClusterSplittingManager.h"
#include "SDSplittingAlgoWithTwoMaximumOnX.h"
#include "SDAliases.h"
#include "SDClusterInfo.h"
#include "SDCluster.h"
#include "SDAliases.h"
#include "sdCommon.h"

#include <TH1.h>
#include <TH2.h>
#include <TF1.h>

ClassImp(SDClusterSplittingManager)

const Char_t* SDClusterSplittingManager::gkHistsMergeInfo = "HistsMergeInfo";

SDClusterSplittingManager::SDClusterSplittingManager() : SDObjectSet(),
		fSpectrum(0)
{ // For reading
}

SDClusterSplittingManager::SDClusterSplittingManager(const char* name, const char* tit) : SDObjectSet(name,tit),
		fSpectrum(0)
{
}

SDClusterSplittingManager::~SDClusterSplittingManager() {
	// TODO Auto-generated destructor stub
}

void SDClusterSplittingManager::Init()
{  // Should be called after Add to parent
   GetList()->Add(BookHistsMergeInfo());

   fSpectrum = new TSpectrum(500, 1.);
 //  Float_t resolution = 1.;
 //  fSpectrum.SetResolution(resolution);
}

TList* SDClusterSplittingManager::GetHistsMergeInfo() {
	return dynamic_cast<TList *>(sda::FindObjectByPartName(GetList(), gkHistsMergeInfo));
}

TList* SDClusterSplittingManager::BookHistsMergeInfo()
{
	TH1::AddDirectory(1);
	TH1* h = 0;

	TString col = GetImageAnalysis()->GetImageStatus().color;

	h = new TH2F("00_NpeakXvsNpeakY","NpeakX vs NpeakY  ; NpeakX ; NpeakY", 5,0.5,5.5, 5,0.5,5.5);
	TString stmp(gkHistsMergeInfo);
	stmp += col;

	TList *l = sda::moveHistsToList(stmp.Data());
	sda::AddToNameAndTitleToList(l,col,col);
	TH1::AddDirectory(0);

	return l;
}

void SDClusterSplittingManager::MakeClusterSplitting()
{
	FindMergingClusters();
}

void SDClusterSplittingManager::FindMergingClusters()
{
	TList *l = GetHistsMergeInfo();
	SDClusterFinderStdOne* clf = GetClusterFinder();

	TString snam = sd::GetSdCommon()->leadingNameSplitAlgo1;
	SDSplittingAlgoBase* sp1 = new SDSplittingAlgoWithTwoMaximumOnX(snam.Data(),"");
	Add(sp1);

	for(Int_t i=0; i<clf->GetClusters()->GetListSize(); ++i) {
		SDClusterInfo* clfi = clf->GetClusterInfo(i);
		TH1* hprox = sda::GetH1(clfi->GetProjHists(), 0);
		Double_t xmin = hprox->GetXaxis()->GetXmin();
		Double_t xmax = hprox->GetXaxis()->GetXmax();
		// Int_t Search(const TH1* hist, Double_t sigma = 2, Option_t* option = "", Double_t threshold = 0.05)
		Int_t npx = fSpectrum->Search(hprox, 2, "nobackground", 0.1); // sigma=2, threshold=0.1
        const Float_t* peaksX = fSpectrum->GetPositionX();
          if(npx==2) {
        	  Double_t xm1 = peaksX[0], xm2 = peaksX[1];
        	  if(xm1>xm2) swap(xm1,xm2);
           hprox->SetAxisRange(xm1, xm2);
           Double_t ymin = hprox->GetBinContent(hprox->GetMinimumBin()); // minimum value in the range
           hprox->SetAxisRange(xmin,xmax);
           Double_t ymax = hprox->GetMaximum();
           Double_t ymax2 = hprox->GetBinContent(hprox->FindBin(peaksX[1]));
      	   printf(" %i xm1 % 6.2f xm2 %6.2f : ymin %6.4f : ymax %6.4f ymax2 %6.4f \n",
      			   i+1, xm1, xm2, ymin, ymax, ymax2);
            if(ymin<0.9*ymax2) { // enough strong minimum
             sp1->SetInPutCluster(clfi);
             printf(" #peaks %i : peaks 1-(%6.2f,%6.2f) : ymin %6.2f  2-(%6.2f,%6.2f)\n ",
            		 npx, peaksX[0], ymax, ymin, peaksX[1], ymax2);
             sp1->MakeClusterSplitting();
             break;
          }
        }

//		TH1* hproy = sda::GetH1(clfi->GetProjHists(), 1);
//		Int_t npy = fSpectrum->Search(hproy, 5, "nobackground", 0.20);
//		sda::FillH2(l,0, double(npx), double(npy),1.);
	}
}
// Splitting utilities
Bool_t SDClusterSplittingManager::SplitClusterByLineParallelYcoord(SDClusterInfo* clIn, Double_t x0, TDataSet*& dsOut)
{
	// June 8, 2013 - for testing
	// clIn - input cluster which will be split by line x=x0;
	// dsOut - container of output clusters (should be two clusters)
   if(clIn==0) return kFALSE;
   SDClusterFinderBase *clf = clIn->GetClusterFinder(); // Get clf from parent cluster

   dsOut = new TDataSet("Splitted clusters");
   TString sn;
   sn.Form("%s Left", clIn->GetName());
   SDClusterInfo *clstOutL = new SDClusterInfo(sn.Data());
   dsOut->Add(clstOutL);
   sn.Form("%s Right", clIn->GetName());
   SDClusterInfo *clstOutR = new SDClusterInfo(sn.Data());
   dsOut->Add(clstOutR);
   clIn->Add(dsOut);

   SDClusterBase *clstIn = clIn->GetCluster();
   pixel *p = 0;

   for(UInt_t i=0; i<clstIn->GetClusterSize(); ++i) {
	   p = clstIn->GetPixel(i);
	   if(Double_t(p->col) < x0) clstOutL->AddPixel(*p);
	   else                      clstOutR->AddPixel(*p);
   }
   clf->CalculateClusterCharachteristics(*clstOutL);
   clf->CalculateClusterCharachteristics(*clstOutR);

   return kTRUE;
}

Bool_t SDClusterSplittingManager::SplitCluster(SDClusterInfo* clIn, Bool_t (*fun)(Double_t*,pixel &p), Double_t *pars, TDataSet*& dsOut)
{
	// June 8, 2013 - for testing
	// clIn - input cluster which will be split by line x=x0;
	// fun - logical function for spliiting
	// pars - parameters for fun
	// dsOut - container of output clusters (should be two clusters)
	   if(clIn==0) return kFALSE;
	   SDClusterFinderBase *clf = clIn->GetClusterFinder(); // Get clf from parent cluster

	   dsOut = new TDataSet("Splitted clusters");
	   TString sn;
	   sn.Form("%s 1", clIn->GetName());
	   SDClusterInfo *clstOutL = new SDClusterInfo(sn.Data());
	   dsOut->Add(clstOutL);
	   sn.Form("%s 2", clIn->GetName());
	   SDClusterInfo *clstOutR = new SDClusterInfo(sn.Data());
	   dsOut->Add(clstOutR);
	   clIn->Add(dsOut);

	   SDClusterBase *clstIn = clIn->GetCluster();
	   pixel *p = 0;

	   for(UInt_t i=0; i<clstIn->GetClusterSize(); ++i) {
		   p = clstIn->GetPixel(i);
		   if(fun(pars,*p)) clstOutL->AddPixel(*p);
		   else             clstOutR->AddPixel(*p);
	   }
	   clf->CalculateClusterCharachteristics(*clstOutL);
	   clf->CalculateClusterCharachteristics(*clstOutR);

	   return kTRUE;
}

Bool_t SDClusterSplittingManager::LineDelimiter(Double_t* pars, pixel& p)
{
	// pars[0]=a, pars[1]=b : y = a*x + b
	Double_t y = pars[0] * p.col + pars[1];
	if (Double_t(p.row) < y)
		return kTRUE;  // under line
	else
		return kFALSE; // over line
}

Bool_t SDClusterSplittingManager::LineDelimiterX0(Double_t* pars, pixel& p)
{
	// pars[0]=x0 -> delimiter line x=x0
	   if(Double_t(p.col) < pars[0]) return kTRUE;
	   else                          return kFALSE;
}
