/*
 * SDSplittingAlgoBase.cpp
 *
 *  Created on: May 24, 2013
 *      Author: Alexei Pavlinov
 */

#include "SDSplittingAlgoBase.h"

ClassImp(SDSplittingAlgoBase);

const Char_t* SDSplittingAlgoBase::gkDSOut = "ClsAfterSplitting";

SDSplittingAlgoBase::SDSplittingAlgoBase() : SDObjectSet(),fInputCli(0)
{
	// for reading only
}

SDSplittingAlgoBase::SDSplittingAlgoBase(const Char_t* name, const Char_t* tit) : SDObjectSet(name, tit),
		fInputCli(0)
{
  Add(new TDataSet(gkDSOut));
  if(strlen(name)==0) SetName("SDSplittingAlgoBase");
}

SDSplittingAlgoBase::~SDSplittingAlgoBase() {
	// TODO Auto-generated destructor stub
}

void SDSplittingAlgoBase::SetInPutCluster(SDClusterInfo* cli)
{
	fInputCli = cli;
	Add(cli); // Will be visible in browser
}

void SDSplittingAlgoBase::MakeClusterSplitting()
{
	printf("<I> you should implemented your own algorithm!");
}
