/*
 * SDClusterSplittingManager.h
 *
 *  Created on: May 23, 2013
 *      Author: Alexei Pavlinov
 */

#ifndef SDCLUSTERSPLITTINGMANAGER_H_
#define SDCLUSTERSPLITTINGMANAGER_H_

#include "SDObjectSet.h"
#include "SDImageAnalysis.h"
#include "SDClusterFinderStdOne.h"
#include <TSpectrum.h>

class SDClusterInfo;
class SDClusterFinderBase;

class SDClusterSplittingManager: public SDObjectSet {
public:
	SDClusterSplittingManager();
	SDClusterSplittingManager(const char* name, const char* tit="Clr splitting manager");
	virtual ~SDClusterSplittingManager();
	void Init();
	// Get
	SDClusterFinderStdOne* GetClusterFinder() {return dynamic_cast<SDClusterFinderStdOne *>(GetImageAnalysis()->GetClusterFinder());}
	TList* GetHistsMergeInfo();
    // Book
	TList* BookHistsMergeInfo();
	//
	void MakeClusterSplitting();
	void FindMergingClusters();  //  *MENU*
	// Splitting utilities
	static Bool_t SplitClusterByLineParallelYcoord(SDClusterInfo* clIn, Double_t x0, TDataSet*& dsOut);
	static Bool_t SplitCluster(SDClusterInfo* clIn, Bool_t (*fun)(Double_t*, pixel&), Double_t *pars, TDataSet*& dsOut);
	static Bool_t LineDelimiterX0(Double_t* pars, pixel& p);
	static Bool_t LineDelimiter(Double_t* pars, pixel& p);
protected:
	TSpectrum* fSpectrum;
	//
	static const Char_t* gkHistsMergeInfo;

	ClassDef(SDClusterSplittingManager,1)
};

#endif /* SDCLUSTERSPLITTINGMANAGER_H_ */
