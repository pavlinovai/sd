/*
 * SDSplittingAlgoWithTwoMaximumOnX.cpp
 *
 *  Created on: May 24, 2013
 *      Author: pavlinov
 */

#include "SDSplittingAlgoWithTwoMaximumOnX.h"

#include "TH2.h"
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"

#include "iostream"
using namespace std;

ClassImp(SDSplittingAlgoWithTwoMaximumOnX)

SDSplittingAlgoWithTwoMaximumOnX::SDSplittingAlgoWithTwoMaximumOnX()
{
	// For reading
}

SDSplittingAlgoWithTwoMaximumOnX::SDSplittingAlgoWithTwoMaximumOnX(const Char_t* name, const Char_t* tit)
:SDSplittingAlgoBase(name,tit)
{
}

SDSplittingAlgoWithTwoMaximumOnX::~SDSplittingAlgoWithTwoMaximumOnX() {
	// TODO Auto-generated destructor stub
}

void SDSplittingAlgoWithTwoMaximumOnX::MakeClusterSplitting()
{
  TH2 *h2 = sda::GetH2(fInputCli->Get2DHists(), 0);
  CalculateEigenValues(h2,1);
}
void SDSplittingAlgoWithTwoMaximumOnX::CalculateEigenValues(TH2* h2, Int_t pri)
{
	// June 9: in progress - hold over due to new idea about cluster scanning
	if(h2==0) return;
                         // Matrix of dispersions |Dxx Dxy|
	TMatrixDSym m(2);    //                       |Dxy Dxx|

	m[0][0] = h2->GetRMS(1)*h2->GetRMS(1); // Dxx
	m[0][1] = h2->GetCovariance(1,2);
	m[1][0] = m[0][1];
    m[1][1] = h2->GetRMS(2)*h2->GetRMS(2); // Dyy

    const TMatrixDSymEigen eigen(m);
    const TVectorD eigenVal = eigen.GetEigenValues();
    const TMatrixD eigenVectors = eigen.GetEigenVectors();
    TMatrixD t = eigenVectors;
    t = t.Transpose(t);
    if(pri>0) {
    	cout<<"\n m \n";
    	m.Print();
    	cout<<" Eigen Values \n";
    	eigenVal.Print();
    	cout<<" Eigen Vectors : and t \n";
    	eigenVectors.Print();
    	t.Print();
    }
    // Transform to new coordinates
    TVectorD xy(2), xyn(2);
//    TH2 *h2out = (TH2*)h2->Clone(h2->GetName());
//    h2out->SetTitle(h2->GetTitle());
//    h2out->Reset();
//    sda::AddToNameAndTitle(h2out,"Trans");
    TH2* h2out = new TH2F("Trans", "x,y after transformation", h2->GetNbinsX(),0.0,-1., h2->GetNbinsY(),0.0,-1.);
    h2out->SetOption("colz");
    GetInputCluster()->GetProjHists()->Add(h2out);
    for(Int_t binx=1; binx<=h2->GetNbinsX(); ++binx){
        for(Int_t biny=1; biny<=h2->GetNbinsY(); ++biny){
        	Double_t a = h2->GetBinContent(binx,biny);
        	if(a<=0.0) continue;
        	xy[0] = h2->GetXaxis()->GetBinCenter(binx) - h2->GetMean(1);
        	xy[1] = h2->GetYaxis()->GetBinCenter(biny) - h2->GetMean(2);
        	xyn   = t*xy;
        	//for(Int_t i=0; i<2; ++i) xyn[i] += h2->GetMean(i+1);
        	h2out->Fill(xyn[0],xyn[1],a);
//            if(pri>0 && a) {
//            	printf(" %6.2f,%6.2f -> %6.2f,%6.2f \n", xy[0],xy[1], xyn[0],xyn[1]);
//            }
        }
    }
}
