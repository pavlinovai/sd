/*
 * SDClusterScan.h
 *
 *  Created on: June 9, 2013
 *  Copyright (C) 2012-2013, Alexei Pavlinov. All rights reserved.
 */

#ifndef SDCLUSTERSCAN_H_
#define SDCLUSTERSCAN_H_

#include "SDObjectSet.h"
#include "sdCommon.h"
#include "SDAliases.h"
#include "SDUtils.h"

#include <TH1.h>
#include <TH2.h>

class SDClusterInfo;
class SDPedestals;
class parsSdScan;

class TDataSet;

class SDClusterScan: public SDObjectSet {
public:
	SDClusterScan();
	SDClusterScan(const char *name);
	virtual ~SDClusterScan();
	//
	SDPedestals*           GetPedestal();
	SDClusterFinderBase*   GetClusterFinder() {return sdu::GetClusterFinderParent(this);}
	TDataSet* GetCLFsPool() {return FindByName(sd::GetSdCommon()->nameCLFContainer.Data());}
	SDClusterFinderStdOne* GetClusterFinderChild(Int_t ind);
	//
	void Init(SDClusterInfo *cli);  //  will be done from parent cluster information
	void CalculateScanPars(const parsSdScan &pars);
	void Scan(Int_t progress=0);
protected:
	Int_t    fNscan;    // number of scanned points
	Double_t fAmpMin;   // first point for amplitude cut
	Double_t fAmpMax;   // last point for amplitude cut
	Double_t fAmpStep;  // amplitude cut step
	Double_t fMaxClsts; // Max number of cluster on particular scan

public:
	Int_t GetNscan() const {return fNscan;}
	void SetNscan(Int_t nscan) {fNscan = nscan;}

	Double_t GetAmpMin() const {return fAmpMin;}
	void SetAmpMin(Double_t ampMin) {fAmpMin = ampMin;}

	Double_t GetAmpStep() const {return fAmpStep;}
	void SetAmpStep(Double_t ampStep) {fAmpStep = ampStep;}

	TH2* GetH2In() {return sda::GetH2(GetList(),0);}
	void SetH2In(TH2*& h2In) {GetList()->Add(h2In);}

	Double_t GetMaxClsts() const {return fMaxClsts;}
	void SetMaxClsts(Double_t maxClsts) {fMaxClsts = maxClsts;}

	ClassDef(SDClusterScan,1)

};

#endif /* SDCLUSTERSCAN_H_ */
