/*
 * SDClusterScan.cpp
 *
 *  Created on: June 9, 2013
 *  Copyright (C) 2012-2013, Alexei Pavlinov. All rights reserved.
 */

#include "SDClusterScan.h"
#include "SDConfigurationInfo.h"
#include "SDImageAnalysis.h"
#include "SDClusterInfo.h"
#include "SDPedestals.h"
#include "SDClusterFinder2DTwo.h"
#include "SDUtils.h"

#include <TGProgressBar.h>

ClassImp(SDClusterScan)
//____________________________________________________________________
// Scan of cluster by threshold
//
SDClusterScan::SDClusterScan() : SDObjectSet(),
fNscan(0), fAmpMin(0.0), fAmpMax(0.0), fAmpStep(0.0), fMaxClsts(0)
{
	// Default constructor for reading
}

SDClusterScan::SDClusterScan(const char* name) : SDObjectSet(name),
fNscan(0), fAmpMin(0.0), fAmpMax(0.0), fAmpStep(0.0), fMaxClsts(4)
{
	// Main constructor
	TString snam = sd::GetSdCommon()->leadNameScan;
	if(strlen(name)) snam += name;
	SetName(snam.Data());
	// Cluster finders container
	TDataSet *ds = new TDataSet(sd::GetSdCommon()->nameCLFContainer.Data());
	Add(ds);
}

SDPedestals* SDClusterScan::GetPedestal()
{
	// June 12,2013 - from the same image branch.
	// Pedestal is not parent of cluster.
	SDPedestals *ped = 0;
	SDImageAnalysis *imAna = sdu::GetParentTempl<SDImageAnalysis>(this);
	if(imAna) ped = imAna->GetPedestals();
	return ped;
}

SDClusterFinderStdOne* SDClusterScan::GetClusterFinderChild(Int_t ind)
{
	SDClusterFinderStdOne *clf = dynamic_cast<SDClusterFinderStdOne *>(GetCLFsPool()->At(ind));
	return clf;
}

SDClusterScan::~SDClusterScan()
{
	// TODO Auto-generated destructor stub
}

void  SDClusterScan::Init(SDClusterInfo *cli)
{
	//  Initialization will be done form parent cluster information
	// SDClusterInfo *cli = dynamic_cast<SDClusterInfo *>(GetParent());
	if(cli == 0) return;
	TH2* hin = sda::GetH2(cli->Get2DHists(),0);
	SetH2In(hin);

	SDPedestals *ped = GetPedestal();

	Double_t ampCut1 = ped->GetPedestalsPars().nsig * ped->GetSigF0Unit();
	Double_t ampCut2 = hin->GetMaximum();
	if(ampCut2<=ampCut1) fNscan = 0;
	Double_t step = (ampCut2-ampCut1)/(fNscan+1); // We don't need case with ampCut = ampCut2
	SetAmpMin(ampCut1);
	SetAmpStep(step);
}

void SDClusterScan::CalculateScanPars(const parsSdScan &pars)
{
	// June 21, 2013
    if(GetH2In()==0) {
       SDClusterInfo *cli = dynamic_cast<SDClusterInfo *>(GetParent());
       Init(cli);
    }

	SDPedestals *ped = GetPedestal();

    TH2* hin = GetH2In();
	SetNscan(pars.fNpScan);
    SetMaxClsts(pars.fMaxNClusts);

	fAmpMin = ped->GetPedestalsPars().nsig * ped->GetSigF0Unit();
	fAmpMax =  hin->GetMaximum() * pars.fMaxRelAmp;

	// Select step
	Double_t step = (fAmpMax - fAmpMin) / (fNscan + 1); // We don't need case with ampCut = fAmpMin
	SetAmpStep(step);
    if(GetAmpStep() < pars.fMinStep) SetAmpStep(pars.fMinStep);

    // Calculate number of scan
    fNscan = Int_t((fAmpMax - (fAmpMin + GetAmpStep()))/GetAmpStep()) + 1;
}

void SDClusterScan::Scan(Int_t progress)
{
	SDClusterFinderBase* clf = GetClusterFinder(), *clfScan=0;
	TGProgressBar* pb = 0;
    if(progress) {
	   pb = sdu::GetHProgressBar(400);
	   sdu::UpdateProgressBar(pb,0,fNscan);
    }

	TString stmp;
	for(Int_t i=1; i<=fNscan; ++i) {
		Double_t ampCut = fAmpMin + fAmpStep*i;
		stmp.Form(" %i scan: ampCut = %6.4f ",i, ampCut);
		TH2* hout = dynamic_cast<TH2 *>(GetH2In()->Clone(stmp.Data()));
		SDPedestals::FillH2WithCut(GetH2In(), ampCut, hout);

		parsClusterFinder pars = clf->GetPars();
		pars.drawing = 0;
		clfScan = sdu::CreateClusterFinder(pars, 0);
		clfScan->SetCFPars(pars);
		clfScan->SetAmpCut(ampCut);
		clfScan->Init();
		clf->SetOnlineDrawing(0);     // no drawing

		clfScan->SetCFName(stmp.Data());
		clfScan->SetInputHist(hout);

        Int_t nclst = clfScan->FindClusters(); // Obligatory - no thread here
		if(pb) sdu::UpdateProgressBar(pb,i,fNscan);
        if(nclst>0) {
		   GetCLFsPool()->Add(dynamic_cast<SDClusterFinderStdOne *>(clfScan));
        } else {
           delete clfScan;
           break;
        }
		if(nclst >= GetMaxClsts()) break; // too many clusters
	}
	if(pb) delete pb;
}
