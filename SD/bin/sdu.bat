REM analog of sdu.sh for windows 
REM 2013: Feb 10; Feb 18
set ROOTSYS=c:\root
set LIBGNU=C:\NewSoft\GnuWin32\bin
set LIBTIFF=C:\NewSoft\libxml2-2.7.8.win32\bin
set PATH=%ROOTSYS%\bin;%ROOTSYS%\lib;%PATH%
set PATH=%PATH%;%LIBGNU%;%LIBTIFF%
REM
set INSTALLDIR=%HOMEPATH%\wrk
set SDMAIN=%INSTALLDIR%\SDWIN
REM --
set SDXML=%SDMAIN%\data
set SDTIFF=%INSTALLDIR%\tiff
set SDOUTPUT=%INSTALLDIR%\sdoutput
