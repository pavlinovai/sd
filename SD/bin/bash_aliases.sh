#! /bin/bash -f
# Sep 27,2012 for SD software
# single qoute - no variables are not expanded
##
function ShowAlias()
{
  alias | grep $1
}
##
alias   Alias=ShowAlias
alias   lsd='ls -CFl|grep ^d'
alias   dalias=". ~/bin/bash_aliases.sh"
##
alias   Find="find . \( -name \!:1 \) -print"
##
alias   dir='ls -alq'
alias   .dir='ls -1adFgLl .\*'
alias   .ddd='ls -l|grep ^d'
alias   del="rm -i"
alias   lo="logout"
## my Definition
alias  up="cd ..; pwd"
alias  up2="cd ..;cd ..; pwd"
alias  up3="cd ..;cd ..;cd ..; pwd"
alias  bye="logout"
alias  cpu="ps -l -u$USER"
alias  .dir="ls -1adFgLl .*"
alias  hi=history
#
## Computer&system information
#
alias showsys='more /etc/issue.net'
alias cpuinfo='more /proc/cpuinfo'
alias meminfo='more /proc/meminfo'
alias version='more /proc/version'
alias edfx="xemacs -fg blue -bg white -font 9x15"
alias edf="emacs -fg blue -bg white -font 9x15"
## Mar 31,2012
##alias cuda_init=". ~/.cuda_init.sh"
##alias cuda_drvt="kextstat | grep -i cuda"
##alias cuda_check_nvcc="nvcc -V"
## Apr 23,2012
##alias updateLocateDb="sudo /usr/libexec/locate.updatedb" # Script to update the locate database
##alias qton="export GUI=qt"
##alias qtoff="export GUI=native"
#
## SD spesific
#
alias sdu=". ~/bin/sdu.sh"
#################################################################
