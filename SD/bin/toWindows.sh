#
##  bash script : 2013: Feb 18;
##  port SD to the windwos from tarfile 
#
if [ $# != 2 ]; then
  echo -e "\t\t Should define two arguments"
  echo -e "\t First  argument is instalation directory" 
  echo -e "\t Second argument is tar file \a"
  exit 1;
fi
#
if [ ! -e $2 ]; then
  echo -e "\t Tar $2 file does not exist! \a"
  exit 2;
fi
#
if [ ! -e $1 ]; then
  mkdir -p $1
fi
# Make absolute name of files
dir=$1
dirpwd=`pwd`
if [ ${dir:0:1} != "/" ]
then
  dir=${dirpwd}/$1
fi
#
tarf=$2
if [ ${tarf:0:1} != "/" ]
then
  tarf=`pwd`/$2
fi
####
#
## Clean up the previous seting
#
unset ROOTSYS
unset LD_LIBRARY_PATH
#
echo "Instalation directory is : $dir"
echo "Tar file is              : $tarf"
#
## Clear everything
cd $dir
rm -f CMakeLists.txt SD*_*_*.tar
rm -r -f cmake macros GUI bin data src HELP build inc
#
cd $dir
_tar=`ls ../SD*.tar`
tar -vxzf $_tar
rm -f src/SDManagerdict.h src/SDManagerdict.cxx
#
ls -l
##
cd ..
for dirout in WIN_DLL WIN_DLL_REL
##for dirout in WIN_DLL_REL
do
  _sddll=${dirpwd}/${1}${dirout}
  echo -e "\n\t\t" _sddll ${_sddll} 
#
  cp $1/src/*.cxx ${_sddll}/
  cp $1/inc/*.h   ${_sddll}/
  cp $1/Simulation/*cxx ${_sddll}/
  cp $1/Simulation/*h   ${_sddll}/
##
  for dirs in bin data macros
  do
    rm -r -f ${_sddll}/${dirs}
    cp -r $dir/${dirs} ${_sddll}/
  done
done
##