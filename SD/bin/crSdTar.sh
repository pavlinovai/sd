#
##  bash script : create tar file for installation
##  2013 : 22 Jan; 29 Jan;
## to do : include program version
#
function saveCrTimeInfo()
{
## Date of tar creation for syncronization
  _fcr=$_dirin/bin/timeInfo.sh
  echo "echo Tar file creation time   : '`date`'" > $_fcr
}
#########################################################
echo " Tar file creation is started "
_dirin=$SDMAIN
if [ -z $SDMAIN ] # true iv var is unset : tests for a zero-length string
then
  _dirin=$HOME/Eclipse/HELIOUS/workspace/SD
else
  _dirin=$SDMAIN
fi
cd $_dirin
##
saveCrTimeInfo
##
_now=$(date +"%m_%d_%Y")
_tarname=SD${_now}.tar
_tar=$HOME/tmp/$_tarname
rm ${_tar}
##
_tarlist="inc src Simulation Splitting bin macros cmake data "
# .rootrc should be in macros directory
_tarlist+="DOC/LaTex/SD/InstallationAndRunning.pdf "
_tarlist+="DOC/LaTex/SD/ClusterReco.pdf "
# Exclude 
_excludelist="--exclude '*CVS*' --exclude '*~' --exclude '*#' --exclude '.root_*' --exclude '*.orig*'"
# Create tar file
tar -czf $_tar $_excludelist $_tarlist
# List of tar file
tar -tzf $_tar
echo -e "\n\t tar -czf $_tar $_excludelist $_tarlist \n"
#ls -l $_tar
# For Ubuntu on Mac : Aug 16,2012
_to=$HOME/Shared/wrk/SD
if [ -e  $_to ]
then
  cp $_tar $_to
  cp $_dirin/bin/installSdFromTar.sh $_to
  cp $_dirin/bin/toWindows.sh $_to
#   Dec 2,2012
  cd $_to
  tar -vxf $_tar # for Windows on MAC
##
  ls -l $_to/$_tarname $_to/installSdFromTar.sh  $_to/toWindows.sh
fi
# `pwd` - show the directory where I run my script
echo -e "\n\t Directory In $_dirin \n"
#
## tar -vxf $_tar  - example for extracting
#