#
## bash file for generation of versionUpdate.h 
## 2013: March 18; Mar 25; 
#
##root.exe -x upVer.C 
#
fout=../inc/versionUpdate.h
echo "#ifndef _SDVERSION_H_"  > $fout 
echo "#define _SDVERSION_H_" >> $fout
echo "#define  VERSION_MAJOR      1" >> $fout
echo "#define  VERSION_MINOR     27" >> $fout
echo "#define  VERSION_BUILD_DATE	`date`" >> $fout
#
#while read line           
#do           
#     lout=$line
#done < VerDebug.dat
#
#echo lout $lout
#echo $(( ++lout )) > VerDebug.dat
#echo "#define	VERSION_DEBUG	 $(( ++lout ))" >> $fout
#
echo "#endif"  >> $fout
#