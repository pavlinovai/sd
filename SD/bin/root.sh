#################################################
# Aug 11,2010                                   #
#################################################
#
function prRootEnv()
{
  echo ROOTSYS "$ROOTSYS"
  echo ROOTSVNREV "$ROOTSVNREV"
  echo DYLD_LIBRARY_PATH "$DYLD_LIBRARY_PATH"
  echo LD_LIBRARY_PATH "$LD_LIBRARY_PATH"
  echo QTDIR ${QTDIR}
  echo GUI style ${GUI}
}
##
if [ $?{ROOTSYS} ]; then
  echo " ROOTSYS is defined : $ROOTSYS"
else
##
  echo narg $# $1
  ver=v5-32-00   # default value 30 Jan,2013
  if [ $# -gt 0 ]; then 
    if [ -e $HOME/ROOT/$1 ]; then
      ver=$1
    fi
  fi
  echo Root version is $ver
#
  export QTDIR=/opt/local/ # Apr 19,2012
  export GUI=native # native or qt : ROOT staff
#
  export ROOTSYS=$HOME/ROOT/$ver
  export PATH=$ROOTSYS/bin:$PATH
  export ROOTSVNREV=`root-config --svn-revision`
## /opt/local/lib used by root first
#if [ "${QTDIR}" != "" ]; then 
##  export DYLD_LIBRARY_PATH="$HOME/macros:$QTDIR:$DYLD_LIBRARY_PATH"
#  export DYLD_LIBRARY_PATH="$QTDIR:$DYLD_LIBRARY_PATH"
#fi
  arch=`uname`
  if [ $arch == "Linux" ]; then
    export LD_LIBRARY_PATH=$ROOTSYS/lib
  else 
    unset DYLD_LIBRARY_PATH # Work well without this on Darwin
    unset LD_LIBRARY_PATH 
  fi
#
fi
#
prRootEnv # Call function
#