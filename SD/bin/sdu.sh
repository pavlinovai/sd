#
## Aug 12,2012 - initialize SD evironment
#    2013; 27 Jan
## First parameter is directory where SD was installed
#
function sdPri()
{
  echo -e "ROOTSYS\t\t$ROOTSYS"
  echo -e "SDMAIN\t\t$SDMAIN"
  echo -e "SDXML\t$SDXML"
  echo -e "SDTIFF\t\t$SDTIFF"
  echo -e "SDOUTPUT\t\t$SDOUTPUT"
  echo -e "GUI\t $GUI"
  if [ ${#DYLD_LIBRARY_PATH} ]; then 
    echo DYLD_LIBRARY_PATH $DYLD_LIBRARY_PATH
  fi
  if [ ${#LD_LIBRARY_PATH} ]; then 
    echo LD_LIBRARY_PATH $LD_LIBRARY_PATH
  fi
  echo -e "\a"
}
###########################################
arch=`uname`
export GUI=native # for sure
## Aug 17,2012
if [ ${#ROOTSYS} ]; then 
  export ROOTSYS=`find $HOME/ROOT -name v5-32-00` # should be sync. wirh instalation staff
  export PATH=$ROOTSYS/bin:$PATH
  export ROOTSVNREV=`root-config --svn-revision`
  if [ $arch == "Linux" ]; then
    export LD_LIBRARY_PATH=$ROOTSYS/lib
    unset DYLD_LIBRARY_PATH
  else 
    unset DYLD_LIBRARY_PATH # Work well without this on Darwin
    unset LD_LIBRARY_PATH 
  fi
fi
#
export INSTALLDIR=$HOME/wrk
#if [ $# -gt 0 ]; then
#  export INSTALLDIR=$HOME/$1
#fi
export SDMAIN=$INSTALLDIR/SD
#
## Next 3 env.variables could be not in SDMAIN area
#
export SDXML=${SDMAIN}/data    # directory for xml files
export SDTIFF=$INSTALLDIR/tiff # directory for tiff images
export SDOUTPUT=$INSTALLDIR/sdoutput # directory for output files
#
if [ $arch == "Linux" ]; then
  export LD_LIBRARY_PATH=${SDMAIN}/bin:${LD_LIBRARY_PATH}
else
## root knows as load root libraries with empty DYLD_LIBRARY_PATH.
## You will have a problem if you include $ROOYSTS/lib to the 
## DYLD_LIBRARY_PATH on MAC OS X
  export DYLD_LIBRARY_PATH=${SDMAIN}/bin
fi
# Define aliases
if [ -e $HOME/bash_aliases.sh ]
then
  source $HOME/bash_aliases.sh
fi
#
sdPri
cd $SDMAIN/macros
pwd
#
