#
##    bash script : Aug 3,2012;
## Sep 19; Nov 21 - try to add Windows system
## 2013 : 24 Jan; 27 Jan; 
#
if [ $# != 2 ]; then
  echo -e "\t\t Should define two arguments"
  echo -e "\t First  argument is instalation directory" 
  echo -e "\t Second argument is tar file \a"
  exit 1;
fi
#
if [ ! -e $2 ]; then
  echo -e "\t Tar $2 file does not exist! \a"
  exit 2;
fi
#
if [ ! -e $1 ]; then
  mkdir -p $1
fi
# Make absolute name of files
dir=$1
if [ ${dir:0:1} != "/" ]
then
  dir=`pwd`/$1
fi
#
tarf=$2
if [ ${tarf:0:1} != "/" ]
then
  tarf=`pwd`/$2
fi
#
## Clean up the previous seting
#
unset ROOTSYS
unset LD_LIBRARY_PATH
#
echo "Instalation directory is : $dir"
echo "Tar file is              : $tarf"
#
export ROOTSYS=`find $HOME/ROOT -name v5-32-00`
export SDMAIN=$dir
#
PATH=$ROOTSYS/bin:$PATH
echo ROOTSYS $ROOTSYS PATH $PATH
export LD_LIBRARY_PATH=${ROOTSYS}/lib:$SDMAIN/bin
export CXX=`which g++`
## Clear everything
cd $dir
rm -f CMakeLists.txt SD*_*_*.tar
rm -r -f cmake macros GUI bin data src HELP build inc
#
cp $tarf $dir
cd $dir
_tar=`ls SD*.tar`
tar -vxzf $_tar
rm -f src/SDManagerdict.h src/SDManagerdict.cxx
cd $dir/inc
make -f MakeDict
#
## Copy to bin ##
#
##if [ ! -e $HOME/bin/sdu.sh ] 
##then
  cp $dir/bin/sdu.sh  $HOME/bin/
##fi
#cp $dir/bin/addToUbuntu.sh $HOME/bin # Put to the installRoot.sh
cp $dir/bin/installRoot.sh $HOME/bin
cp $dir/bin/bash_aliases.sh $HOME/bin
#
cp $dir/macros/.rootrc  $HOME/bin
#
## Build ##
#
echo -e "\n\n\t ** MAKE is started ; dir `pwd` **"
cd $dir
cp cmake/CMakeLists.txt .
#
mkdir build
rm -f build/*
cd build
os=`uname -s`
#if [ ${os:0:6} = "CYGWIN" ] 
#then
# This is win32
#  cmake -G "Visual Studio 10" ..
#else 
  cmake ..
#fi
make VERBOSE=1
### Clean up source
if [ $USER != "pavlinov" ] ; then
  rm -r -f $SDMAIN/src/*.cxx $SDMAIN/inc/*.h 
  rm -r -f $SDMAIN/Simulation/*.cxx $SDMAIN/Simulation/*.h
fi
###
export SHRLIB=`find $SDMAIN -name libSD.*`
lensh=${#SHRLIB}
if [ "$lensh" -gt 0 ]; then
  echo -e "\n\t<I> Shared library $SHRLIB is built succefully "
else
  echo -e "\n\t<E> No shared Library";
fi
echo -e "\n\n\t ** MAKE is ended ** \n\n"
#
## Log file
#
_now=$(date +"%m_%d_%Y")
_logfile=$dir/install.log  # Name of log file should me more informative?
echo "     Instalation of SD software  " 
echo "User                     : $USER" >> ${_logfile}
echo "HOSTNAME                 : $HOSTNAME" >> ${_logfile}
##echo "Inet Addr                " `ifconfig eth0 | grep 'inet addr'` >> ${_logfile}
echo "Instalation directory is : $dir" >> ${_logfile}
echo "CXX      is              : $CXX" >> ${_logfile}
echo "Instalation command   is : $0 $1 $2" >  ${_logfile}
echo "Tar file is              : $tarf" >> ${_logfile}
echo "ROOTSYS  is              : $ROOTSYS" >> ${_logfile}
echo "LD_LIBRARY_PATH  is      : $LD_LIBRARY_PATH" >> ${_logfile}
echo "Shared library           : $SHRLIB" >> ${_logfile}
bash $dir/bin/timeInfo.sh >> ${_logfile}
echo "Instalation time         : `date`" >> ${_logfile}
echo "PATH     is              : $PATH" >> ${_logfile}
#
echo -e "\a\n\t Instalation Summary \n"
more $_logfile
echo -e "\n\t"
###
unset ROOTSYS
unset SDMAIN
unset CXX
###############################################
##source ~/bin/sdu.sh
##cd ../macros
##echo -e "\t  testSDInstalation() pwd `pwd` SDMAIN $SDMAIN \n"
##root.exe<<EOF
##//
##TString st("SD is ready!  ");
##if(gui == 0) {
##  st = " Somehing wrong with SD library !  ";
##}
##TString snew = st(0,1);
##TText tt(0.1,0.5,snew.Data());
##tt.SetTextColor(kRed);
##tt.SetTextSize(0.2);
##tt.Draw();
##gSystem->Sleep(200);
##//
##for(UInt_t ii=2; ii<=st.Length(); ++ii){
##  snew = st(0,ii);
##  tt.SetText(0.1,0.5, snew.Data());
##  tt.SetTextColor(st.Length()-ii+2);
##  gSystem->ProcessEvents();
##  gPad->Update();
##  gSystem->Sleep(500);
##}
##gSystem->Sleep(1000);
##//
##.q
##EOF
###
## Should be tested
##dialog --title "Instalation Summary" --textbox "$_logfile" 14 100
##