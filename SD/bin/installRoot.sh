#
## Sep 25-27,2012 - Install ROOT version v5-32-00
## bash installRoot.sh
#

## Install additional packages
#
arch=`uname`
# This staf we need for Ubuntu
if [ "$arch" = "Linux" ]
then
  sudo apt-get install subversion dpkg-dev make g++ gcc
  sudo apt-get install libx11-dev libxpm-dev libxft-dev
  sudo apt-get install binutils libxext-dev dialog
  sudo apt-get install libldap2-dev libxml2-dev
  # UBUNTU on AMD processor has not libtiff4-dev, Sep 27,2012
  sudo apt-get install libtiff4-dev
  # 
  sudo apt-get install cmake xemacs21
# For information 
  dpkg --get-selections \*tiff\*
fi
# 
hroot=${HOME}/ROOT
ver=v5-32-00 # Sep 25,2012
echo "Arch $arch hroot $hroot ver $ver"
# root.exe exist ?
rootexe=`find ${hroot}/${ver}/bin -name root.exe`
echo "1 rootexe len ${#rootexe} $rootexe"
#
# Build ROOT
#
#if [ -z ${rootexe} ]  # tests for a zero-length string
#then
##echo "2 rootexe len ${#rootexe} $rootexe"
##else
if [ ! -e ${hroot} ]
then
  mkdir -p $hroot
fi
cd $hroot
#
## rm -r -f $hroot/$ver/graf2d/* # one time action - Sep 27,2012
#
#
## Check out ROOT
#
svn co http://root.cern.ch/svn/root/tags/$ver $ver
#
## Configure ROOT
#
cd $hroot/$ver
./configure --enable-table --enable-minuit2 --enable-roofit
make -j2
echo -e "\a\n\t ROOT is ready! \n"
#
#else
#  echo -e "\a\n\t root.exe is $rootexe \n"
#fi
#
## Check ROOT
#
echo -e "\n\tsource ${hroot}/${ver}/bin/thisroot.sh\n"
source ${hroot}/${ver}/bin/thisroot.sh
#
root.exe<<EOF
//
TString st("Root is ready!");
//
TString snew;
snew = st(0,1);
TText tt(0.1,0.5,snew.Data());
tt.SetTextColor(kRed);
tt.SetTextSize(0.2);
tt.Draw();
gSystem->Sleep(200);
//
for(UInt_t i=2; i<=st.Length();i++){
  snew = st(0,i);
  tt.SetText(0.1,0.5, snew.Data());
  tt.SetTextColor(st.Length()-i+2);
  gSystem->ProcessEvents();
  gPad->Update();
  gSystem->Sleep(200);
}
gSystem->Sleep(1000);
//
.q
EOF
#

