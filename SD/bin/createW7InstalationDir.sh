##
## Bash file : should be run under Cygwin : May 19,2013
## 
# bash -v /cygdrive/c/Users/pavlinov/Documents/VisualStudio2010/Projects/SD/SD/macros//createW7InstalationDir.sh
##
export SDMAIN=/cygdrive/c/Users/pavlinov/Documents/VisualStudio2010/Projects/SD
export INSDIR=/cygdrive/c/Users/pavlinov/tmp/instal
export ROOTSYS=/cygdrive/c/Users/pavlinov/ROOT/v5-32-04deb
##
cd $INSDIR
if [ -d ROOT ]
then 
#  rm -r -f ROOT/*
   echo ROOT exists!
else 
  mkdir ROOT 
fi
#
cd ROOT; pwd 
rsync -avz --exclude "*.pdb" --exclude "*roota.*" ${ROOTSYS}/bin .
rsync -avz --exclude "*.exp" --exclude "*.rootmap" --exclude "libRoot.a" ${ROOTSYS}/lib .
rsync -avz --exclude "*.exp" --exclude "*.rootmap" ${ROOTSYS}/include .
rsync -avz --exclude "*.o" --exclude "*.d" --exclude "*.pdb" ${ROOTSYS}/cint .
rsync -avz ${ROOTSYS}/etc .
rsync -avz ${ROOTSYS}/fonts .
rsync -avz ${ROOTSYS}/icons .
rsync -avz ${ROOTSYS}/tutorials .
cd $INSDIR
ls -l ROOT/*
##
##################### SD ######
##
if [ -d SD ]
then 
   rm -r -f SD/*
   echo SD exists!
else 
  mkdir SD 
fi
# Directories
cd SD
rsync -avz --exclude "*.pdb" ${SDMAIN}/SD/data .
rsync -avz --exclude "*~" ${SDMAIN}/SD/macros .
##### Exe files and libs
export type=Debug # or Release
## for root.exe
cd $INSDIR
rsync -avz ${SDMAIN}/SD/${type}/SD.lib ${INSDIR}/ROOT/bin
rsync -avz ${SDMAIN}/SD/${type}/SD.lib ${INSDIR}//ROOT/bin
## Seperate exe file (instead of root)
rsync -avz ${SDMAIN}/SD/${type}/SDEXE.exe ${INSDIR}/ROOT/bin
rsync -avz ${SDMAIN}/SD/${type}/SDEXE.lib ${INSDIR}/ROOT/bin
# Static SD libraries and exe file
rsync -avz ${SDMAIN}/SD/${type}/SDLIB.lib ${INSDIR}/ROOT/bin
rsync -avz ${SDMAIN}/SD/${type}/SDEXESTAT.exe ${INSDIR}/ROOT/bin
dir -l ${INSDIR}/ROOT/bin/SD*.*
rsync -avz /cygdrive/c/NewSoft/GnuWin32/bin/*.dll ${INSDIR}/ROOT/bin/
##rsync -avz /cygdrive/c/NewSoft/GnuWin32/bin/zlib1.dll ${INSDIR}/ROOT/bin/
##rsync -avz /cygdrive/c/NewSoft/GnuWin32/bin/glut32.dll ${INSDIR}/ROOT/bin/
#
## SDOUTPUT & TIFF
#
if [ -d SDOUPUT ]
then 
   rm -r -f SDOUTPUT/*
   echo SDOUTPUT exists!
else 
   mkdir SDOUTPUT 
fi
rsync -avz /cygdrive/c/Users/pavlinov/wrk/tiff/2012/Oct01/4acon.tiff ${INSDIR}/TIFF/
#
## Instalation batch file
# 
rsync -avz ${SDMAIN}/SD/bin/sdW7Debug.bat .
#
## tar staff
#
#TAR=instalation21May.tar
#TARLIST=ROOT/ SD/ SDOUTPUT/ TIFF/ sdW7Debug.bat
#echo 'tar -cf $TAR $TARLIST'
tar cfz instalation21May.tar.gz ROOT SD TIFF SDOUTPUT sdW7Debug.bat
tar tvf instalation21May.tar.gz
ls -l -h $TAR instalation21May.tar.gz
##
