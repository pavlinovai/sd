/*
 * SDControlBar.cpp
 *
 *  Created on: May 29, 2012
 *      Author: pavlinov
 */

#include "SDControlBar.h"
#include "SDManager.h"
#include "SDConfigurationParser.h"
#include "SDConfigurationInfo.h"

#include <TSystem.h>
#include <TControlBar.h>
#include "TGClient.h"
#include <TGFileDialog.h>
#include <TGMsgBox.h>

#include <iostream>

using namespace std;

SDManager* gMan; // Global variable

ClassImp(SDControlBar)

SDControlBar::SDControlBar() : TDataSet() , fBar(0){
	// TODO Auto-generated constructor stub
}

SDControlBar::SDControlBar(const char* name) : TDataSet(name), fBar(0)
{
}

SDControlBar::~SDControlBar() {
	delete fBar;
}

void SDControlBar::Init(Int_t brief)
{
	gMan = dynamic_cast<SDManager *> (GetParent());
	cout << " gMan " << gMan << endl;

	TControlBar *fBar = 0;
	fBar = new TControlBar("vertical", "SD simple bar");

	fBar->AddButton(" Standard browser ",
			"new TBrowser(\"browser\",\"StandardBr\")",
			"new TBrowser(\"browser\",\"StandardBr\",300,200)");

	// Select configuration file
	TString cmd("SDControlBar::SelectXMLFileDialog()");
	fBar->AddButton(" Select configuration file ", cmd.Data(),
			"select conf. file");
	// Select tif file
	cmd = "SDControlBar::SelectTifFileDialog()";
	fBar->AddButton(" Select tif file ", cmd.Data(),
			"select tif file");
    if(brief==0) {
	// Init configuration file
	  cmd = "SDControlBar::InitConfiguration()";
	  fBar->AddButton("Initialize configuration", cmd.Data(),
			"Initialize configuration");
    }
	// ReadTif();
	cmd = "SDControlBar::ReadTif()";
	fBar->AddButton("Read tif file", cmd.Data(), "Read tif file");

    if(brief==0) {
	// Initialize Pedestals
	  cmd = "SDControlBar::InitPedestals()";
	  fBar->AddButton("Initialize Pedestals", cmd.Data(), "Initialize Pedestals");
    }
	// Do Pedestals
	cmd = "SDControlBar::DoPedestals()";
	fBar->AddButton("Do Pedestals", cmd.Data(), "Do Pedestals");

    if(brief==0) {
	// Initialize Cluster Finders
	  cmd = "SDControlBar::InitClusterFinders()";
	  fBar->AddButton("Initialize Cluster Finders", cmd.Data(), "Initialize Cluster Finders");
    }
	// Do Cluster Finders
	cmd = "SDControlBar::DoClusterFinder()";
	fBar->AddButton("Do Cluster Finders", cmd.Data(), "Do Cluster Finder");

	// Save SDManger to root file (text file too)
	cmd = "SDControlBar::SaveSD()";
	fBar->AddButton("Save SDManger", cmd.Data(),
			"Save SDManger to root file (text file too)");

	fBar->Show();
}

void SDControlBar::SelectXMLFileDialog()
{
	if(gMan->GetStatus() < SDManager::kInitMain) {
		cout<<"<W> Before SelectXmlFile() InitMain() should be call \n";
	}
	gMan->SelectXMLFileDialog();
}

void SDControlBar::SelectTifFileDialog()
{
	if(gMan->GetStatus() < SDManager::kInitMain) {
		cout<<"<W> Before SelectTifFile() InitMain() should be call \n";
	}
	gMan->SelectTifFileDialog();
}

void SDControlBar::InitConfiguration()
{
	if(gMan->GetStatus() < SDManager::kSelectXmlFile) SelectXMLFileDialog();
	cout<<" gMan->InitConfiguration() \n";
	gMan->InitConfiguration();
}

void SDControlBar::ReadTif()
{
	if(gMan->GetStatus() < SDManager::kInitConfiguration) InitConfiguration();
	gMan->ReadTif();
}

void SDControlBar::InitPedestals()
{
	if(gMan->GetStatus() < SDManager::kReadTiff) ReadTif();
	gMan->InitPedestals();
}

void SDControlBar::DoPedestals()
{
	if(gMan->GetStatus() < SDManager::kInitPedestals) InitPedestals();
	gMan->DoPedestals();
}

void SDControlBar::InitClusterFinder()
{
	if(gMan->GetStatus() < SDManager::kDoPedestals) DoPedestals();
	gMan->InitClustersFinder();
}

void SDControlBar::SaveSD()
{
	const char *openTypes[] = { "ROOT files",   "*.root",
                                "txt files",    "*.txt",
	                            "All files",    "*",
	                            0,              0 };
	TGMsgBox* mbox = 0;
	// You can save SDManager at any time!
	//	if(gMan->GetStatus() < SDManager::kDoClusterFunding) DoClusterFinder();
    TString dir;
	SDConfigurationInfo* confInfo = gMan->GetConfigurationParser()->GetLastConfigurationInfo();
	parsInputOutput fout = confInfo->GetParsOutput();
	parsInputOutput fin  = confInfo->GetParsInput();

//	if(fout.name=="auto" || fout.name.Length()<6) { // Will get name form tif file
	if(1) { // Sep 20,2012 - now will ask in any case
	  TGFileInfo fi;
      fi.fFileTypes = openTypes;
      fi.fIniDir    = StrDup(fout.dir.Data());
      TString snam = fin.name;
      snam = snam(0, snam.Index(".tif")); // Discard extension : ".tif" or ".tiff"
      snam += ".root";
      fi.fFilename  = StrDup(snam.Data());

      new TGFileDialog(gClient->GetRoot(), 0, kFDSave, &fi); // Should be on heap !

// fi.fFileName is absolute file name now
      if (snam.Contains(fi.fFilename)) {
          TString tit("Try again!"), msg(Form("File name is template file name : |%s|",snam.Data()));
      	  cout<<msg<<endl;
      	  mbox = new TGMsgBox(gClient->GetRoot(), 0, tit.Data(), msg.Data()
      		,kMBIconExclamation, kMBOk);
      	  return;
      } else {
          TString absname(StrDup(fi.fFilename)), name=absname;
          cout<<" fi.fFilename "<<fi.fFilename<<" after dialog \n";
          if     (absname.Contains(".root")) absname.ReplaceAll(".root","");
          else if(absname.Contains(".txt"))  absname.ReplaceAll(".txt","");
          gMan->SetFileNameWithoutExtension(absname);
          // Put info to configuration file
          fout.dir = StrDup(fi.fIniDir);
          name.ReplaceAll(fout.dir,"");
    	  fout.name = name;
          cout<<"<I> Name of root file "<<fout.name<<endl;
      }
    } else {
      cout<<"<I> Name of root file "<<fout.name<<" from configuration \n";
    }

	TString tit = "Job is done!", msg;
	if(fout.name != "nosave") {
		msg = Form("root file %s ",fout.name.Data()); // Add name of txt file
		gMan->SaveSD();
		gMan->SaveTxtFileTwo();
	} else {
		msg = "No saving to root file.";
	}
	//
	// Final message
	//
	EMsgBoxButton buttons = kMBOk;
	mbox = new TGMsgBox(gClient->GetRoot(), 0, tit.Data(), msg.Data()
		    ,kMBIconExclamation, buttons);
	// Will delete after 5 second
	// gSystem->Sleep(5000);
	// delete mbox;
}

