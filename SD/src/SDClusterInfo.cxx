/*
 * SDClusterInfo.cpp
 *
 *  Created on: May 21, 2012
 *      Author: Alexei Pavlinov
 */

#include "SDClusterInfo.h"
#include "SDManager.h"
#include "SDUtils.h"
#include "SDAliases.h"
#include "SDCluster.h"
#include "SDCluster2D.h"
#include "SDPedestals.h"
#include "sdpixel.h"
#include "SDClusterFinderBase.h"
#include "SDClusterSplittingManager.h"
#include "SDSplittingAlgoWithTwoMaximumOnX.h"
#include "SDClusterScan.h"

#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TSpectrum.h>
#include <TSpectrum2.h>
#include <TPolyMarker.h>

ClassImp(SDClusterInfo)

const char* SDClusterInfo::gk2DHists   = "2D Hists";
const char* SDClusterInfo::gkProjHists = "ProjHists";

SDClusterInfo::SDClusterInfo() : SDObjectSet()
{
	// Default constructor for reading
}

SDClusterInfo::SDClusterInfo(const char* name) : SDObjectSet(name)
{
    TString clustName(name);
    Init(clustName);
}

SDClusterInfo::SDClusterInfo(const int clustNum) : SDObjectSet("tmp")
{
    TString clustName("ClusterInfo ");
    clustName += clustNum;
    Init(clustName);
}

void SDClusterInfo::Init(TString &clustName, const int nmaxRow)
{
    TList *l = new TList;
    l->SetName(gk2DHists);
    GetList()->Add(l);

    SetName(clustName.Data());
	clustName.ReplaceAll("Info","");
	Int_t key = 1; // O-table; 1 - 2d
	SDClusterBase *clb = sdu::CreateCluster(clustName.Data(),nmaxRow,key);
	TDataSet *ds = dynamic_cast<TDataSet *>(clb);
	Add(ds);
}

SDClusterInfo::~SDClusterInfo() {
	// TODO Auto-generated destructor stub
}

SDClusterFinderBase* SDClusterInfo::GetClusterFinder()
{
//	TDataSet *parent=0, *child = this;
//	SDClusterFinderBase* clf=0;
//	while((parent=child->GetParent())){
//		clf = dynamic_cast<SDClusterFinderBase *>(parent);
//		if(clf == 0) child = parent;
//		else         return clf;
//	}
//	return 0;
	return dynamic_cast<SDClusterFinderBase *>(GetParent()->GetParent());
}

SDCluster* SDClusterInfo::GetClusterTable(const char *name) const
{
   TDataSet* ds = FindByName(name);
   if(ds==0) ds = At(0); // return first
   return dynamic_cast<SDCluster *>(ds);
}

SDClusterBase* SDClusterInfo::GetCluster(const char *name) const
{
   TDataSet* ds = FindByName(name);
   if(ds==0) ds = At(0); // return first
   SDCluster *cl = dynamic_cast<SDCluster *>(ds);
   if(cl) {
	   return dynamic_cast<SDClusterBase *>(cl);
   } else {
	   SDCluster2D *cl2d = dynamic_cast<SDCluster2D *>(ds);
       if(cl2d) return dynamic_cast<SDClusterBase *>(cl2d);
   }
   return 0;
}

void SDClusterInfo::AddPixel(pixel &c)
{
	GetCluster()->AddPixel(c);
}

void SDClusterInfo::ChangeName(const Int_t ncl)
{
    SDClusterBase *cl = GetCluster();
	TString snam = Form("%i ClInfo %ipxs", ncl, cl->GetClusterSize());
	snam += Form(" c:%i-%i r:%i-%i",fClstCharFromF0.colmin,fClstCharFromF0.colmax,
			fClstCharFromF0.rowmin,fClstCharFromF0.rowmax);
	SetName(snam.Data());
	snam.ReplaceAll("Info", "");
	dynamic_cast<TDataSet *>(cl)->SetName(snam.Data());
}


void SDClusterInfo::Create2DHists(int force)
{
	// 2012: Nov 2;
  TH2 *h2 = dynamic_cast<TH2 *>(Get2DHists()->At(0)); // first hist
  TH2* h2ForCF = 0;
  if(force && h2) {
	  Get2DHists()->RemoveAt(0);
	  delete h2;
	  h2 = 0;
  }
  if(h2) return; // Hist exists
  SDManager* man = GetSDManager();
//  if(man) h2ForCF = man->GetImageHistForCF();
  SDClusterFinderBase* clf = GetClusterFinder();
  TList *l = clf->GetMainHists();
  h2ForCF = sda::GetH2(l,0);

  TH1::AddDirectory(kFALSE);
  SDClusterBase *clst = GetCluster();
  // Then number of bins more on two for more drawing
  // as for x axis as for y one
  Int_t nx = fClstCharFromF0.colmax - fClstCharFromF0.colmin + 1;
  if(nx%4) nx = (nx/4+1)*4;
  nx += 8;
  Double_t xmin = fClstCharFromF0.colmin-4.5;
  xmin = xmin<-0.5?-0.5:xmin;
  Double_t xmax = xmin + nx;
  xmax = xmax>2047.5?2047.5:xmax;
  nx = Int_t(xmax-xmin+0.5); // nearest positive integer
  Int_t ny = fClstCharFromF0.rowmax - fClstCharFromF0.rowmin + 1;
  if(ny%4) ny = (ny/4+1)*4;
  ny += 8;
  Double_t ymin = fClstCharFromF0.rowmin-4.5;
  ymin = ymin<-0.5?-0.5:ymin;
  Double_t ymax = ymin + ny;
  ymax = ymax>511.5?511.5:ymax;
  ny = Int_t(ymax-ymin+0.5); // nearest positive integer
  TString snam(Form("Hist%s",GetName()));
  TString stit(dynamic_cast<TDataSet *>(clst)->GetName());
//		  fClstCharFromF0.colmin,fClstCharFromF0.colmax, fClstCharFromF0.rowmin,fClstCharFromF0.rowmax));
    h2 = new TH2F(snam.Data(), stit.Data(), nx, xmin, xmax, ny, ymin, ymax);
    if(h2ForCF) {
      h2->GetXaxis()->SetTitle(h2ForCF->GetXaxis()->GetTitle());
	  h2->GetYaxis()->SetTitle(h2ForCF->GetYaxis()->GetTitle());
    }
	for (UInt_t i = 0; i < clst->GetClusterSize(); ++i) {
		pixel* c = clst->GetPixel(i);
		if (c) h2->Fill(c->col, c->row, c->amp);
	}
	stit = h2->GetTitle();
	stit += Form(" : corr %4.2f ", h2->GetCorrelationFactor());
	h2->SetTitle(stit.Data());
	h2->SetOption("colz");
	Get2DHists()->AddAt(h2, 0);

  // Create initial, remnant and raw hists
  TH2* h2i = sdu::CloneHist(h2, " In "," Input hist (corr)");
  Get2DHists()->AddAt(h2i,1);
  TH2* h2In = sda::GetH2(l,0);

  TH2* h2rem = sdu::CloneHist(h2, " Rem"," Remnant hist ");
  Get2DHists()->AddAt(h2rem,2);
  TH2* h2Rem = sda::GetH2(l,1);

  TH2* h2FoverF0Clust = sdu::CloneHist(h2, " Raw"," Raw hist (F/F0 no cuts)");
  Get2DHists()->AddAt(h2FoverF0Clust,3);
  if(man) {
      TH2* h2FoverF0 = (TH2*)(GetImageAnalysis()->GetPedestals()->GetFoverF0Hist());
      for(int ix=1; ix<=h2i->GetNbinsX(); ++ix){
	     double x = h2i->GetXaxis()->GetBinCenter(ix);
	     for(int iy=1; iy<=h2i->GetNbinsY(); ++iy){
		    double y = h2i->GetYaxis()->GetBinCenter(iy);
		    double rc = h2In->GetBinContent(h2In->FindBin(x,y));
		    if(rc>1.e-7) h2i->SetBinContent  (ix,iy, rc);
		    rc = h2Rem->GetBinContent(h2Rem->FindBin(x,y));
		    if(rc>1.e-7) h2rem->SetBinContent(ix,iy, rc);
		    rc = h2FoverF0->GetBinContent(h2FoverF0->FindBin(x,y));
//		    if(rc>0.001)
		    h2FoverF0Clust->SetBinContent(ix,iy, rc); // Take negative too : Oct 3,2012
	    }
     }
  }
//  h2->SetMaximum(h2i->GetMaximum());
  h2rem->SetMaximum(h2i->GetMaximum());
//  h2FoverF0->SetMaximum(h2i->GetMaximum());
}

void SDClusterInfo::CreateProjHists(Int_t ind)
{
  TH2 *h2Clust = dynamic_cast<TH2 *>(Get2DHists()->At(ind)); // cluster or (F/F0/-1) hists
  TString stmp(GetName()), sopt, snam(gkProjHists);
  if(ind==3) {
	  sopt = "NoCuts";
	  stmp += sopt;
	  snam += sopt;
  }
  Int_t scaleProj = 1; // Keep similar scale as in TH2
  TList *l = sdu::BookAndFill1DHistsFromClusterHist(h2Clust, stmp.Data(), scaleProj);

  if(l) {
    l->SetName(snam.Data());
    GetList()->Add(l);
  }
}

Int_t SDClusterInfo::Compare(const TObject* obj) const
{
	// Use comparing only information from SDCluster now : Sep 29,2012
    const SDClusterInfo* clstChar = dynamic_cast<const SDClusterInfo *>(obj);
	const SDClusterBase *clst = clstChar->GetCluster();
	const SDClusterBase *clst2 = this->GetCluster();
	return clst2->Compare(clst);
}

void SDClusterInfo::Draw2DHists()
{
  TCanvas* c = sda::DrawListOfHist(Get2DHists(),2,2,4);
  TString snam("C 2Dhists");
  snam += GetImageAnalysis()->GetSignatureForCanvas();
  c->SetName(snam.Data());
  c->SetTitle(snam.Data());
  c->SetWindowSize(600,400);
  c->Update();
}

void SDClusterInfo::DrawProjHists(Int_t mode)
{
  TCanvas* c1 = new TCanvas("ctmp","ctmp", 600,400);
  c1->Divide(2,2);
  sdClusterChar clch = GetClstCharFromF0();
  TString optProx("L");

  TList *lh = GetProjHists(mode);
  TH1* hprox = sda::GetH1(lh,0);
  c1->cd(1);
  hprox->SetStats(kFALSE);
  sda::DrawHist(hprox,2,1,optProx.Data());
  hprox->SetMinimum(0.0);
  Double_t xahm = hprox->GetMaximum()/2., c=1.05;
  TLine* linex = new TLine(clch.xhm1,xahm, clch.xhm2,xahm);
  linex->SetLineColor(kRed);
  linex->Draw();
  TString stmp(Form(" Duration %5.2fms",clch.duration)); // Stay here
  TLatex *latx = new TLatex(clch.xhm1,c*xahm, stmp.Data());
  latx->SetTextColor(kRed);
  latx->Draw();

  TH1* hproy = sda::GetH1(lh,1);
  c1->cd(2);
  hproy->SetStats(kFALSE);
  sda::DrawHist(hproy,2,1,optProx.Data());
  hproy->SetMinimum(0.0);
  Double_t yahm = hproy->GetMaximum()/2.;
  TLine* liney = new TLine(clch.yhm1,yahm, clch.yhm2,yahm);
  liney->SetLineColor(kRed);
  liney->Draw();
  TLatex *laty = new TLatex(clch.yhm1,c*yahm, Form(" Size %5.3fum",clch.size));
  laty->SetTextColor(kRed);
  laty->Draw();

  TH1* hamp = sda::GetH1(lh,2);
  c1->cd(3);
  hamp->SetStats(kFALSE);
  hamp->SetTitle(Form(" #bar{Amp}=%6.3f max(Amp)=%9.3f ", clch.aveamp, clch.maxamp));
  sda::DrawHist(hamp,2);

  c1->cd(4);
  Int_t ind = mode==0?0:3;
  TH2* h2 = dynamic_cast<TH2 *>(Get2DHists()->At(ind));
  h2->Draw();

  TString snam("ClustProj");
  snam += GetImageAnalysis()->GetSignatureForCanvas();
  c1->SetName(snam.Data());
  c1->SetTitle(snam.Data());

  c1->Update();
}

Int_t SDClusterInfo::SearchTH1(Int_t proxy, Double_t sigma, Option_t* option, Double_t threshold, int pri)
{  // June 6,2013
   TH1* h1 = sda::GetH1(GetProjHists(),proxy);
   auto_ptr<TSpectrum> s(new TSpectrum(100));
   Int_t nclust = s->Search(h1,sigma,option,threshold);
   if(pri) {
	   printf("proxy %i sigma %5.2f opt %s th %5.4f \n", proxy, sigma, option, threshold);
	   s->Print();
	   h1->Draw();
   }
   return nclust;
}

Int_t SDClusterInfo::SearchTH2(Double_t sigma, Option_t* option, Double_t threshold, Int_t w, int pri)
{  // June 6,2013
   TH2* h2 = sda::GetH2(Get2DHists(), 0);
   auto_ptr<TSpectrum2> s2(new TSpectrum2(100));
   s2->SetAverageWindow(w);
   Int_t nclust = s2->Search(h2, sigma,option,threshold);
   if(pri) {
	   printf("h2 %s sigma %5.2f opt %s th %5.4f \n", h2->GetName(), sigma, option, threshold);
	   s2->Print();
	   TList *functions = h2->GetListOfFunctions();
	   TPolyMarker *pm = (TPolyMarker*)functions->FindObject("TPolyMarker");
	   pm->SetMarkerColor(kBlack);
	   h2->Draw();
    }
   return nclust;
}

Bool_t SDClusterInfo::SplitClusterByLineParallelYcoord(Double_t x0)
{  // June 6,2013
   TDataSet* dsOut;
   Double_t pars[1];
   pars[0] = x0;
   Bool_t key = SDClusterSplittingManager::SplitCluster(this, SDClusterSplittingManager::LineDelimiterX0, pars, dsOut);
   //   Bool_t key = SDClusterSplittingManager::SplitClusterByLineParallelYcoord(this, x0, dsOut);
   return key;
}

Bool_t SDClusterInfo::SplitClusterByPCA()
{
   SDSplittingAlgoWithTwoMaximumOnX spliiter("Splitter PCA");
   spliiter.SetInPutCluster(this);
   spliiter.MakeClusterSplitting();
   return kTRUE;
}

SDClusterScan* SDClusterInfo::GetClusterScan()
{
	// Jun 10,2013
	SDClusterScan *scan = dynamic_cast<SDClusterScan *>(sda::FindObjectByPartName(fList, sd::GetSdCommon()->leadNameScan));
	if(scan==0) {
		scan = new SDClusterScan("test"); // should be change
		Add(scan);
	}
	return scan;
}

Bool_t SDClusterInfo::ScanTest(Int_t nScan)
{
	// June 12,2013
	SDClusterScan *scan = GetClusterScan();
	scan->SetNscan(nScan);
    scan->Init(this);

    scan->Scan();

	return kTRUE;
}

Bool_t SDClusterInfo::Scan(const parsSdScan& pars)
{
	// June 21,2013
	Int_t progress = 0;
	GetClstCharFromF0().SetScan(1);
	GetClstCharFromZero().SetScan(1);
	SDClusterScan *scan = GetClusterScan();
    scan->CalculateScanPars(GetImageAnalysis()->GetParsScan());
    scan->Scan(progress);

	return kTRUE;
}
