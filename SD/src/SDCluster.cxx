/*
 * SDCluster.cpp
 *
 *  Created on: May 20, 2012
 *  Revised   : Nov 1;
 *      Author: Alexei Pavlinov
 */

#include "SDCluster.h"
#include "SDClusterInfo.h"

#include <TH2.h>
#include <TBrowser.h>
#include <TPolyLine.h>

SDClusterInfo* SDCluster::GetClusterInfo() const {
	return dynamic_cast<SDClusterInfo *> (GetParent());
}

void SDCluster::AddPixel(pixel &p) {
	// Adding pixel and calculate min(max) values
	// which are using in cluster finder
	sdClusterChar& clstChar = GetClusterInfo()->GetClstCharFromF0();

	if (GetNRows() == 0) {
		clstChar.colmin = clstChar.colmax = p.col;
		clstChar.rowmin = clstChar.rowmax = p.row;
		clstChar.maxamp = p.amp;
		fPolyLine = 0;
	} else {
		clstChar.colmin = clstChar.colmin > p.col ? p.col : clstChar.colmin;
		clstChar.colmax = clstChar.colmax < p.col ? p.col : clstChar.colmax;
		clstChar.rowmin = clstChar.rowmin > p.row ? p.row : clstChar.rowmin;
		clstChar.rowmax = clstChar.rowmax < p.row ? p.row : clstChar.rowmax;
		if(clstChar.maxamp < p.amp) clstChar.maxamp = p.amp;
	}
	AddAt(&p);
	clstChar.numpixels = GetClusterSize(); // Nov 2,2012
}

pixel* SDCluster::GetPixel(UInt_t ind)
{
  return GetTable(ind);
}

pixel*  SDCluster::GetPixel(UInt_t col, UInt_t row)
{
  for(UInt_t i=0; i<GetNRows(); ++i){
	pixel* px = GetTable(i);
	if(px->col==col && px->row==row) return px;
  }
  return 0;
}

void SDCluster::PrintClusterInfo(const int pri) const {
	cout << " Clust : " << GetName() << " nrows " << GetNRows() << endl;
	if (pri > 1)
		for (int i = 0; i < GetNRows(); i++)
			cout << (*GetTable(i));
	cout<<GetClusterInfo()->GetClstCharFromF0()<<endl;
}

Int_t SDCluster::Compare(const SDClusterBase *clb) const {
	// Method using for sorting in TList only
	// clb should be SDClusterBase type
	// Nov 16,2012
	const SDClusterBase *clst = dynamic_cast<const SDClusterBase *> (clb);
	int diff = this->GetClusterSize() - clst->GetClusterSize();
	diff = diff < 0 ? -1 : diff;
	diff = diff > 0 ? 1 : 0;
	return diff;
}

Bool_t SDCluster::IsNormalCluster() const
{
	return GetClusterInfo()->GetClstCharFromF0().IsNormalCluster();
}

TPolyLine* SDCluster::CreatePolyline()
{
	// Simple box : Nov 17,2012
	Int_t lineWidth = 2;
	Double_t x[5], y[5];
	sdClusterChar clch = GetClusterInfo()->GetClstCharFromF0();
	x[0] = x[1] = x[4] = Double_t(clch.colmin) - 0.5;
	x[2] = x[3] = Double_t(clch.colmax) + 0.5;
	y[0] = y[3] = y[4] = Double_t(clch.rowmin) - 0.5;
	y[1] = y[2] = Double_t(clch.rowmax) + 0.5;
	fPolyLine = new TPolyLine(5, x, y);
	fPolyLine->SetFillColor(0);
	fPolyLine->SetLineColor(1);
	fPolyLine->SetLineWidth(lineWidth);
	return fPolyLine;
}

ostream& operator<<(ostream &os, const pixel &p)
{
#ifndef NEIGHBORS
	return os<<" r "<<p.row<<" c "<<p.col<<" a "<<cout.width(6)<<p.amp<<endl;
#else
	return os<<" r "<<p.row<<" c "<<p.col<<" a "<<cout.width(6)<<p.amp<<" neighbor "<<p.neighbors<<endl;
#endif
}

TableClassImpl(SDCluster,pixel)
