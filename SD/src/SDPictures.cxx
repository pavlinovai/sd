/*
 * SDPictures.cpp
 *
 *  Created on: Nov 9, 2012
 *      Author: Alexei Pavlinov
 */

#include "SDPictures.h"
#include "SDAliases.h"
#include "SDUtils.h"
#include "SDManager.h"
#include "SDConfigurationParser.h"
#include "SDConfigurationInfo.h"
#include "SDPedestals.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TFile.h>
#include <TBrowser.h>
#include <TCanvas.h>
#include <TH2.h>
#include <TF1.h>
#include <TF2.h>
#include <TText.h>
#include <TLine.h>

ClassImp(SDPictures)

SDPictures::SDPictures(const char* dir) : SDObjectSet("SDPictures"),
fDir("/Users/pavlinov/wrk/sdoutput/NOV2012/"), fSDPool(new TDataSet("SDPool")), fSDMan(0)
{
	cout<<"<I> SDPictures::SDPictures("<<dir<<") is started \n";
    gROOT->GetListOfBrowsables()->Add(this);
	Add(fSDPool);
	if(strlen(dir)) fDir=dir;
}

SDPictures::~SDPictures()
{
	// TODO Auto-generated destructor stub
}

SDManager* SDPictures::ReadSdObject()
{
    // Users/pavlinov/wrk/sdoutput/NOV2012/4_ca100_red_channel
	fSDMan = dynamic_cast<SDManager *>(sda::ReadFirstObjectFromDirectory(fDir.Data()));
	fSDPool->Add(fSDMan);
	TString st =fSDMan->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsInput().name;
	SetName(Form("%s %s", GetName(), st.Data()));
	return fSDMan;
}
//
//        F0 P i c t u r e s
//
TCanvas* SDPictures::DrawF0PictOne()
{
	// Pict.1 - demonstrate the F0 zone selection
   if(fSDMan==0) return 0;
   SDPedestals *ped = fSDMan->GetPedestals();
   if(ped==0) return 0;

   TCanvas* c = sda::Canvas("cf0p1","cf0p1",20,20,600,400);
   c->Divide(3,1);

   TH1* hminChi2 = ped->CreateChi2List("good", 1.);
   c->cd(1);
   gStyle->SetOptStat(101);
   gStyle->SetOptFit(111);
   hminChi2->SetTitle(" best #chi^{2}/ndf");
   sda::DrawHist(hminChi2,2,1);
   Double_t ymax = hminChi2->GetMaximum();
   TText *at = new TText(0.7,ymax/2.,"a)");
   at->SetTextSize(0.15);
   at->Draw("same");

   TH1* hmaxChi2 = ped->CreateChi2List("bad", 2.5);
   hmaxChi2->SetTitle(" worst #chi^{2}/ndf");
   c->cd(2);
   gStyle->SetOptStat(101);
   gStyle->SetOptFit(111);
   sda::DrawHist(hmaxChi2,2,1);
   ymax = hmaxChi2->GetMaximum();
   TText *bt = new TText(0.7,ymax/2.,"b)");
   bt->SetTextSize(0.15);
   bt->Draw("same");

   TH1* hchi2 = sda::GetH1(ped->GetListAmplitudesFitQAHists(), 0);
   c->cd(3);
   gStyle->SetOptStat(1101);
   sda::DrawHist(hchi2,2,1);
   ymax = hchi2->GetMaximum();
   TText *ct = new TText(5.,ymax/2.,"c)");
   ct->SetTextSize(0.15);
   ct->Draw("same");

   c->Update();

   return c;
}

TCanvas* SDPictures::DrawF0PictTwo()
{
	// Pict.2 - parameters of F0
	   if(fSDMan==0) return 0;
   SDPedestals *ped = fSDMan->GetPedestals();
   if(ped==0) return 0;

   TCanvas* c = sda::Canvas("cf0p2","cf0p2",20,20,600,400);
   c->Divide(2,1);

   TH1* h = ped->GetHistWithLastPedFunction();
   c->cd(1);
   gStyle->SetOptStat(101);
   gStyle->SetOptFit(111);
//   hminChi2->SetTitle(" best #chi^{2}/ndf");
   sda::DrawHist(h,2,1);
   Double_t ymax = h->GetMaximum();
   TText *at = new TText(50.,ymax*0.1,"a)");
   at->SetTextSize(0.15);
   at->Draw("same");

   TH1* h2 = sda::GetH1(ped->GetListF0WorkHists(),10); // hidt for row #10
   c->cd(2);
   gStyle->SetOptStat(101);
   gStyle->SetOptFit(111);
//   hminChi2->SetTitle(" best #chi^{2}/ndf");
   sda::DrawHist(h2,2,1);
   ymax = h2->GetMaximum();
   TText *bt = new TText(50.,ymax*1.0,"b)");
   bt->SetTextSize(0.15);
   bt->Draw("same");

   c->Update();

   return c;
}

TCanvas* SDPictures::DrawF0PictThree()
{
	// Pict.3 - parameters of F0D
	   if(fSDMan==0) return 0;
   SDPedestals *ped = fSDMan->GetPedestals();
   if(ped==0) return 0;

   TCanvas* c = sda::Canvas("cf0p3","cf0p3",20,20,600,400);

   TH1* h = sda::GetH1(ped->GetListF0SumHists(), 5);
   h->SetTitle("A_{0}[i] as histogram - F_{0}(d); space bin; A_{0}[i]");
   h->SetStats(kFALSE);
   sda::DrawHist(h,2,1);

   TF1* f = sda::GetFunction(ped->GetHistWithLastPedFunction(),0);
   Double_t a0 = f->GetParameter(0);
   TLine* line = new TLine(-0.5,a0, 511.5,a0);
   line->SetLineColor(kRed);
   line->SetLineWidth(2);
   line->Draw("same");
   c->Update();

   return c;
}
