/*
 * sdClusterChar.cxx
 *
 *  Created on: Nov 16, 2012
 *      Author: Alexei Pavlinov
 */
#include "sdClusterChar.h"

void clusterType::Print()
{
  // To do:
}

sdClusterChar::sdClusterChar() : numpixels(0),
			 maxamp(-1.), aveamp(-1.), duration(-1.),size(-1.),
			 colmin(2048),colmax(0),rowmin(2048),rowmax(0),
			 xhm1(-1),xhm2(-1), yhm1(-1),yhm2(-1), type(0)
{;}

Bool_t sdClusterChar::IsNormalCluster() const
{
	clusterType clt;
	clt.type = type;
	return (clt.tdecomp.border==0);
}

ostream& operator<<(ostream &os, const sdClusterChar &c)
{
  os<<" numpixels "<<c.numpixels<<" maxamp "<<c.maxamp;
  os<<" aveamp "<<c.aveamp<<" duration "<<c.duration<<" size "<<c.size << endl;
  os<<" colmin " <<c.colmin<<" colmax "<<c.colmax;
  os<<" rowmin " <<c.rowmin<<" rowmax "<<c.rowmax;
  os<<" type "<< c.type;
  os<<" border "<<c.GetBorder()<<" scan "<<c.GetScan();
  return os;
}
