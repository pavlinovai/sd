/*
 * SDConfigurationParser->cpp
 *
 *  Created on: May 26, 2012
 *      Author: pavlinov
 */

#include "SDConfigurationParser.h"
#include "SDConfigurationInfo.h"
#include "SDTransformation.h"
#include <TSystem.h>
#include <TDOMParser.h>
#include <TXMLDocument.h>
#include <TXMLNode.h>
#include <TBrowser.h>
#include <TClass.h>
#include <TDataMember.h>
#include <TMethodCall.h>

#include <iostream>
#include <assert.h>

using namespace std;

ClassImp(SDConfigurationParser)

SDConfigurationParser::SDConfigurationParser(const char* name) :
	TDataSet(name) , fDebug(1)
{
	fParser = new TDOMParser();
	// Default name of configuration
//	SDConfigurationInfo* confDef = CreateDefaultConfiguration();
//	AddAt(confDef,0); // 0 index keeps default configuration
}

SDConfigurationParser::~SDConfigurationParser()
{
	delete fParser;
}

SDConfigurationInfo* SDConfigurationParser::GetConfigurationInfo(Int_t ind) {
	return dynamic_cast<SDConfigurationInfo *> (At(ind));
}

SDConfigurationInfo* SDConfigurationParser::GetLastConfigurationInfo() {
	return dynamic_cast<SDConfigurationInfo *>(Last());
}

void SDConfigurationParser::ParseFile(const char *name) {
    if(fDebug>=1) cout<<" DConfigurationParser::ParseFile "<<name<<" is started \n";
	Int_t cond = fParser->ParseFile(name);
	if (cond < 0) { // Zero is good
		cerr << fParser->GetParseCodeMessage(cond) << endl;
		cout << " SDConfigurationParser::ParseFile : cond " << cond << endl;
		assert(0);
	}

	TString stmp = name;
	stmp.ReplaceAll("/", "_");
	SDConfigurationInfo* confInfo = 0;
	if(GetListSize()==0) {
	  confInfo = new SDConfigurationInfo(stmp.Data());
	} else {
	  confInfo = new SDConfigurationInfo(*GetConfigurationInfo(0));
	}
    confInfo->SetTitle(name);
	confInfo->SetName(stmp.Data());
	Add(confInfo);
	ParseElements(confInfo);
}

void SDConfigurationParser::ParseElements(SDConfigurationInfo* ci)
{
	// 2013: June 13,29-30;
	TXMLNode* node = fParser->GetXMLDocument()->GetRootNode();
	node = node->GetChildren(); // First children

	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode)
			continue;
		if(strcmp(node->GetNodeName(),"input")==0) {
			ParseInput(node, ci);
		} if(strcmp(node->GetNodeName(),"fillhists")==0) {
				ParseFillHists(node, ci);
		} if(strcmp(node->GetNodeName(),"threads")==0) {
				ParseThreads(node, ci);
		} if(strcmp(node->GetNodeName(),"transformation")==0) {
				ParseTransformation(node, ci);
		} if(strcmp(node->GetNodeName(),"pedestals")==0) {
				ParsePedestals(node, ci);
		} if(strcmp(node->GetNodeName(),"clusterfinder")==0) {
				ParseClusterFinder(node, ci);
		} if(strcmp(node->GetNodeName(),"scan")==0) {
				ParseParsScan(node, ci);
		} if(strcmp(node->GetNodeName(),"output")==0) {
			ParseOutput(node, ci);
		}
	}
	CheckNameOfOutputFile();
}

void  SDConfigurationParser::ParseInput(TXMLNode* node, SDConfigurationInfo* ci)
{
	node = node->GetChildren();
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode)
			continue;
		if(strcmp(node->GetNodeName(),"dir")==0) {
            TString stmp(node->GetText());
			GetDirectoryName(stmp,"$(SDTIFF)");
			ci->GetParsInput().dir = stmp;
		} else if(strcmp(node->GetNodeName(),"name")==0) {
			ci->GetParsInput().name = node->GetText();
		}
    }
	if(fDebug>=2) cout<<" ** input \n"<<ci->GetParsInput()<<endl;
}

void  SDConfigurationParser::ParseFillHists(TXMLNode* node, SDConfigurationInfo* ci)
{
	node = node->GetChildren();
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode)
			continue;
		TString stmp(node->GetText());
		if(strcmp(node->GetNodeName(),"scale")==0) {
			CheckFloat(stmp);
			ci->GetParsFillHists().scale = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"ndiv")==0) {
			CheckDigit(stmp);
			ci->GetParsFillHists().ndiv = stmp.Atoi();
		}
    }
	if(fDebug>=2) cout<<" ** fillhists \n"<<ci->GetParsFillHists()<<endl;
}

void SDConfigurationParser::ParseThreads(TXMLNode* node, SDConfigurationInfo* ci)
{
	// June 29,2013 -> similar to ParseParsScan
	parsSdThreads& pars = ci->GetParsThreads();
	TClass *cl = pars.Class();
	TList  *ld = cl->GetListOfDataMembers();
    Int_t n = ld->GetSize();

	node = node->GetChildren();
	TString stmp;
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode) continue;
        n = 1; // used only one data member
		for(Int_t i=0; i<n; ++i){
			TDataMember *dm = dynamic_cast<TDataMember *>(ld->At(i));
			stmp = dm->GetName();
			if(stmp.Contains(node->GetNodeName(),TString::kIgnoreCase)) {
				TMethodCall *setter = dm->SetterMethod(cl);
				setter->Execute(&pars,node->GetText());
			}
		}
	}
}

void SDConfigurationParser::ParseTransformation(TXMLNode* node, SDConfigurationInfo* ci)
{
	// June 29,2013 -> similar to ParseParsScan
	parsTransformation& ptrans = ci->GetParsTranformation();
	TClass *cl = ptrans.Class();
	TList  *ld = cl->GetListOfDataMembers();
    Int_t n = ld->GetSize() - 1;

	node = node->GetChildren();
	TString stmp;
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode) continue;
        n = 3; // used only three data members
		for(Int_t i=0; i<n; ++i){
			TDataMember *dm = dynamic_cast<TDataMember *>(ld->At(i));
			stmp = dm->GetName();
			if(stmp.Contains(node->GetNodeName(),TString::kIgnoreCase)) {
				TMethodCall *setter = dm->SetterMethod(cl);
				setter->Execute(&ptrans,node->GetText());
				break;
			}
		}
	}
}

void  SDConfigurationParser::ParsePedestals(TXMLNode* node, SDConfigurationInfo* ci)
{
	node = node->GetChildren();
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode)
			continue;
		TString stmp(node->GetText());
		if(strcmp(node->GetNodeName(),"set")==0) {
			CheckDigit(stmp);
			ci->GetParsPedestals().set = stmp.Atoi();
		} else if(strcmp(node->GetNodeName(),"nsig")==0) {
			CheckFloat(stmp);
			ci->GetParsPedestals().nsig = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"chi2cut")==0) {
			CheckFloat(stmp);
			ci->GetParsPedestals().chi2cut = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"smooth")==0) {
			ci->GetParsPedestals().smooth = stmp;
		} else if(strcmp(node->GetNodeName(),"algo")==0) {
			ci->GetParsPedestals().algo = stmp;
		} else if(strcmp(node->GetNodeName(),"sigcut")==0) {
			CheckFloat(stmp);
			ci->GetParsPedestals().sigcut = stmp.Atof();
		}
    }
	if(ci->GetParsPedestals().algo=="idl") ci->GetParsPedestals().smooth = "mean1x8"; // Oct 31,2012
	if(fDebug>=2) cout<<" ** pedestals \n"<<ci->GetParsPedestals()<<endl;
}

void SDConfigurationParser::ParseClusterFinder(TXMLNode* node, SDConfigurationInfo* ci)
{
	node = node->GetChildren();
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode)
			continue;
		TString stmp(node->GetText());
		if(strcmp(node->GetNodeName(),"name")==0) {
			ci->GetParsClusterFinder().name = stmp;
		} else if(strcmp(node->GetNodeName(),"seed")==0) {
			CheckFloat(stmp);
			ci->GetParsClusterFinder().seed = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"timescale")==0) {
			CheckFloat(stmp);
			ci->GetParsClusterFinder().timescale = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"spacescale")==0) {
			CheckFloat(stmp);
			ci->GetParsClusterFinder().spacescale = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"minpixels")==0) {
			CheckDigit(stmp);
			ci->GetParsClusterFinder().minpixels = stmp.Atoi();
		} else if(strcmp(node->GetNodeName(),"minsize")==0) {
			CheckFloat(stmp);
			ci->GetParsClusterFinder().minsize = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"minduration")==0) {
			CheckFloat(stmp);
			ci->GetParsClusterFinder().minduration = stmp.Atof();
		} else if(strcmp(node->GetNodeName(),"drawing")==0) {
			CheckDigit(stmp);
			ci->GetParsClusterFinder().drawing = stmp.Atoi();
		}
    }
	ci->GetParsClusterFinder().CalculateSizesInPixels();
	if(fDebug>=2) cout<<" ** cluster finder \n"<<ci->GetParsClusterFinder()<<endl;
}

void  SDConfigurationParser::ParseOutput(TXMLNode* node, SDConfigurationInfo* ci)
{
	node = node->GetChildren();
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode)
			continue;
		if(strcmp(node->GetNodeName(),"dir")==0) {
            TString stmp(node->GetText());
			GetDirectoryName(stmp,"$(SDOUTPUT)");
			ci->GetParsOutput().dir = stmp;
		} else if(strcmp(node->GetNodeName(),"name")==0) {
			ci->GetParsOutput().name = node->GetText();
		}
    }
	if(fDebug>=2) cout<<" ** Output \n"<<ci->GetParsOutput()<<endl;
}

void SDConfigurationParser::ParseParsScan(TXMLNode* node, SDConfigurationInfo* ci)
{
	// June 13,2013 : common method
	// Would works if you change the class.
	parsSdScan& pars = ci->GetParsSdScan();
	TClass *cl = pars.Class();
	TList  *ld = cl->GetListOfDataMembers();

	node = node->GetChildren();
	TString stmp;
	for (; node; node = node->GetNextNode()) {
		if (node->GetNodeType() != TXMLNode::kXMLElementNode) continue;
		for(Int_t i=0; i<ld->GetSize(); ++i){
			TDataMember *dm = dynamic_cast<TDataMember *>(ld->At(i));
			stmp = dm->GetName();
			if(stmp.Contains(node->GetNodeName(),TString::kIgnoreCase)) {
				TMethodCall *setter = dm->SetterMethod(cl);
				setter->Execute(&pars,node->GetText());
				break;
			}
		}
	}
}

void SDConfigurationParser::GetDirectoryName(TString &stmp, TString envString)
{
	if(fDebug>=2) cout<<" getDirectoryName() : "<<stmp<<" env. "<<envString<<endl;
	if(stmp == envString) {
// Returns kFALSE in case of success or kTRUE in case of error.
// http://root.cern.ch/root/html534/TUnixSystem.html#TUnixSystem:ExpandPathName
        Bool_t key = gSystem->ExpandPathName(stmp);
    	if(key == kTRUE) {
    		envString.ReplaceAll("$(","");
    		envString.ReplaceAll(")","");
    		cout<<"<E> Environment variable "<<envString<<endl;
    		cout<<"    has wrong directory name "<<gSystem->Getenv(envString.Data())<<endl;
    		assert(0);
    	}
    }
    TString OS(gSystem->Getenv("OS"));
    TString osSymb="\\"; // for Windows
    if(OS.Contains("_NT")==0) osSymb = "/"; // for Unix
    if(stmp[stmp.Length()-1] != osSymb[0]) stmp += osSymb;
}

void SDConfigurationParser::CheckNameOfOutputFile()
{
	SDConfigurationInfo* ci = GetLastConfigurationInfo();

	TString stmp(ci->GetParsOutput().name);
	stmp.ToLower();
	if(stmp.Contains(".root")) return;
	if(stmp.Contains("no"))    return; // No saving in this case
	if(stmp.Contains("auto"))  return; // Auto generation of output file name

	stmp = ci->GetParsInput().name;
	stmp.ToLower();
	stmp = stmp(0,stmp.Index(".tif"));
	ci->GetParsOutput().name = stmp;
	if(fDebug>=2) cout<<"Name of output file is "<<stmp<<endl;
}

Bool_t SDConfigurationParser::SaveToRootFile()
{
	SDConfigurationInfo* ci = GetLastConfigurationInfo();
	if(ci->GetParsOutput().name == "no") return kFALSE;
	return kTRUE;
}

void SDConfigurationParser::CheckFloat(TString &s)
{
	if(s.IsFloat() == kFALSE) {
		cerr<<s<<" is not float ";
		assert(0);
	}
}

void SDConfigurationParser::CheckDigit(TString &s)
{
	if(s.IsDigit() == kFALSE) {
		cerr<<s<<" is not float ";
		assert(0);
	}
}

void SDConfigurationParser::PrintInfoXml(int key) {
	TXMLNode* node = fParser->GetXMLDocument()->GetRootNode();
	SDConfigurationInfo *ci = GetLastConfigurationInfo();
	cout << " xml file : " << ci->GetTitle() << endl;
	cout << " Root node : name " << node->GetNodeName() << endl;
	if (key > 0) {
		Int_t inode = 0;
		node = node->GetChildren();
		while ((node = node->GetNextNode())) {
			if (node->GetNodeType() == TXMLNode::kXMLElementNode) { // Element Node
				cout << inode++ << " " << node->GetNodeName();
				cout << " " << node->GetText() << endl;
			}
		}
		cout << endl;
	}
}

SDConfigurationInfo* SDConfigurationParser::CreateDefaultConfiguration(const char* defName)
{
	// Create default configuration object SDConfigurationInfo
	SDConfigurationParser* parser = new SDConfigurationParser("dparser");
	parser->ParseFile(defName);

	SDConfigurationInfo *defInfo = new SDConfigurationInfo(*parser->GetConfigurationInfo());
	defInfo->SetName("DefaultConfiguration");
	delete parser;
	return defInfo; // You should care about this object
}
