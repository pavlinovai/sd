/*
 * SDPedestals.cpp
 *
 *  Created on: May 14, 2012
 *      2013: Apr 02;
 *      Author: Alexei Pavlinov
 */
#include "SDManager.h"
#include "SDConfigurationParser.h"
#include "SDPedestals.h"
#include "sdpixel.h"
#include "SDUtils.h"

#include <TSystem.h>
#include <TH2.h>
#include <TProfile.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TThread.h>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/WrappedMultiTF1.h"

#include "assert.h"

const char* SDPedestals::gkNameWork = "WorkHists";
const char* SDPedestals::gkNameAmp  = "AmpHists";
const char* SDPedestals::gkNameSum  = "Sum";
const char* SDPedestals::gkNameProj = "ProjOfSum";
const char* SDPedestals::gkNameF0   = "F0";
const char* SDPedestals::gkSplittedHists = "SplittedHists";

SDPedestals::SDPedestals() : SDObjectSet(),
		fNworkHists(0), fPars(), fBexp(0), fSigF0Unit(0.),
		fF0Y(0), fF0YC(0), fF0Max(0),
		fMaxGoodBGCoord(0), fMaxChi2(0.),fGoodBgCoords(), fEmptyBgCoords(),
		fFitOpt(""), fSignature(""),fFitter(0)
{ }

SDPedestals::SDPedestals(const char* name) : SDObjectSet(name),
		fNworkHists(4), fPars(),fBexp(0), fSigF0Unit(0.),
		fF0Y(0), fF0YC(0),fF0Max(0),
        fMaxGoodBGCoord(5), fMaxChi2(0.), fGoodBgCoords(), fEmptyBgCoords(),
        fFitOpt("RQ0+"), fSignature(""), fFitter(0)
{
	// fFitOptAdd : "RQ0+" - no drawing: "RQ+" - add function and draw
	fFitter = SDPedestals::InitNewFitter(); // Initialize minuit2

	if (strlen(name) == 0)
		SetName("Pedestals");

	TList *l = new TList;
	l->SetName(gkNameWork);
	GetList()->Add(l);

	l = new TList;
	l->SetName(gkNameAmp);
	GetList()->Add(l);
    // June 27,2012
	const char* tit[]={"All","WithCuts","Fit QA"};
	Int_t is=sizeof(tit)/sizeof(char*);
	for(Int_t i=0; i<is; ++i) {
	  TList* ll = new TList; // list of all amp.hists
	  ll->SetName(Form("%s %s",gkNameAmp,tit[i]));
	  l->Add(ll);
	}
    BookFitQAHists(GetListAmplitudesFitQAHists());
}

SDPedestals::~SDPedestals()
{
	// TODO Auto-generated destructor stub
}

TString SDPedestals::GetSignature()
{
	// Mar 11, 2013;
	if(fSignature.Length()) return fSignature;

	if(fPars.algo.Length()) fSignature += (fPars.algo + " ");
    if(fPars.smooth.Length()) fSignature += fPars.smooth;
	fSignature += Form(" Nsig%3.1f",fPars.nsig);
	if(GetSigF0Unit()>0.0) fSignature += Form("(%4.3f) ",GetSigF0Unit() * fPars.nsig);

	return fSignature;
}

TF1* SDPedestals::GetPedestalFun(int indh)
{
	// indh should be 0,2,4
	if((indh==0||indh==2||indh==4)==kFALSE) {
	  std::cout<<" SDPedestals::GetPedestalFun indh "<<indh;
	  std::cout<<" => should be 0,2 or 4 \n";
	  assert(0);
	}

	TList *lsum = GetListSummaryHists();
	if (lsum == 0) return 0;
	TH1* h1 = dynamic_cast<TH1 *>(lsum->At(indh));
	return sda::GetFunction(h1);
}

TF1* SDPedestals::GetPedestalRmsFun(int indh)
{
	// indh should be 1,3,5
	if((indh==1||indh==3||indh==5)==kFALSE) {
	 std::cout<<" SDPedestals::GetPedestalRmsFun indh "<<indh;
	 std::cout<<" => should be 1,3 or 5 \n";
	  assert(0);
	}
	TList *lsum = GetListSummaryHists();
	if (lsum == 0) return 0;
	TH1* h1 = dynamic_cast<TH1 *>(lsum->At(indh));
	return sda::GetFunction(h1);
}

void SDPedestals::GetPedestalFunctions(int set, TF1 *funs[2])
{
   if(set <0 || set>2) {
		 std::cout<<" SDPedestals::GetPedestalFunctions set "<<set;
		 std::cout<<" => should be 0,1,2 \n";
		 assert(0);
   }
   funs[0] = GetPedestalFun(2*set);
   funs[1] = GetPedestalRmsFun(2*set+1);
}

TH1* SDPedestals::GetHistWithLastPedFunction()
{
	TList *lsum = GetListSummaryHists();
	if (lsum == 0) return 0;

	Int_t indh = 4;
	if(lsum->GetSize()>6) indh = lsum->LastIndex();
	return sda::GetH1(lsum,indh);
}

SDPedestals& SDPedestals::SetInput2DHist(TH2* h2raw /*, UInt_t set*/)
{
	// Oct 14, 2012
	GetListWorkHists()->AddAt(h2raw, 0);
	parsPedestals* pp = sdu::GetParsPedestals(this);

	Int_t ic = 0, reset = 1;
	TString sn, st, stmp;
	if (pp->algo.Contains("idl", TString::kIgnoreCase)) {
		sn = Form("%2.2i_ImgTcorrIdl", ++ic);
		st = Form("%s with time(Bleach) correction : idl", h2raw->GetTitle());
		TH2* hImgTcor = sdu::CloneHist(h2raw, sn.Data(), st.Data(), reset);
		GetListWorkHists()->AddAt(hImgTcor, ic);

		stmp = pp->smooth;
		sn = Form("%2.2i_ImgTcorrIdl%s", ++ic, stmp.Data());
		st = Form("%s with time(Bleach) corr.+smoothing %s", h2raw->GetTitle(),
				stmp.Data());
		TH2* hImgTcorSmooth = sdu::CloneHist(h2raw, sn.Data(), st.Data(), reset);
		GetListWorkHists()->AddAt(hImgTcorSmooth, ic);

		sn = Form("%2.2i_hFOverF0Idl", ++ic);
		st = Form("%s F/F0 %s : idl ", h2raw->GetTitle(), stmp.Data());
		TH2* hFOverF0 = sdu::CloneHist(h2raw, sn.Data(), st.Data(), reset);
		GetListWorkHists()->AddAt(hFOverF0, ic);

		sn = Form("%2.2i_hFOverF0Idl-1Cut", ++ic);
		st = Form("%s F/F0-1 %s Cut : idl", h2raw->GetTitle(), stmp.Data());
		TH2* hFOverF0MinusOneCut = sdu::CloneHist(h2raw, sn.Data(), st.Data(),
				reset);
		GetListWorkHists()->AddAt(hFOverF0MinusOneCut, ic); // for cluster finder
		fNworkHists = 5;
	} else if (pp->algo.Contains("sd", TString::kIgnoreCase)) {
		sn = Form("%2.2i_hFOverF0Sd", ++ic);
		st = Form("%s F/F0 SD algoritmh", h2raw->GetTitle());
		TH2* hFOverF0 = sdu::CloneHist(h2raw, sn.Data(), st.Data(), reset);
		GetListWorkHists()->AddAt(hFOverF0, ic);

		stmp = pp->smooth.Data();
		sn = Form("%2.2i_hFOverF0Sd%s", ++ic, stmp.Data());
		st = Form("%s F/F0 SD algoritmh : smooth %s", h2raw->GetTitle(), stmp.Data());
		TH2* hFOverF0Smooth = sdu::CloneHist(h2raw, sn.Data(), st.Data(), reset);
		GetListWorkHists()->AddAt(hFOverF0Smooth, ic);

		sn = Form("%2.2i_hFOverF0Sd%s-1Cut", ++ic, stmp.Data());
		st = Form("%s F/F0-1 Cut SD algoritmh : smooth %s", h2raw->GetTitle(), stmp.Data());
		TH2* h3 = sdu::CloneHist(h2raw, sn.Data(), st.Data(), reset);
		GetListWorkHists()->AddAt(h3, ic);
		fNworkHists = 4;
	}
	return (*this);
}

TList* SDPedestals::BookSummaryHists()
{
	TH1::AddDirectory(1);
	TH1 *hg=0;

	Int_t ih = 0, nh = GetListAmplitudesAllHists()->GetSize();
	double xmi=-0.5, xma = double(nh)-0.5;
	TString snam(Form("%2.2i_MeanVsTimeBin", ih++));
	TString stit(" #bar{A} vs time bin; time bin; #bar{A}");
	new TH1F(snam.Data(), stit.Data(), nh,xmi,xma);
	snam = Form("%2.2i_RmsVsTimeBin", ih++);
	stit = " #sigma vs time bin; time bin; #sigma ";
	new TH1F(snam.Data(), stit.Data(), nh,xmi,xma);

	snam = Form("%2.2i_MeanFitVsTimeBin", ih++);
	stit = " #bar{A} from fit vs time bin; time bin; #bar{A}";
	hg = new TH1F(snam.Data(), stit.Data(), nh, xmi,xma);
	hg->SetLineColor(kBlue);
	snam = Form("%2.2i_RmsFitVsTimeBin", ih++);
	stit = " #sigma from fit vs time bin; time bin; #sigma";
	hg = new TH1F(snam.Data(), stit.Data(), nh, xmi,xma);
	hg->SetLineColor(kBlue);

	snam = Form("%2.2i_MeanFitVsTimeBinWithCuts", ih++);
	stit = " #bar{A} from fit vs time bin with cuts; time bin; #bar{A} from fit";
	hg = new TH1F(snam.Data(), stit.Data(), nh, xmi,xma);
	hg->SetLineColor(kYellow);
	snam = Form("%2.2i_RmsFitVsTimeBinWithCuts", ih++);
	stit = " #sigma from fit vs time bin with cuts; time bin; #sigma from fit";
	hg = new TH1F(snam.Data(), stit.Data(), nh, xmi,xma);
	hg->SetLineColor(kYellow);

	TList *l = sda::moveHistsToList(gkNameSum);
//	GetList()->Add(l);

	TH1::AddDirectory(0);
	return l;
}

TList* SDPedestals::BookF0Hists()
{
	// Jun 27,2012
	TList* l = 0;
	TH2* himg = dynamic_cast<TH2 *>(GetListWorkHists()->At(0));
	if(himg == 0) return l;
	TH1::AddDirectory(0);

    l = new TList;
    l->SetName(gkNameF0);
    GetList()->Add(l);
    TList* lf0 = new TList;
    lf0->SetName(Form("%s ProY",gkNameF0));
    l->Add(lf0);

	TAxis* xax = himg->GetXaxis();
    TAxis* yax = himg->GetYaxis();
	for (int iy = 1; iy<=yax->GetNbins(); ++iy) {
	  TString snam(Form("SpaceBin%i",iy));
	  TString stit(Form("Space bin%i; time bin; Amp",iy));
	  TH1F* h = new TH1F(snam.Data(), stit.Data(), xax->GetNbins(), xax->GetXmin(), xax->GetXmax());
	  lf0->Add(h);
	}
    //
	// Sum
	//
    TList* lf0sum = new TList;
    lf0sum->SetName(Form("%s Sum",gkNameF0)); // keep for comparing
    l->Add(lf0sum);

    Int_t nx = himg->GetNbinsX(), ny = himg->GetNbinsY(), ih=0;
    //
    //    X axis - time
    //
	TH1D* hdtemp = himg->ProjectionX(Form("%2.2i_proX",ih),1,ny,"");
	hdtemp->Scale(1./ny);
	hdtemp->SetMinimum(0.0);
	lf0sum->AddAt(hdtemp,ih++);

	TH1F* hf0x = new TH1F(Form("%2.2i_F0X",ih)," F0X as histogram; time bin; F0(x)",
			xax->GetNbins(), xax->GetXmin(), xax->GetXmax());
	hf0x->SetMinimum(0.0);
	lf0sum->AddAt(hf0x,ih++);

	TH1F* hratVsX  = new TH1F(Form("%2.2i_prox/F0(x)VsX",ih)," ratio prox(Norm) / F0(x) vs x(time bin);time bin; R ",
			xax->GetNbins(), xax->GetXmin(), xax->GetXmax());
	hratVsX->SetMinimum(0.0);
	lf0sum->AddAt(hratVsX,ih++);

	TH1F* hrat1dx = new TH1F(Form("%2.2i_prox/F0(x)",ih), " proX/F0(X) - 1d ", 120, 0.8, 1.4);
	lf0sum->AddAt(hrat1dx,ih++);
    //
    //    Y axis - space
    //
	hdtemp = himg->ProjectionY(Form("%2.2i_proY",ih),1,nx,"");
	hdtemp->Scale(1./nx);
	hdtemp->SetMinimum(0.0);
	lf0sum->AddAt(hdtemp,ih++);

	TH1F* hf0y = new TH1F(Form("%2.2i_F0Y",ih)," F0Y as histogram; space bin; F0(y)",
			yax->GetNbins(), yax->GetXmin(), yax->GetXmax());
	hf0y->SetMinimum(0.0);
	lf0sum->AddAt(hf0y,ih++);

	TH1F* hf0yc = new TH1F(Form("%2.2i_F0YCenter",ih)," F0Y center as histogram; space bin; F0(y)",
			yax->GetNbins(), yax->GetXmin(), yax->GetXmax());
	hf0yc->SetMinimum(0.0);
	lf0sum->AddAt(hf0yc,ih++);

	TH1F* hratVsY  = new TH1F(Form("%2.2i_proy/F0(y)VsY",ih)," ratio proY(Norm) / F0(y) center vs y(space bin); space bin; r ",
			yax->GetNbins(), yax->GetXmin(), yax->GetXmax());
	hratVsY->SetMinimum(0.0);
	lf0sum->AddAt(hratVsY,ih++);

	TH1F* hrat1dy = new TH1F(Form("%2.2i_proy/F0(y) center",ih)," proY/F0(y) center - 1d ", 120, 0.8, 1.4);
	lf0sum->AddAt(hrat1dy,ih++);

	TH1F* hrat2d = new TH1F(Form("%2.2i_F(x,y)/F0(x,y)",ih)," F(x,y)/F0(x,y) - 2d", 200, 0.0, 4.0);
	lf0sum->AddAt(hrat2d,ih++);
    // Take into account only time bins for F0 calculation.
	// We will make estimation of sigma in relative F0 unit
	TH1F* hrat2dF0 = new TH1F(Form("%2.2i_F(x,y)/F0(x,y)FromF0",ih)," F(x,y)/F0(x,y) from F0 - 2d", 200, 0.0, 4.0);
	lf0sum->AddAt(hrat2dF0,ih++);
	TH1F* hRmsFoverF0vsBinx = new TH1F(Form("%2.2i_RmsF(x,y)/F0(x,y)FromF0vsBinX",ih), "RMS ofF(x,y)/F0(x,y) vs binX from F0",
			2048, -0.5, 2047.5);
	lf0sum->AddAt(hRmsFoverF0vsBinx, ih++);
    //
	// QA of F0(y) calculations
	//
    TList* lf0qa = new TList;
    lf0qa->SetName(Form("%s QA",gkNameF0));
    l->Add(lf0qa);

    ih=0;
	TH1F* hchi2 = new TH1F(Form("%2.2i_Chi2/ndfF0(y)",ih)," #chi^{2}/ndf - QA F0(y); #chi^{2}/ndf; N",50,0.0,5.);
	lf0qa->AddAt(hchi2,ih++);

	TH1F* hrelErr = new TH1F(Form("%2.2i_RelErrfF0(y)",ih),"A_{0} relative error (%) - QA F0(y); #delta A/A %; N",50,0.0,5.);
	lf0qa->AddAt(hrelErr,ih++);

    //
	// Calculation of sigma (standard deviation) from F0 after smoothing
	//
	TList* lf0Smooth = new TList;
	lf0Smooth->SetName(Form("%s Smooth", gkNameF0));
	l->Add(lf0Smooth);

	ih = 0;
	TH1F* h = new TH1F(Form("%2.2i_F(x,y)/F0(x,y)Smooth",ih)," F(x,y)/F0(x,y) after smooth - 2d", 100, 0.7, 1.7);
	lf0Smooth->AddAt(h,ih++);

    // Take into account only time bins for F0 calculation.
	// We will make estimation of sigma in relative F0 unit for F0 after smmothing
	h = new TH1F(Form("%2.2i_F(x,y)/F0(x,y)Smooth_F0",ih)," F(x,y)/F0(x,y) after smooth from F0 - 2d", 100, 0.7, 1.7);
	lf0Smooth->AddAt(h,ih++);

	h = new TH1F(Form("%2.2i_RmsF(x,y)/F0(x,y)Smooth_F0vsBinX",ih), "RMS of F(x,y)/F0(x,y) smooth vs binX from F0",
			2048, -0.5, 2047.5);
	lf0Smooth->AddAt(h,ih++);

	return lf0;
}

TList* SDPedestals::BookFitQAHists(TList *lqa)
{
	TH1::AddDirectory(0);

  	Int_t ih=0;
	TString snam = Form("%2.2i_Chi2", ih++);
	TH1 *h = new TH1F(snam.Data(), " #chi^{2}/ndf - amp fit by gaus; #chi^{2}/ndf; N", 100, 0.0, 10.);
    lqa->Add(h);

	snam = Form("%2.2i_Sigma", ih++);
	h = new TH1F(snam.Data(), " #sigma - amp fit by gaus; #sigma ; N", 100, 0.0, 0.2);
    lqa->Add(h);

    snam = Form("%2.2i_N(gaus)o/Nentry", ih++);
	h = new TH1F(snam.Data(), "N(gaus)/N(entries) - fit by gaus; N(gaus)/N(entries); N", 60, 0.5, 1.1);
    lqa->Add(h);

    return lqa;
}

bool SDPedestals::FillInitialAmplitudesDistribution() {
	// Oct 11,2012
	TH2* h2 = sda::GetH2(GetListWorkHists(),0);
	if (h2 == 0)
		return kFALSE;
	TList *lall = GetListAmplitudesAllHists();
	if (lall == 0)
		return kFALSE;

	return FillInitialAmplitudesDistributionFrom2dHist(h2, lall);
}

bool SDPedestals::FillInitialAmplitudesDistributionFrom2dHist(TH2* h2, TList *lall)
{
  TH1::AddDirectory(0);
  Int_t ih = 0;
  // cycle on each y slices (time)
  for (int ix = 1; ix<=h2->GetNbinsX(); ++ix) {
	TString stit(Form("proy%i; Amp [av]; N ",ix));
    TH1D *hd = h2->ProjectionY(Form("proy%i",ix),ix,ix);
	TH1* h1 = FillAndFit1dAmplitudeDistribution(hd, fFitOpt.Data());
    if(h1) {
    	lall->Add(h1);
    	++ih;
    }
    delete hd;
  }
  if(lall->GetSize()) return kTRUE;
  else                return kFALSE;
}

TH1* SDPedestals::FillAndFit1dAmplitudeDistribution(TH1D *hd, const char* fitOpt)
{
	TH1 *h=0;
	if (hd) {
		TString snam(hd->GetName());
		snam += " amp";
		int nch = 50;
		double ymax = hd->GetMaximum();
		if(ymax<1.) ymax = 1.;
		else ymax = TMath::CeilNint(ymax);
		TString stit(hd->GetTitle());
		stit += "; Amp [au] ; N";
		h = new TH1F(snam.Data(), stit.Data(), nch, 0.0, ymax);
		for (int binx = 1; binx <= hd->GetNbinsX(); ++binx) {
			h->Fill(hd->GetBinContent(binx));
		}
		// fit by Gaus
		int imax = h->GetMaximumBin(), nsig=4;
		Double_t xmax = h->GetBinLowEdge(imax) + h->GetBinWidth(imax) + nsig*h->GetRMS();
		Double_t xmin = h->GetBinLowEdge(imax) - nsig*h->GetRMS();
		xmin = xmin<0.0?0.0:xmin;
		TF1* gi = sda::gausi(h->GetName(), xmin, xmax, (TH1F*)h);
		h->Fit(gi, fitOpt," ");
	}
	h->SetOption("e"); // for drawing
	return h;
}

void SDPedestals::FillAmpDistWithCutsQaAndSum()
{
  TList *lall = GetListAmplitudesAllHists();
	if (lall == 0)
		return;
    TList* lcuts = GetListAmplitudesCutsHists();

	TList *lsum = GetListSummaryHists();
    if(lsum) sda::ResetListOfHists(lsum);
    else {
    	lsum = BookSummaryHists();
    	GetListAmplitudesHists()->Add(lsum);
    }

	TList* lqa = GetListAmplitudesFitQAHists();

	TF1* gi = 0;
	TH1* hm = dynamic_cast<TH1 *> (lsum->At(0));
	TH1* hg = dynamic_cast<TH1 *> (lsum->At(2));
	TH1* hgcut = dynamic_cast<TH1 *> (lsum->At(4));
	TH1* hms = dynamic_cast<TH1 *> (lsum->At(1));
	TH1* hgs = dynamic_cast<TH1 *> (lsum->At(3));
	TH1* hgscut = dynamic_cast<TH1 *> (lsum->At(5));
	for (int itime = 0; itime < lall->GetSize(); ++itime) {
		TH1* h1 = dynamic_cast<TH1 *> (lall->At(itime));
		if (h1) {
			Double_t y=h1->GetMean(), ey = h1->GetMeanError();
			Double_t sig=h1->GetRMS(), esig=h1->GetRMSError();
			hm->SetBinContent(itime + 1, y);
			hm->SetBinError(itime + 1, ey);
			hms->SetBinContent(itime + 1, sig);
			hms->SetBinError(itime + 1, esig);
			gi = dynamic_cast<TF1 *> (h1->GetListOfFunctions()->At(0));
			if (gi) {
				hg->SetBinContent(itime + 1, gi->GetParameter(1));
				hg->SetBinError(itime + 1, gi->GetParError(1));
				hgs->SetBinContent(itime + 1, gi->GetParameter(2));
				hgs->SetBinError(itime + 1, gi->GetParError(2));
				int ndf = gi->GetNDF();
				if (ndf > 0) {
					sda::FillH1(lqa, 0, gi->GetChisquare() / ndf);
					sda::FillH1(lqa, 1, gi->GetParameter(2));
					sda::FillH1(lqa, 2, gi->GetParameter(0)/h1->GetEntries());
					if (gi->GetChisquare() / ndf < fPars.chi2cut) {
                        Int_t fillStyle=0;
                        if(fillStyle==0) {
						  hgcut->SetBinContent(itime + 1, y);
						  hgcut->SetBinError(itime + 1, ey);
						  hgscut->SetBinContent(itime + 1, sig);
						  hgscut->SetBinError(itime + 1, esig);
                        } else {
						  hgcut->SetBinContent(itime + 1, gi->GetParameter(1));
						  hgcut->SetBinError(itime + 1, gi->GetParError(1));
						  hgscut->SetBinContent(itime + 1, gi->GetParameter(2));
						  hgscut->SetBinError(itime + 1, gi->GetParError(2));
                        }
						lcuts->Add(h1);
					}
				}
			}
		}
	}
	// fit by exponent  : [0]*exp(-[1]*x)
	TString fitOpt = "REQ0+";
	TF1* expom = sdu::Expo("Mean", hm->GetXaxis()->GetXmin(),
			hm->GetXaxis()->GetXmax(), "A_{0}");
	hm->Fit(expom, fitOpt.Data(), "");
	TF1* expoms = sdu::Expo("Rms", hm->GetXaxis()->GetXmin(),
			hm->GetXaxis()->GetXmax(), "#sigma_{0}");
	hms->Fit(expoms, fitOpt.Data(), "");

	TF1* expog = sdu::Expo("Fit", hg->GetXaxis()->GetXmin(),
			hg->GetXaxis()->GetXmax(), "A_{0}");
	hg->Fit(expog, fitOpt.Data(), "");
	TF1* expogs = sdu::Expo("Fit", hg->GetXaxis()->GetXmin(),
			hg->GetXaxis()->GetXmax(), "#sigma_{0}");
	hgs->Fit(expogs, fitOpt.Data(), "");

	TF1* expogcut = sdu::Expo("Fit", hgcut->GetXaxis()->GetXmin(),
			hgcut->GetXaxis()->GetXmax(), "A_{0}");
	if (hgcut && hgcut->GetEntries() >= 2) {
		FitWithPointsRejection(hgcut, expogcut, 2, fPars.sigcut, 2.);
	} else {
		cout << "<W> hist " << hgcut->GetName() << " has "
				<< hgcut->GetEntries() << ". To small \n";
	}
	TF1* expogscut = sdu::Expo("Fit", hgcut->GetXaxis()->GetXmin(),
			hgcut->GetXaxis()->GetXmax(), "#sigma_{0}");
	if (hgscut && hgscut->GetEntries() >= 2) {
		hgscut->Fit(expogscut, fitOpt.Data(), "");
	} else {
		cout << "<W> hist " << hgscut->GetName() << " has "
				<< hgscut->GetEntries() << ". To small \n";
	}
	// For threads
	Int_t i = 1;
	TH1* h = sda::GetH1(lqa,0);
	for(i=1; i<=h->GetNbinsX(); ++i) {
		if(h->Integral(1,i) >= fMaxGoodBGCoord) break;
	}
	fMaxChi2 = h->GetBinLowEdge(i) + h->GetBinWidth(i);
	for (int itime = 0; itime < lall->GetSize(); ++itime) {
		TH1* h = dynamic_cast<TH1 *> (lall->At(itime));
		TF1* f = sda::GetFunction(h,0);
		pair<int,float> rec;
        rec.second = f->GetChisquare()/f->GetNDF();
        if(rec.second <= fMaxChi2) {
           rec.first  = itime;
           fGoodBgCoords.push_back(rec);
        }
	}
}

void SDPedestals::FitWithPointsRejection(TH1 *h, TF1* f, Int_t niter, Double_t nSigCut, Double_t chi2OverNdfOut)
{
	// niter   - number of iterations
	// nSigCut - number of sigma for cutting
	// chi2OverNdfOut - quality cut
	TString fitOpt= "REQ0+";
	h->Fit(f,fitOpt.Data(),"");
	if(f->GetChisquare()/f->GetNDF()<chi2OverNdfOut) return; // good fit

    TH1* hin = h;
	for(Int_t it=0; it<niter; ++it) {
		TH1* hnew = RejectPoints(hin,f,nSigCut,it);
		if(hin == hnew) break;

		TF1* fnew = dynamic_cast<TF1 *>(f->Clone());
		fnew->SetName(Form("%sIt%i",f->GetName(),it));
		hnew->Fit(fnew,fitOpt.Data(),"");
		if(f->GetChisquare()/f->GetNDF()<chi2OverNdfOut) return; // good fit

		GetListSummaryHists()->Add(hnew);
		hin = hnew;
		nSigCut *= 0.8;
		nSigCut = nSigCut>1.5?nSigCut:1.5;
	}
}

TH1* SDPedestals::RejectPoints(TH1* h, TF1* f, Double_t nSigCut, Int_t it)
{
	if(h==0) return 0;
	static TString snam0, tit0;
	if(it==0) {
		snam0 = h->GetName();
		tit0  = h->GetTitle();
	}

	Int_t nexclude = 0;
	TH1 *hnew = dynamic_cast<TH1*>(h->Clone());
	hnew->Reset();
	for(Int_t i=1; i<h->GetNbinsX(); ++i) {
		Double_t y = h->GetBinContent(i);
		if(y==0.0) continue;

		Double_t x = h->GetBinCenter(i);
		Double_t e = h->GetBinError(i);
		if((y - f->Eval(x)) < nSigCut*e) { // Cut only higher values
			hnew->SetBinContent(i,y);
			hnew->SetBinError(i,e);
		} else ++nexclude;
	}
	if(nexclude) {
		TString snam = snam0 + Form("it %i",it);
        hnew->SetName(snam.Data());
        TString tit = tit0 + Form(" it %i nexclude %i",it,nexclude);
        hnew->SetTitle(tit.Data());
		return hnew;
	}  else {
        delete hnew;
		return h;
	}
}

TH2* SDPedestals::DeductPedestal(int set, int nsig)
{
	// Deduct pedestal and fill hist
    TH2* h2in = dynamic_cast<TH2 *>(GetListWorkHists()->At(0));
    TH2* h2th = dynamic_cast<TH2 *>(GetListWorkHists()->At(1));
    if(h2in == 0 || h2th == 0) return 0;

	// Fill output hist h2th
	UInt_t sizex = (UShort_t) h2in->GetNbinsX();
	UInt_t sizey = (UShort_t) h2in->GetNbinsY();
	UInt_t col, row;
	Double_t ped = 0., sig = 0., th = 0., amp = 0., x=0.0, y=0.0;
	TF1* pedFun[2];
	GetPedestalFunctions(set, pedFun);
	for (UInt_t ix = 1; ix <= sizex; ++ix) {
		x  = h2in->GetXaxis()->GetBinCenter(ix);
		col = UInt_t(x);
		ped = pedFun[0]->Eval(x);
		sig = pedFun[1]->Eval(x);
		th = nsig * sig;
//		if(ix==1) { // Where define the seed
//			seed = nseed * sig;
//			seed = 30.;
//			cout<<" col "<<col<<" ped "<<ped<<" sig "<<sig;
//			cout<<" th "<<th<<" seed "<<seed<<endl;
////			assert(0);
//		}
		for (UInt_t iy=1; iy<=sizey; ++iy) {
			y = h2in->GetYaxis()->GetBinCenter(iy);
			row = UInt_t(y);
			amp = h2in->GetBinContent(ix, iy) - ped;
			if (amp >= th) {
				h2th->SetBinContent(ix, iy, amp);
			}
		}
	}
	TList *l = sdu::BookAndFill1DHistsFromClusterHist(h2th, " Pedh2th");
	l->SetName(gkNameProj);
	GetList()->Add(l);
	return h2th;
}

void SDPedestals::CalculateF0(TH2* himg, TH1* hPedNoise)
{
	if(himg==0 || hPedNoise==0) return;
    TF1* f = dynamic_cast<TF1 *>(hPedNoise->GetListOfFunctions()->At(0));
    fBexp = f->GetParameter(1);
    if(f==0) return;
    TList* lall = GetListAmplitudesAllHists(); // For error
    TList* lf0  = BookF0Hists();
    TList* lf0sum = GetListF0SumHists();
    TList* lf0qa = GetListF0QaHists();
    //
    //  F0X :
    //
    TH1F* hprox   = (TH1F*)lf0sum->At(0);
    TH1F* hf0x    = (TH1F*)lf0sum->At(1);
    TH1F* hratVsX = (TH1F*)lf0sum->At(2);
    TH1F* hrat1dx = (TH1F*)lf0sum->At(3);
    for(Int_t bin=1; bin<=hf0x->GetNbinsX(); ++bin) {
       Double_t x   = hf0x->GetXaxis()->GetBinCenter(bin);
       Double_t f0x = f->Eval(x);
       hf0x->SetBinContent(bin, f0x);
       Double_t ratx = hprox->GetBinContent(bin)/f0x;
       hratVsX->SetBinContent(bin, ratx);
       hrat1dx->Fill(ratx);
    }
	TF1* fpol0x = new TF1(Form("const_%s",hratVsX->GetName()),"pol0",0.0,Double_t(hratVsX->GetNbinsX()));
    fpol0x->SetLineColor(kRed);
	hratVsX->Fit(fpol0x,"REQ0+","");
	gStyle->SetOptFit(111);
	hratVsX->SetMinimum(0.0);
	TF1 *fgx = sda::gausi(hrat1dx->GetName(), 0.8, 1.8, hrat1dx);
	hrat1dx->Fit(fgx, "REQ0+", "");
    //
    //  F0Y :
    //
    for(Int_t binx=1; binx<=himg->GetNbinsX(); ++binx){
    	Double_t y = hPedNoise->GetBinContent(binx);
    	Int_t ix = binx - 1;
    	if(y<=0.0) continue;
    	TH1* hamp = dynamic_cast<TH1 *>(lall->At(ix));
    	if(hamp==0) continue;
    	TF1* famp = dynamic_cast<TF1 *>(hamp->GetListOfFunctions()->At(0));
    	if (famp==0) continue;
    	Double_t sigz = famp->GetParameter(2); // sig from gaus fit

    	for(Int_t biny=1; biny<=himg->GetNbinsY(); ++biny) {
    		Int_t iy = biny - 1;
    		TH1* h = (TH1*)(lf0->At(iy));
    		Double_t zraw = himg->GetBinContent(binx,biny);
    		h->SetBinContent(binx,zraw);
    		h->SetBinError(binx,sigz);
//    		cout<<" binx "<<binx<<" biny "<<biny<<" zraw "<<zraw<<" sigz "<<sigz<<endl;
    	}
    }
    //
    // Fit by exponent with fixed slope;
    // The first parameter is F0(biny).
    //
    TH1* hproy = sda::GetH1(lf0sum,4);
    fF0Y  = sda::GetH1(lf0sum,5);
    fF0YC = sda::GetH1(lf0sum,6);
    TH1* hratVsY = sda::GetH1(lf0sum,7);
	Double_t coefCenter = GetF0X(1024.);
	for(Int_t iy=0; iy<lf0->GetSize(); ++iy) {
		TH1* h = (TH1*)(lf0->At(iy));
		TF1* fc = (TF1*)f->Clone(Form("%sProy%3.3i",f->GetName(),iy));

		fc->FixParameter(1, fBexp);
//		h->Fit(fc,"REQ0+","");
//		FitExpoByMinuit2(h, fc, fBexp, 0);
		FitExpoAnalytically(h, fc, fBexp, 0);

		Int_t biny = iy + 1;
		fF0Y->SetBinContent(biny, fc->GetParameter(0));
		fF0Y->SetBinError(biny,   fc->GetParError(0));
		fF0YC->SetBinContent(biny, fc->GetParameter(0)*coefCenter);
		fF0YC->SetBinError(biny,   fc->GetParError(0)*coefCenter);
		// QA
		Int_t ndf = fc->GetNDF();
		if(ndf>0) {
		  sda::FillH1(lf0qa,0, fc->GetChisquare()/ndf);
		  sda::FillH1(lf0qa,1,100.*fc->GetParError(0)/fc->GetParameter(0));
		}
	}
	// Fill F0 QA hists
	// proY/F0Y in center vs biny
	for(Int_t bin=1; bin<=hratVsY->GetNbinsX(); ++bin) {
	   Double_t  f0yCenter = fF0YC->GetBinContent(bin);
	   Double_t ef0yCenter = fF0YC->GetBinError(bin);
	   Double_t r  = hproy->GetBinContent(bin)/f0yCenter;
	   Double_t er = ef0yCenter/hproy->GetBinContent(bin);
	   hratVsY->SetBinContent(bin,r);
	   hratVsY->SetBinError(bin,er);
	}
	TF1* fpol0 = new TF1(Form("const_%s",hratVsY->GetName()),"pol0",0.0,Double_t(hratVsY->GetNbinsX()));
    fpol0->SetLineColor(kRed);
	hratVsY->Fit(fpol0,"REQ0+","");
	gStyle->SetOptFit(111);
	// hratVsY->Draw();
	// 1d - proY/F0Y
	TH1* hrat1dy = sda::GetH1(lf0sum,8);
	for(Int_t bin=1; bin<=hratVsY->GetNbinsX(); ++bin) {
		hrat1dy->Fill(hratVsY->GetBinContent(bin));
	}
	TF1 *fgy = sda::gausi(hrat1dy->GetName(), 0.8, 1.8, hrat1dy);
	hrat1dy->Fit(fgy, "REQ0+", "");
//	Double_t xmax = fg->GetParameter(1) + 4.0*fg->GetParameter(2);
//	Double_t xmin = fg->GetParameter(1) - 4.0*fg->GetParameter(2);
//	gStyle->SetOptFit(111);
//	hrat1dy->Fit(fg, "REQ0+", "",xmin,xmax);
//	hrat1dy->Draw();
//    //
	// 2d - image / F0(x,y)
	//
	TH1* hrat2d   = sda::GetH1(lf0sum,9);
	TH1* hrat2dF0 = sda::GetH1(lf0sum,10);
	// Selection good time bins for F0 calculation
	TH1F* hgoodfit = (TH1F*)GetListSummaryHists()->Last();
	for(Int_t binx=1; binx<=himg->GetNbinsX(); ++binx) {
		for(Int_t biny=1; biny<=himg->GetNbinsY(); ++biny) {
		   Double_t x  = himg->GetXaxis()->GetBinCenter(binx);
		   Double_t F0 = GetF0XY(x, biny);
		   Double_t rat2d = himg->GetBinContent(binx,biny)/F0;
		   hrat2d->Fill(rat2d);
		   // Estimation of F0 sigma
		   if(hgoodfit->GetBinContent(binx)>0.0) {
			   hrat2dF0->Fill(rat2d);
		   }
		}
	}
	fSigF0Unit = hrat2dF0->GetRMS();
	// RMS of F/F0 vs binx (time)
	TList *lsum = GetListSummaryHists();
	TH1* hAmpRms = sda::GetH1(lsum,5);
	TH1* hAmp    = sda::GetH1(lsum, lsum->GetLast());
	TH1* hRmsFoverF0vsBinx = sda::GetH1(lf0sum,11);
	hRmsFoverF0vsBinx->Add(hAmpRms);
	hRmsFoverF0vsBinx->Divide(hAmp);
	TF1* expom = sdu::Expo("funRmsF/F0vsBinx", hRmsFoverF0vsBinx->GetXaxis()->GetXmin(),
			hRmsFoverF0vsBinx->GetXaxis()->GetXmax(), "A_{0}");
	expom->SetLineColor(kBlue);
	TString fitOpt("RIQ0+");
	hRmsFoverF0vsBinx->Fit(expom, fitOpt.Data(), "");

    Int_t draw=0;
	if(draw) {
	  DrawF0YFitQA();
	  DrawF0XorYvsProections(0);
	  DrawF0XorYvsProections(1);
	  // DrawFoverF0(1);
    }
}

void SDPedestals::FillH2FoverF0(TH2* h2in, TH2* h2FOverF0)
{
	// SD case : apply time&space correction simultaneously
    if(h2in==0 || h2FOverF0==0) return;

	for (Int_t ix = 1; ix <= h2in->GetNbinsX(); ++ix) {
		Double_t x   = h2in->GetXaxis()->GetBinCenter(ix);
		Double_t f0x = GetF0X(x);
		for (Int_t iy=1; iy<=h2in->GetNbinsY(); ++iy) {
			Double_t y = h2in->GetYaxis()->GetBinCenter(iy);
			Double_t f0y = GetF0Y(y);
			Double_t amp = h2in->GetBinContent(ix, iy);
			Double_t FOverF0 = amp/(f0x*f0y);
			h2FOverF0->SetBinContent(ix, iy, FOverF0);
		}
	}
	fF0Max = h2FOverF0->GetMaximum(); // For drawing
}
//
//     Static  methods
//
void SDPedestals::FillH2WithTimeCorrection(const TH2 *hin, const TF1* fcorr, TH2* hout) {
	// Bleaching(time correction)
	// hin   - input hist;
	// fcorr - time(bleaching) correction function, default is exponent.
	// hout  - output hist : hout = hin/fcorr
	if (hin == 0 || fcorr == 0 || hout == 0)
		return;

	for (Int_t ix = 1; ix <= hin->GetNbinsX(); ++ix) {
		Double_t x = hin->GetXaxis()->GetBinCenter(ix);
		Double_t f0x = fcorr->Eval(x);
		for (Int_t iy = 1; iy <= hin->GetNbinsY(); ++iy) {
			Double_t amp = hin->GetBinContent(ix, iy);
			Double_t ampCorr = amp / f0x;
			hout->SetBinContent(ix, iy, ampCorr);
		}
	}
}

void SDPedestals::FillH2WithSpaceCorrection(const TH2 *hin, TH1* hcorr, TH2* hout) {
	// Bleaching(time correction)
	// hin   - input hist;
	// hcorr - space(blurring) correction function, default is exponent.
	// hout  - output hist : hout = hin/hcorr = F/F0
	if (hin == 0 || hcorr == 0 || hout == 0)
		return;

	for (Int_t iy = 1; iy <= hin->GetNbinsY(); ++iy) {
		Double_t y   = hin->GetYaxis()->GetBinCenter(iy);
		Double_t f0y = hcorr->Interpolate(y);
	    for (Int_t ix = 1; ix <= hin->GetNbinsX(); ++ix) {
			Double_t amp = hin->GetBinContent(ix, iy);
			Double_t ampCorr = amp / f0y;
			hout->SetBinContent(ix, iy, ampCorr);
		}
	}
}

void SDPedestals::FillFOverF0MinusOneWithCut(const TH2 *hin, const Double_t threshold, TH2* hout) {
	// Bleaching(time correction)
	// hin   - input hist;
	// threshold - threshold for (amp-1.)
	// hout  - output hist with (amp-1.)>=threshold
	if (hin == 0 || hout == 0) return;

	Double_t ymin = hin->GetMaximum() - 1.;
	for (Int_t iy = 1; iy <= hin->GetNbinsY(); ++iy) {
	    for (Int_t ix = 1; ix <= hin->GetNbinsX(); ++ix) {
			Double_t amp1 = hin->GetBinContent(ix, iy) - 1.;
			if(amp1>=threshold) {
			  hout->SetBinContent(ix, iy, amp1);
			  ymin = ymin>amp1?amp1:ymin;
			}
		}
	}
	TAxis* xax = hout->GetXaxis();
	TString st = xax->GetTitle();
	st.Prepend(Form("Y_{min=%3.2f}^{max=%3.2f}   #color[2]{#ocopyright PAI}         ",ymin,hout->GetMaximum()));
	xax->SetTitle(st.Data());
}

void SDPedestals::FillH2WithCut(const TH2* hin, const Double_t ampCut, TH2* hout)
{
    if(hin==0 || hout==0) return; // June 12,2013
    hout->Reset();

	for (Int_t ix = 1; ix <= hin->GetNbinsX(); ++ix) {
		for (Int_t iy=1; iy<=hin->GetNbinsY(); ++iy) {
			Double_t amp = hin->GetBinContent(ix, iy);
            if(amp > ampCut) hout->SetBinContent(ix, iy, amp);
		}
	}
}

void SDPedestals::FillF0SmoothForSigma(TH2* hf0smooth, TH1* hreper, TList* lout)
{
	if(hf0smooth==0 || hreper==0 || lout==0) return;

    TList *l = new TList;
    l->SetName(Form("AmpHistsWithCuts %s",lout->GetName()));
    lout->Add(l);
	for(Int_t binx=1; binx<=hf0smooth->GetNbinsX(); ++binx) {
		TH1 *h1 = 0;
		Double_t chi2OverNdf=1000.;
		if(hreper->GetBinContent(binx)>0.0) {
		    TH1D *hd = hf0smooth->ProjectionY(Form("proy%i",binx),binx,binx);
			h1 = FillAndFit1dAmplitudeDistribution(hd);
		    if(h1) l->Add(h1);
		    delete hd;
		    // Sometimes we have big deviation in rms of hist -> get from good fit
            // sda::FillH1(lout,2, Double_t(binx), h1->GetRMS(), h1->GetRMSError()); // FO/F0 in F0
		    TF1 *f = sda::GetFunction(h1,0);
		    chi2OverNdf = f->GetChisquare()/f->GetNDF();
		    if(chi2OverNdf<2.)
			sda::FillH1(lout,2, Double_t(binx), f->GetParameter(2), f->GetParError(2)); // FO/F0 in F0
		}
		for(Int_t biny=1; biny<=hf0smooth->GetNbinsY(); ++biny) {
			Double_t amp = hf0smooth->GetBinContent(binx,biny);
			sda::FillH1(lout,0,amp); // FO/F0
			if(hreper->GetBinContent(binx)>0.0 && chi2OverNdf<2.) {
				sda::FillH1(lout,1,amp); // FO/F0 in F0
			}
		}
	}
	TH1* h = sda::GetH1(lout,2);
	TF1* fpol0x = new TF1(Form("const_%s",h->GetName()),"pol1",0.0,Double_t(h->GetNbinsX()));
    fpol0x->SetLineColor(kRed);
	h->Fit(fpol0x,"REQ0+","");
	gStyle->SetOptFit(111);
	h->SetMinimum(0.0);
}

void SDPedestals::FillEmptyTimeZone(TH1* h, vector<pair<float,float> >&  v)
{
	// Apr 8,2013;
	// pair<int,int> first,second is first(last) "zero" time zone
	if(h==0) {
		printf(" SDPedestals::FillEmptyTimeZone | h=%p\n", h);
		return;
	}
 //   TCanvas *c = new TCanvas("ctmp","ctmp", 10,10,800,500);
 //   sdu::DrawHistInThreads(h,c);

	pair<float,float> rec;
	rec.first = rec.second = -1;
	Float_t amp = 0.0;
	for(Int_t binx=1; binx<=h->GetNbinsX(); ++binx)
	{
        amp = h->GetBinContent(binx);
		if(amp<=1.e-6) {
		   if(rec.first<0) rec.first = rec.second = binx-1;
		   else            rec.second = binx-1;
	   } else {
		   if(rec.first>=0) { // >= 0 !
			   v.push_back(rec);
			   rec.first = rec.second = -1;
		   }
	   }
	}
	// cout<<" FillEmptyTimeZone #zone " <<v.size()<<endl;
}

void SDPedestals::Split2DHist(TH2* hin, vector<pair<float,float> > vcoords, Int_t maxThreads, TList *lout)
{
	// Apr 9,2013;
	// maxThreads = 2
	Int_t pri=1;
	if(pri) cout<<" SDPedestals::Split2DHist : vcoords.size "<<vcoords.size()<<endl;
	if(hin==0 || vcoords.size()==0 || maxThreads<1 || lout==0) return;

    TAxis* xax = hin->GetXaxis(), *yax = hin->GetYaxis();
    Double_t xmean = (xax->GetXmin() + xax->GetXmax())/2.;
    vector<Double_t> vd;

    pair<float,float> r;
    for(UInt_t i=0; i<vcoords.size(); ++i){
    	r = vcoords[i];
    	vd.push_back(TMath::Abs((r.first+r.second)/2.-xmean));
    }
    vector<Double_t>::iterator it = min_element(vd.begin(),vd.end());
    UInt_t imin = distance(vd.begin(),it);
    if(pri) cout<<" imin "<<imin<<" value "<<*it<<endl;
    //
	r = vcoords[imin];
	Int_t nbin = (r.first + r.second)/2;
	xmean = xax->GetXmin() + Double_t(nbin);
    TString snam, stit;
    snam.Form("%s_1", hin->GetName());
    stit.Form("%s : part 1", hin->GetTitle());
    TH2* h2_1 = new TH2F(snam.Data(),stit.Data(), nbin, xax->GetXmin(), xmean,
    		yax->GetNbins(), yax->GetXmin(), yax->GetXmax());
    lout->Add(h2_1);

    nbin = xax->GetNbins() - nbin;
    snam.Form("%s_2", hin->GetName());
    stit.Form("%s : part 2", hin->GetTitle());
    TH2* h2_2 = new TH2F(snam.Data(),stit.Data(), nbin, xmean, xax->GetXmax(),
    		yax->GetNbins(), yax->GetXmin(), yax->GetXmax());
    lout->Add(h2_2);

    for(Int_t binx=1; binx<=xax->GetNbins(); ++binx) {
        for(Int_t biny=1; biny<=yax->GetNbins(); ++biny) {
           Double_t a = hin->GetBinContent(binx,biny);
           for(Int_t ih=0; ih<lout->GetSize();++ih) {
               TH2* h = sda::GetH2(lout,ih);
               Int_t bin = h->FindBin(xax->GetBinCenter(binx), yax->GetBinCenter(biny));
               if(bin>0) h->SetBinContent(bin,a);
               h->SetOption("colz");
           }
        }
   }
}
//
// End of static methods
//
TH2* SDPedestals::DoAll()
{
	TH2 *h2out=0;
	TString algo = sdu::GetParsPedestals(this)->algo;
	if(algo.Contains("sd",TString::kIgnoreCase)) {
	   h2out = SDAlgorithm();
	} else if(algo.Contains("idl",TString::kIgnoreCase)) {
		h2out = IdlLikeAlgorithm();
	} else {
		cout<<"<E> Wrong pedestal algorithm "<<algo<<endl;
		assert(0);
	}
	// Multi threads
	parsSdThreads sdTh = GetSDManager()->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsThreads();
	if(sdTh.IsMultiThreadingPossible()) {
	   TList *l = new TList;
	   l->SetName(gkSplittedHists);
	   GetListWorkHists()->Add(l);
	   Split2DHist(h2out, fEmptyBgCoords, sdTh.GetMaxThreads(), l);
	}

	return h2out;
}

TH2* SDPedestals::SDAlgorithm()
{
	return SDAlgorithm(fPars.set, fPars.nsig);
}

TH2* SDPedestals::SDAlgorithm(int set, int nsig)
{
    TH2* h2Th = 0;
	bool key = FillInitialAmplitudesDistribution();
	if(key) {
	  FillAmpDistWithCutsQaAndSum();

	  TH2* h2img = sda::GetH2(GetListWorkHists(),0);
	  TList *lsum = GetListSummaryHists();
	  Int_t size = lsum->GetSize(), ind = size-2;
	  if(size>6) ind = size - 1;
	  TH1* hPedNoise = dynamic_cast<TH1 *>(lsum->At(ind));
	  CalculateF0(h2img, hPedNoise);

	  TH2* h2FOverF0 = sda::GetH2(GetListWorkHists(),1);
	  FillH2FoverF0(h2img, h2FOverF0);
	  // Apply filter
//	  parsClusterFinder *pcf = sdu::GetParsClusterFinder(this);
	  TH2* h2FOverF0Smooth = sda::GetH2(GetListWorkHists(),2);
      parsPedestals *pp = sdu::GetParsPedestals(this);
	  sdu::Smooth(h2FOverF0, pp->smooth, h2FOverF0Smooth);

	  TH1* hreper = sda::GetH1(GetListSummaryHists(), GetListSummaryHists()->LastIndex());
	  FillF0SmoothForSigma(h2FOverF0Smooth, hreper, GetListF0SmoothHists());

	  TH2* hFOverF0MinusOneCut = sda::GetH2(GetListWorkHists(),3);
	  fSigF0Unit = sda::GetH1(GetListF0SmoothHists(),1)->GetRMS();
	  Double_t ampCut = fSigF0Unit * fPars.nsig; // GetAmpCut()
	  FillFOverF0MinusOneWithCut(h2FOverF0Smooth, ampCut, hFOverF0MinusOneCut);
	  h2Th = hFOverF0MinusOneCut;
// Control hists - Apr 8,2013
	  TList *lproj = sdu::BookAndFill1DHistsFromClusterHist( h2Th , h2Th->GetName(), 1);
      lproj->SetName("F/F0-1 Proj");
	  GetListWorkHists()->Add(lproj);
      lproj = sdu::BookAndFill1DHistsFromClusterHist(h2FOverF0Smooth, h2FOverF0Smooth->GetName(), 1);
      lproj->SetName("F/F0 Smooth Proj");
	  GetListWorkHists()->Add(lproj);

	  FillEmptyTimeZone(sda::GetH1(lproj,0), fEmptyBgCoords);
	}
    return h2Th;
}

TH2* SDPedestals::IdlLikeAlgorithm()
{
	// IDL algorithm
    TH2* h2Th = 0;
	bool key = FillInitialAmplitudesDistribution();
	if(key) {
	  FillAmpDistWithCutsQaAndSum();

	  TH2*   h2img = sda::GetH2(GetListWorkHists(),0);
	  TList* lsum  = GetListSummaryHists();
	  Int_t size = lsum->GetSize(), ind = size-2;
	  if(size>6) ind = size - 1;
	  TH1* hPedNoise = sda::GetH1(lsum,ind); // need for exponent parameter
	  CalculateF0(h2img, hPedNoise);

      // IDL like part
	  // Time(bleaching) correction
	  TH2* h2imgTcor = sda::GetH2(GetListWorkHists(),1);
	  TH1* hlast = sda::GetH1(lsum, lsum->LastIndex());
	  TF1 *fexpo = dynamic_cast<TF1 *>(sda::GetFunction(hlast,0)->Clone(Form("fCopy")));
	  fexpo->SetParameter(0,1.);  // Need only time dependence
	  FillH2WithTimeCorrection(h2img, fexpo, h2imgTcor);
	  delete fexpo;

	  // Apply filter to h2imgTcor
//	  parsPedestals *pp = sdu::GetParsPedestals(this);
	  parsClusterFinder *pcf = sdu::GetParsClusterFinder(this);
	  TH2* h2imgTcorSmooth = sda::GetH2(GetListWorkHists(),2);
      //sdu::Smooth(h2imgTcor, pp->smooth, h2imgTcorSmooth);
	  // ss=(0.8/psize); st=(8./tscan) ;0.8-um and 8-ms spatial(temporal) smoothing filter
	  UInt_t wx = TMath::Nint(8.0/pcf->timescale/2.);
	  UInt_t wy = TMath::Nint(0.8/pcf->spacescale/2.);
	  sdu::MeanFilter(h2imgTcor,wx,wy, h2imgTcorSmooth);

	  TH2* hFOverF0 = sda::GetH2(GetListWorkHists(),3);
	  FillH2WithSpaceCorrection(h2imgTcorSmooth, fF0Y, hFOverF0);

	  TH1* hreper = sda::GetH1(GetListSummaryHists(), GetListSummaryHists()->LastIndex());
	  FillF0SmoothForSigma(hFOverF0, hreper, GetListF0SmoothHists());

	  fSigF0Unit = sda::GetH1(GetListF0SmoothHists(),1)->GetRMS();
	  Double_t threshold = fSigF0Unit * fPars.nsig;
	  TH2* hFOverF0MinusOneCut = sda::GetH2(GetListWorkHists(),4);
	  FillFOverF0MinusOneWithCut(hFOverF0, threshold, hFOverF0MinusOneCut);
	  h2Th = hFOverF0MinusOneCut;
	  // Control hists - Apr 8,2013
	  TList *lproj = sdu::BookAndFill1DHistsFromClusterHist( h2Th , h2Th->GetName(), 1);
	  lproj->SetName("F/F0-1 Proj");
	  GetListWorkHists()->Add(lproj);

	  FillEmptyTimeZone(sda::GetH1(lproj,0), fEmptyBgCoords);
	}
    return h2Th;
}
//
//   D r a w i n g
//
void SDPedestals::DrawPedestalsFunctions()
{
	// Draw nh pedestals functions
	int ind[]={0,2,4};
//	const char* tit[]={"Mean from hists","Mean from fit ","Mean from fit&cuts"};
	const char* tit[]={" H"," F"," F&C"};
	int nh=sizeof(ind)/sizeof(int), idraw=0;
	TCanvas *c = sda::Canvas("cPedFuns","cPedFuns", 10,10, 600, 400);

	TLegend *leg = 0;
	for(int ih=0; ih<nh; ++ih){
	  TF1 *f = GetPedestalFun(ind[ih]);
	  if(idraw==0 && f) {
		  ++idraw;
		  f->Draw();
		  TH1* h = f->GetHistogram();
		  h->SetMinimum(12.0);
		  leg = new TLegend(0.12, 0.14, 0.88, 0.50, "Pedestals functions");
	  } else {
		  ++idraw;
		  f->Draw("same");
	  }
	  f->SetLineColor(idraw);
	  TString stit;
	  stit += Form("%s=%8.2e #pm %8.2e ", f->GetParName(0), f->GetParameter(0),f->GetParError(0));
	  stit += Form("%s=%8.2e #pm %8.2e ", f->GetParName(1), f->GetParameter(1),f->GetParError(1));
	  stit += tit[ih];
	  TLegendEntry *le = leg->AddEntry(f, stit.Data(), "L");
	  le->SetTextColor(f->GetLineColor());
	}
	leg->Draw();
	c->Update();
}

void SDPedestals::DrawPedExamples()
{
   // 4_ca100_red_channel.tif
   Int_t nind[]={4,26};
   Int_t nh = sizeof(nind)/sizeof(Int_t);
   TList *l = new TList;
   for(Int_t i=0; i<nh; i++) l->Add(GetListAmplitudesHists()->At(nind[i]));

   TCanvas* c = sda::DrawListOfHist(l,2,1);

   gStyle->SetOptFit(111);
   c->SetWindowSize(600,400);
   c->SetTitle("PedExamples");
   c->SetName("PedExamples");
   c->Update();
}

void SDPedestals::DrawChi2AndSigma()
{
	TCanvas* c = sda::DrawListOfHist(GetListSummaryHists(), 2, 1, 2, 6);
	c->SetWindowSize(600, 400);
	c->SetTitle("Chi2AndSigma");
	c->SetName("Chi2AndSigma");
	c->Update();
}
void SDPedestals::DrawAmpAndSig(int ind)
{
	TCanvas* c = sda::DrawListOfHist(GetListSummaryHists(), 2, 1, 2, 2*ind);
	gStyle->SetOptFit(111);
	gStyle->SetOptStat(1000000);
	c->SetWindowSize(600, 400);
	c->SetTitle("MeanAndSigma");
	c->SetName("MeanAndSigma");
	c->Update();
}

void SDPedestals::DrawF0YFitQA()
{
	TCanvas* c = sda::DrawListOfHist(GetListF0QaHists(), 2, 1, 2, 0);
	gStyle->SetOptStat(1111111);
	c->SetWindowSize(600, 400);
	c->SetTitle("F0YFitQA");
	c->SetName("F0YFitQA");
	c->Update();
}

void SDPedestals::DrawF0XorYvsProections(Int_t ind)
{
    const char* stit[]={"x","y"};
    ind = ind==0?0:1;
    TString ctit("cF0");
    ctit += stit[ind];
	TCanvas *c = sda::Canvas(ctit.Data(),ctit.Data(), 10,10, 600, 400);
	c->Divide(2,1);

    Int_t indh = 5*ind; // was 4
	TList *lf0sum = GetListF0SumHists();
	TH1* hpro = (TH1*)lf0sum->At(indh);
	TH1* hf0   = (TH1*)lf0sum->At(indh+1);

    c->cd(1);
	sda::DrawHist(hpro, 1, 2);
	sda::DrawHist(hf0, 2,1,"same");
	if(hf0->GetMaximum()>hpro->GetMaximum()) hpro->SetMaximum(hf0->GetMaximum());
	TLegend *leg = new TLegend(0.12, 0.14, 0.82, 0.44, Form(" F0 & pro %s",stit[ind]));
	TLegendEntry *le = leg->AddEntry(hf0, Form(" F0(%s)",stit[ind]), "L");
	le = leg->AddEntry(hpro, Form(" proj %s (norm) ",stit[ind]), "L");
	le->SetTextColor(hpro->GetLineColor());
	leg->Draw();

	TH1* hratVs = (TH1*)lf0sum->At(indh+2);
	TH1* hrat   = (TH1*)lf0sum->At(indh+3);
	TVirtualPad* pad = c->cd(2);
	pad->Divide(1,2);

	pad->cd(1);
	hratVs->SetMinimum(0.);
	sda::DrawHist(hratVs, 1);

	pad->cd(2);
	hrat->SetAxisRange(0.8,1.8);
	sda::DrawHist(hrat, 2,1,"e");

	c->Update();
}

void SDPedestals::Draw2DRatioFoverF0()
{
    TString ctit("c2dRF0/F0");
	TCanvas *c = sda::Canvas(ctit.Data(),ctit.Data(), 10,10, 600, 400);
	c->Divide(2,1);

	TList *lf0sum = GetListF0SumHists();
	TH1* h9  = sda::GetH1(lf0sum, 9);
	TH1* h10 = sda::GetH1(lf0sum, 10);

	c->cd(1);
	gPad->SetLogy(1);
	TH1::AddDirectory(kFALSE);
	TH1* h10c = (TH1*)h10->Clone(Form("%sc",GetName()));
	Double_t norm = h9->Integral() / h10->Integral();
	h10c->Scale(norm);
	h10c->SetLineColor(kRed);
	sda::DrawHist(h9, 2);
	sda::DrawHist(h10c, 2, 2, "same");

	c->cd(2);
	sda::DrawHist(h10, 2);

	c->Update();
}

void SDPedestals::DrawFoverF0(UInt_t createProjs) {
	TList* l = GetListWorkHists();
	TH2* h2 = sda::GetH2(l, 0);
	if (h2 == 0)
		return;

	TString ctit("cQA_F/F0 0");
	TCanvas *c0 = sda::Canvas(ctit.Data(), ctit.Data(), 10, 10, 600, 400), *c1 = 0;
	h2->Draw();
	c0->Update();

	if (createProjs) {
		TList *lprojs = sda::CreateProjection(h2, "proXY F0","only");
		l->Add(lprojs);
		ctit = "cQA_F/F0 1";
		c1 = sda::Canvas(ctit.Data(), ctit.Data(), 10, 10, 600, 400);
		sda::DrawListOfHist(lprojs, 1, 2, 2, 0);
		c1->Update();
	}
	gSystem->ProcessEvents();
}

void SDPedestals::PrintSplittingLines()
{
	//Apr 22,2013
	printf("<I> fGoodBgCoords.size() %4i", int(fGoodBgCoords.size()));
	for(UInt_t i=0; i<fGoodBgCoords.size(); ++i) {
		pair<float,float> p = fGoodBgCoords[i];
		printf("%i:%i  ", int(p.first), int(p.second));
		if((i+1)%10 == 0) printf("\n");
	}
	printf("\n");
	printf("<I> fEmptyBgCoords.size() %4i", int(fEmptyBgCoords.size()));
	for(UInt_t i=0; i<fEmptyBgCoords.size(); ++i) {
		pair<float,float> p = fEmptyBgCoords[i];
		printf("%i:%i  ", int(p.first), int(p.second));
		if((i+1)%10 == 0) printf("\n");
	}
	printf("\n");
}

TH1* SDPedestals::CreateChi2List(const char* opt, const double chi2Cut)
{
	//
	// Select histogramms with good or bad fit
	// Return hist with smallest(largest) chi2/ndf
	//
	TList *lamp = GetListAmplitudesAllHists();
	if (lamp->GetSize() == 0) {
        cout<<" SDPedestals::CreateChi2List lamp "<<lamp<<endl;
		return 0;
	}

	TList *l = new TList;
	l->SetName(opt);
	l->SetOwner(kFALSE);
	TString sopt(opt);
	sopt.ToLower();

	TF1 *gi = 0, *gihout = 0;
	TH1* hout = dynamic_cast<TH1 *> (lamp->At(0));
	for (int i = 0; i < lamp->GetSize(); ++i) {
		TH1* h1 = dynamic_cast<TH1 *> (lamp->At(i));
		gi     = sda::GetFunction(h1);
		gihout = sda::GetFunction(hout);
		if (gi && gi->GetNDF()>0) {
			double chi2d  = gi->GetChisquare() / gi->GetNDF();
			double chi2dh = gihout->GetChisquare() / gihout->GetNDF();
			if (sopt.Contains("good") && chi2d < chi2Cut) {
				l->Add(h1);
			    if(chi2d<chi2dh) hout = h1;
			} else if (sopt.Contains("bad") && chi2d> chi2Cut) {
				l->Add(h1);
			    if(chi2d>chi2dh) hout = h1;
			}
		}
	}
	std::cout << " List of " << opt << " has " << l->GetSize()
			<< " hists \n";
	if (l->GetSize() > 0) {
		GetList()->Add(l);
	} else {
		delete l;
		hout = 0;
	}
	return hout;
}

ROOT::Fit::Fitter* SDPedestals::InitNewFitter(const char* name)
{
	//   Apr 2,2013;
	//   Select minuit2 with the same functionality but no annoying output.
   ROOT::Fit::Fitter*  fitter = new ROOT::Fit::Fitter;
   TString snam(name);
   if(snam.Contains("fumi", TString::kIgnoreCase)) fitter->Config().SetDefaultMinimizer("Fumili");
   return fitter;
}

void SDPedestals::FitExpoByMinuit2(TH1* hexp, TF1 *fexp, Double_t slope, Int_t pri)
{
	// Apr 3,2013 : working good
	if (hexp == 0 || fexp == 0)
		return;

	fexp->SetParameters(hexp->GetBinContent(1), slope);
	ROOT::Math::WrappedMultiTF1 wexp(*fexp, 1);

	ROOT::Fit::BinData data;
	ROOT::Fit::FillData(data, hexp);

	ROOT::Fit::Fitter fitter;
	fitter.SetFunction(wexp);
	fitter.Config().ParSettings(1).Fix();
	int iparB[2] = { 0, 1 };

	fitter.Config().MinimizerOptions().SetPrintLevel(0);
	fitter.Config().SetMinimizer("Minuit2", "fumili");

	fitter.Fit(data);
	ROOT::Fit::FitResult result = fitter.Result();
	if (pri >= 1) {
		std::cout<<" Hist "<<hexp->GetName()<<" expo "<<fexp->GetName()<<std::endl;
		result.Print(std::cout);
	}
	// For drawing
	fexp->SetFitResult(result, iparB);
	fexp->SetLineColor(kBlue);
	hexp->GetListOfFunctions()->Add(fexp);
}

void SDPedestals::FitExpoAnalytically(TH1* hexp, TF1* fexp, Double_t slope, int pri)
{
	// Apr 4,2013: Slope is constant, a0 would be calculated
	// Significantly faster
	// pri = 1 - print fit result;
	if(hexp==0 || fexp==0) return;
	std::vector<Double_t> xexp, y, ey;

	Double_t yc = 0.0, eyc = 0.0, xcexp = 0.0, wc = 0.0;
	Double_t sumy = 0.0, suma = 0.0;
	for(Int_t i=1; i<=hexp->GetNbinsX(); ++i) {
		yc = hexp->GetBinContent(i);
		if(yc > 0.0) {
			eyc = hexp->GetBinError(i);
			if(eyc<=0.0) eyc = TMath::Sqrt(yc);
			xcexp = TMath::Exp(slope*hexp->GetBinCenter(i));
			wc = 1./(eyc*eyc);
			sumy += (yc*xcexp)*wc;
			suma += xcexp*xcexp*wc;
			xexp.push_back(xcexp);
			y.push_back(yc);
			ey.push_back(eyc);
		}
	}
	Int_t n = xexp.size();
	Double_t a0 = 0.0, ea0=0.0, chi2=0.0, diff = 0.0;
	Double_t a0log = 0.0, ea0log = 0.0;;
	if(n>0) {
	   a0 = sumy/suma;
	   a0log = TMath::Log(a0);
	   for(Int_t i=0; i<n; ++i) {
		   diff = y[i] - a0*xexp[i];
		   diff /= ey[i];
           chi2 += diff*diff;
	   }
	}
	ea0    = 1./TMath::Sqrt(suma);
    // b=log(a0); db = da0/a0 -<abs error of b equal relative arror of a0;
    ea0log = ea0/a0; // !!	fexp->SetParameters(a0, slope);
	fexp->SetLineColor(kGreen);
	fexp->SetLineWidth(2);
//	hexp->GetListOfFunctions()->Add(fexp);
	fexp->SetParameter(0, a0);
	fexp->SetParError (0,ea0);
	fexp->SetChisquare(chi2);
	fexp->SetNDF(n - 1);
	if(pri) {
		printf(" npoints\t %i slope %7.6f\n", n, slope);
		printf(" a0\t %6.3f+/-%6.5f log(a0) %7.5f+/-%7.6f\n", a0,ea0,  a0log,ea0log);
		printf(" chi2\t %7.5f NDF %i \n", chi2, n - 1);
	}
}

//
// Clean up
//
void SDPedestals::DeleteListAmplitudesHists()
{
	// Delete all hists from list and list itself too
   TList *l = GetListAmplitudesHists();
   if(l==0) return;
   l->Delete(); // Remove all objects from the list AND delete all heap based objects.
   GetList()->Remove(l);
   delete l;
}

ClassImp(SDPedestals)

