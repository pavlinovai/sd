/*
 * SDImageAnalysis.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: Alexei Pavlinov
 */

#include "SDImageAnalysis.h"
#include "SDManager.h"
#include "SDConfigurationParser.h"
#include "SDAliases.h"
#include "SDUtils.h"
#include "SDPedestals.h"
#include "SDClusterFinderStdOne.h"
#include "SDClusterInfo.h"
#include "sdCommon.h"
#include "SDTransformation.h"

#include <iostream>
using namespace std;

ClassImp(SDImageAnalysis)

SDImageAnalysis::SDImageAnalysis() : SDObjectSet() , fImageStatus(), fSig(),fSig2(), fParsScan(),fCF(0)
{
	// For reading
}

SDImageAnalysis::SDImageAnalysis(imageStatus imgStat) : SDObjectSet("Image") , fImageStatus(imgStat)
, fSig(""),fSig2(""), fParsScan(), fCF(0)
{
  TString snam("Image");
  snam += imgStat.color;
  SetName(snam);
}

SDImageAnalysis::~SDImageAnalysis() {
	// TODO Auto-generated destructor stub
}

SDTiffFileInfo* SDImageAnalysis::GetTiffFileInfo()
{
	return dynamic_cast<SDTiffFileInfo *>(sdu::GetTiffFileInfo(this));
}

TString SDImageAnalysis::GetSignature()
{
	// Signature for online drawing
	if(fSig.Length()) return fSig;

	SDTransformation *trans = GetSDManager()->GetTiffFileInfo()->GetSDTransformation();
	fSig  = (trans->GetSignature() + " ");

	fSig += (GetPedestals()->GetSignature() + GetClusterFinder()->GetSignature());
	return fSig;
}

TString  SDImageAnalysis::GetSignatureForCanvas()
{
	// Signature for names(titles) of canvases
	if(fSig2.Length()) return fSig2;
	sdCommon* sdc = sdCommon::GetSdCommon();
	fSig2 = (fImageStatus.color + " ");
	fSig2 += sdc->CPAI2;
	return fSig2;
}

SDPedestals* SDImageAnalysis::GetPedestals()
{
	return dynamic_cast<SDPedestals *>(sda::FindObjectByPartName(fList, sd::GetSdCommon()->leadingPedName.Data()));
}

SDClusterFinderStdOne* SDImageAnalysis::GetClusterFinder()
{
    // June 1,2013
	if(fCF == 0) fCF = InitClusterFinder();
	return fCF;
	// return dynamic_cast<SDClusterFinderStdOne *>(sda::FindObjectByPartName(fList, sd::GetSdCommon()->leadingPCFName.Data()));
}

SDClusterFinderStdOne* SDImageAnalysis::InitClusterFinder()
{
	// Similar as void SDManager::InitClustersFinder(){

	parsClusterFinder pcf =
					GetSDManager()->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsClusterFinder();
	SDClusterFinderBase *clf = sdu::CreateClusterFinder(pcf);
	clf->SetCFPars(pcf);
	clf->Init();
	TH2 *h2th = GetPedestals()->GetHistForClusterFinder();
	clf->SetInputHist(h2th);
	// Transition to F0 units
	clf->SetSeed(clf->GetSeed() * GetPedestals()->GetSigF0Unit());

	SDClusterFinderStdOne* clf2 = dynamic_cast<SDClusterFinderStdOne *>(clf);
	Double_t ampCut = GetPedestals()->GetAmpCut();
	clf2->SetAmpCut(ampCut);	// June 12,2013:
	Add(clf2);
	return clf2;
}

void SDImageAnalysis::ClustersScan(UInt_t pri)
{
	// June 16,2013
	if(pri) cout<<" SDImageAnalysis::ClustersScan("<<pri<<")\n";
	SDClusterFinderStdOne* clf = GetClusterFinder();
	if(clf==0) return;

	for(Int_t i=0; i<clf->GetClusters()->GetListSize(); ++i)
	{
       SDClusterInfo* cli = clf->GetClusterInfo(i);
       cli->Scan(GetParsScan());
	}

	SetImageStatus(SDTiffFileInfo::kCSDone);
}

