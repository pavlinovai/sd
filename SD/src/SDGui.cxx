/*
 * SDGui.cpp
 *
 *  Created on: 2012: Aug 29; Oct 1;
 *              2013: Feb 1; Mar 9;
 *      Author: Alexei Pavlinov
 */

#include "SDGui.h"
#include "SDUtils.h"
#include "SDTiffFileInfo.h"
#include "SDClusterFinderBase.h"
#include "SDClusterFinderStdOne.h"
#include "SDPedestals.h"
#include "SDConfigurationParser.h"
#include "SDImagesPool.h"
#include "SDImageAnalysis.h"
#include "sdCommon.h"
#include "SDTransformation.h"
#include "versionUpdate.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TApplication.h>
#include <TClass.h>
#include <TCanvas.h>
#include <TGTab.h>
#include <TGLabel.h>
#include <TG3DLine.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include <TGFont.h>
#include <TGGC.h>
#include <TGButtonGroup.h>
#include <TGNumberEntry.h>
#include <TGDoubleSlider.h>
#include <TGStatusBar.h>
#include <TFitParametersDialog.h>
#include <TGMsgBox.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TGResourcePool.h>
#include <TGFontDialog.h>
#include <TGTextEdit.h>
#include <TMap.h>
#include <TStopwatch.h>

#include <TBrowser.h>
#include <TDataMember.h>
#include <TMethodCall.h>
#include <TDataType.h>

#include <assert.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

ostream& operator<<(ostream& os, const tabRec& r)
{
	os<<"r.compFrame "<<r.compFrame<<" r.groupFrame "<<r.groupFrame;
	os<<"r.textEdit "<<r.textEdit<<" "<<r.textEdit->GetName()<<endl;
	return os;
}

UInt_t widthSD  = 400;  // 600 default
UInt_t heightSD = 900;

SDGui* SDGui::fgSDGuiDialog = 0;

TString titF0Button("Calculate F0 ");
TString titCFButton("Find clusters ");
TString scanCFButton("Scan clusters ");

Int_t nSmoothComboBox=0, nAlgoBox=0;
vector<const Char_t*> SmoothComboBox;
vector<const Char_t*> AlgoBox;

ClassImp(SDGui)

//______________________________________________________________________________
SDGui* SDGui::GetInstance(SDManager *sdman)
{
   // Static method - opens the SD GUI panel.

   if (fgSDGuiDialog == 0) {
	 fgSDGuiDialog = new SDGui(sdman);
	 fgSDGuiDialog->SetName("SDGui");
   } else {
//     fgSDGuiDialog->Show();
   }
   return fgSDGuiDialog;
}

//______________________________________________________________________________
SDGui::~SDGui()
{
   // Fit editor destructor.

//   DisconnectSlots();
//
//   // Disconnect all the slot that were no disconnected in DisconnecSlots
//   fCloseButton->Disconnect("Clicked()");
//   fTiffData->Disconnect("Selected(Int_t)");
//   fUpdateButton->Disconnect("Clicked()");
//   TQObject::Disconnect("TCanvas", "Selected(TVirtualPad *, TObject *, Int_t)",
//                        this, "SetFitObject(TVirtualPad *, TObject *, Int_t)");

   gROOT->GetListOfCleanups()->Remove(this);

   //Clean up the members that are not automatically cleaned.
   Cleanup();
//   delete fLayoutNone;
//   delete fLayoutAdd;
//   delete fLayoutConv;

   // Set the singleton reference to null
   fgSDGuiDialog = 0;
}

//______________________________________________________________________________
SDGui::SDGui(SDManager *sdman) : TGMainFrame(gClient->GetRoot(), 500, 20),
		fSdManager(sdman), fTifInfo(0), fImgAna(0), fPed(0), fCF(0),
		fConfInfo(0),
		fTiffLabel(0), fTiffData(0),
		fSelectXMLButton(0),   fSelectTIFButton(0),
		fReadTIFButton(0),     fFill2DHists(0),
		fCalculateF0Button(0), fFindClustsButton(0),
		fClustersScanButton(0),fSaveButton(0),
		fBrowserButton(0), fExitButton(0), fSelectFont(0), fStatusBar(0),
		fTab(0), fTabContainer(0), fNShift(2),
		fConfTab(0), fConfFrame(0), fTEofTifDir(0),
		fTEofTifName(0), fRGBSelection(0), fStringSelected(""),
		fButtonsGf(), fTransformChksList(),
		fScaleEntry(0), fFilterEntry(0),
		fNF0Entries(0), fF0Entries(0), fNCFEntries(0), fCFEntries(0),
		fThreadsFrame(0),fThreadsList(0),
		fScanFrame(0), fScanList(0),
		fTEofOutDir(0), fTEofOutName(0), fNResTabs(0), fResultTab(),
		fResultFrame(), fEdit(), fResultTabs(), fFont(0),
		fTransColor(0),
		fBrowser(0)
{
   // Constructor of SDManager GUI. 'sdman' is the object which will be managed.

   sdCommon *sdc = sdCommon::GetSdCommon();
   SetCleanup(kDeepCleanup);
   fFont = gClient->GetResourcePool()->GetDefaultFont(); // Font pointer
   fBrowser = 0;

   Int_t buttonId=0;
//   CreateMainGroup3x2(buttonId);
   CreateButtonsGroup4x2(buttonId);

   // Tab(s) staf
	fTab = new TGTab(this, 10, 10);
	AddFrame(fTab, new TGLayoutHints(kLHintsExpandY | kLHintsExpandX));
	fTab->SetCleanup(kDeepCleanup);
	fTab->Associate(this);

	CreateConfigurationTab();

//	const char* color="green";
//	UInt_t indCurrent = 0;
//	CreateResultTabs(color, indCurrent);

	CreateServiceGroup(buttonId);

	CreateStatusBar();

    gROOT->GetListOfCleanups()->Add(this);
    MapSubwindows();
//   fGeneral->HideFrame(fSliderZParent);
//
//   // do not allow resizing
   TGDimension size = GetDefaultSize();
   TString stmp;
   stmp.Form("Signal Detection(LCR):%s %s %s", sdu::GetVersion().Data(),
		   sdc->CPAI2.Data(), gSystem->GetBuildArch());
   SetWindowName(stmp.Data());
   SetIconName("SD : PAI ");

   ConnectControlBarsSlotsOne();

   Resize(size);
   MapWindow();

   gVirtualX->RaiseWindow(GetId());

//   ChangeOptions(GetOptions() | kFixedSize);
   SetWMSize(widthSD, heightSD);
   PrintInfo();
   UInt_t w,h;
   GetWMSize(w,h);
   // cout<<"WM size : w"<<w<<" h "<<h<<endl;
//   gROOT->GetListOfBrowsables(this, "SDGui");
}

void SDGui::CreateMainGroup3x2(Int_t &id)
{
   // Creates the Frame that contains all buttons in matrix (3x2) - obsolete
	TGGroupFrame *mainFrame = new TGGroupFrame(this, "Main frame ", kHorizontalFrame | kFitWidth);

    // Select xml,tif and "start TBrowser" buttons
    UInt_t vsleft=5, vsright=vsleft, vsbottom=10,vstop=vsbottom;
	TGCompositeFrame *vf1 = new TGCompositeFrame(mainFrame, widthSD/2, 26,kVerticalFrame | kFixedWidth| kRaisedFrame);
	vf1->SetCleanup(kDeepCleanup);
	mainFrame->AddFrame(vf1, new TGLayoutHints(kLHintsExpandX, vsleft,vsright, vsbottom,vstop));

	id=0;
	// FontStruct_t labelfont = fFont->GetFontStruct(); // FontStruct_t is UInt_t

	UInt_t hints = kLHintsTop | kLHintsCenterX | kLHintsExpandX;
	fSelectXMLButton = new TGTextButton(vf1, "Select XML file", id++);
	fSelectXMLButton->Associate(this);
	vf1->AddFrame(fSelectXMLButton, new TGLayoutHints(hints, 5, 5, 2, 2));

	fSelectTIFButton = new TGTextButton(vf1, "Select TIFF file", id++);
	fSelectTIFButton->Associate(this);
	vf1->AddFrame(fSelectTIFButton, new TGLayoutHints(hints, 5, 5, 2, 2));

	fReadTIFButton = new TGTextButton(vf1, "Read TIF file", id++);
	fReadTIFButton->Associate(this);
	vf1->AddFrame(fReadTIFButton, new TGLayoutHints(hints, 5,5,2,2));
//	fReadTIFButton->SetOn(kFALSE);
	fReadTIFButton->SetEnabled(kFALSE);

	// Read TIF file, calculate F0 and find clusters  buttons
	TGCompositeFrame *vf2 = new TGCompositeFrame(mainFrame, widthSD/2, 26,kVerticalFrame | kFixedWidth | kRaisedFrame);
	vf2->SetCleanup(kDeepCleanup);
	mainFrame->AddFrame(vf2, new TGLayoutHints(kLHintsExpandX, vsleft,vsright, vsbottom,vstop));

	fCalculateF0Button = new TGTextButton(vf2, titF0Button.Data(), id++);
	fCalculateF0Button->Associate(this);
	vf2->AddFrame(fCalculateF0Button, new TGLayoutHints(hints, 5, 5, 2,2));
//	fCalculateF0Button->SetOn(kFALSE);
	fCalculateF0Button->SetEnabled(kFALSE);

	fFindClustsButton = new TGTextButton(vf2, titCFButton.Data(), id++);
	fFindClustsButton->Associate(this);
	vf2->AddFrame(fFindClustsButton, new TGLayoutHints(hints, 5,5, 2,2));
//	fFindClustsButton->SetOn(kFALSE);
	fFindClustsButton->SetEnabled(kFALSE);

	fSaveButton = new TGTextButton(vf2, "Save Root File", id++);
	fSaveButton->Associate(this);
	vf2->AddFrame(fSaveButton, new TGLayoutHints(hints, 5,5, 2, 2));
//	fSaveButton->SetOn(kFALSE);
	fSaveButton->SetEnabled(kFALSE);

	AddFrame(mainFrame, new TGLayoutHints(kLHintsExpandX, 5, 5, 10, 10));
}

void SDGui::CreateButtonsGroup4x2(Int_t &id)
{
    // Creates the Frame that contains all buttons in matrix (4x2)
	// 2013: Mar 30;
	TGGroupFrame *mainFrame = new TGGroupFrame(this, "Main frame ", kHorizontalFrame | kFitWidth);

    // First column : select xml or tiff files
    UInt_t vsleft=3, vsright=vsleft, vsbottom=3,vstop=vsbottom;
	TGCompositeFrame *vf1 = new TGCompositeFrame(mainFrame, widthSD/3, 26,kVerticalFrame | kFixedWidth| kRaisedFrame);
	vf1->SetCleanup(kDeepCleanup);
	mainFrame->AddFrame(vf1, new TGLayoutHints(kLHintsExpandX, vsleft,vsright, vsbottom,vstop));
	id=1;
	UInt_t hints = kLHintsTop | kLHintsCenterX | kLHintsExpandX;
	fSelectXMLButton = new TGTextButton(vf1, "Select XML file", id++);
	fSelectXMLButton->Associate(this);
	vf1->AddFrame(fSelectXMLButton, new TGLayoutHints(hints, 2,2, 2, 2));

	fSelectTIFButton = new TGTextButton(vf1, "Select TIFF file", id++);
	fSelectTIFButton->Associate(this);
	vf1->AddFrame(fSelectTIFButton, new TGLayoutHints(hints, 2,2, 2, 2));
	// -------------------------------

	// Second column: read TIF file or calculate F0 and find clusters  buttons
	TGCompositeFrame *vf2 = new TGCompositeFrame(mainFrame, widthSD/3, 26,kVerticalFrame | kFixedWidth | kRaisedFrame);
	vf2->SetCleanup(kDeepCleanup);
	mainFrame->AddFrame(vf2, new TGLayoutHints(kLHintsExpandX, vsleft,vsright, vsbottom,vstop));
	fReadTIFButton = new TGTextButton(vf2, "Read TIF file", id++);
	fReadTIFButton->Associate(this);
	fReadTIFButton->SetEnabled(kFALSE);
	vf2->AddFrame(fReadTIFButton, new TGLayoutHints(hints, 2,2,2,2));

	fFill2DHists = new TGTextButton(vf2, "Fill 2D hists", id++);
	fFill2DHists->Associate(this);
	fFill2DHists->SetEnabled(kFALSE);
	vf2->AddFrame(fFill2DHists, new TGLayoutHints(hints, 2,2,2,2));
	// -------------------------------

	// Third column:
	TGCompositeFrame *vf3 = new TGCompositeFrame(mainFrame, widthSD/3, 26,kVerticalFrame | kFixedWidth | kRaisedFrame);
	vf3->SetCleanup(kDeepCleanup);
	mainFrame->AddFrame(vf3, new TGLayoutHints(kLHintsExpandX, vsleft,vsright, vsbottom,vstop));
	fCalculateF0Button = new TGTextButton(vf3, titF0Button.Data(), id++);
	fCalculateF0Button->Associate(this);
	vf3->AddFrame(fCalculateF0Button, new TGLayoutHints(hints, 2,2, 2,2));
	fCalculateF0Button->SetEnabled(kFALSE);

	fFindClustsButton = new TGTextButton(vf3, titCFButton.Data(), id++);
	fFindClustsButton->Associate(this);
	vf3->AddFrame(fFindClustsButton, new TGLayoutHints(hints, 2,2, 2,2));
	fFindClustsButton->SetEnabled(kFALSE);
	// -------------------------------

	// 4th column:
	TGCompositeFrame *vf4 = new TGCompositeFrame(mainFrame, widthSD/3, 26,kVerticalFrame | kFixedWidth | kRaisedFrame);
	vf4->SetCleanup(kDeepCleanup);
	mainFrame->AddFrame(vf4, new TGLayoutHints(kLHintsExpandX, vsleft,vsright, vsbottom,vstop));
	fClustersScanButton = new TGTextButton(vf4, scanCFButton.Data(), id++);
	fClustersScanButton->Associate(this);
	vf4->AddFrame(fClustersScanButton, new TGLayoutHints(hints, 2,2, 2,2));
	fClustersScanButton->SetEnabled(kFALSE);

	fSaveButton = new TGTextButton(vf4, "Save", id++);
	fSaveButton->Associate(this);
	vf4->AddFrame(fSaveButton, new TGLayoutHints(hints, 2,2, 2,2));
	fSaveButton->SetEnabled(kFALSE);

	AddFrame(mainFrame, new TGLayoutHints(kLHintsExpandX, 3,3, 4,4));
}

void SDGui::CreateServiceGroup(Int_t &id)
{
	// Service buttons - don't need a slots definitions
	TGGroupFrame *serviceFrame = new TGGroupFrame(this, "  Service frame ",
			kHorizontalFrame | kFitWidth);

	TGHorizontalFrame *hf1 = new TGHorizontalFrame(serviceFrame, 20, 20, kFitWidth);
	hf1->SetCleanup(kDeepCleanup);

	fBrowserButton = new TGTextButton(hf1, "Start Browser", id++);
	fBrowserButton->Associate(this);
	UInt_t hints = kLHintsTop | kLHintsLeft;
	hf1->AddFrame(fBrowserButton, new TGLayoutHints(hints, 2,2, 2,2));

	fExitButton = new TGTextButton(hf1, "  &Exit  ", id++);
	fExitButton->Associate(this);
	hints = kLHintsTop | kLHintsRight;
	hf1->AddFrame(fExitButton, new TGLayoutHints(hints, 2,2, 2,2));

	serviceFrame->AddFrame(hf1, new TGLayoutHints(kLHintsCenterX | kLHintsExpandX, 3,3, 3,3));
	AddFrame(serviceFrame, new TGLayoutHints(kLHintsCenterX | kLHintsExpandX, 3,3, 2,2));
}

void SDGui::CreateConfigurationTab()
{
    // Create 'Configuration' tab.
	// SetEnabled(kFALSE) for all entries : May 26,2013
    fTabContainer = fTab->AddTab("Configuration");
    fConfTab = new TGCompositeFrame(fTabContainer, 10, 10, kVerticalFrame);
    fTabContainer->AddFrame(fConfTab, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 5, 5, 10, 10));

    // Frame with configuration
    fConfInfo = fSdManager->GetConfigurationParser()->GetLastConfigurationInfo();
    fConfFrame = new TGGroupFrame(fTabContainer, fConfInfo->GetName(), kFitWidth);

    TGTextEntry* arr[3];
    CreateOutFrame(fConfFrame, fConfInfo->GetParsInput(), "TIFF", arr);
    fTEofTifDir  = arr[0];
    fTEofTifName = arr[1];
	SetEnabledTifFrame(kFALSE);

	CreateThreadsFrame(fConfFrame);

	int inew = 1;
    Int_t ng=2; // ng=2 before Feb 24,2013
	if(inew)  {
	    CreateTransformationAndRgbFrame(fConfFrame);
	} else {
		CreatePedestalsFrame(fConfFrame, fConfInfo->GetParsPedestals(), "Pars for F0 calculation", ng);
	}
    SetEnabledTList(fF0Entries, kFALSE);

    CreateClusterFinderFrame(fConfFrame, fConfInfo->GetParsClusterFinder(), "Pars for cluster finder");
    SetEnabledTList(fCFEntries, kFALSE);

    CreateScanFrame(fConfFrame); // June 15,2013

    CreateOutFrame(fConfFrame, fConfInfo->GetParsOutput(), "OUTPUT", arr);
    fTEofOutDir  = arr[0];
    fTEofOutName = arr[1];
	SetEnabledOutFrame(kFALSE);

    fTabContainer->AddFrame(fConfFrame, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 10, 10));
}

void SDGui::CreateOutFrame(TGGroupFrame *gf, parsInputOutput& parsOut, const char* tit,
		TGTextEntry *arr[3])
{
	if (!gf) return;
	TString stmp(tit);

	CreateFrameTitle(gf,tit);

	TGHorizontalFrame *hf2 = new TGHorizontalFrame(gf);
	hf2->SetCleanup(kDeepCleanup);
	TGTextEntry *dir = new TGTextEntry(hf2, parsOut.dir);
	// Int_t tw = 200;
	dir->SetWidth(260);
	arr[0] = dir;
	hf2->AddFrame(dir, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 2,2, 2, 2));

	TGTextEntry *name = new TGTextEntry(hf2, parsOut.name);
	name->SetWidth(140);
	arr[1] = name;
	UInt_t hint = kLHintsRight | kLHintsCenterY;
	if(stmp.Contains("TIFF", TString::kIgnoreCase)) hint = kLHintsLeft | kLHintsCenterY;
	hf2->AddFrame(name, new TGLayoutHints(hint, 2, 2, 2, 2));
	name->SetEnabled(kFALSE);

	if(stmp.Contains("TIF", TString::kIgnoreCase)) {
       // For selection of RGB images id more than one
		fRGBSelection = new TGComboBox(hf2);
		fRGBSelection->SetName("RGBSelection");
		fStringSelected = "no images yet";
		fRGBSelection->AddEntry(fStringSelected.Data(),0);
		fRGBSelection->Select(0);
		fRGBSelection->Resize(120,22);
		hf2->AddFrame(fRGBSelection, new TGLayoutHints(kLHintsRight | kLHintsCenterY, 2,2, 2,2));
	}

	gf->AddFrame(hf2, new TGLayoutHints(kLHintsExpandX, 5, 5,  5,5));
}

void SDGui::SetEnabledTifFrame(Bool_t flag, Int_t indMax)
{
	fTEofTifDir->SetEnabled(flag);
	fTEofTifName->SetEnabled(flag);
	if(fRGBSelection==0) return;
	if(flag==kFALSE || (flag&&indMax>2)) fRGBSelection->SetEnabled(flag);
}

void SDGui::SetEnabledOutFrame(Bool_t flag)
{
	fTEofOutDir->SetEnabled(flag);
	fTEofOutName->SetEnabled(flag);
}

void SDGui::SetEnabledTList(TList *l, Bool_t flag)
{
	if(l && l->GetSize()) {
		for(Int_t i=0; i<l->GetSize(); ++i){
			TGTextEntry* te = dynamic_cast<TGTextEntry *>(l->At(i));
			if(te) {
				te->SetEnabled(flag);
				continue;
			}
			TGNumberEntry* tn = dynamic_cast<TGNumberEntry *>(l->At(i));
			if(tn) {
				tn->SetState(flag);
				continue;
			}
			TGComboBox* cb = dynamic_cast<TGComboBox *>(l->At(i));
			if(cb) {
			  cb->SetEnabled(flag);
			  continue;
			}
			TGButton* but = dynamic_cast<TGButton *>(l->At(i));
			if(but) {
			  but->SetEnabled(flag);
			  continue;
			}
		}
	}
}

void SDGui::CreateFillHistFrame(TGGroupFrame *gf, parsFillHists& pars, const char* tit)
{
	if (!gf) return;

	CreateFrameTitle(gf,tit);

	TGHorizontalFrame *hf2 = new TGHorizontalFrame(gf);
	hf2->SetCleanup(kDeepCleanup);
	TGLabel *labelScale = new TGLabel(hf2, " Scale ");
	hf2->AddFrame(labelScale,
			new TGLayoutHints(kLHintsNormal | kLHintsCenterY, 2, 20, 5, 5));
    fScaleEntry = new TGNumberEntryField(hf2, -1, pars.scale,
    		TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
	hf2->AddFrame(fScaleEntry,
			new TGLayoutHints(kLHintsNormal | kLHintsCenterY, 2, 20, 5, 5));

	TGLabel *labelFilter = new TGLabel(hf2, " Filter ");
	hf2->AddFrame(labelFilter,
			new TGLayoutHints(kLHintsNormal | kLHintsCenterY, 2, 20, 5, 5));
	fFilterEntry = new TGComboBox(hf2);
	Int_t ic=1, i=0;
	fFilterEntry->AddEntry("no filter",ic++);
    // Median filters
	for(i=1; i<=5; i++) // 3x3,5x5 .. 11x11
		fFilterEntry->AddEntry(Form("m%i",i), ic++);
    // Filter from ROOT
    fFilterEntry->AddEntry("r5a",ic++); // 5x5
    fFilterEntry->AddEntry("r5b",ic++); // 5x5
    fFilterEntry->AddEntry("r3a",ic++); // 3x3

    fFilterEntry->Select(1); // no filter
	fFilterEntry->Resize(100,22);

	hf2->AddFrame(fFilterEntry,
			new TGLayoutHints(kLHintsRight | kLHintsCenterY, 2, 20, 5, 5));

	gf->AddFrame(hf2, new TGLayoutHints(kLHintsExpandX, 5, 5,  5,5));
}

void SDGui::CreateThreadsFrame(TGGroupFrame *gf)
{
	// 2013 : Apr 23; May 22;
    TString stmp;
	TGCompositeFrame* vf = new TGCompositeFrame(gf, 60, 20, kVerticalFrame);
	TGLayoutHints* hintVf = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 3, 0);
	gf->AddFrame(vf, hintVf);
	TGCompositeFrame* hf1 = new TGCompositeFrame(vf, 60, 20, kHorizontalFrame);
	vf->AddFrame(hf1, hintVf);

	parsSdThreads& pars = fConfInfo->GetParsThreads();
    stmp.Form("     Threads :   #cpu  %i  ", pars.GetNcpus());

 	TGLayoutHints* hintGf  = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 3, 0);
	fThreadsFrame = new TGGroupFrame(hf1, new TGString(stmp.Data()),kHorizontalFrame | kFixedWidth);
	hf1->AddFrame(fThreadsFrame,hintGf);

	CreateFrame(fThreadsFrame, pars.Class(), (void*)&pars, 0,1, &fThreadsList, 1);

	TGNumberEntryField* intEntry = (TGNumberEntryField*)(fThreadsList.At(0));
    intEntry->Connect("TextChanged(char*)", "SDGui", this, "UpdateThreadsPars(char*)");
    SetEnabledTList(&fThreadsList, kFALSE);
}

void SDGui::CreatePedestalsFrame(TGGroupFrame *gf, parsPedestals& pars, const char* tit, int ng)
{
	if (!gf) return;
	if(strlen(tit)) // Mar 30, 2013;
    CreateFrameTitle(gf,tit);

	fF0Entries = new TList;
	fF0Entries->SetName("F0 Entries");

	Int_t pri = 0;
	Int_t n = pars.Class()->GetNdata() - 1; // Last is fgIsA(type TClass)
	cout<<" number of parsPedestals data member "<<n<<" ng "<<ng<<endl;
	for(Int_t i=0; i<n; i+=ng) {
		Int_t ind1 = i, ind2 = ind1 + ng;
		ind2 = ind2>n?n:ind2;
		CreateFrame(gf, pars.Class(), (void*)&pars, ind1,ind2, fF0Entries, pri);
	}
    fNF0Entries = fF0Entries->GetSize();
}

TGFrame* SDGui::CreateSmoothComboBox(TGHorizontalFrame *hf)
{  // May 29,2013 : algo selection of idl or sd are the same
	TGComboBox *objFrame = new TGComboBox(hf);
	Int_t ic=01, i=0;

	// Mean filters : Default mean filter : 1x8
	for(i=8; i>=4; --i) {
		SmoothComboBox.push_back(Form("mean1x%i",i));
		objFrame->AddEntry(SmoothComboBox.back(), ic++);
	}
	for(i=8; i>=4; --i) {
		SmoothComboBox.push_back(Form("mean0x%i",i));
		objFrame->AddEntry(SmoothComboBox.back(), ic++);
	}

	// Median filters
	for(i=1; i<=5; ++i) {// 3x3,5x5 .. 11x11
		SmoothComboBox.push_back(Form("median%i",i));
		objFrame->AddEntry(SmoothComboBox.back(), ic++);
	}

	// Filter from ROOT
	SmoothComboBox.push_back("r5a"); // 5x5
	objFrame->AddEntry(SmoothComboBox.back(),ic++);
	SmoothComboBox.push_back("r5b"); // 5x5
	objFrame->AddEntry(SmoothComboBox.back(),ic++);
	SmoothComboBox.push_back("r3a"); // 3x3
	objFrame->AddEntry(SmoothComboBox.back(),ic++);
	SmoothComboBox.push_back("no filter");
	objFrame->AddEntry(SmoothComboBox.back(), ic++);

	//nSmoothComboBox = ic;

	vector<const Char_t *>::iterator it;
	for(it=SmoothComboBox.begin(); it!=SmoothComboBox.end(); ++it) {
	  if(strcmp(*it,fConfInfo->GetParsPedestals().smooth.Data())==0) break;
	}
	Int_t ind = distance(SmoothComboBox.begin(),it) + 1;
    objFrame->Select(ind);
	objFrame->Resize(100,22);

	// cout<<" SDGui::CreateSmoothComboBox : smooth "<<fConfInfo->GetParsPedestals().smooth;
	// cout<<" ind (dist +1) "<<ind<<" *it "<<*it<<" nSmoothComboBox "<<nSmoothComboBox<<endl;

	return (TGFrame*)objFrame;
}

void SDGui::AllignSmoothComboBox(TString st)
{   // May 29,2013 : algo selection of idl or sd are the same
	TGComboBox *cbSmooth = dynamic_cast<TGComboBox *>(fF0Entries->At(1));
	parsPedestals& pp = fConfInfo->GetParsPedestals();

	if(st=="idl"){
	    cbSmooth->Select(1);
	} else if(st=="sd"){
		Int_t isel = 0;
		for(UInt_t i=0; i<SmoothComboBox.size(); ++i) {
			if(pp.smooth.Contains(SmoothComboBox[i],TString::kIgnoreCase)) {
				isel = i + 1;
				printf(" SDGui::AllignSmoothComboBox : isel %i i %i %s\n", isel,i, pp.smooth.Data());
			}
		}
	    cbSmooth->Select(isel);
	}
	gSystem->ProcessEvents();
}

TGFrame* SDGui::CreateAlgoComboBox(TGHorizontalFrame *hf)
{
	TGComboBox *objFrame = new TGComboBox(hf);

	AlgoBox.push_back("idl");
	AlgoBox.push_back("sd");
	nAlgoBox = 2;

	for(Int_t i=0; i<nAlgoBox; i++)
	objFrame->AddEntry(AlgoBox[i], i);

	vector<const Char_t *>::iterator it;
	for(it=AlgoBox.begin(); it!=AlgoBox.end(); ++it) {
	  if(strcmp(*it,fConfInfo->GetParsPedestals().algo.Data())==0) break;
	}
	Int_t ind = distance(AlgoBox.begin(),it);
    objFrame->Select(ind);

	objFrame->Resize(80,22);

	return (TGFrame*)objFrame;
}

void SDGui::CreateClusterFinderFrame(TGGroupFrame *gf, parsClusterFinder& pars, const char* tit)
{
	if (!gf) return;

	//  name of cluster finder and include to the frame title
	TString stit(tit);
    stit += pars.name;
    CreateFrameTitle(gf, stit.Data());

    Int_t ng=2;
	fCFEntries = new TList();
	fCFEntries->SetName("CF Entries");

	Int_t n = pars.Class()->GetNdata();
	n = 8; // May 4,2013 -> hide the minsizePxls and mindurationPxls 
	cout<<" number of parsClusterFinder data member "<<n<<endl;
	for(Int_t i=0; i<pars.Class()->GetNdata(); i+=ng) { // Skip the name of CF
		Int_t ind1 = i, ind2 = ind1 + ng;
		ind2 = ind2>n?n:ind2;
		CreateFrame(gf, pars.Class(), (void*)&pars, ind1,ind2, fCFEntries);
	}
	fNCFEntries = fCFEntries->GetSize();
}

void SDGui::CreateTransformationAndRgbFrame(TGGroupFrame *gf)
{
	// Mar 30,2013; Apr 25,2013 - not so understandable
    TString stmp;
	TGCompositeFrame* vf = new TGCompositeFrame(gf, 60, 20, kHorizontalFrame);
	vf->SetWidth(200);
	// kFixedWidth <= kLHintsExpandX : Apr 25,2013
	TGLayoutHints* hintVf = new TGLayoutHints(kLHintsTop | kFixedWidth, 2, 2, 3, 2);
	gf->AddFrame(vf, hintVf);
	TGCompositeFrame* hf1 = new TGCompositeFrame(vf, 60, 20, kHorizontalFrame);
	vf->AddFrame(hf1, hintVf);

	parsTransformation ptrans = fConfInfo->GetParsTranformation();
	Int_t n = ptrans.Class()->GetNdata() - 1; // Discard last member gIsA
    TList *ld = ptrans.Class()->GetListOfDataMembers();
 //   fChk - size ?

 	TGLayoutHints* hintGf  = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 2, 2);
	TGLayoutHints* hintchk = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 0, 5, 0);
	TGGroupFrame* gf1 = new TGGroupFrame(hf1, new TGString("Transformation"),kVerticalFrame | kFixedWidth);
	gf1->SetMaxWidth(150);
	hf1->AddFrame(gf1,hintGf);
	fButtonsGf.Add(gf1);
	gf1->SetEditable(kFALSE);
    for(Int_t i=0; i<n; ++i) {
		TDataMember *dm = dynamic_cast<TDataMember *> (ld->At(i));
		TString dnam(dm->GetName()); // discard first symbol (f);
		if(dm==0) break;

	    TGCheckButton *chk  = new TGCheckButton(gf1, new TGHotString(dnam(1,dnam.Length()-1).Data()), -1);
	    chk->SetName(dnam(1,dnam.Length()-1).Data());
	    gf1->AddFrame(chk, hintchk);
		Int_t iv = sdu::GetPointerInt((void*)(&ptrans),dm->GetOffset());
        if(iv) chk->SetOn(kTRUE);
        if(i==3) chk->SetEnabled(kFALSE);
        fTransformChksList.Add(chk);
        chk->Connect("Clicked()", "SDGui", this, "UpdateTransformationPars()");
        chk->SetEnabled(kFALSE);
    }

    if(fTransColor) { // 0 0r 1?
	   TGGroupFrame* gf2 = new TGGroupFrame(hf1, new TGString("Channels"),kVerticalFrame|kRaisedFrame);
	   hf1->AddFrame(gf2,hintGf);
	   Int_t nmax = 9; // March 31;
	   nmax = nmax>n-1?n-1:nmax;
       for(Int_t i=4; i<nmax; ++i) {
		   TDataMember *dm = dynamic_cast<TDataMember *> (ld->At(i));
		   TString dnam(dm->GetName()); // discard first symbol (f);
		   if(dm==0) break;
	       TGCheckButton *chk  = new TGCheckButton(gf2, new TGHotString(dnam(1,dnam.Length()-1).Data()), -1);
	       stmp.Form("%i chk",i);
	       chk->SetName(stmp.Data());
	       gf2->AddFrame(chk, hintchk);
		   Int_t iv = sdu::GetPointerInt((void*)(&ptrans),dm->GetOffset());
           if(iv) chk->SetOn(kTRUE);
           fTransformChksList.Add(chk);
           chk->Connect("Clicked()", "SDGui", this, "DoTransformationCheckButton() ???");
           chk->SetEnabled(kTRUE);
        }
	    fButtonsGf.Add(gf2);
    }

	stmp.Form("Pars for F0 calculation");
	TGCompositeFrame* hf2 = new TGCompositeFrame(vf, 60, 20, kHorizontalFrame);
	vf->AddFrame(hf2, hintVf);
	TGGroupFrame* gf3 = new TGGroupFrame(hf2, new TGString(stmp.Data()),kVerticalFrame|kRaisedFrame);
	TGLayoutHints* hintGf3  = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2, 2, 3, 0);
	hf2->AddFrame(gf3,hintGf3);
	CreatePedestalsFrame(gf3, fConfInfo->GetParsPedestals(), "", 3);
}

void SDGui::CreateScanFrame(TGGroupFrame *gf)
{
	// 2013 : June 13;
    TString stmp;
	TGCompositeFrame* vf = new TGCompositeFrame(gf, 60, 20, kVerticalFrame);
	TGLayoutHints* hintVf = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 1, 1, 1, 0);
	gf->AddFrame(vf, hintVf);
	TGCompositeFrame* hf1 = new TGCompositeFrame(vf, 60, 20, kHorizontalFrame);
	vf->AddFrame(hf1, hintVf);

	parsSdScan pars = fConfInfo->GetParsSdScan();
	Int_t n = pars.Class()->GetNdata() - 1; // Last is fgIsA(type TClass)
    stmp.Form("     Clusters Scan");

 	TGLayoutHints* hintGf  = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 2,2, 1, 0);
	fScanFrame = new TGGroupFrame(hf1, new TGString(stmp.Data()),kHorizontalFrame | kFixedWidth);
	hf1->AddFrame(fScanFrame,hintGf);

	CreateFrame(fScanFrame, pars.Class(), (void*)&pars, 0,n, &fScanList, 1);

    for(Int_t i=0; i<n; ++i)
    {
	   TGNumberEntryField* intEntry = (TGNumberEntryField*)(fScanList.At(i));
       intEntry->Connect("TextChanged(char*)", "SDGui", this, "UpdateScanPars(char*)");
    }
    SetEnabledTList(&fScanList, kFALSE);
	fNShift = pars.GetNpScan()<=0?2:4;
}

// Utils
void SDGui::CreateFrameTitle(TGGroupFrame *gf, const char* tit)
{
	// Apr 24,2013
	TGHorizontalFrame *hf1 = new TGHorizontalFrame(gf,1,1);
	hf1->SetName(tit); // for TBrowser
	hf1->SetCleanup(kDeepCleanup);
	TGLabel *label = new TGLabel(hf1, tit);
	hf1->AddFrame(label,
			new TGLayoutHints(kLHintsNormal | kLHintsCenterY, 2, 2, 5, 5));
	 TGHorizontal3DLine *hline1 = new TGHorizontal3DLine(hf1);
	hf1->AddFrame(hline1, new TGLayoutHints(kLHintsCenterY | kLHintsExpandX));
	gf->AddFrame(hf1, new TGLayoutHints(kLHintsExpandX, 2, 2, 2,2));
}

void SDGui::CreateFrame(TGGroupFrame *gf, TClass *cl, void *pars,UInt_t ind1,UInt_t ind2,TList *l, Int_t pri)
{
	// 2013: Feb 24;
	if(pri) cout<<" SDGui::CreateFrame : class "<<cl->GetName() << endl;

    TList *ld = cl->GetListOfDataMembers();

	TGHorizontalFrame *hf = new TGHorizontalFrame(gf);
	hf->SetCleanup(kDeepCleanup);
	UInt_t hints = kLHintsNormal | kLHintsCenterY;
	Float_t vf=0.0;
	Int_t   vi=0, iadd = 0;
	TString st;

	Int_t np = ind2 - ind1 + 1;
	for (UInt_t i = ind1; i < ind2; i++) {
		TDataMember *dm = dynamic_cast<TDataMember *> (ld->At(i));
		if(dm==0) break;
		TString stmp(dm->GetName());
		if(stmp[0]=='f') stmp = (dm->GetName()+1); // Discard first f if exists

		TGFrame* entryObj=0;
		TGHorizontalFrame *hfi = new TGHorizontalFrame(hf);
		hfi->SetCleanup(kDeepCleanup);
		TGLabel *label = new TGLabel(hfi, stmp.Data());
		hfi->AddFrame(label,
				new TGLayoutHints(kLHintsNormal | kLHintsCenterY, 1, 1, 1, 1));
		if (strcmp(dm->GetTypeName(), "Float_t") == 0) {
			vf = sdu::GetPointerFloat(pars,dm->GetOffset());
			entryObj = new TGNumberEntryField(hfi, -1, vf,
					TGNumberFormat::kNESReal, TGNumberFormat::kNEAAnyNumber);
			entryObj->SetWidth(60);
		} else if (strcmp(dm->GetTypeName(), "Int_t") == 0) {
//			if(strcmp(dm->GetName(),"set")!=0) { // miss variable set
			  vi = sdu::GetPointerInt(pars,dm->GetOffset());
			  entryObj = new TGNumberEntryField(hfi, -1, vi,
					TGNumberFormat::kNESInteger, TGNumberFormat::kNEAAnyNumber); // June 16 : allow any integer
			  entryObj->SetWidth(60);
//			}
		} else if (strcmp(dm->GetTypeName(), "TString") == 0) {
			st = sdu::GetPointerTString(pars,dm->GetOffset());
			if(stmp == "smooth") {
				entryObj = CreateSmoothComboBox(hfi);
			} else if(stmp == "algo") {
				entryObj = CreateAlgoComboBox(hfi);
			} else {
			  entryObj = new TGTextEntry(hfi, st.Data());
		      entryObj->SetWidth(80); // in pixels
			}
		} else if (strcmp(dm->GetTypeName(), "TClass") == 0) {
			entryObj = 0;
			continue;
		}
		hints = kLHintsLeft | kLHintsCenterY;
		if(i != ind1 && np<4) hints = kLHintsRight| kLHintsCenterY;
		if (entryObj) {
			iadd++;
			entryObj->SetName(stmp.Data()); // Apr 16,2013
			l->Add(entryObj);
			hfi->AddFrame(entryObj,
					new TGLayoutHints((kLHintsRight | kLHintsCenterY), 3,10, 1,1));
			hfi->SetWidth(200);
			hf->AddFrame(hfi, new TGLayoutHints(hints, 1, 1, 1, 1));
		}
		if(pri) {
		  cout << i<<"\t "<<dm->GetName() << "\t type " << dm->GetTypeName() << "\t vi "<<vi<<"\t vf "<<vf;
		  cout<<"\t offset "<<dm->GetOffset()<<"\t entryObj "<<entryObj<<endl;
		}
	} // Cycle on data members
	if(iadd>0) gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2,2, 1,1));
}

void SDGui::CreateFrameText(TGGroupFrame *gf, TClass *cl,void* pars, UInt_t ind1,UInt_t ind2,TGTextEntry** entries)
{
	// As CreateFrmaeTwo for text input
    TList *ld = cl->GetListOfDataMembers();

	TGHorizontalFrame *hf = new TGHorizontalFrame(gf);
	hf->SetName("hfclf");
	hf->SetCleanup(kDeepCleanup);
	UInt_t hints = kLHintsNormal | kLHintsCenterY;
	for (UInt_t i = ind1; i < ind2; i++) {
		Int_t iEntry = i-ind1;
		TGHorizontalFrame *hfi = new TGHorizontalFrame(hf);
		hfi->SetCleanup(kDeepCleanup);
		TDataMember *dm = dynamic_cast<TDataMember *> (ld->At(i));
		TGLabel *label = new TGLabel(hfi, dm->GetName());
		hfi->AddFrame(label,
				new TGLayoutHints(kLHintsNormal | kLHintsCenterY, 2, 10, 2, 2));
		if (strcmp(dm->GetTypeName(), "TString") == 0) {
			TString* as = reinterpret_cast<TString *>(sdu::GetPointer(pars,dm->GetOffset()));
			entries[iEntry] = new TGTextEntry(hfi, as->Data());
			entries[iEntry]->SetWidth(80); // in pixels
		    hfi->AddFrame(entries[iEntry], new TGLayoutHints(hints, 2, 20, 5, 5));
		    hf->AddFrame(hfi, new TGLayoutHints(hints, 5, 5, 2, 2));
			if (i == ind1) hints = kLHintsRight | kLHintsCenterY;
		} else {
			cout<<"<W> SDGui::CreateFrameText : wrong data type "<<dm->GetTypeName()<<endl;
		}
	}
	gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 5, 5, 2, 2));
}

TDataMember* SDGui::FindIndexOfDataMember(TPair* pair, TClass *cl)
{
	// 2013: Feb 24
	// obj is some graphics entry like TGNumberEntryField
	// cl - TClass
	TDataMember *dm = 0;
	if(pair && cl) {
		Int_t i=0;
	    TList *ld = cl->GetListOfDataMembers();
	    TString snam = pair->Key()->GetTitle();
	    for(i=0; i<ld->GetSize(); ++i) {
	       dm = dynamic_cast<TDataMember *> (ld->At(i));
	       if(snam == dm->GetName()) break;
	    }
	    if(dm) cout<<" i "<<i<<" snam "<<snam<<" dm->GetName() "<<dm->GetName()<<endl;
	}
	return dm;
}

void SDGui::UpdateF0andCFandScanButtons(const char* newtext)
{
	TString newLabel = titF0Button + newtext;
	fCalculateF0Button->SetText(newLabel);
	newLabel = titCFButton + newtext;
	fFindClustsButton->SetText(newLabel);
	newLabel = scanCFButton + newtext;
	fClustersScanButton->SetText(newLabel);

	fStringSelected = newtext; // Keep here
	// Get new images and pedestal
	fImgAna  = fTifInfo->GetImagesPool()->GetSDImageAnalysis(fStringSelected.Data());
	fImgAna->SetParsScan(fConfInfo->GetParsSdScan()); // important
	fPed     = fImgAna->GetPedestals();
}

void SDGui::UpdateTabConfigurationInfo(SDConfigurationInfo* conf)
{
	// Oct 27,2012
	// 2013 : June 2,15,29
	Int_t pri=0;
	fConfFrame->SetTitle(conf->GetName());
	//
	fTEofTifDir->SetText(conf->GetParsInput().dir.Data());
	fTEofTifName->SetText(conf->GetParsInput().name.Data());
	//
	parsSdThreads& pthreads = conf->GetParsThreads();
	if(pri) cout<<" pp "<<pthreads<<endl;
    UpdateNumbersEntries(pthreads.Class(),(void *)&pthreads, &fThreadsList, pri);

	parsTransformation& ptrans = conf->GetParsTranformation(); // June 30,3013
	if(pri) cout<<" ptrans "<<ptrans<<endl;
    UpdateNumbersEntries(ptrans.Class(),(void *)&ptrans, &fTransformChksList, 1);

//    if(fTifInfo) {
//       SDTransformation *trans = fTifInfo->GetSDTransformation();
//	   if(trans) {
//			  parsTransformation& ptrans = trans->GetPars();
//			  if(pri) cout<<" ptrans "<<ptrans<<endl;
//		      UpdateNumbersEntries(ptrans.Class(),(void *)&ptrans, &fTransformChksList, 1);
//	   }
//    }

	parsPedestals& pp = conf->GetParsPedestals();
    if(pri) cout<<" pp "<<pp<<endl;
	UpdateNumbersEntries(pp.Class(),(void *)&pp, fF0Entries, pri);

	parsClusterFinder& pcf = conf->GetParsClusterFinder();
    if(pri) cout<<" pcf "<<pcf<<endl;
	UpdateNumbersEntries(pcf.Class(),(void *)&pcf,fCFEntries, pri);
	//
	pri=0;
	parsSdScan& psc = conf->GetParsSdScan();
	if(pri) cout<<" psc "<<psc<<endl;
	UpdateNumbersEntries(psc.Class(),(void *)&psc, &fScanList, pri);
	//
	fTEofOutDir->SetText(conf->GetParsOutput().dir.Data());
	fTEofOutName->SetText(conf->GetParsOutput().name.Data());
	//
	gSystem->ProcessEvents();
}

void SDGui::UpdateNumbersEntries(TClass *cl,void* p, TList *l, Int_t pri)
{
    // Put current values of pars to the GUI; May 26,2013
	// Don't touch this without necessity
	UInt_t n = l->GetSize();
	if (pri) cout << "\n\t  SDGui::UpdateNumbersEntries : class " << cl->GetName() << " n " << n << endl;
	TObject *objEntry = 0;
    TList* ldm = cl->GetListOfDataMembers();
	for (UInt_t i = 0; i < n; ++i) {
        TDataMember *dm = dynamic_cast<TDataMember *> (ldm->At(i));
		if(dm==0) continue;

		objEntry = l->At(i); // May 26,2013 - error was here
		TString stmp;
		if (strcmp(dm->GetTypeName(), "Float_t") == 0) {
			Float_t vf = sdu::GetPointerFloat(p, dm->GetOffset());
			stmp.Form("%7.5f", vf);
            while(stmp.EndsWith("0")){ // discard "0" at the end
            	stmp.Remove(stmp.Length()-1);
            }
			TGNumberEntryField *entry = dynamic_cast<TGNumberEntryField *>(objEntry);
    		if(entry) entry->SetText(stmp.Data());
		} else if (strcmp(dm->GetTypeName(), "Int_t") == 0) {
			Int_t vi = sdu::GetPointerInt(p,dm->GetOffset());
			stmp += vi;
            TGNumberEntryField *entry = dynamic_cast<TGNumberEntryField *>(objEntry);
    		if(entry) {
    			entry->SetText(stmp.Data());
    		} else { // June 30,2013 for check button
    		   TGCheckButton *chkb = dynamic_cast<TGCheckButton *>(objEntry);
    		   if(chkb) {
    			  if (pri) cout <<" TGCheckButton : vi "<<vi<<" dm->GetName() " << dm->GetName()<<endl;
    			  chkb->SetEnabled(kTRUE);
    		      if(vi>0) chkb->SetOn(kTRUE);
    		      else     chkb->SetOn(kFALSE);
    			  chkb->SetEnabled(kFALSE);
    		   }
    		}
		} else if (strcmp(dm->GetTypeName(), "TString") == 0) {
            stmp = sdu::GetPointerTString(p,dm->GetOffset());
            TGTextEntry *entry = dynamic_cast<TGTextEntry *>(objEntry);
    		if(entry) entry->SetText(stmp.Data());
    		objEntry->Dump();
		}
		if (pri)
			cout << i << "\t" << dm->GetName() << "\t" << dm->GetTypeName()
					<< " val \t" << stmp << " objEntry \t"<<objEntry<<endl;
	}
	//Update
	gSystem->ProcessEvents();
}

void SDGui::CreateResultTabs(const char* color, UInt_t &indCurrent)
{
	//    2013: Mar 13; June 23;
	Int_t pri = 0;
	const char* rtabs[]={"Result ","(CSV) ", "Scan Result ", "Scan (CSV) "};
	const char* rtabsTit[]={"Result:text file ","Result: CSV text file ","Result with Scan:text file ",
			"Result with Scan:CSV text file "};
    UInt_t n = fNShift; // June 23;
	indCurrent = fResultTabs.size(); // Start from 0 first time : June 23,2013
    if(pri) cout<<" SDGui::CreateResultTabs : color "<<color<<" indCurrent "<<indCurrent<<endl;
	for(UInt_t i=0; i<n; ++i) {
	  tabRec rec;
	  TString stmp(rtabs[i]), stmpTit = rtabsTit[i];
	  stmp += color; stmpTit += color;
	  fTabContainer = fTab->AddTab(stmp.Data());
	  rec.compFrame = new TGCompositeFrame(fTabContainer, 10, 10, kVerticalFrame);
	  fTabContainer->AddFrame(rec.compFrame,new TGLayoutHints(kLHintsTop | kLHintsExpandX, 5, 5, 2, 2));

	  rec.groupFrame = new TGGroupFrame(fTabContainer, stmpTit.Data(), kFitWidth);

	  rec.textEdit = new TGTextEdit(rec.groupFrame, 600, 400, kSunkenFrame | kDoubleBorder);
	  rec.groupFrame->AddFrame(rec.textEdit, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 3, 3, 3, 3));
	  rec.textEdit->EnableMenu(kTRUE);

	  fTabContainer->AddFrame(rec.groupFrame, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 10, 10));
	  fResultTabs.push_back(rec);
	}
}

void SDGui::CreateResultTabsOld()
{
	// Result tabs
	const char* rtabs[]={"Result ","Result(CSV)"};
	const char* rtabsTit[]={"Result as Text file","Result as CSV text file"};
	Int_t fNResTabs = sizeof(rtabs)/sizeof(char*);
	for(int i=0; i<fNResTabs; ++i) {
	  fTabContainer = fTab->AddTab(rtabs[i]);
	  fResultTab[i] = new TGCompositeFrame(fTabContainer, 10, 10, kVerticalFrame);
	  fTabContainer->AddFrame(fResultTab[i],
			new TGLayoutHints(kLHintsTop | kLHintsExpandX, 5, 5, 2, 2));

	  fResultFrame[i] = new TGGroupFrame(fTabContainer, rtabsTit[i], kFitWidth);

	  fEdit[i] = new TGTextEdit(fResultFrame[i], 600, 400, kSunkenFrame | kDoubleBorder);
	  fResultFrame[i]->AddFrame(fEdit[i], new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 3, 3, 3, 3));
	  fEdit[i]->EnableMenu(kTRUE);

	  fTabContainer->AddFrame(fResultFrame[i],
			new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 10, 10));
	}
}

//void SDGui::DoOpen()
//{
//   SetTitle();
//}

void SDGui::CreateStatusBar()
{
	   Int_t parts[] = { 20, 20, 20, 20, 20 };
	   Int_t nparts = sizeof(parts) / sizeof(Int_t);
	   fStatusBar = new TGStatusBar(this, 10, 10);
	   fStatusBar->SetParts(parts, nparts);
	   AddFrame(fStatusBar, new TGLayoutHints(kLHintsBottom |
	                                          kLHintsLeft   |
	                                          kLHintsExpandX));
}

void SDGui::ConnectControlBarsSlotsOne()
{
  fSelectXMLButton->Connect("Clicked()", "SDGui", this, "SelectXMLFileDialog()");
  fSelectTIFButton->Connect("Clicked()", "SDGui", this, "SelectTifFileDialog()");
  // Browser&Exit button
  fBrowserButton->Connect("Clicked()", "SDGui", this, "StartTBrowser()");
  fExitButton->Connect("Clicked()", "SDGui", this, "ExitFromRoot()");
}

void SDGui::ConnectControlBarsSlotsTwo()
{
  fReadTIFButton->Connect("Clicked()", "SDGui", this, "ReadTIF()");
  fCalculateF0Button->Connect("Clicked()","SDGui", this , "CalculateF0()");
  fFill2DHists->Connect("Clicked()", "SDGui", this, "Fill2DHists()");
  fFindClustsButton->Connect("Clicked()", "SDGui", this, "FindClusters()");
  fClustersScanButton->Connect("Clicked()", "SDGui", this, "ScanClusters()");
  fSaveButton->Connect("Clicked()", "SDGui", this, "SaveSD()");
}

void SDGui::ConnectConfigurationSlots(Int_t pri)
{
  // Should be called after xml(tif) file selection
  // Tif
  if(pri) cout<<"ConnectConfigurationSlots() is started"<<endl;

  fTEofTifDir->Connect("TextChanged(char*)","SDGui",this,"UpdateTifDir(char*)");
  fTEofTifName->Connect("TextChanged(char*)","SDGui",this,"UpdateTifName(char*)");
  fRGBSelection->Connect("Selected(char*)","SDGui",this,"UpdateRGBSelection(char*)"); // Mar 9;
  // F0 calculation
  sdu::GetFrame(fF0Entries,0)->Connect("Selected(char*)","SDGui",this,"UpdateF0Algo(char*)");
  sdu::GetFrame(fF0Entries,1)->Connect("Selected(char*)","SDGui",this,"UpdateFilter(char*)");

  sdu::GetFrame(fF0Entries,2)->Connect("TextChanged(char*)","SDGui",this,"UpdateChi2Cut(char*)");
  sdu::GetFrame(fF0Entries,3)->Connect("TextChanged(char*)","SDGui",this,"UpdateSet(char*)");
  sdu::GetFrame(fF0Entries,4)->Connect("TextChanged(char*)","SDGui",this,"UpdateSet(char*)");
  sdu::GetFrame(fF0Entries,5)->Connect("TextChanged(char*)","SDGui",this,"UpdateNsig(char*)");
  // Cluster Finder
  sdu::GetFrame(fCFEntries,0)->Connect("TextChanged(char*)","SDGui",this,"UpdateCFName(char*)");
  sdu::GetFrame(fCFEntries,1)->Connect("TextChanged(char*)","SDGui",this,"UpdateSeed(char*)");
  sdu::GetFrame(fCFEntries,2)->Connect("TextChanged(char*)","SDGui",this,"UpdateTimeScale(char*)");
  sdu::GetFrame(fCFEntries,3)->Connect("TextChanged(char*)","SDGui",this,"UpdateSpaceScale(char*)");
  sdu::GetFrame(fCFEntries,4)->Connect("TextChanged(char*)","SDGui",this,"UpdateMinPixels(char*)");
  sdu::GetFrame(fCFEntries,5)->Connect("TextChanged(char*)","SDGui",this,"UpdateMinSize(char*)");
  sdu::GetFrame(fCFEntries,6)->Connect("TextChanged(char*)","SDGui",this,"UpdateMinDuration(char*)");
  sdu::GetFrame(fCFEntries,7)->Connect("TextChanged(char*)","SDGui",this,"UpdateDrawing(char*)");
   // Out
  fTEofOutDir->Connect("TextChanged(char*)","SDGui",this,"UpdateOutputDir(char*)");
  fTEofOutName->Connect("TextChanged(char*)","SDGui",this,"UpdateOutputName(char*)");
  //  fSelectFont->Connect("Clicked()", "SDGui", this, "FontDialog(");

  if(pri) cout<<"ConnectConfigurationSlots() is ended"<<endl;
}
//
//   S l o t s     S l o t s     S l o t s     S l o t s
//
// Manage of program
void SDGui::SelectXMLFileDialog()
{
	if(fSdManager->GetStatus() < SDManager::kInitMain) {
		cout<<"<W> Before SelectXmlFile() InitMain() should be call \n";
	}
	if(fSdManager->SelectXMLFileDialog()==kFALSE) return;
	ConnectConfigurationSlots();

	fSdManager->InitConfiguration();
	fConfInfo = fSdManager->GetConfigurationParser()->GetLastConfigurationInfo();

	UpdateButtonsState();
	UpdateTabConfigurationInfo(fConfInfo);

	SetEnabledTifFrame(kTRUE);
}

void SDGui::SelectTifFileDialog()
{
	if(fSdManager->GetStatus() < SDManager::kInitMain) {
		cout<<"<W> Before SelectTifFile() InitMain() should be call \n";
	}
	if(fSdManager->SelectTifFileDialog()==kFALSE) return;
	ConnectConfigurationSlots();

	// Update information on GUI window
	fSdManager->InitConfiguration();
	fConfInfo = fSdManager->GetConfigurationParser()->GetLastConfigurationInfo();

	UpdateButtonsState();

	UpdateTabConfigurationInfo(fConfInfo);

	SetEnabledTifFrame(kTRUE);
}

void SDGui::ReadTIF()
{
	if(fSdManager->GetStatus() < SDManager::kInitConfiguration) {
		fSdManager->InitConfiguration();
	}

	fSelectXMLButton->SetEnabled(kFALSE);
	fSelectTIFButton->SetEnabled(kFALSE);
	fReadTIFButton->SetEnabled(kFALSE);
	SetEnabledTifFrame(kFALSE); // July 3;

	fFill2DHists->SetEnabled(kTRUE);

	fTEofTifDir->SetEditDisabled(1);
	fTEofTifName->SetEditDisabled(1);

	fTifInfo = fSdManager->ReadTif();

	SetEnabledTList(&fTransformChksList, kTRUE);
	fTifInfo->GetSDTransformation()->SetPars(fConfInfo->GetParsTranformation());
	SetEnabledTList(&fThreadsList, kTRUE);
	if(fTransColor) SetCheckButtonsAfterReadTIF();
}

void SDGui::Fill2DHists()
{
	// 2013: Mar 30.
	TStopwatch watch;
	watch.Start();
	fFill2DHists->SetEnabled(kFALSE);
	SetEnabledTList(&fTransformChksList, kFALSE);

	fTifInfo = fSdManager->GetTiffFileInfo();
	fTifInfo->Fill2DHists();

	if(fTifInfo->GetImagesStatus().size()>1){
		fTEofTifDir->SetEnabled(kFALSE);
		fTEofTifName->SetEnabled(kFALSE);

		for(UInt_t i=0; i<fTifInfo->GetImagesStatus().size(); ++i){
			imageStatus pr = fTifInfo->GetImagesStatus()[i];
			if(pr.status==SDTiffFileInfo::kAvailable) {
				TString stmp = pr.color;
				fRGBStrings.push_back(stmp);
			}
		}
		fRGBSelection->RemoveAll();
		for(UInt_t i=0; i<fRGBStrings.size(); ++i) fRGBSelection->AddEntry(fRGBStrings[i],i);
		if(fRGBStrings.size()>=1) {
		   fRGBSelection->Select(0);
		   UpdateF0andCFandScanButtons(fRGBStrings[0]);
		   if(fRGBStrings.size()>1) fRGBSelection->SetEnabled(kTRUE); // First string is selected color
		}
		cout<<"<I> ReadTiff  #Combo entries "<<fRGBStrings.size()<<endl;
	} else if(fTifInfo->GetImagesStatus().size()==1) {
		fRGBSelection->RemoveAll();
		fRGBSelection->AddEntry("Grey",0);
		UpdateF0andCFandScanButtons("Grey");
		fRGBSelection->Select(0);
		SetEnabledTifFrame(kFALSE);
	} else { // no picture at all
		TString msg("No pictures!");
		new TGMsgBox(gClient->GetRoot(), 0, "No TIFF image ", msg.Data()
			,kMBIconExclamation, 0);
		// TO DO here something
	}

	SetEnabledTList(&fThreadsList, kFALSE);
	fCalculateF0Button->SetEnabled(kTRUE);
	SetEnabledTList(fF0Entries, kTRUE);
	// CF is not available yet.
	// You have to create CF when all parameters were defined
	fFindClustsButton->SetEnabled(kTRUE);
	SetEnabledTList(fCFEntries,kTRUE);

	// June 15-16,2013
	SetEnabledTList(&fScanList,kTRUE); // Should be available in any case
	if(fConfInfo->GetParsSdScan().fNpScan>0) fClustersScanButton->SetEnabled(kTRUE);

	fImgAna  = fTifInfo->GetImagesPool()->GetSDImageAnalysis(fStringSelected.Data());
	fPed = fImgAna->GetPedestals();

	if(fTransColor) SetCheckButtonsAfterFill2DHists();
	watch.Stop();
	printf("SDGui::Fill2DHists() %s => Cpu %6.3f(sec) Real  %6.3f(sec) \n ",
		fImgAna->GetImageStatus().color.Data(), watch.CpuTime(), watch.RealTime());
}

void SDGui::CalculateF0()
{
	// Mar 5,2013
	TStopwatch watch;
	watch.Start();
	Int_t pri = 0;
	if(pri) {
		printf("<I> SDGui::CalculateF0() : fStringSelected.Data() %s \n", fStringSelected.Data());
		if(pri>1) fImgAna->Dump();
	}
	if(fSdManager->GetStatus() < SDManager::kReadTiff) ReadTIF();

	fCalculateF0Button->SetEnabled(kFALSE);
	SetEnabledTList(fF0Entries, kFALSE);
	fRGBSelection->SetEnabled(kFALSE);

    fPed->SetPedestalsPars(fConfInfo->GetParsPedestals());
	TH2* h2 = fTifInfo->GetTifAs2DHist(fStringSelected.Data());
	fPed->SetInput2DHist(h2);

	fPed->DoAll();
	fImgAna->SetImageStatus(SDTiffFileInfo::kF0Done);

	SetEnabledTList(fCFEntries,kTRUE);

	watch.Stop();
	printf("SDGui::CalculateF0() %s => Cpu %6.3f(sec) Real  %6.3f(sec) \n ",
		fImgAna->GetImageStatus().color.Data(), watch.CpuTime(), watch.RealTime());
}

void SDGui::FindClusters()
{
	// Mar 5,2013
	UInt_t pri = 0;
	fImgAna  = fTifInfo->GetImagesPool()->GetSDImageAnalysis(fStringSelected.Data());
	if(fImgAna->GetImageStatus().status<SDTiffFileInfo::kF0Done){
	   CalculateF0();
	}
	TStopwatch watch;
	watch.Start();

	fCF  = fImgAna->GetClusterFinder(); // Initialized CF here
	fFindClustsButton->SetEnabled(kFALSE);
	SetEnabledTList(fCFEntries,kFALSE);

	// Apr 14,2013
	Int_t nclust=0;
	Bool_t keyThreads = fConfInfo->GetParsThreads().IsMultiThreadingPossible();
	if(keyThreads) {
	   nclust = fCF->FindClsutersWithThreads();
//	   nclust = cf->FindClsutersWithPosixThreads();
	} else {
	   nclust = fCF->FindClusters();
	}
//	GetListHistsInputForCF()->Add(clf->GetMainHists()->At(1)); // Add remnant hists
	imageStatus stat = fImgAna->GetImageStatus();
	parsSdScan  psc = fImgAna->GetParsScan();
	fImgAna->SetImageStatus(SDTiffFileInfo::kCFDone);

    UInt_t indCurrent = 0; // return fTabs index
	if(nclust>0) {
	    CreateResultTabs(stat.color.Data(), indCurrent);
	    fTab->MapSubwindows(); // These are
	    fTab->Layout();        // very important

	    // Should be called in any case
	    UInt_t scan = 0;
	    CreateOutputTextFiles(indCurrent, scan);

        UInt_t nRgbSelection = fRGBSelection->GetNumberOfEntries();
		if( nRgbSelection > 1) {
			if(psc.GetNpScan()<=0) RGBImagesMoreThanOne(pri);
		// Get new images and pedestal
		    fImgAna  = fTifInfo->GetImagesPool()->GetSDImageAnalysis(fStringSelected.Data());
		    fPed     = fImgAna->GetPedestals();
		} else { // fRGBSelection->GetNumberOfEntries() == 1
            if(psc.GetNpScan()<=0) RGBnoImages();
		}
        ResetGUI();
	}

	SetEnabledOutFrame(kTRUE);
	watch.Stop();
	printf("SDGui::FindCluster() %s : status %i => Cpu %6.3f(sec) Real  %6.3f(sec)\n",
		fImgAna->GetImageStatus().color.Data(), fImgAna->GetImageStatus().status,
		watch.CpuTime(), watch.RealTime());
}

void SDGui::ScanClusters()
{
	// 2013: June 16,23;
	static UInt_t  indCurrent = 2; // 2 for first color
	UInt_t pri = 0;
	fImgAna = fTifInfo->GetImagesPool()->GetSDImageAnalysis(fStringSelected.Data());
	if(fImgAna->GetImageStatus().status < SDTiffFileInfo::kCFDone) FindClusters();

	fClustersScanButton->SetEnabled(kFALSE);
	SetEnabledTList(&fScanList,kFALSE);
	gSystem->ProcessEvents();

	TStopwatch watch;
	watch.Start();

    pri = 0;
	fImgAna->ClustersScan(pri);
    UInt_t  scan = 1;
     CreateOutputTextFiles(indCurrent, scan);

	UInt_t nRgbSelection = fRGBSelection->GetNumberOfEntries();
	if( nRgbSelection > 1) {
		RGBImagesMoreThanOne(pri);
	} else { // fRGBSelection->GetNumberOfEntries() == 1
        RGBnoImages();
	}

	watch.Stop();
	printf("SDGui::ScanClusters() %s : status %i => Cpu %6.3f(sec) Real  %6.3f(sec) \n ",
		fImgAna->GetImageStatus().color.Data(), fImgAna->GetImageStatus().status,
		watch.CpuTime(), watch.RealTime());

	indCurrent += 4; // Go to next color
}

void SDGui::SaveSD()
{
	const char *openTypes[] = { "ROOT files",   "*.root",
	                            "All files",    "*",
	                            0,              0 };
	// You can save SDManager at any time!
	Int_t pri = 0;
    TString dir;
	SDConfigurationInfo* confInfo = fSdManager->GetConfigurationParser()->GetLastConfigurationInfo();
	parsInputOutput fout = confInfo->GetParsOutput();
	TString absname, name;

	TGFileInfo fi;
	fi.fFileTypes = openTypes;
	fi.fIniDir = StrDup(fout.dir.Data());
	TString templateName = fout.name;
	fi.fFilename = StrDup(templateName.Data());

	new TGFileDialog(gClient->GetRoot(), 0, kFDSave, &fi); // Should be on heap !

	absname = StrDup(fi.fFilename);
	if(pri) cout << "<I> SDGui::SaveSD() : absname root file " << absname << endl;
	name = absname;
	absname.ReplaceAll(".root", "");
	fSdManager->SetFileNameWithoutExtension(absname);
	// Put info to configuration file
	fout.dir = StrDup(fi.fIniDir);
	name.ReplaceAll(fout.dir, "");
	fout.name = name;
	if(pri) cout << "<I> SDGui::SaveSD() : Name of root file " << fout.name << endl;
	TString tit = "Job is done!", msg;

	if(fout.name != "nosave") {
		msg = Form("Root File %s ",name.Data());
		fSdManager->SaveSD();
	} else {
		msg = "No saving to root file.";
	}
	SetEnabledOutFrame(kFALSE);
	//
	// Final message
	//
	new TGMsgBox(gClient->GetRoot(), 0, tit.Data(), msg.Data()
		,kMBIconExclamation, 0);
}
// >>>>>> Service methods for manage buttons : June 15,2013
void SDGui::RGBnoImages()
{
    // can do just save ROOT file here
	fRGBSelection->RemoveAll();
	fRGBSelection->AddEntry("no images ",0);
	fRGBSelection->Select(0);
	fRGBSelection->SetEnabled(kFALSE);
}

void SDGui::RGBImagesMoreThanOne(Int_t pri)
{
   if(pri) printf("<I> SDGui::RGBImagesMoreThanOne \n");
   sdu::DeleteVectorMember(fRGBStrings, fStringSelected, 1);
   fRGBSelection->RemoveAll();
   for(UInt_t i=0; i<fRGBStrings.size(); ++i) {
	   fRGBSelection->AddEntry(fRGBStrings[i],i);
	   if(pri) printf("<I> %i %s \n", i, fRGBStrings[i].Data());
   }
   fRGBSelection->Select(0);
   UpdateF0andCFandScanButtons(fRGBStrings[0]);

   if(pri) printf(" 1 fRGBSelection->GetNumberOfEntries() %i \n", fRGBSelection->GetNumberOfEntries());
   if(fRGBSelection->GetNumberOfEntries()==2) {
	   fRGBSelection->SetEnabled(kTRUE);
   } else if(fRGBSelection->GetNumberOfEntries()==1) {
//			   fRGBSelection->SetEnabledTList(kTRUE); // For testing
   }
   fCalculateF0Button->SetEnabled(kTRUE);
   SetEnabledTList(fF0Entries, kTRUE);
   fFindClustsButton->SetEnabled(kTRUE);
   SetEnabledTList(fCFEntries,kTRUE);

   SetEnabledTList(&fScanList,kTRUE);
   if(pri) fImgAna->Dump();
   if(fImgAna->GetParsScan().GetNpScan()>1) fClustersScanButton->SetEnabled(kTRUE);

}

void SDGui::CreateOutputTextFiles(UInt_t indCurrent, UInt_t scan)
{
	// 2013,June 23 - separate methods
	UInt_t pri = 0;
	char ctit[2] = { ' ', ';' };
	for (UInt_t i = 0; i < 2; ++i) {
		tabRec rec = fResultTabs[indCurrent + i];
		TString snam = fCF->SaveTxtFile(ctit[i], scan);
		rec.textEdit->LoadFile(snam.Data());
		TString stmp(rec.groupFrame->GetTitle());
		stmp += " ";
		stmp += snam;
		rec.groupFrame->SetTitle(stmp.Data());
		if(pri) {
		  cout<<" SDGui::CreateOutputTextFiles indCurrent "<<indCurrent<<" scan "<<scan<<endl;
		  cout<<rec<<endl;
		}
	}
}

void SDGui::ResetGUI()
{
	MapSubwindows();
	MapWindow();
	Layout();
	gSystem->ProcessEvents();
}
// <<<<<< Service methods for manage buttons : June 15,2013

void SDGui::SetCheckButtonsAfterReadTIF()
{
	// 2013: Mar 31;
	tifHeader header = fTifInfo->GetTifHeader();
	Int_t indGrey = 4;
	TGCheckButton* chGrey = (TGCheckButton*)(fTransformChksList.At(indGrey));
	if(header.samplesPerPixel == 1) { // grey image
		chGrey->SetOn(kTRUE);
		for(int i=indGrey+1; i<indGrey+5; ++i){
			TGCheckButton* chk = (TGCheckButton*)(fTransformChksList.At(i));
			chk->SetOn(kFALSE);
		}
	} else if(header.samplesPerPixel > 1) {
		chGrey->SetOn(kFALSE);
		for(int i=indGrey+1; i<indGrey+5; ++i){
			TGCheckButton* chk = (TGCheckButton*)(fTransformChksList.At(i));
			chk->SetOn(kTRUE);
		}
	}
}

void SDGui::SetCheckButtonsAfterFill2DHists()
{
	// 2013: Mar 31;
  SetEnabledTList(&fTransformChksList, kFALSE);
}

//         Update configuration values
void SDGui::UpdateTifDir(const char * newtext)
{
   // Handle text entry for Tif directory
	if(strlen(newtext)==0) return;
	fConfInfo->GetParsInput().dir = newtext;
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateTifName(const char * newtext)
{
   // Handle text entry for Tif name
	if(strlen(newtext)==0) return;
	fConfInfo->GetParsInput().name = newtext;
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateRGBSelection(const char *newtext)
{
	if(strlen(newtext)==0) return;
	fStringSelected = newtext;
	UpdateF0andCFandScanButtons(newtext);
	gSystem->ProcessEvents();
}

// Transformation
void SDGui::UpdateTransformationPars()
{  // Mar 30, 2013;
	Int_t pri = 1;
	TGButton *btn = (TGButton *) gTQSender;
	TString stmp(btn->GetName());
	Int_t ind = stmp.Index("chk");
	TString sb = stmp(0, ind);
	Int_t ibnt = sb.Atoi();
	if(pri) printf("<I> SDGui::UpdateTransformationPars : ibnt %i name %s : state %i \n",
			ibnt, btn->GetName(), btn->GetState());

    SDTiffFileInfo *tifInfo = fSdManager->GetTiffFileInfo();
    if(tifInfo == 0) return;

	SDTransformation *trans = tifInfo->GetSDTransformation();
	if(trans == 0) return;

	TClass *cl = trans->Class();
	TList  *ld = cl->GetListOfDataMembers();
	TDataMember *dm = dynamic_cast<TDataMember *>(ld->At(ibnt));
	if (dm) {
		if(pri) printf(" name %s dm->GetOffset() %li sizeof(Int_t) %li \n",
				dm->GetName(), dm->GetOffset(),sizeof(Int_t));
		TMethodCall* setter = dm->SetterMethod(cl);
		if(setter) {
			setter->Execute(trans,Form("%i",btn->GetState()));
		} else {
			if(pri) printf(" setter %p \n", setter);
		}
	}
}

void SDGui::UpdateThreadsPars(const char *newtext)
{
	// May 23 - discard check box and use integer as MaxThread
	Int_t pri = 0;
	TString stmp(newtext);

	parsSdThreads& pars = fConfInfo->GetParsThreads();
	Int_t oldv= pars.GetMaxThreads();
	pars.SetMaxThreads(stmp.Atoi());
	gSystem->ProcessEvents();
	if(pri) {
		printf(" SDGui::UpdateThreadsPars(%s)  ", newtext);
		printf(" pars.fMaxThreads : old %i -> %i new \n", oldv, pars.GetMaxThreads());
	}
}

void SDGui::UpdateScanPars(const char  *newtext)
{
	// June 15-16,2013 : look to the xml parser for example.
	Int_t pri = 0;
	if(pri) cout<<"SDGui::UpdateScanPars("<<newtext<<")\n";
	TGNumberEntryField *ef = (TGNumberEntryField *) gTQSender;
	// String should contains a floating point or integer number - June 16,2013
	TString stmp(newtext);
	if(stmp.IsFloat()==0) return;

	parsSdScan& pars = fConfInfo->GetParsSdScan(); // update automatically
	TClass *cl = pars.Class();
	TList  *ld = cl->GetListOfDataMembers();
	Int_t n = ld->GetSize() - 1;
	for(Int_t i=0; i<n; ++i){
		TDataMember *dm = dynamic_cast<TDataMember *>(ld->At(i));
		TString stmp = dm->GetName();
		if(stmp.Contains(ef->GetName(),TString::kIgnoreCase)) {
			TMethodCall *setter = dm->SetterMethod(cl);
			setter->Execute(&pars, newtext);
			if(pri) {
				TMethodCall *getter = dm->GetterMethod(cl); //find a method that gets value!
				Long_t l;                                   // declare a storage for this value;
				getter->Execute(cl,"",l);
				cout<<" after setter : "<<dm->GetDataType()->GetTypeName()<<" Long_t l "<<l<<endl;
			}
		}
	}
	fClustersScanButton->SetEnabled(pars.GetNpScan()<=0?kFALSE:kTRUE);
	fNShift = pars.GetNpScan()<=0?2:4; // Important for fTab
	if(fImgAna) fImgAna->SetParsScan(pars);
}

void SDGui::UpdateThreadsParsChk()
{   // Apr 26-27, 2013;
	// May 23 - discard check box and use integer as MaxThread
	Int_t pri = 0;
	TGButton *btn = (TGButton *) gTQSender;
	TString stmp(btn->GetName());
	Int_t ind = stmp.Index("chk");
	TString sb = stmp(0, ind);
	Int_t ibnt = sb.Atoi();
	if(pri) printf("<I> SDGui::UpdateThreadsParsChk : ibnt %i name %s : state %i \n",
			ibnt, btn->GetName(), btn->GetState());

	parsSdThreads& pars = fConfInfo->GetParsThreads(); // Reference or pointer

	TClass *cl = pars.Class();
	TList  *ld = cl->GetListOfDataMembers();
	TDataMember *dm = dynamic_cast<TDataMember *>(ld->At(ibnt));
	if (dm) {
		if(pri) printf(" name %s dm->GetOffset() %li sizeof(Int_t) %li \n",
				dm->GetName(), dm->GetOffset(),sizeof(Int_t));
		TMethodCall* setter = dm->SetterMethod(cl);
		if(setter) {
			setter->Execute(&pars, Form("%i",btn->GetState()));
		} else {
			if(pri) printf(" setter %p \n", setter);
		}
	}
}

//
// F0 calculation
//
void SDGui::UpdateF0Algo(const char *newtext)
{
	fConfInfo->GetParsPedestals().algo = newtext;
	if(fPed) fPed->GetPedestalsPars().algo = newtext;
	AllignSmoothComboBox(newtext);
	gSystem->ProcessEvents();
}

void SDGui::UpdateFilter(const char *newtext)
{
	fConfInfo->GetParsPedestals().smooth = newtext;
	if(fPed) fPed->GetPedestalsPars().smooth = newtext;
	gSystem->ProcessEvents();
}

void SDGui::UpdateChi2Cut(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsPedestals().chi2cut = stmp.Atof();
	if(fPed) fPed->GetPedestalsPars().chi2cut = stmp.Atof();
	gSystem->ProcessEvents();
}

void SDGui::UpdateNsig(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsPedestals().nsig = stmp.Atof();
	if(fPed) fPed->GetPedestalsPars().nsig = stmp.Atof();
	gSystem->ProcessEvents();
}

void SDGui::UpdateSet(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsPedestals().set = stmp.Atoi();
	if(fPed) fPed->GetPedestalsPars().set = stmp.Atoi();
}

void SDGui::UpdateSigCut(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsPedestals().sigcut = stmp.Atof();
	if(fPed) fPed->GetPedestalsPars().sigcut = stmp.Atof();
}
//
// Cluster Finder parameters : put to configuration
// cf pars put to CF when cf created
//
void SDGui::UpdateCFName(const char* newtext)
{
	fConfInfo->GetParsClusterFinder().name = newtext;
//	fCF->SetCFName(fConfInfo->GetParsClusterFinder().name.Data());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateSeed(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsClusterFinder().seed = stmp.Atof();
	//if(fCF) fCF->SetCFPars(fConfInfo->GetParsClusterFinder());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateTimeScale(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsClusterFinder().timescale = stmp.Atof();
	//if(fCF) fCF->SetCFPars(fConfInfo->GetParsClusterFinder());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateSpaceScale(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsClusterFinder().spacescale = stmp.Atof();
	//if(fCF) fCF->SetCFPars(fConfInfo->GetParsClusterFinder());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateMinPixels(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsClusterFinder().minpixels = stmp.Atof();
	//if(fCF) fCF->SetCFPars(fConfInfo->GetParsClusterFinder());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateMinSize(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsClusterFinder().minsize = stmp.Atof();
	//if(fCF) fCF->SetCFPars(fConfInfo->GetParsClusterFinder());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateMinDuration(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsClusterFinder().minduration = stmp.Atof();
	//if(fCF) fCF->SetCFPars(fConfInfo->GetParsClusterFinder());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateDrawing(const char *newtext)
{
	TString stmp(newtext);
	fConfInfo->GetParsClusterFinder().drawing = stmp.Atoi();
	//if(fCF) fCF->SetCFPars(fConfInfo->GetParsClusterFinder());
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateOutputDir(const char * newtext)
{
   // Handle text entry for Out directory
	if(strlen(newtext)==0) return;
	fConfInfo->GetParsOutput().dir = newtext;
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::UpdateOutputName(const char * newtext)
{
   // Handle text entry for Out name
	if(strlen(newtext)==0) return;
	fConfInfo->GetParsOutput().name = newtext;
	UpdateTabConfigurationInfo(fConfInfo);
}

void SDGui::FontDialog()
{
	// Unused now
   TGFontDialog::FontProp_t prop;
   new TGFontDialog(gClient->GetRoot(), 0, &prop);
   if (prop.fName != "")
   printf("Selected font: %s, size %d, italic %s, bold %s, color 0x%lx, align %u\n",
          prop.fName.Data(), prop.fSize, prop.fItalic ? "yes" : "no",
          prop.fBold ? "yes" : "no", prop.fColor, prop.fAlign);
}

void SDGui::UpdateButtonsState()
{
	// Switch to enabled last 4 buttons
	// after tif(xml) file selection
	fReadTIFButton->SetEnabled(kTRUE);
	fCalculateF0Button->SetEnabled(kTRUE);
	fFindClustsButton->SetEnabled(kTRUE);
	fSaveButton->SetEnabled(kTRUE);

	ConnectControlBarsSlotsTwo();

	fCalculateF0Button->SetEnabled(kFALSE);
	fFindClustsButton->SetEnabled(kFALSE);
}


void SDGui::StartTBrowser()
{
	if(fBrowser && gROOT->GetListOfBrowsers()->FindObject(fBrowser)) {
		delete fBrowser;
		gROOT->GetListOfBrowsers()->Remove(fBrowser);
	}
	fBrowser = new TBrowser;
}

void SDGui::ExitFromRoot()
{
	// Oct 29,2012
	Int_t retCode = -1;
	new TGMsgBox(gClient->GetRoot(), 0, "Exit From ROOT",
			" Do you really want to exit from ROOT?", kMBIconQuestion,
			kMBYes|kMBNo, &retCode);
	if(retCode==kMBYes) gApplication->Terminate(0);
}

void SDGui::PrintInfo() {
	UInt_t w, h;
	GetWMSize(w, h);
	cout << "WM size : w" << w << " h " << h << endl;
}
//
//         <<  O b s o l e t e
//
void SDGui::CalculateF0Old()
{
	// Keep for information, used before Mar 10,2013
	if(fSdManager->GetStatus() < SDManager::kReadTiff) ReadTIF();

	fCalculateF0Button->SetEnabled(kFALSE);

    fSdManager->InitPedestals();
	fSdManager->GetPedestals()->SetPedestalsPars(fConfInfo->GetParsPedestals()); //
	fSdManager->DoPedestals();

	SetEnabledTList(fF0Entries, kFALSE);
	SetEnabledTList(fCFEntries,kTRUE);
}

//
//         >> O b s o l e t e
//
