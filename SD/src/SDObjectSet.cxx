/*
 * SDObjectSet.cpp
 *
 *  Created on: May 14, 2012
 *  2013: Feb 24;
 *      Author: Alexei Pavlinov
 */

#include "SDObjectSet.h"
#include "SDUtils.h"
#include <TList.h>

ClassImp(SDObjectSet)

SDObjectSet::SDObjectSet() : TObjectSet(), fDebug() {}

SDObjectSet::SDObjectSet(const char* name, const char* title) : TObjectSet(name), fDebug(0)
{
	SetTitle(title);
	Bool_t makeOwner = kTRUE;
	this->AddObject(InitHists(), makeOwner); // fObj
}

TList* SDObjectSet::InitHists() {
  // Create a list with name "hists"
	TList* l = new TList;
	l->SetName("hists");
	return l;
}

SDObjectSet::~SDObjectSet()
{ }

void SDObjectSet::ClearSDObject()
{
	// 2013: Feb 24
	// Delete everything
//	if(fObj) {delete fObj; fObj=0;}
	Delete("all");
}

TList* SDObjectSet::GetList()
{
	if(fObj==0) fObj = InitHists();
	return (TList*)((fObj));
}

TList* SDObjectSet::GetListHists(int ind)
{
	// Get list by index
	return dynamic_cast<TList *>((GetList()->At(ind)));
}

TList* SDObjectSet::GetListHists(const char *name)
{
	// Get list by name
	return dynamic_cast<TList *>((GetList()->FindObject(name)));
}

SDManager* SDObjectSet::GetSDManager()
{
	return sdu::GetSDManager(this);
}

SDImageAnalysis* SDObjectSet::GetImageAnalysis()
{
	TDataSet *parent=0, *child=this;
	SDImageAnalysis* imgAna = 0;
	while((parent=child->GetParent())){
		imgAna = dynamic_cast<SDImageAnalysis *>(parent);
		if(imgAna == 0) child = parent;
		else            return imgAna;
	}
    return 0;
}

TString  SDObjectSet::GetSignature()
{
	TString stmp("");
	return stmp;
}
