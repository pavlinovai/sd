/*
 * SDTransformation.cxx
 *
 *  Created on: Mar 28, 2013
 *      Author: pavlinov
 */

#include "SDTransformation.h"

ClassImp(SDTransformation)

SDTransformation::SDTransformation() : SDObjectSet(), fPars(0)
{
   // For reading only
}

SDTransformation::SDTransformation(Int_t set) : SDObjectSet(),fPars((set==0?0:1))
{
	switch (set) {
	   case 1:
		   InitOne();
		   break;
	   default:
		   InitOne();
	}
	TString stmp;
	stmp.Form("SDTansformation %i", set);
	SetName(stmp);
}

SDTransformation::~SDTransformation()
{}

void SDTransformation::InitOne()
{
	// Nothing to do now : June 30,2013
}

void SDTransformation::Transform(Int_t col, Int_t row, Int_t &colNew, Int_t &rowNew)
{
	Int_t x=col, y=row; // Current x(col) and y(row)
	if(fPars.fRotate90) {
		TransformRotate90(x,y, colNew,rowNew);
		x = colNew; y = rowNew;
	}
	if(fPars.fFlipVert) {
		TransformFlipVert(x,y, colNew,rowNew);
		x = colNew; y = rowNew;
	}
	if(fPars.fCombine) {
		TransformCombine(x,y, colNew,rowNew);
		x = colNew; y = rowNew;
	}
//	if(fFlipHor) {
//		TransformFlip(x,y, colNew,rowNew);
//		x = colNew; y = rowNew;
//	}
}

TString SDTransformation::GetSignature()
{
	TString stmp;
	if(fPars.fRotate90) stmp += TString::Format("Rotate90");
	if(fPars.fFlipVert) stmp += TString::Format("+FlipVert");
	if(fPars.fCombine)  stmp += TString::Format("+Combine");
    return stmp;
}

void SDTransformation::TransformRotate90(Int_t col, Int_t row, Int_t& colNew, Int_t& rowNew)
{
	// Rotate to right      //   ^ ynew          ---->y
	colNew = row;           //   |         =>    |
	rowNew = 1023 - col;    //   |----> xnew     V x
}

void SDTransformation::TransformFlipVert(Int_t col, Int_t row, Int_t& colNew, Int_t& rowNew)
{
	// Vertical flip : col(x) the same  //   ^ ynew          ---->x
	colNew = col;                       //   |         =>    |
	rowNew = 1023 - row;                //   |----> xnew     V y
}

void SDTransformation::TransformCombine(Int_t col, Int_t row, Int_t &colNew, Int_t &rowNew)
{
  if(row <  512) { // bottom part first
	  colNew = col;
	  rowNew = row;
  } else {         // top part second
	  colNew = col + 1024;
	  rowNew = row - 512;
  }
}

void SDTransformation::TransformFlipHor(Int_t col, Int_t row, Int_t& colNew, Int_t& rowNew)
{
	// Horizontal flip; row(y) the same ; unused yet;
	colNew = 1023 - col;
	rowNew = row;
}

