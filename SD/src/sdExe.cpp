//
// Program for creating ROOT like application : June 6,2012
//
#include <TROOT.h>
#include <TSystem.h>
#include <SDManager.h>
#include <TApplication.h>

Int_t main(Int_t argc, char** argv)
{
    TApplication theApp("SDMain", &argc, argv);

    TString ROOTSYS(gSystem->Getenv("ROOTSYS"));
	ROOTSYS.ToUpper();

	gSystem->Load("libXMLParser");
	if (ROOTSYS.Contains("QT")) {
		gSystem->Load("libGQt");
		gSystem->Load("libQtRootGui");
	}

	SDManager* sdman = new SDManager("sdman");
	sdman->InitMain();

    theApp.Run();

    return 0;

}
