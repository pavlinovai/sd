/*
 * SDClusterFinderStdOne.cpp
 *
 *  Created on: May 17, 2012
 *      Author: pavlinov
 */

#include "SDManager.h"
#include "SDClusterFinderStdOne.h"
#include "SDAliases.h"
#include "SDUtils.h"
#include "SDClusterInfo.h"
#include "SDCluster.h"
#include "SDConfigurationInfo.h"
#include "sdCommon.h"
#include "SDImageAnalysis.h"
#include "SDPedestals.h"
#include "SDTransformation.h"
#include "SDClusterScan.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TLine.h>
#include <TPolyLine.h>
#include <TText.h>
#include <TDataSetIter.h>

const char* SDClusterFinderStdOne::gkNameInputHists  = "InputHists";
const char *SDClusterFinderStdOne::gkNameHistsMain   = "MainHists";
const char *SDClusterFinderStdOne::gkNameHistsCFSum  = "SumHists";
const char *SDClusterFinderStdOne::gkBgLines = "BG Lines";

ClassImp(SDClusterFinderStdOne)

SDClusterFinderStdOne::SDClusterFinderStdOne() : SDClusterFinderBase(),SDObjectSet(),
fPars(), fOnlineDrawing(0), fThreadsStatus(0), fAmpCut(0.),
fNClstsAll(0), fC(0), fHInCopy(0)
{
	// For reading
}

SDClusterFinderStdOne::SDClusterFinderStdOne(const char* name) : SDClusterFinderBase(),
		SDObjectSet(name),
fPars(), fOnlineDrawing(0), fThreadsStatus(kNoThreads), fAmpCut(0.),
fNClstsAll(0), fC(0), fHInCopy(0)
{
   // fPars has default value here : if you want to change use SetCFPars(parsClusterFinder pars);
}

SDClusterFinderStdOne::~SDClusterFinderStdOne()
{
	// 2013: Feb 24;
	if(fC) {
		if(gROOT->GetListOfCanvases()->FindObject(fC)) {
		   delete fC;
		   fC=0;
		   fHInCopy = 0;
		}
	}
	if(fHInCopy) {delete fHInCopy; fHInCopy=0;}
}

SDPedestals* SDClusterFinderStdOne::GetPedestal()
{
	return GetImageAnalysis()->GetPedestals();
}

TString SDClusterFinderStdOne::GetSignature()
{
	// Mar 11, 2013;
	fSignature =  fPars.name;
	fSignature += Form(" Seed%3.1f ",fPars.seed/GetPedestal()->GetSigF0Unit());
	fSignature += Form("(%4.3f)", GetSeed());
	fSignature += Form(" min: #pxls %i ",fPars.minpixels);
	fSignature += Form(", %i(X)",fPars.mindurationInPxs);
	fSignature += Form(", %i(Y)",fPars.minsizeInPxs);

	return fSignature;
}

void SDClusterFinderStdOne::ClearClusterFinder()
{
 // 2013; Feb 24;
	ClearSDObject();

	if(fC) {
		if(gROOT->GetListOfCanvases()->FindObject(fC)) {
		   delete fC;
		   fC=0;
		   fHInCopy = 0;
		}
	}
	if(fHInCopy) {delete fHInCopy; fHInCopy=0;}
}

void SDClusterFinderStdOne::Init()
{
   TDataSet *ds = new TDataSet("Clusters");
   Add(ds);
   // Lists
   TList *l = new TList;
   l->SetName(SDClusterFinderStdOne::gkNameHistsMain);
   GetList()->Add(l);
}

TList *SDClusterFinderStdOne::GetHists()
{
  return GetList(); // From SDObjectSet
}

SDClusterInfo *SDClusterFinderStdOne::GetClusterInfo(const int ind)
{
	return dynamic_cast<SDClusterInfo *>(GetClusters()->At(ind));
}

TDataSet *SDClusterFinderStdOne::GetClusters()
{
	// Sep 29: the name could be "Clusters N".
	// Its provides additional information in browser
    for(Int_t i=0; i<GetListSize(); i++) {
    	TDataSet *ds = At(i);
    	TString snam(ds->GetName());
    	if(snam.Contains("Clusters", TString::kIgnoreCase)) return ds;
    }
    return 0;
//   return FindByName("Clusters");
}

TList* SDClusterFinderStdOne::GetMainHists()
{
  return dynamic_cast<TList *>(GetListHists(SDClusterFinderStdOne::gkNameHistsMain));
}

TList* SDClusterFinderStdOne::GetSummaryHists()
{
  return dynamic_cast<TList *>(GetListHists(SDClusterFinderStdOne::gkNameHistsCFSum));
}

Int_t SDClusterFinderStdOne::FindClusters()
{
	TH2* hin = sda::GetH2(GetMainHists(),0);
	if(hin==0) return 0;
	GetList()->Add(BookSummaryHists());
	DrawInputHistInCanvas(hin);

	clusterSimple tabIn; // input table of pixels
	Bool_t sort=kTRUE;
	sdu::FillClusterFromHist(hin, tabIn, sort);
	//return;

	clusterSimple::iterator it;
	TDataSet* clusts = GetClusters();

	for(;;) {
    NEWCLUSTER:
	  pixel pixel = tabIn.front(); // seed pixel
      if(pixel.amp<fPars.seed) break; // end of cluster finder

	  SDClusterInfo *clinfo = new SDClusterInfo(Int_t(clusts->GetListSize()));
	  SDClusterBase *clust  = clinfo->GetCluster();
	  clusts->Add(clinfo);
 	  clust->AddPixel(pixel);
 	  if(fDebug>=1) cout<<" SDClusterFinderStdOne::FindClusters() : new cluster "<<clinfo->GetName()<<endl;

      tabIn.pop_front();

    CLUSTERCYCLE:
      for(it=tabIn.begin(); it!=tabIn.end(); ++it){
    	  pixel = *it;
          if(IsPixelNeighbourToCluster(pixel, (clust))) {
        	  clust->AddPixel(pixel);
    	      tabIn.erase(it);
    	 	  if(fDebug>=3) cout<<" add pixel "<<pixel<<" #pixel "<<clust->GetClusterSize()<<endl;
    	      goto CLUSTERCYCLE;
    	  }
      }
  	  if(fDebug) cout<<" #pixels "<<clust->GetClusterSize()<<endl;
//	  Int_t ic = static_cast<Int_t>(GetListSize());
//      DrawClusterBox(fC, clstInfo, ic);
      goto NEWCLUSTER;
	} // cycle on pixels
//
	fNClstsAll = clusts->GetListSize();
	TH2* h2Rem = FillRemnantHist(hin, &tabIn);
	GetMainHists()->Add(h2Rem);

//    FillAmpDistWithCutsQaAndSum();

	SortAndRenameClusters(11);

	Int_t clstsReal = clusts->GetListSize();
	DrawFinalMessage(fC, clstsReal, fNClstsAll);

	return clstsReal;
}

// New method
void SDClusterFinderStdOne::SetInputHist(TH2* hin)
{
	// Apr 14 - create and added h2Remnant
	GetMainHists()->Add(hin,0); // First hist on the list
    TH2* h2Remnant = sdu::CloneHist(hin, "Remnant", "Remnant histogram", 0);
    GetMainHists()->Add(h2Remnant);
}

void SDClusterFinderStdOne::SortAndRenameClusters(Int_t mode)
{
	Int_t pri = mode / 10;
	// crHist = mode % 10;
	TDataSet *clusts = GetClusters();
	TList *l = clusts->GetList();
	if (l==0 || (l && l->GetSize()==0)) {
		if(fDebug>0) cout << " No clusters at all !\n";
		return;
	}
	Int_t nclustIn = l->GetSize();

	// Look to method Int_t SDClusterInfo::Compare and Int_t SDCluster::Compare
	l->Sort(kSortDescending);

	for (Int_t i=0; i < l->GetSize(); ++i) {
		SDClusterInfo *clinfo = dynamic_cast<SDClusterInfo *> (l->At(i));
		if (clinfo == 0)  break;

		clinfo->ChangeName(i+1); // Mar 16; Number starts from one now for compatibility
		CalculateClusterCharachteristics(*clinfo);
		SDCluster* cl = clinfo->GetClusterTable();
		if(pri) cl->PrintClusterInfo(pri);
	}

	FillSummaryHists(); // Jan 6,2013

    if(fDebug>0) cout<<"\t nclustIn "<<nclustIn<<" : number of clusters "<<l->GetSize()<<endl;
	SetName(Form("%s %i Clusters",GetName(), l->GetSize()));
}

TList* SDClusterFinderStdOne::BookSummaryHists(const char* opt)
{
	// Would be call from method FindClusters because the data configuration
	// should be defined.
	// printf("<I>  SDClusterFinderStdOne::BookSummaryHists() is started \n");
	TH1::AddDirectory(1);

	Int_t nx=151, i=1;
	TArrayD arr(nx);
	arr[0] = 0.5;
	Double_t step=1.;
	for(i=1; i<nx; ++i) {
		arr[i] = arr[i-1] + step;
		if(i>100)     step=200;
		else if(i>50) step=10.;
	}

	TH1* h = 0;
	new TH1F("00_NofPixelsInClusts","number of pixels in all clusters; #pixels; N", nx-1, arr.GetArray());
	new TH1F("01_XSizeOfClust","X size of all clusters; pixels; N", 71, arr.GetArray());
	new TH1F("02_YSizeOfClust","Y size of all clusters; pixels; N",101, arr.GetArray());

    new TH1F("03_MaxAmpOfClust","Maximum amplitude of cluster (F0 unit); F0 unit; N", 50, 0.0, 5.);
    new TH1F("04_AverageAmpOfClust","Average amplitude of cluster (F0 unit); F0 unit; N", 50, 0.0, 5.);

    Int_t nmax = 50;
	Double_t xmax = fPars.timescale*nmax;
	new TH1F("05_DurationOfClust","Duration of real clusters (in ms); ms; N", nmax, 0.0, xmax);

	nmax = 100;
	xmax = fPars.spacescale*nmax;
	new TH1F("06_SizeOfClust","size of real clusters (in um); um; N", nmax, 0.0, xmax);

	arr[0] = 0.0;
	step=1.;
	for(i=1; i<nx; ++i) {
		arr[i] = arr[i-1] + step;
		if(i>100)     step=100.;
		else if(i>50) step=10.;
	}
    new TH1F("07_VolumeAmpOfClust","Volume(energy) of cluster (F0 unit); F0 unit; N", nx-1, arr.GetArray());
    // Cuts in cluster finder
    nx=6;
    h = new TH1F("08_CutsOnRealCluster","Cuts on real cluster", nx,0.5,Double_t(nx)+0.5);
    TAxis *xax = h->GetXaxis();
    Int_t ic=1;
    xax->SetBinLabel(ic++, "#Candidates");
    xax->SetBinLabel(ic++, "#Good clsts");
    xax->SetBinLabel(ic++, Form("# pixels<%i",fPars.minpixels));
    xax->SetBinLabel(ic++, Form("Space<%ipxls", fPars.minsizeInPxs));
    xax->SetBinLabel(ic++, Form("Duration<%ipxls", fPars.mindurationInPxs));

    TList *l = sda::moveHistsToList(gkNameHistsCFSum);

	if(strlen(opt)) {
		sda::AddToNameAndTitleToList(l, opt, opt);
	}

	TH1::AddDirectory(1);
	return l;
}

void SDClusterFinderStdOne::FillSummaryHists()
{
	// Should call before SortAndRenameClusters
	TList *l = GetSummaryHists();
	if (l == 0) {
	   l = BookSummaryHists();
	   GetList()->Add(l);
	}
//	else sda::ResetListOfHists(l);

	TDataSet* clusts = GetClusters();
	for (Int_t i = 0; i < clusts->GetListSize(); ++i) {
		SDClusterInfo *clinfo =
				dynamic_cast<SDClusterInfo *> (clusts->At(i));
		if (clinfo == 0)
			break;
		SDClusterBase* clb = clinfo->GetCluster();
		Int_t nrows = Int_t(clb->GetClusterSize());
		sda::FillH1(l, 0, Double_t(nrows));
		sdClusterChar clChar = clinfo->GetClstCharFromF0();
		sda::FillH1(l, 1, clChar.colmax - clChar.colmin + 1);
		sda::FillH1(l, 2, clChar.rowmax - clChar.rowmin + 1);
		sda::FillH1(l, 3, clChar.maxamp);
		sda::FillH1(l, 4, clChar.aveamp);
		sda::FillH1(l, 5, clChar.duration);
		sda::FillH1(l, 6, clChar.size);

        TH2* h2 = sda::GetH2(clinfo->Get2DHists(),0);
		if(h2) sda::FillH1(l, 7, h2->Integral());
	}
}

TH2* SDClusterFinderStdOne::FillRemnantHist(TH2* hin, clusterSimple* tab)
{
	if(hin==0 || tab==0) return 0;
	TH1::AddDirectory(0);

	TString snam(hin->GetName());
	snam += "Rem";
	TH2* hrem = (TH2*)hin->Clone(snam.Data());
	hrem->Reset();
	hrem->SetTitle(" Remnant hist");
	clusterSimple::iterator it;
	for(it=tab->begin(); it!=tab->end(); ++it) {
	  pixel cell = *it;
	  hrem->Fill(cell.col,cell.row, cell.amp);
	}
	hrem->SetMaximum(hin->GetMaximum());
	return hrem;
}

void SDClusterFinderStdOne::CalculateClusterCharachteristics(SDClusterInfo &clinfo)
{
	// 2012: Oct 23;
	// 2013; Jan  7; May 22
	//
	sdClusterChar& clch = clinfo.GetClstCharFromF0();
	if(clch.duration>0.0) return; // Calculate cluster characteristics already

	clinfo.Create2DHists();
	clinfo.CreateProjHists(0); // with cuts
//	clinfo.CreateProjHists(3); // no cuts

	SDClusterBase *clb  = clinfo.GetCluster();
	clch.numpixels = clb->GetClusterSize();
//	clch.maxamp    = cl->GetTable(0)->amp; // don't need anymore - Nov 17,2012
	//	TH1* h = dynamic_cast<TH1 *>(clinfo.GetProjHists()->At(2));
	Double_t sum = 0.0;
	for(UInt_t i=0; i<clb->GetClusterSize();++i) sum += clb->GetPixel(i)->amp;
	clch.aveamp    = sum/clb->GetClusterSize();
	//
	Int_t wMean=2; // for MeanFilter
	TList* lpro = clinfo.GetProjHists();
	TH1* hprox  = sda::GetH1(lpro,0);
	TH1* hproy  = sda::GetH1(lpro,1);
	TH1* hamp   = sda::GetH1(lpro,2);

	if(wMean>0) {
		lpro->Clear();
		TH1* hproxSmooth = sdu::MeanFilter(hprox,wMean,0);
		delete hprox;
		hprox = hproxSmooth;
		lpro->Add(hproxSmooth);
	}
	sdu::GetFWHM(hprox, clch.xhm1, clch.xhm2);
	clch.duration = (clch.xhm2 - clch.xhm1)*fPars.timescale;

    Int_t nrebin=4, nbins = hproy->GetNbinsX();
	if(wMean>0) {
		TH1* hproySmooth = sdu::MeanFilter(hproy,wMean*2,0); // Double window
		delete hproy;
		hproy = hproySmooth;
		lpro->Add(hproySmooth);
		lpro->Add(hamp);
	} else {
      if     (nbins<=10) nrebin = 1;
      else if(nbins<=20) nrebin = 2;
      if(nrebin>1) {
    	 hproy = sdu::RebinAndReplace(lpro,1, hproy,nrebin);
       } else if(nrebin<0) {
    	 hproy->Smooth(TMath::Abs(nrebin));
      }
	}

	sdu::GetFWHM(hproy, clch.yhm1, clch.yhm2);
	clch.size = (clch.yhm2 - clch.yhm1)*fPars.spacescale;

    // 0-normal cluster, 1 - cluster was cut by the border : May 22, 2013
	clusterType clt;
	if(clch.colmin==0||clch.colmax==2013||clch.rowmin==0||clch.rowmax==511) clt.tdecomp.border = 1;
	clch.type = clt.type;
	//
	sdClusterChar& clchFromZero = clinfo.GetClstCharFromZero();
	clchFromZero = clch;
	// return to F/F0 scale - count from zero
	clchFromZero.maxamp  += 1.;
	clchFromZero.aveamp  += 1.;
}

Bool_t SDClusterFinderStdOne::IsThisClusterReal(SDClusterInfo &clinfo, parsClusterFinder* pcf)
{
	// 2012 - Jul 18; Nov 1
	// Call in cluster finder now
	if(pcf==0) return kTRUE; // no checking - all cluster are "real"

	TH1* h = sda::GetH1(GetSummaryHists(),8);
    h->Fill(1.); // Number of cluster candidates
    sdClusterChar clch = clinfo.GetClstCharFromF0();
	if(clch.numpixels < UInt_t(pcf->minpixels)) {
        h->Fill(3.);
		return kFALSE;
	}
	// check space size
	Int_t ssize = clch.rowmax - clch.rowmin + 1;
	if(ssize>0 && ssize < pcf->minsizeInPxs) {
        h->Fill(4.);
		return kFALSE;
	}
	// check duration size (X coord)
	Int_t tsize = clch.colmax - clch.colmin + 1;
	if(tsize>0.0 && tsize < pcf->mindurationInPxs) {
        h->Fill(5.);
		return kFALSE;
	}

	h->Fill(2.); // Number of accepted cluster

	return kTRUE;
}
//
// Threads : Apr 21, 2013
//
Int_t SDClusterFinderStdOne::MergeClusterFinders(TDataSet* dsIn)
{
	if(dsIn == 0 || dsIn->GetListSize()==0) {
		TThread::Printf("dsIn in is zero or empty dsIn %p ",dsIn);
		return 0;
	}
	// Move clusters to parent cluster finder
	Int_t indSum = 0;
	TDataSet *dsClfOut = GetClusters();
	// Should update remnant histogram
	TH2* hrOut = sda::GetH2(GetMainHists(),1);
	hrOut->Reset();
	for(Int_t i=0; i<dsIn->GetListSize();  ++i) {
		SDClusterFinderStdOne* clf = dynamic_cast<SDClusterFinderStdOne *>(dsIn->At(i));
		TDataSet* dsClfIn = clf->GetClusters();
		TDataSetIter it(dsClfIn);
		TDataSet *clst = 0;
		while((clst=it.Next())) {
			clst->SetName(Form("ClstName%i",++indSum));
			dsClfIn->Remove(clst);
			dsClfOut->Add(clst);
			it.Reset();
		}
		dsIn->Remove(dsClfIn);
		delete dsClfIn;
		//
		TH2* hrIn = sda::GetH2(clf->GetMainHists(),1);
		Float_t x, y, amp;
		for(Int_t binx=1; binx<=hrIn->GetNbinsX(); ++binx){
			for(Int_t biny=1; biny<=hrIn->GetNbinsY(); ++biny){
				amp = hrIn->GetBinContent(binx,biny);
				x = hrIn->GetXaxis()->GetBinCenter(binx);
				y = hrIn->GetYaxis()->GetBinCenter(biny);
				hrOut->Fill(x,y, amp);
			}
		}
	}
	TH2* h2= sda::GetH2(GetMainHists(),0);
	hrOut->SetMaximum(h2->GetMaximum()); // For Drawing
    //   Clean up
	dsIn->Delete();
	Remove(dsIn);
	delete dsIn;
//	dsIn->Remove(clf);
//	delete clf; // Tune this

	Int_t mode=1; // Int_t pri = mode / 10;
	this->SortAndRenameClusters(mode);
	this->DrawInputHistInCanvasWithClusters(1);
//	TCanvas*  c = 0;
//	if(clf->GetOnlineDrawing()) c = clf->DrawInputHistInCanvas(hin, clf->GetCanvas());
//	this->DrawFinalMessage(c, clstsReal, clusts->GetListSize());

	return dsClfOut->GetListSize();
}
//
// Drawing
//
void SDClusterFinderStdOne::DrawCFSummary()
{
	TCanvas *c = sda::DrawListOfHist(GetListHistsSum(), 3, 2, 6, 0);
	TString snam(GetListHistsSum()->GetName());
	snam += GetImageAnalysis()->GetSignatureForCanvas();
	c->SetName(snam.Data());
	c->SetTitle(snam.Data());
	for(int i=1; i<=3; ++i) {
		c->cd(i);
		gPad->SetLogy(1);
		if(i==1) {
		   gPad->SetLogx(1);
		} else if(i==2) {
		  TF1* f2expo = new TF1("f2expo","expo(0)+expo(2)",0.0,40.0);
		  f2expo->SetLineColor(kRed);
		  TH1* h = sda::GetH1(GetListHistsSum(),1);
		  h->Fit(f2expo,"E+","");
		}
	}
	c->SetWindowPosition(600,400);
	c->Update();
}

void SDClusterFinderStdOne::DrawInputHistInCanvasWithClusters(Int_t redraw)
{
	// June 12-15,2013
  if(fC && redraw==0) return; // Already drawn
  if(GetOnlineDrawing() == 0 && redraw==0) return;

  TH2* hin = sda::GetH2(GetMainHists(),0);
  fC = DrawInputHistInCanvas(hin, fC);

  for(Int_t ic=0; ic<GetClusters()->GetListSize(); ++ic){
	SDClusterInfo* cl = GetClusterInfo(ic);
	DrawClusterBox(fC, cl, ic+1);
  }
  if(fC) fC->Update();
}

TCanvas* SDClusterFinderStdOne::DrawInputHistInCanvas(TH2* h, TCanvas *c)
{
	// Online Drawing
	if (h == 0) return 0; // Histogram is undefined
	SDImageAnalysis   *imgAna = GetImageAnalysis();
	if(imgAna==0) return 0;

	if (c==0) {
	  TString snam("Cluster Finding : Image ");
	  snam += imgAna->GetSignatureForCanvas();
	  c = new TCanvas(snam.Data(), snam.Data(), 5, 5, 1400, 850);
	} else {
	  c->Clear();
	}

	fHInCopy = h->DrawCopy();
	TString stit("MC ");
	stit = imgAna->GetSignature();
	stit += " No clusters yet";
	fHInCopy->SetTitle(stit.Data());

	fHInCopy->SetStats(kFALSE);

	c->Update();
	return c;
}

void SDClusterFinderStdOne::DrawBackgroundColumns(Int_t var)
{
	// Apr 6,2013 -> for tuning program for threading
	if (fC == 0) return;
	fC->cd();

	Double_t ymin=0., ymax = 512.;
//	vector<pair<float,float> > v = GetPedestal()->GetGoodBgCoord();
	vector<pair<float,float> > v = GetPedestal()->GetEmptyBgCoords();
	TList* lines = GetListOfBgLines();
	if(lines==0) {
		lines = new TList;
		lines->SetName(gkBgLines);
		GetHists()->Add(lines);
	}
	for (UInt_t i = 0; i < v.size(); ++i) {
		pair<float,float> rec = v[i];
		Double_t x = rec.first;
		Int_t n=1;
		if(var) n = Int_t(rec.second-rec.first + 1.);
		for(Int_t j=0; j<n; ++j) {
		  TLine *line = new TLine(x+j, ymin, x+j, ymax);
		  line->SetLineColor(kRed);
		  line->Draw("same");
		  lines->Add(line);
		}
	}
	fC->Update();
	gSystem->ProcessEvents();
}

void SDClusterFinderStdOne::DrawClusterBox(TCanvas* c, SDClusterInfo* cli, Int_t ic)
{
	UInt_t minPixelForDrawing=10;
	if (c == 0 || cli == 0) return;
	if (cli->GetCluster()->GetClusterSize() < minPixelForDrawing) return; // Don't draw small cluster

	c->cd();
	sdClusterChar clch = cli->GetClstCharFromF0();
	TPolyLine *pline = cli->GetCluster()->CreatePolyline();
	pline->Draw();

	// Don't draw the cluster number with small number of pixels
	if (cli->GetCluster()->GetClusterSize() > UInt_t(fPars.minpixels)) {
		TString st; st += ic;
		Double_t xst = (clch.colmin + clch.colmax)/2.;
		Double_t yst = (clch.rowmin + clch.rowmax) / 2.;
		TText *num = new TText(xst, yst, st.Data());
		Float_t textSize = ic<=9?0.03:0.02;
		num->SetTextSize(textSize);
		num->Draw();
	}
	// Update title
	TString stit("MC ");
	SDImageAnalysis *imgAna = GetImageAnalysis();
	if(imgAna) stit = imgAna->GetSignature();
	stit += Form("    Cluster %i", ic++);
	fHInCopy->SetTitle(stit.Data());

	gSystem->ProcessEvents();
}

TString SDClusterFinderStdOne::SaveTxtFile(const char csv, UInt_t scan)
{
	// Mar 12, 2013 : look to TString SDManager::SaveTxtFileTwo()
	// csv=' ' - text file;
	// csv=';' ',' ':' csv file
	// May 22; June 23;
	TString snam = GetSDManager()->GetFileNameWithoutExtension(0); // This is absolute file name
	imageStatus status = GetImageAnalysis()->GetImageStatus();
	snam += "_" + GetImageAnalysis()->GetImageStatus().color;
	if(csv != ' ') {snam += "_CSV";}
	if(scan>0)  {snam += "_Scan";}

	snam += ".txt";
	FILE* fout = fopen(snam.Data(), "w");
	TDataSet *cls = GetClusters();
	sdCommon *sdc = sdCommon::GetSdCommon();
	if (fout) {
		if (csv == ' ') { // Text file
			TString stmp;
			stmp.Form("       Signal Detection(LCR):%s  %s ", sdu::GetVersion().Data(), gSystem->GetBuildArch());
			fprintf(fout, "%s\n",stmp.Data());
			fprintf(fout, "%s\n", sdc->CPAI2.Data());
			// fprintf(fout, "%s\n", sdc->rcsid.Data());
			GetSDManager()->PrintVersion(fout);
			fprintf(fout, "FILE: %s\n", snam.Data());

			stmp.Form("User: %s ", gSystem->Getenv("USER"));
			stmp += Form("HOST %s ", gSystem->HostName());
			TDatime dt;
			stmp += dt.AsString();
			fprintf(fout, "%s \n", stmp.Data());

			if (cls->GetListSize() == 0) {
				fprintf(fout, "No cluster : color %s status %i ",
						status.color.Data(), status.status);
				fclose(fout);
				return snam;
			}
			parsInputOutput *pin = sdu::GetParsInput(this);
			fprintf(fout, "%s %s \n", pin->dir.Data(), pin->name.Data());
			//parsFillHists *pf = sdu::GetParsFillHists(this);
			SDTransformation *trans = GetSDManager()->GetTiffFileInfo()->GetSDTransformation();
			fprintf(fout,"Transformation : %s \n", trans->GetSignature().Data());

			parsPedestals peds = GetPedestal()->GetPedestalsPars();
			fprintf(fout, "algorithm\t\t%s\n", peds.algo.Data());
			fprintf(fout, "type of smoothing(filter)\t%s\n", peds.smooth.Data());
			fprintf(fout, "chi2cut %6.4f sigcut %6.4f set %i nsig %6.4f \n",
					peds.chi2cut, peds.sigcut, peds.set, peds.nsig);

			fprintf(fout, "clf %s seed %5.3f ", fPars.name.Data(), fPars.seed);
			fprintf(fout, "spacescale %8.6f(um) ", fPars.spacescale);
			fprintf(fout, " timescale %8.6f(ms) \n\n", fPars.timescale);

			fprintf(fout, "    Number of clusters (sparks) %i\n",
					cls->GetListSize());
			fprintf(fout,
					"#clusts  #pixels  Ave_amp     Peak    Size(um)    Duration(ms) Border   Position(pxls)        Scan\n");
		} else { // CSV text file
			const char* nameOfColumns[]={"nclusts","npixels","ave_amp","peak","size_um",
					                      "duration_ms","Border","Scan",
					                      "Position_pxls"}; // Don't print this
	        Int_t nc = sizeof(nameOfColumns)/sizeof(char*);
	        for(int i=0; i<nc-1; ++i) fprintf(fout,"%s%c",nameOfColumns[i], csv);
	        fprintf(fout,"\n");
		}
		for (Int_t i = 0; i < cls->GetListSize(); i++) {
			SDClusterInfo* cli = GetClusterInfo(i);
			sdClusterChar clch = cli->GetClstCharFromZero(); // is it correct?
			if (csv == ' ') { PrintRecord(fout, cli, i);}
			else          { PrintRecordCSV(fout, cli,i, csv);}
			SDClusterScan* clsc = cli->GetClusterScan();
			if(scan>0 && clsc && clsc->GetNscan()>0) {
				if (csv == ' ') {PrintClusterScan(fout, cli);}
				else            {PrintClusterScanCSV(fout,cli,csv);}
			}
		}
		fclose(fout);
	} else {
		// File was not open
		snam.Form("<Error> File %s is not open! Check the name of file!\n", snam.Data());
		printf("%s", snam.Data());
	}
	return snam;
}

void SDClusterFinderStdOne::PrintRecord(FILE* fout, SDClusterInfo* cli, UInt_t i)
{
	// 2013: Mar 15-17; May 22
	sdClusterChar clch = cli->GetClstCharFromZero();
	Int_t ifort = i + 1;
	fprintf(fout, "%4i     %6i   ", ifort, clch.numpixels);
	fprintf(fout, "%6.3f %9.3f ", clch.aveamp, clch.maxamp);
	fprintf(fout, "%10.3f   %10.3f ", clch.size, clch.duration);
	fprintf(fout, "     %3i", clch.GetBorder());
	fprintf(fout, "  c:%4i-%4i r:%4i-%4i",clch.colmin,clch.colmax, clch.rowmin,clch.rowmax);
	if(clch.GetScan()==0) fprintf(fout,"    %i", clch.GetScan());
	else                  fprintf(fout,"    %i", cli->GetClusterScan()->GetCLFsPool()->GetListSize());
	fprintf(fout,"\n");
}

void SDClusterFinderStdOne::PrintRecordCSV(FILE* fout, SDClusterInfo* cli, UInt_t i, const char csv)
{
	// 2013: Mar 15-17; May 22; June 23
	sdClusterChar clch = cli->GetClstCharFromZero();
	Int_t ifort = i + 1;
	fprintf(fout, "%i%c%i%c", ifort,csv, clch.numpixels,csv);
	fprintf(fout, "%.3f%c%.3f%c", clch.aveamp,csv, clch.maxamp,csv);
	fprintf(fout, "%.3f%c%.3f%c", clch.size,csv, clch.duration,csv);
	fprintf(fout, "%i%c", clch.GetBorder(), csv);
	if(clch.GetScan()==0) fprintf(fout,"%i%c", clch.GetScan(),csv);
	else                  fprintf(fout,"%i%c", cli->GetClusterScan()->GetCLFsPool()->GetListSize(),csv);
	fprintf(fout,"\n");
}

void SDClusterFinderStdOne::PrintClusterScan(FILE* fout, SDClusterInfo* cli)
{
	// June 23, 2013
	SDClusterScan* clsc = cli->GetClusterScan();
	TDataSet* ds = clsc->GetCLFsPool();
	Int_t n = ds->GetListSize();
	if(n==0) return;
	for(Int_t isc=0; isc<n; ++isc) {
        SDClusterFinderStdOne* clf = clsc->GetClusterFinderChild(isc);
        fprintf(fout,"scan %2.2i ampCut %6.4f #clust %i\n",isc+1,clf->GetAmpCut(),clf->GetClusters()->GetListSize());
        for(Int_t ic=0; ic<clf->GetClusters()->GetListSize(); ++ic){
        	SDClusterInfo *clid = clf->GetClusterInfo(ic);
			sdClusterChar clch = clid->GetClstCharFromZero(); // is it correct
			PrintRecord(fout, clid, ic);
        }
	}
    fprintf(fout,"\n");
}

void SDClusterFinderStdOne::PrintClusterScanCSV(FILE* fout, SDClusterInfo* cli, const char csv)
{
	// June 23, 2013
	SDClusterScan* clsc = cli->GetClusterScan();
	TDataSet* ds = clsc->GetCLFsPool();
	for(Int_t isc=0; isc<ds->GetListSize(); ++isc) {
        SDClusterFinderStdOne* clf = clsc->GetClusterFinderChild(isc);
        fprintf(fout,"%i%c%6.4f%c%i%c\n",isc+1,csv,clf->GetAmpCut(),csv,
        		clf->GetClusters()->GetListSize(),csv);
        for(Int_t ic=0; ic<clf->GetClusters()->GetListSize(); ++ic){
        	SDClusterInfo *clid = clf->GetClusterInfo(ic);
			sdClusterChar clch = clid->GetClstCharFromZero(); // is it correct
			PrintRecordCSV(fout, clid, ic, csv);
        }
	}
    fprintf(fout,"\n");
}

void SDClusterFinderStdOne::DrawFinalMessage(TCanvas* c, Int_t clstsReal, Int_t clstsAll)
{
	if(c==0) return;

	TString stmp;
	stmp.Form(" #real clsts %i (%i)", clstsReal, clstsAll);

	TText *text = new TText(0.60,0.90, stmp);
	text->SetNDC(kTRUE);
	text->SetTextColor(kRed);
	text->Draw();

	gSystem->ProcessEvents();
}

