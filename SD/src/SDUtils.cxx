/*
 * SDUtil.cpp
 *
 *  Created on: Apr 17, 2012
 *      2013  : Apr 18;
 *      Author: Alexei Pavlinov
 */

#include "SDUtils.h"
#include "SDManager.h"
#include "SDCluster.h"
#include "SDCluster2D.h"
#include "SDAliases.h"
#include "SDConfigurationParser.h"
#include "SDConfigurationInfo.h"
#include "SDClusterFinderBase.h"
#include "SDClusterFinderStdOne.h"
#include "SDClusterFinderStdTwo.h"
#include "SDClusterFinder2DOne.h"
#include "SDClusterFinder2DTwo.h"
#include "SDClusterFinder2DThree.h"
#include "SDPedestals.h"
#include "sdCommon.h"
#include "SDTiffFileInfo.h"
#include "versionUpdate.h"

#include "SDSimulationManager.h"

#include <TDataSet.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TH2.h>
#include <TF1.h>
#include <TRegexp.h>
#include <TGFileDialog.h>
#include <TGProgressBar.h>
#include <TASImage.h>
#include <TClass.h>
// #include <TThread.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>

using namespace std;

ClassImp(SDUtils)

// Try to do instantiation
//template SDManager* ReadManager<SDManager>(const char* );

SDUtils::SDUtils(const char* name) : TDataSet(name){
	// TODO Auto-generated constructor stub

}

SDUtils::~SDUtils() {
	// TODO Auto-generated destructor stub
}

// sdu::GetParent<SDPedestals> SDPedestals* GetParentPedestal(TDataSet *child);

SDManager* SDUtils::GetSDManager(TDataSet *dsChild, int pri)
{
	// Get first SDManager object from TDataSet tree
    if(dsChild==0) return 0;
	if(dynamic_cast<SDManager *>(dsChild)) return dynamic_cast<SDManager *>(dsChild);
	if(pri) cout<<" SDUtils::GetSDManager : dsChild "<<dsChild->GetName()<<endl;

	TDataSet *parent=0, *child=dsChild;
	SDManager *man = 0;
	while((parent=child->GetParent())){
		if(pri) cout<<" parent "<<parent?parent->GetName():"no parent ";
		man = dynamic_cast<SDManager *>(parent);
		if(man == 0) child = parent;
		else {
			if(pri) cout<<" SD Manager "<<man<<" "<<man->GetName()<<endl;
			return man;
		}
	}
	return 0;
}

SDTiffFileInfo* SDUtils::GetTiffFileInfo(TDataSet* dsChild)
{
	SDManager *man = sdu::GetSDManager(dsChild);
	if(man) return man->GetTiffFileInfo();
	return 0;
}

SDImageAnalysis*  SDUtils::GetImageAnalysis(TDataSet* dsChild)
{
	return sdu::GetParentTempl<SDImageAnalysis>(dsChild);
}

SDPedestals*  SDUtils::GetPedestals(TDataSet* dsChild)
{
	SDManager *man = sdu::GetSDManager(dsChild);
	if(man) return man->GetPedestals();
	return 0;
}

SDPedestals* SDUtils::GetPedestalsParent(TDataSet* dsChild)
{
	// Get nearest parent pedestal
	TDataSet *parent = 0;
	SDPedestals* ped = 0;
	while ((parent = dsChild->GetParent())) {
		ped = dynamic_cast<SDPedestals *>(parent);
		if (ped == 0)
			dsChild = parent;
		else
			return ped;
	}
	return 0;
}

SDClusterFinderBase*  SDUtils::GetClusterFinder(TDataSet* dsChild, const Int_t ind)
{
	SDManager *man = sdu::GetSDManager(dsChild);
	if(man) return man->GetClusterFinder(ind);
	return 0;
}

SDClusterFinderBase*  SDUtils::GetClusterFinderParent(TDataSet* dsChild)
{
	// SDClusterFinderBase* SDClusterInfo::GetClusterFinder()
	// Get nearest parent cluster finder
		TDataSet *parent=0;
		SDClusterFinderBase* clf=0;
		while((parent=dsChild->GetParent())){
			clf = dynamic_cast<SDClusterFinderBase *>(parent);
			if(clf == 0) dsChild = parent;
			else         return clf;
		}
		return 0;
}

parsClusterFinder* SDUtils::GetParsClusterFinder(TDataSet *dsChild, int pri)
{
	SDManager *man = sdu::GetSDManager(dsChild, pri);
	if(man==0) 	return 0;
	return &(man->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsClusterFinder());

}

parsInputOutput*   SDUtils::GetParsInput(TDataSet* dsChild)
{
	SDManager *man = sdu::GetSDManager(dsChild);
	if(man) return &(man->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsInput());
	return 0;
}

parsInputOutput*   SDUtils::GetParsOutput(TDataSet* dsChild)
{
	SDManager *man = sdu::GetSDManager(dsChild);
	if(man) return &(man->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsOutput());
	return 0;
}

parsFillHists*     SDUtils::GetParsFillHists(TDataSet* dsChild)
{
	SDManager *man = sdu::GetSDManager(dsChild);
	if(man) return &(man->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsFillHists());
	return 0;
}

parsPedestals*  SDUtils::GetParsPedestals(TDataSet* dsChild)
{
	SDManager *man = sdu::GetSDManager(dsChild);
	if(man) return &(man->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsPedestals());
	return 0;
}

Double_t SDUtils::RelError(Double_t a1, Double_t a2)
{
	// Calculate relative difference in %
	if(a1==0.0) return 0.;
	return 100.*(1. - a2/a1);
}

// Pixel is neighbor to cluster if it is neighbor to any pixels from cluster
Bool_t SDUtils::IsPixelNeighbourToCluster(const pixel &p, clusterSimple* vp, Bool_t (*fun)(const pixel &p1, const pixel &p2))
{
  clusterSimple::iterator it;
  for(it=vp->begin(); it!=vp->end(); ++it){
     if(fun(p,*it)) return kTRUE;
  }
  return kFALSE;
}

Bool_t SDUtils::IsPixelNeighbourToCluster(const pixel &p, SDCluster *clst, Bool_t (*fun)(const pixel &c1, const pixel &c2))
{
  for(Int_t i=0; i<clst->GetNRows(); ++i) {
     if(fun(p,*(clst->GetTable(i)))) return kTRUE;
  }
  return kFALSE;
}

TImagePalette* SDUtils::GetGrayPallete(Int_t npoints, Int_t nbits, Int_t pri)
{
   npoints = npoints<=0?256:npoints;
   nbits   = nbits<8?8:nbits;
   Int_t max = (1<<nbits)-1;
   TImagePalette* pal = new TImagePalette(npoints);
   Double_t step = 1. / Double_t(max); // Normalization to one
   if(pri>0) cout<<" max "<<max<<" nbits "<<nbits<<" step "<<step<<endl;

    for (int i = 0; i < npoints; i++) {
       pal->fPoints[i] = i*step;
//       pal->fColorRed[i] = pal->fColorGreen[i] = pal->fColorBlue[i] = i << 8; // Shift for what
       pal->fColorRed[i] = pal->fColorGreen[i] = pal->fColorBlue[i] = 256*i; // Shift for what
       pal->fColorAlpha[i] = 0xffff;
//       pal->fColorAlpha[i] = 0xff;
   }
    if(pri>1) PrintPallete(pal);
    return pal;
}

void SDUtils::PrintPallete(const TImagePalette* pal)
{
	if(pal==0) return;
	cout<<" Palette Name "<< pal->GetName()<<" fNumPoints "<<pal->fNumPoints<<endl;
    for (UInt_t i = 0; i < pal->fNumPoints; i++) {
    	   cout<<i<<"\t fPoints "<<pal->fPoints[i]<<"\t R G B \t";
    	   cout<<pal->fColorRed[i]<<"\t"<<pal->fColorGreen[i]<<"\t"<<pal->fColorBlue[i];
    	   cout<<"\talpha "<<pal->fColorAlpha[i]<<endl;
    }
	cout<<" fNumPoints "<<pal->fNumPoints<<endl;
}

void SDUtils::FillPixel(TH2* hin, Int_t binx, Int_t biny, pixel &p,Int_t reset)
{

	p.col = static_cast<UShort_t>(hin->GetXaxis()->GetBinCenter(binx));
	p.row = static_cast<UShort_t>(hin->GetYaxis()->GetBinCenter(biny));
	p.amp = static_cast<Float_t>(hin->GetBinContent(binx, biny));
#ifdef NEIGHBORS
    if(p.neighbors==0 && 0) {
		for (Int_t ix = binx - 1; ix <= binx + 1; ++ix) {
			for (Int_t iy = biny - 1; iy <= biny + 1; ++iy) {
				if (ix == binx && iy == biny)
					continue; // pixel itself
				if (hin->GetBinContent(ix, iy) > 0.0)
					++(p.neighbors);
			}
		}
		// cout << " pixel " << p << endl;
    }
#endif
	if (reset > 0) hin->SetBinContent(binx, biny, 0.0);
}

//void SDUtils::FillPixelWithNeighbors(const TH2* hin, Int_t binx, Int_t biny, pixel &p)
//{
////	  cout<<" pixel "<<p<<endl; // stay here ->should be discard
//  p.col = static_cast<UShort_t>(hin->GetXaxis()->GetBinCenter(binx));
//  p.row = static_cast<UShort_t>(hin->GetYaxis()->GetBinCenter(biny));
//  p.amp = static_cast<Float_t>(hin->GetBinContent(binx,biny));
//#ifdef NEIGHBORS
//  // May 25,2013 : fill neighbors
//  for(Int_t ix=binx-1; ix<=binx+1; ++ix){
//	  for(Int_t iy=biny-1; iy<=biny+1; ++iy){
//		  if(ix==binx && iy==biny) continue; // pixel itself
//		  if(hin->GetBinContent(ix,iy)>0.0) ++(p.neighbors);
//	  }
//  }
//  cout<<" pixel "<<p<<endl;
//#endif
////  cin>>reset;
//}

void SDUtils::FillCluster(const TH2 *hin, const Double_t th, const Double_t ped, clusterSimple & vp, TH2* h2th)
{
    static Int_t pri=1;
	if(hin==0) return;

	pixel c;
	UInt_t sizex = (UShort_t)hin->GetXaxis()->GetNbins();
	UInt_t sizey = (UShort_t)hin->GetYaxis()->GetNbins();
	for (UInt_t col = 0; col < sizex; ++col) {
		for (UInt_t row = 0; row < sizey; ++row) {
			if(hin->GetBinContent(col,row)>=th) {
				c.amp = static_cast<Float_t>(hin->GetBinContent(col,row));
				if(ped>0.0) c.amp -= ped; // th depend from time bin but not now
#ifdef Pixel_ONE_
				c.ind = SDUtils::GetIndexFromColRow((UShort_t)col, (UShort_t)row);
#else
				c.col = UShort_t(col);
				c.row = UShort_t(row);
#endif
				vp.push_back(c);
				h2th->SetBinContent(col,row, c.amp);
			}
		}
	}
#ifndef LIST_CONT_
	sort(vp.begin(), vp.end(), (compPixels())); // compPixels() is entity of class compPixels
#else
	vp.sort((compPixels())); // compPixels() is entity of class compPixels
#endif
	if(pri) {
		cout<<" h "<<hin->GetName()<<" deque size "<<vp.size();
		cout<<" th "<<th<<" max "<<h2th->GetMaximum()<<endl;
	}
}

void SDUtils::FillCluster2d(const TH2* hin, const Double_t th,
		clusterContainerSimple& vp, TH2* h2th) {
  // vp : y dim (max row), x - real number of col.
	static Int_t pri = 0;
	if (hin == 0)
		return;

	pixel c;
	UInt_t sizex = (UShort_t) hin->GetXaxis()->GetNbins();
	UInt_t sizey = (UShort_t) hin->GetYaxis()->GetNbins();
	clusterContainerSimple::iterator it;
	for (UInt_t col = 0; col < sizex; ++col) {
		for (UInt_t row = 0; row < sizey; ++row) {
			if (hin->GetBinContent(col, row) >= th) {
				c.amp
						= static_cast<Float_t> (hin->GetBinContent(col, row));

#ifdef Pixel_ONE_
				c.ind = SDUtils::GetIndexFromColRow((UShort_t)col, (UShort_t)row);
#else
				c.col = UShort_t(col);
				c.row = UShort_t(row);
#endif
//                it = (vp.begin()+Int_t(row)); // does not work for list
				it = vp.begin(); for(UShort_t i=0;i<row; ++i) ++it;

				clusterSimple* clst= (*it);
				clst->push_back(c);
				h2th->SetBinContent(col, row, c.amp);
			}
		}
	}

	if (pri) {
		cout << " h " << hin->GetName() << " vp size " << vp.size();
		cout << " th " << th << " max " << h2th->GetMaximum() << endl;
	}
}

void SDUtils::FillClusterFromHist(const TH2* hin, clusterSimple &cl, Bool_t sort)
{
	if (hin == 0)
		return;

	pixel c;
	UInt_t sizex = (UShort_t) hin->GetNbinsX();
	UInt_t sizey = (UShort_t) hin->GetNbinsY();
	for (UInt_t ix = 1; ix <= sizex; ++ix) {
		UInt_t col = UInt_t(hin->GetXaxis()->GetBinCenter(ix));
		for (UInt_t iy = 1; iy <= sizey; ++iy) {
			UInt_t row = UInt_t(hin->GetYaxis()->GetBinCenter(iy));
			c.amp = static_cast<Float_t> (hin->GetBinContent(ix, iy));
			if (c.amp > 0.0) {
				c.col = col;
				c.row = row;
				cl.push_back(c);
			}
		}
	}
    std::sort (cl.begin(), cl.end(), (compPixels()));
}

//void SDUtils::FillClusterFromHist(const TH2* hin, SDCluster &cl)
void SDUtils::FillClusterFromHist(const TH2* hin, SDClusterBase &cl)
{
	if(hin==0) return;

	pixel c;
	UInt_t sizex = (UShort_t) hin->GetNbinsX();
	UInt_t sizey = (UShort_t) hin->GetNbinsY();
	for (UInt_t ix = 1; ix <= sizex; ++ix) {
        UInt_t col = UInt_t(hin->GetXaxis()->GetBinCenter(ix));
		for (UInt_t iy = 1; iy <= sizey; ++iy) {
	        UInt_t row = UInt_t(hin->GetYaxis()->GetBinCenter(iy));
			c.amp = static_cast<Float_t> (hin->GetBinContent(col, row));
			if(c.amp>0.0) {
			  c.col = col;
			  c.row = row;
			  cl.AddPixel(c);
			}
		}
	}
}
// from clusters to hists
TH2* SDUtils::FillHistFromCluster(clusterSimple* clust, Int_t nclust)
{
	UShort_t col,row,  colMin,colMax, rowMin,rowMax;
	clusterSimple::iterator it;
	for(it=clust->begin(); it!=clust->end(); ++it){
	   pixel c = *it;
	   col = c.col; row = c.row;
	   if(it==clust->begin()){
		 colMin = colMax = col;
		 rowMin = rowMax = row;
	   } else {
		     if(colMin>col) colMin = col;
		     if(colMax<col) colMax = col;
		     if(rowMin>row) rowMin = row;
		     if(rowMax<row) rowMax = row;
	   }
	}
	Int_t nx = colMax - colMin + 1;
	Int_t ny = rowMax - rowMin + 1;
	Double_t xmi = Double_t(colMin) - 0.5;
	Double_t xma = Double_t(colMax) + 0.5;
	Double_t ymi = Double_t(rowMin) - 0.5;
	Double_t yma = Double_t(rowMax) + 0.5;;
	TString snam(Form("hClust %i",nclust));
	TString stit(Form("Cluster %i, #Deltax %i #Deltay %i", nclust, nx, ny));
	TH2* h2 = new TH2F(snam.Data(),stit.Data(), nx,xmi,xma, ny, ymi,yma);
	h2->SetOption("colz");

	for(it=clust->begin(); it!=clust->end(); ++it){
	   pixel c = *it;
	   col = c.col; row = c.row;
	   h2->Fill(double(col),double(row),double(c.amp));
	}
	return h2;
}

TDataSet* SDUtils::ReadDirectory(const char* dir)
{
	TString DIR(dir);
	gSystem->ExpandPathName(DIR);
	DIR.ReplaceAll(":", "");
	Int_t nfile = 0; // number of root files
	FileStat_t buf;
	if (gSystem->GetPathInfo(DIR.Data(), buf) == 1) { // no directory
		cout<<"ReadDirectory => No directory "<<DIR.Data()<<endl;
		return 0;
	}
	cout<<"readDirectory DIR "<<DIR.Data()<<endl;
	TString nlist(Form("%s/tmpFileList.lst", DIR.Data()));
	TString cmd(Form(".!ls %s/*.root > %s ", DIR.Data(), nlist.Data()));
	gROOT->ProcessLine(cmd.Data());
	// See readAllAnaObjectsFromDirectory
	ifstream in;
	in.open(nlist.Data());
	char st[200];

	TDataSet *ds = new TDataSet("ds ReadDir");

	if (in.is_open() == 0)
		return 0;
// Read just plain directory : look to TTestBeamPictures::readDirectory
	while (1) {
		in >> st;
		if (!in.good())
			break;
		TString nam(Form("%s", st)), nst(st);

		if (nst.Contains(".root")) { // simple file
            TDataSet* ds1 = dynamic_cast<SDManager *>(sda::ReadFirstObjectFromDirectory(nst.Data()));
            ds1->SetName(Form("%i %s",nfile,ds1->GetName()));
			ds->Add(ds1);
			nfile++;
			printf("%i read : %s \n", nfile, nam.Data());
		}
	}
	cout<<"Read "<<ds->GetListSize()<<" files \n";
	return ds;
}

//template<typename T>
//T* SDUtils::ReadManager(const char *nf)
//{
//	TObject* obj = sda::ReadFirstObject(nf);
//	return dynamic_cast<T *>(obj);
//}
//template<typename T>
//T* SDUtils::ReadManagerFromDirectory(const char* dir)
//{
//	TString sdir(dir);
//	if(dir==0) sdir += gSystem->Getenv("SDOUTPUT");
//	cout<<"\n\t ReadFileFromDirectory("<<sdir<<")\n";
//
//	const char *openTypes[] = { "root files",   "*.root",
//	                            "All files",    "*",
//	                            0,              0 };
//
//    TGFileInfo fi;
//    fi.fFileTypes = openTypes;
//    fi.fIniDir    = StrDup(sdir.Data());
//    new TGFileDialog(gClient->GetDefaultRoot(), 0, kFDOpen,&fi);
//    if (!fi.fFilename) return 0;
//
//    // fi.fIniDir does not change after dialog.
//    // fi.fFileName will keep absolute file name.
//    TObject *obj = sda::ReadFirstObject(fi.fFilename);
//    return dynamic_cast<T *>(obj);
//}

void SDUtils::FillFitInfoToHist(TH2* h2, Double_t sigma, Option_t* option, Double_t threshold, Double_t thAbs, const char *name)
{
  TString tity("1px 0.05 #mum");
  TString titx(Form("#sigma %4.1f opt %s th %4.3f(%5.1f) : 1px 2.09 #mus",sigma,option,threshold,thAbs));
  sda::Titles(h2, titx.Data(), tity.Data());
}

TH2* SDUtils::CloneHist(TH2* hin, const char* addName, const char* tit, int reset)
{
  if(hin == 0) return 0;
  TH1::AddDirectory(kFALSE);
  // cout<<" hin "<<hin<<" addName "<<addName<<" tit "<<tit<<" reset "<<reset<<endl;

  TString snam(addName);
  if(strlen(addName)==0) snam.Prepend(hin->GetName());
  TH2* hout = (TH2*)hin->Clone(snam.Data());
  hout->SetTitle(tit);
  if(reset>0) hout->Reset();

  return hout;
}

void SDUtils::AddCorrValueToTitle(TH2* h2) {
	if (h2 == 0) return;
	TString stit = h2->GetTitle();
	stit += Form(" : corr %6.4f ", h2->GetCorrelationFactor());
	h2->SetTitle(stit.Data());
}

TList* SDUtils::BookAndFill1DHistsFromClusterHist(TH2* h2, const char* opt, Int_t scaleProj, TH1* hamp)
{
	// Book set of histogram for TH2* hist.
	// Need for tuning (controlling) set of cuts for cluster finder
	// or calculation cluster characteristics
	if(h2==0 || (h2&&h2->GetEntries()==0)) {
		cout<<"<W> SDUtils::BookAndFill1DHistsFromClusterHist : h2 "<<h2<<endl;
		if(h2) cout<<"\t Entries "<<h2->GetEntries()<<endl;
		return 0;
	}
	TH1::AddDirectory(1);

    Int_t ic=0;

	TString snam(Form("%2.2i ProjX",ic++)), stit;
	TH1D *hd = h2->ProjectionX(snam.Data());
	stit = " X proj";
	if(scaleProj>0) {
      Int_t scale=h2->GetNbinsY();
	  stit = Form("X proj / %i ",scale);
	  hd->Scale(1./scale);
	}
	hd->SetTitle(stit.Data());

	snam = Form("%2.2i ProjY",ic++);
	hd = h2->ProjectionY(snam.Data());
	stit = " Y proj";
	if(scaleProj>0) {
	  Int_t scale=h2->GetNbinsX();
	  stit = Form("Y proj / %i",scale);
      hd->Scale(1./scale);
	}
	hd->SetTitle(stit.Data());

	snam = Form("%2.2i_AmpDist",ic++);
    stit = "Amp dist. ; Amp ; N";
    Int_t scale=1, nbin=255; // due to one bit for digitization
    Double_t xmax = h2->GetMaximum()+0.001, xmin = h2->GetMinimum();
    if(hamp==0 && scale>1.) {
      if(xmax<100.0) xmax = Int_t(h2->GetMaximum()/scale + 1)*scale;
      if     (xmin > 0) xmin = 0.0;
      else if(xmin < 0) xmin = (xmin/scale - 1)*scale;
    } else if(hamp){
      xmin = hamp->GetXaxis()->GetXmin();
      xmax = hamp->GetXaxis()->GetXmax();
      nbin = hamp->GetNbinsX();
    }
	TH1F* h = new TH1F(snam.Data(), stit.Data(), nbin, xmin ,xmax);

	for (UInt_t ix = 1; ix <= UInt_t(h2->GetNbinsX()); ++ix) {
		for (UInt_t iy=1; iy<=UInt_t(h2->GetNbinsY()); ++iy) {
			Double_t amp = h2->GetBinContent(ix, iy);
    // Discard bin with zero amplitude
		    if(amp != 0.0) h->Fill(amp);
		}
	}

	TList *l = sda::moveHistsToList("SumProj");
	sda::AddToNameAndTitleToList(l, opt, opt);

	TH1::AddDirectory(0);
	return l;
}

SDClusterFinderBase* SDUtils::CreateClusterFinder(parsClusterFinder &pcf, int pri)
{
	SDClusterFinderBase *clf = 0;
	vecStr& nclfs = sd::GetSdCommon()->nameOfClfs;

	TString snam("CF ");
	snam += pcf.name;
	if        (pcf.name == nclfs[4].c_str()) {
		clf = new SDClusterFinder2DThree(snam.Data());
	} else if (pcf.name == nclfs[3].c_str()) {
			clf = new SDClusterFinder2DTwo(snam.Data());
	} else if (pcf.name == nclfs[2].c_str()) {
		clf = new SDClusterFinder2DOne(snam.Data());
	} else if (pcf.name == nclfs[1].c_str()) {
		clf = new SDClusterFinderStdTwo(snam.Data());
	} else if (pcf.name == nclfs[0].c_str()) {
		clf = new SDClusterFinderStdOne(snam.Data());
	} else { // bad name => get default : 2dtwo
		cout << " SDClusterFinderBase* SDUtils::CreateClusterFinder \n";
		cout << " Cluster finder " << pcf.name << " does not exist \n";
		pcf.name = nclfs[3].c_str();
		snam = "CF ";
		snam += pcf.name;
		clf = new SDClusterFinder2DTwo(snam.Data());
	}
	clf->SetCFPars(pcf);
	if(pri>0) {
		cout<<" SDUtils::CreateClusterFinder(parsClusterFinder : pcf.name "<<pcf.name<<endl;
		cout<<" type "<<clf->Class()->GetName()<<" name "<<snam.Data()<<endl;
		if(pri>1) cout<<pcf<<endl;
	}
	return clf;
}

SDClusterBase* SDUtils::CreateCluster(const char* name, const UInt_t nmax,const UInt_t key)
{
	SDClusterBase* clb=0;
	if(key==0) {
		clb = new SDCluster(name, nmax);
	} else {
		clb = new SDCluster2D(name);
	}
	return clb;
}

TF1* SDUtils::Expo(const char *addName, double xmi, double xma, const char* name0,
		const char* name1, Double_t val0, Double_t val1)
{
	TString name("expo");
	name += addName;
	TF1 *fun = new TF1(name.Data(), "[0]*TMath::Exp([1]*x)", xmi, xma);
	fun->SetLineColor(kRed);
	fun->SetParName(0,name0);
	fun->SetParName(1,name1);
	fun->SetParameter(0,val0);
	fun->SetParameter(1,val1);
	return fun;
}

void SDUtils::PrintChar(char ch)
{
	Int_t ich=int(ch);
	cout<<ch<<" dec "<<dec<<ich;
	cout<<" oct "<<oct<<ich<<" hex "<<hex<<ich<<endl;
}

void SDUtils::PrintBlock(byte* vch, Int_t start, Int_t end, const Char_t *tit)
{
    if(strlen(tit)) cout<<tit<<endl;
	for (Int_t i = start; i < end; i++) {
		cout << " i " << dec << i << " vch ";
        PrintChar	(vch[i]);
    }
}

void SDUtils::PrintBlock(vector<char> memblock, Int_t start, Int_t end, const Char_t *tit)
{
    if(strlen(tit)) cout<<tit<<endl;
    for(Int_t i=start; i<end; i++) {
    	cout<<" i "<<dec<<i<<" mem ";
    	PrintChar(memblock[i]);
    }
}

void SDUtils::PrintBlock(UShort_t* vshort, Int_t start, Int_t end, const Char_t *tit)
{
    if(strlen(tit)) cout<<tit<<endl;
    for(Int_t i=start; i<end; i++) {
    	cout<<" i   "<<dec<<i<<" Shor_t(dec) "<<dec<<vshort[i];
    	cout<<" oct "<<oct<<vshort[i]<<" hex "<<hex<<vshort[i]<<endl;
    }
}

UShort_t SDUtils::GetUShort(byte* vch, const UInt_t ind, Bool_t switchByte)
{
	static byte bt[2];
	UShort_t* p = 0;
	if(switchByte) {
		for(UInt_t i=0; i<2; i++) bt[i] = vch[ind+1-i];
		p = reinterpret_cast<UShort_t*>(&bt[0]);
	} else {
		p = reinterpret_cast<UShort_t*>(&vch[ind]);
	}

	return *p;
}

void SDUtils::DeleteVectorMember(vector<TString> &v, TString &m, int pri)
{
    if(pri) cout<<"<I> SDUtils::DeleteVectorMember  v.size() (in) "<<v.size()<<" m "<<m;

	vector<TString>::iterator it;
	for(it=v.begin();it != v.end(); ++it) {
		if(*it == m){
			v.erase(it);
			break;
		}
	}
    if(pri) cout<<" v.size() (out) "<<v.size()<<endl;
	return;
}

UInt_t SDUtils::GetUInt(byte* vch, const UInt_t ind, Bool_t switchByte)
{
	static byte bt[4];
	UInt_t* p = 0;
	if(switchByte) {  // 0,1,2,3 => 3,2,1,0 : change byte order
		for(UInt_t i=0; i<4; i++) bt[i] = vch[ind+3-i];
		p = reinterpret_cast<UInt_t*>(&bt[0]);
	} else {
	  p = reinterpret_cast<UInt_t*>(&vch[ind]);
	}
	return *p;
}

Float_t  SDUtils::GetFloat(byte* vch, const UInt_t ind, Bool_t switchByte)
{
	static byte bt[4];
	Float_t* p = 0;
	if(switchByte) {  // 0,1,2,3 => 3,2,1,0 : change byte order
		for(UInt_t i=0; i<4; i++) bt[i] = vch[ind+3-i];
		p = reinterpret_cast<Float_t*>(&bt[0]);
	} else {
	  p = reinterpret_cast<Float_t*>(&vch[ind]);
	}
	return *p;
}

Long_t  SDUtils::GetPointer(void* p, Long_t offset)
{
   // GUI with dictionary
   return (reinterpret_cast<Long_t>(p) + offset);
}

Float_t  SDUtils::GetPointerFloat(void* p, Long_t offset)
{
	Float_t* pf = reinterpret_cast<Float_t *>(GetPointer(p,offset));
	return (*pf);
}

Int_t  SDUtils::GetPointerInt(void* p, Long_t offset)
{
	Int_t* pf = reinterpret_cast<Int_t *>(GetPointer(p,offset));
	return (*pf);
}

TString SDUtils::GetPointerTString(void* p, Long_t offset)
{
	TString* pf = reinterpret_cast<TString *>(GetPointer(p,offset));
	return (*pf);
}

template <class T> T SDUtils::GetPointer(void* p, Long_t offset)
{
  T* pt = reinterpret_cast<T *>(GetPointer(p,offset));
  return *pt;
}

TGFrame* SDUtils::GetFrame(TList *l,Int_t i)
{
	if(l==0) return 0;
	return dynamic_cast<TGFrame *>(l->At(i<0?0:i));
}

Bool_t SDUtils::GetFWHM(TH1* h, Double_t &xhm1, Double_t &xhm2)
{
	// AHM  - amplitude at half-maximum
	// FWHM - full width at half maximum (space)
	// FWDM - full duration at half maximum (time)
	// FWHM = xhm2 - xmh1
	// For finding xhm1(2) we use the linear assumption between
	// two neighbor points of histogram: one point has y value more
	// than AHM and another point has y value less than AHM.
	if(h==0) return kFALSE; // histogram undefined

	Double_t amax = h->GetMaximum(), ahm = amax/2.;
	TAxis *xax = h->GetXaxis();
	Int_t bfirst = h->FindFirstBinAbove(ahm);
	Int_t blast  = h->FindLastBinAbove(ahm);
//	xhm1 = xax->GetXmin();
//	xhm2 = xax->GetXmax();
//	if(bfirst==1 && blast==xax->GetNbins()) return kTRUE; // Kind of "flat" distribution

	// Find left point - xhm1
	Double_t x1=xax->GetBinCenter(bfirst), y1=h->GetBinContent(bfirst);
	Int_t b2 = bfirst - 1;
	Double_t x2=xax->GetBinCenter(b2), y2=h->GetBinContent(b2); // if b2>=1
	if(b2==0) {
		y2 = 0.0;
		x2 = x1 - xax->GetBinWidth(bfirst);
	}
	xhm1 = GetX(ahm, x1,y1, x2,y2);

	// Find right point - xhm2
	x1=xax->GetBinCenter(blast);
	y1=h->GetBinContent(blast);
	b2 = blast + 1;
	x2=xax->GetBinCenter(b2);
	y2=h->GetBinContent(b2); // if b2 > Nbins
	if(b2>xax->GetNbins()) {
		y2 = 0.0;
		x2 = x1 + xax->GetBinWidth(blast);
	}
	xhm2 = GetX(ahm, x1,y1, x2,y2);

	return kTRUE;
}

Bool_t SDUtils::GetFWHM(TH1* h, Double_t &fwhm)
{
	Double_t xm1, xm2;
	Bool_t key = sdu::GetFWHM(h,xm1,xm2);
	fwhm = xm2 - xm1;
	return key;
}

void SDUtils::GetFWHMs(TH1* hx, TH1* hy, sdClusterChar& sdCh)
{
	Bool_t key = sdu::GetFWHM(hx,sdCh.xhm1,sdCh.xhm2);
	sdCh.duration = sdCh.xhm2 - sdCh.xhm1;
	key = sdu::GetFWHM(hy,sdCh.yhm1,sdCh.yhm2);
	sdCh.size = sdCh.yhm2 - sdCh.yhm1;
}

Double_t SDUtils::GetX(Double_t y, Double_t x1,Double_t y1, Double_t x2,Double_t y2)
{
	// Resolve linear equation between two points: (x1,y1) and (x2,y2)
	// x = (y-y1)*(x2-x1)/(y2-y1) + x1
	Double_t x = (y-y1)*(x2-x1)/(y2-y1) + x1;
	return x;
}

TH1* SDUtils::RebinAndReplace(TList *l, Int_t ind, TH1* hin, Int_t nrebin)
{
	if(hin==0) return 0;

	if(nrebin>1) {
		TH1* hnew = hin->Rebin(nrebin,Form("%sRebin%i",hin->GetName(),nrebin));
		hnew->SetTitle(Form("%s Rebin %i", hin->GetTitle(),nrebin));
		if(hnew) {
		  l->Remove(hin);
		  delete hin;
		  hin = hnew;
		  l->AddAt(hin,ind);
		}
	}
	return hin;
}
// Median filter
TH2* SDUtils::MedianFilter(TH2* hin, UInt_t window, TH2 *hout)
{
	// Input:
    // hin    - input histogram
	// window - window for median filter, could be 1,2,3..
	// Output:
	// Will be created 2d histogram after median filter
	// if hout==0 initially

	// window=1 : square 3x3
	//        i : square (2i+1)x(2i+1)

	cout<<"<I> SDUtils::MedianFilter hin "<<hin<<" window "<<window<<endl;
	if(hin == 0) return 0;

	UInt_t nmax = TMath::Min(hin->GetNbinsX(),hin->GetNbinsY());
	window = window>nmax?nmax:window;
	window = window<1?1:window;

    // Create output histogram
	Long_t size=2*window+1;
    if(hout==0) {
      TH1::AddDirectory(kFALSE);
  	  TString snam(hin->GetName()), stit(hin->GetTitle());
	  snam += Form("m%i",window);
	  stit += Form("Median filter, m%i(%ix%i)", window, Int_t(size),Int_t(size));
	  hout = dynamic_cast<TH2 *>(hin->Clone(snam.Data()));
    }

	size *= size;
	TArrayF arr(size); // Working array

	for(Int_t binx=1; binx<=hin->GetNbinsX(); binx++){
		Int_t xmin=binx-window, xmax=binx+window;
		xmin = xmin<1?1:xmin;
		xmax = xmax>hin->GetNbinsX()?hin->GetNbinsX():xmax;
		for(Int_t biny=1; biny<=hin->GetNbinsY(); biny++){
			Int_t ymin = biny-window, ymax=biny+window;
			ymin = ymin<1?1:ymin;
			ymax = ymax>hin->GetNbinsY()?hin->GetNbinsY():ymax;
			Int_t i=0;
			for(Int_t ix=xmin; ix<=xmax; ++ix){
				for(Int_t iy=ymin; iy<=ymax; ++iy){
					arr[i++] = Float_t(hin->GetBinContent(ix,iy));
				}
			}
			// i < arr.GetSise() for boundary cells
			Double_t median = TMath::Median(i, arr.GetArray());
			hout->SetBinContent(binx,biny,median);
		}
	}
	return hout;
}

TH2* SDUtils::MedianFilter(TH2* hin, TString sopt, TH2 *hout)
{
//   cout<<"<W> sopt "<<sopt<<" should be m1,m2 and so on \n";
   Int_t window = 1; // Default value if opt has wrong value
   if(sopt.Contains("median")) {
	  TString st = sopt(6,sopt.Length()-1);
	  if(st.IsDigit()) window = st.Atoi();
   }
   return MedianFilter(hin,window, hout);
}

TH2* SDUtils::MeanFilter(TH2* hin,UInt_t wx,UInt_t wy, TH2* hout)
{

	// Input:
    // hin    - input histogram
	// wx(wy) - window for mean filter in x(y) direction, could be 1,2,3..
	// Output:
	// Will be created 2d histogram after median filter
	// if hout==0 initially

	cout<<"<I> SDUtils::MeanFilter hin "<<hin<<" wx "<<wx<<" wy "<<wy<<endl;
	if(hin == 0) return 0;

    // Create output histogram
    if(hout==0) {
      TH1::AddDirectory(kFALSE);
  	  TString snam(hin->GetName()), stit(hin->GetTitle());
	  snam += Form("Mean2d %ix%i",wx,wy);
	  stit += Form("Mean filter(2d), %i x %i)", wx,wy);
	  hout = dynamic_cast<TH2 *>(hin->Clone(snam.Data()));
	  hout->SetTitle(stit.Data());
    }

	UInt_t size = (2*wx+1)*(2*wy+1);
	TArrayF arr(size); // Working array

	for(Int_t binx=1; binx<=hin->GetNbinsX(); ++binx){
		Int_t xmin=binx-wx, xmax=binx+wx;
		xmin = xmin<1?1:xmin;
		xmax = xmax>hin->GetNbinsX()?hin->GetNbinsX():xmax;
		for(Int_t biny=1; biny<=hin->GetNbinsY(); ++biny){
			Int_t ymin = biny-wy, ymax=biny+wy;
			ymin = ymin<1?1:ymin;
			ymax = ymax>hin->GetNbinsY()?hin->GetNbinsY():ymax;
			Int_t i=0;
			for(Int_t ix=xmin; ix<=xmax; ++ix){
				for(Int_t iy=ymin; iy<=ymax; ++iy){
					arr[i++] = Float_t(hin->GetBinContent(ix,iy));
				}
			}
			// i < arr.GetSise() for boundary cells
			Double_t mean = TMath::Mean(i, arr.GetArray());
			hout->SetBinContent(binx,biny,mean);
//			if(biny==1) {
//				cout<<" binx "<<binx<<" A "<<hin->GetBinContent(binx,biny);
//				cout<<" i "<<i<<" mean "<<mean<<endl;
//			}
		}
	}
	return hout;
}

TH2* SDUtils::MeanFilter(TH2* hin, TString sopt, TH2 *hout)
{
//   cout<<"<W> sopt "<<sopt<<" should be m1,m2 and so on \n";
   Int_t wx=1, wy=8; // Default value if opt has wrong value
   if(sopt.Contains("mean")) {
	  TString st = sopt(4,sopt.Length()-1);
	  Int_t indx = st.Index("x");
	  TString stwx = st(0,indx);
	  if(stwx.IsDigit()) wx = stwx.Atoi();
	  TString stwy = st(indx+1,st.Length()-indx-1);
	  if(stwy.IsDigit()) wy = stwy.Atoi();
   }
   return MeanFilter(hin,wx,wy, hout);
}

TH1* SDUtils::MeanFilter(TH1* hin,UInt_t w, TH1* hout, Int_t pri)
{
	// Input:
	// hin    - input histogram
	// w - window for mean filter, could be 1,2,3..
	// Output:
	// Will be created 1d histogram after median filter
	// if hout==0 initially

	if(pri) cout << "<I> SDUtils::MeanFilter hin " << hin << " w " << w <<endl;
	if (hin == 0) return 0;

	// Create output histogram
	TH1* houtn = 0;
	if (hout == 0) {
	    TH1::AddDirectory(kFALSE);
		TString snam(hin->GetName()), stit(hin->GetTitle());
		snam += Form("Mean1d %i", w);
		stit += Form("Mean filter(1d), %i)", w);
		houtn = dynamic_cast<TH1 *> (hin->Clone(snam.Data()));
		houtn->SetTitle(stit.Data());
	} else houtn = hout;

	UInt_t size = (2*w + 1);
	TArrayF arr(size); // Working array

	for (Int_t binx = 1; binx <= hin->GetNbinsX(); ++binx) {
		Int_t xmin = binx - w, xmax = binx + w;
		xmin = xmin < 1 ? 1 : xmin;
		xmax = xmax > hin->GetNbinsX() ? hin->GetNbinsX() : xmax;
		Int_t i = 0;
		for (Int_t ix = xmin; ix <= xmax; ++ix) {
			arr[i++] = Float_t(hin->GetBinContent(ix));
		}
		// i < arr.GetSise() for boundary cells
		Double_t mean = TMath::Mean(i, arr.GetArray());
		houtn->SetBinContent(binx, mean);
	}
	if(pri) cout<<" houtn " << houtn << endl;
	return houtn;
}

TH2* SDUtils::Smooth(TH2* hin, TString sopt, TH2* hout)
{
	 // Apply hbook,median or mean filter
	if (sopt.Contains("r5",TString::kIgnoreCase) || sopt.Contains("r3",TString::kIgnoreCase)) {
		//    Smoothing as in ROOT
		// Look to: http://root.cern.ch/root/html534/TH2.html#TH2:Smooth
		// Smooth bin contents of this 2-d histogram using kernel algorithms
		// similar to the ones used in the raster graphics community.
		// Bin contents in the active range are replaced by their smooth values.
		Int_t ntimes = 1; // Currently only ntimes=1 is supported
		sdu::CopyHistContent(hin,hout);
		hout->Smooth(ntimes, sopt.Data());
	} else if (sopt.Contains("median", TString::kIgnoreCase)) {
		hout = sdu::MedianFilter(hin, sopt, hout);
	} else if (sopt.Contains("mean", TString::kIgnoreCase)) {
		hout = sdu::MeanFilter(hin, sopt, hout);
	} else {
		sdu::CopyHistContent(hin,hout);
		cout << " No filter " << sopt << endl; // no filter
	}
	cout<<" SDUtils::Smooth : hin "<<hin->GetName()<<" sopt "<<sopt<<" hout "<<hout->GetName()<<endl;
	return hout;
}


void SDUtils::CopyHistContent(TH2* hin, TH2* hout)
{
    // hin and hout should have similar binning
	if(hin==0 || hout==0) return;

	hout->Reset();
	for(Int_t binx=1; binx<=hin->GetNbinsX(); ++binx){
		for(Int_t biny=1; biny<=hin->GetNbinsY(); ++biny){
			hout->SetBinContent(binx,biny, hin->GetBinContent(binx,biny));
		}
	}
}

TList* SDUtils::CalculateHistCharacteristics(TH2* h2, sdClusterChar &sdChar)
{
	// Dec 31,2012 - for simulation
	if(h2==0) return 0;
    // do we need number of entries - ?
	sdChar.maxamp = h2->GetMaximum();

	Int_t scaleProj = 1;
	TH1 *hamp = 0;
	TString opt("");
	TList* l = sdu::BookAndFill1DHistsFromClusterHist(h2, opt, scaleProj, hamp);
    sdChar.aveamp = hamp->GetMean();

    sdu::GetFWHM(sda::GetH1(l,0), sdChar.duration);

    sdu::GetFWHM(sda::GetH1(l,1), sdChar.size);

	return l;
}

void SDUtils::PrintCvsInfo()
{
	// print version control information : Feb 3,2013
	cout<<"\t cvs inf : $Id: SDUtils.cxx,v 1.25 2013/03/20 16:15:54 pavlinov Exp $ \n";
}

TString SDUtils::GetVersion()
{
	TString stmp;
	stmp.Form("V%i_%i.%i", VERSION_MAJOR , VERSION_MINOR , VERSION_DEBUG );
//	stmp.Format("V%i_%i.%i", 1 , 25 , 11 );
	return stmp;
}

//   S I M U L A T I O N
TGProgressBar* SDUtils::GetHProgressBar(Int_t hsize)
{
	TGHProgressBar *pb = new TGHProgressBar(gClient->GetRoot(), hsize);
	pb->SetBarColor(0x000030);
	pb->SetPosition(0.0);
	pb->ShowPosition(kTRUE, kFALSE, "%.0f%% events");
	pb->MapWindow();
	return pb;
}

void SDUtils::UpdateProgressBar(TGProgressBar *pb, Int_t nevCurrent, Int_t nev)
{
  if(pb==0 || nev <=0) return;

  Double_t per = 100.*nevCurrent/nev;
  pb->SetPosition(per);
  gSystem->ProcessEvents(); // Very important for updating
}

TList* SDUtils::BookHistsPars1VsPars2(const char* name, const char* opt, Int_t pri)
{
	// For simulation
   if(pri>0) cout<<"\t<I> SDUtils::BookHistsPars1VsPars2 is started : name "<<name<<" opt "<<opt<<endl;
   TH1::AddDirectory(kTRUE);
   Double_t scale = 10.;
   new TH1F("00_maxRelDiff","Relative difference of maximum values; in %; 100.(1.-max2/max1)", 100, 0.0, -1.0);
   new TH1F("01_meanRelDiff","Relative difference of mean values; in %; 100.(1.-mean2/mean1)", 100, 0.0, -1.0);
   new TH1F("02_FwhmXRelDiff","Relative difference of FwhmX values; in %; 100.(1.-FwhmX2/FwhmX1)", 100, 3.*scale, -3.*scale);
   new TH1F("03_FwhmYRelDiff","Relative difference of FwhmY values; in %; 100.(1.-FwhmY2/FwhmY1)", 100, 3.*scale, -3.*scale);

   new TH1F("04_NumPixsRelDiff","Relative difference of number of pixels; in %; 100.(1.-nPixs2/nPixs1)",100, 4*scale, -3.*scale);

   TList *l = sda::moveHistsToList(name);

	if(strlen(opt)) {
		sda::AddToNameAndTitleToList(l, opt, opt);
	}

   TH1::AddDirectory(kFALSE);

   if(pri>0) cout<<"\t<I> SDUtils::BookHistsPars1VsPars2 is ended \n";

   return l;
}

void SDUtils::FillHistsPars1VsPars2(TList* l, const sdClusterChar& par1, const sdClusterChar& par2)
{
	// For simulation
   if(l==0) return;

   sda::FillH1(l,0, sdu::RelError(par1.maxamp,par2.maxamp));
   sda::FillH1(l,1, sdu::RelError(par1.aveamp,par2.aveamp));
   sda::FillH1(l,2, sdu::RelError(par1.duration,par2.duration));
   sda::FillH1(l,3, sdu::RelError(par1.size,par2.size));
   if(par1.numpixels>0) sda::FillH1(l,4, sdu::RelError(Double_t(par1.numpixels),Double_t(par2.numpixels)));
}

SDClusterFinderStdOne* SDUtils::InitClusterFinder(parsClusterFinder *pcf, TH2* hin, Double_t sigF0Unit)
{
	if(pcf==0) return 0;

	SDClusterFinderStdOne *clf = dynamic_cast<SDClusterFinderStdOne *>(CreateClusterFinder(*pcf));
	clf->Init();
    clf->SetInputHist(hin);
    // Transition to F0 units
    clf->SetSeed(clf->GetSeed()*sigF0Unit);
    return clf;
}

//
//    Posix  thread
//

#ifndef WIN32
size_t SDUtils::SetPThreadStacksize(size_t newStackSize, Int_t pri)
{
	// Apr 18, 2013
	// newStackSize<=0 -> return current stack size
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	if (newStackSize > 0)
		pthread_attr_setstacksize(&attr, newStackSize);
	size_t stackSize = 0;
	pthread_attr_getstacksize(&attr, &stackSize);
	TThread::Printf("Stack size %li bytes ", stackSize);

	return stackSize;
}
#endif

//Int_t  SDUtils::InitThread(pthread_attr_t& attr, pthread_t &thread, Int_t detach)
//{
//  // #define PTHREAD_CREATE_JOINABLE       0
//  // #define PTHREAD_CREATE_DETACHED       1
//	Int_t returnVal=-1;
//    returnVal = pthread_attr_init(&attr);
//    assert(!returnVal);
//    returnVal = pthread_attr_setdetachstate(&attr, detach);
//    assert(!returnVal);
//    return returnVal;
//}
void  SDUtils::DrawHistInThreads(TH1* h, TCanvas* c)
{    // Its works
	if(h==0 || c==0) return;

	TString stmp;
	stmp.Form("th%s",h->GetIconName());
	pair<TH1*,TCanvas*>* phc = new pair<TH1*,TCanvas*>;
	phc->first  = h;
	phc->second = c;
	void* arg = (void*)(phc);
//	cout<<" arg "<<arg<<endl;
    TThread *th = new TThread(stmp.Data(), SDUtils::DrawHist, arg);
    th->Run();
    th->Join();
}

void* SDUtils::DrawHist(void* arg)
{
//	cout<<" arg "<<arg<<endl;
	pair<TH1*,TCanvas*>* phc = (pair<TH1*,TCanvas*>*)arg;
	TThread::Lock();
    TH1* h     = phc->first;
    TCanvas *c = phc->second;
    c->cd();
    h->Draw();
    c->Modified();
    c->Update();
    TThread::UnLock();
    return (void*)0;
}
//--- From sdmain.C
void SDUtils::LoadAliasLib() 
{
	const char* libs[]={"XMLParser","Gui"};
	int n = sizeof(libs)/sizeof(char*);
	for(int i=0; i<n; i++) gSystem->Load(Form("lib%s",libs[i]));
	gSystem->ListLibraries();
	printf("\n\t\t gSystem->ListLibraries()\n");
}
