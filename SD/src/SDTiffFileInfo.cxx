/*
 * SDTiffFileInfo.cpp
 *
 *  Created on: July 2, 2012
 * Copyright (C) 2012-2013, Alexei Pavlinov
 * All rights reserved.
 */

#include "SDTiffFileInfo.h"
#include "SDManager.h"
#include "SDPedestals.h"
#include "SDTransformation.h"
#include "SDConfigurationParser.h"
#include "SDConfigurationInfo.h"
#include "SDAliases.h"
#include "SDUtils.h"
#include "sdCommon.h"
#include "SDImagesPool.h"
#include "SDImageAnalysis.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TBrowser.h>
#include <TImage.h>
#include <TASImage.h>
#include <TH1.h>
#include <TH2.h>
//#include "asvisual.h"

#include <iostream>
#include <fstream>
#include <assert.h>

#include "tiffio.h"

using namespace std;

const char* SDTiffFileInfo::gkName="TifFileInfo";
const char* SDTiffFileInfo::gkNameHistInput = "Input hist";


TString nameImagesPool("ImagesPool ");

ClassImp(SDTiffFileInfo)

// The purpose of this class is reading of tiff image from file,
// converting tiff image to the two dimensional histogram.
// values of time and space scales wil be extract if available .
//

ostream& operator<<(ostream& os, const tifHeader& h)
{
  return os<<" tif.Header "<<" 0-1(hex) "<<hex<<h.byte0_1<<" 2-3(dec) "<<dec<<h.byte2_3<<
		  " 4-7(dec) "<<h.byte4_7;
}

ostream& operator<<(ostream& os, const tag866c& t)
{
    os<<" timeScale  "<<t.timeScale<< "(ms/line) ";
    os<<" spaceScale "<<t.spaceScale<<"(um/pixel)\n";
	return os;
}

#ifndef __ARGB__
void pix32::Print()
{
	cout<<" UInt_t  "<<hex<<ui32<<endl;
	cout<<" alpha "<<UInt_t(alpha)<<" charArr[0] "<<UInt_t(charArr[0]) <<endl;
	cout<<" red   "<<UInt_t(red)<<  " charArr[1] "<<UInt_t(charArr[1]) <<endl;
	cout<<" green "<<UInt_t(green)<<" charArr[2] "<<UInt_t(charArr[2]) <<endl;
	cout<<" blue  "<<UInt_t(blue)<< " charArr[3] "<<UInt_t(charArr[3]) <<endl;
}
#else
void pix32::Print()
{
	cout<<" UInt_t  "<<hex<<ui32<<endl;
	cout<<" blue  "<<UInt_t(blue)<< " charArr[0] "<<UInt_t(charArr[0]) <<endl;
	cout<<" green "<<UInt_t(green)<<" charArr[1] "<<UInt_t(charArr[1]) <<endl;
	cout<<" red   "<<UInt_t(red)<<  " charArr[2] "<<UInt_t(charArr[2]) <<endl;
	cout<<" alpha "<<UInt_t(alpha)<<" charArr[3] "<<UInt_t(charArr[3]) <<endl;
}
#endif

// ClassImp(imageStatus)
ostream& operator<<(ostream& os, const imageStatus& pr)
{
	return os<<pr.color<<" status "<<pr.status;
}


// -------------------------------------------------

SDTiffFileInfo::SDTiffFileInfo() : SDObjectSet(),
		    fTiffReader(), fImage(0), fTiff(0), fTransform(0), fSize(0),fTifHeader(),
		    fTifHeader2(), fTag866c(), fGoodImagesInput(),fImagesStatus()
{
	// For reading
}

SDTiffFileInfo::SDTiffFileInfo(const char* name, const char* tifName) :
	SDObjectSet(name, gSystem->ExpandPathName(tifName)),
			fTiffReader("ROOT Tiff Reader"), fImage(0), fTiff(0), fTransform(0),
			fSize(0),fTifHeader(), fTifHeader2(), fTag866c(), fGoodImagesInput(0),
			fImagesStatus()
{
	// tifName is absolute name of tiff file and keep in title
	if(strlen(name)==0) SetName(gkName);
	fTransform = new SDTransformation(1);
	Add(fTransform);
}

SDTiffFileInfo::~SDTiffFileInfo()
{
	if(fImage) delete fImage;
}

SDImagesPool* SDTiffFileInfo::GetImagesPool()
{
	return dynamic_cast<SDImagesPool *>(sda::FindObjectByPartName(fList, nameImagesPool.Data()));
}

TList* SDTiffFileInfo::GetListHistsInput(const char* color)
{
	return dynamic_cast<TList *>(sda::FindObjectByPartName(GetList(), color));
}

SDImagesPool * SDTiffFileInfo::CreateImagesPool(const char* opt)
{
    TString name = nameImagesPool;
    name += fImage->GetName();
    if(strlen(opt)) name += Form(" %s",opt);
	SDImagesPool *pool = new SDImagesPool(name.Data());
	return pool;
}

Bool_t   SDTiffFileInfo::IsThisLittleEndian()
{
	// In the �II� format, byte order is always from the least significant byte to the most
	// significant byte, for both 16-bit and 32-bit integers This is called little-endian byte
	// order.
	return (fTifHeader.byte0_1 == 0x4949);	// 0x4949 == "II"
}

TH2* SDTiffFileInfo::GetTifAs2DHist(const char* color)
{
	TList* l = 	dynamic_cast<TList *>(sda::FindObjectByPartName(GetList(), color));
	return sda::GetH2(l, 0);
}

Bool_t SDTiffFileInfo::DecodeTiffFile()
{
	// Look to:
	// http://partners.adobe.com/public/developer/en/tiff/TIFF6.pdf
	// for description of tiff format
	// ~/Documents/NIH_NIA_IRP/ImagePro contains 3 rtf file from Victor Maltcev
	fDebug = 0;
	fTifHeader.switchByte = kFALSE; // depend from system archirecture !
	const char* fname = GetTitle();
	ifstream fin(fname, ios::in | ios::binary | ios::ate);
    if(fin.is_open()) {
       fSize = (UInt_t) fin.tellg();
       byte*  vcht = new byte[fSize];
       UInt_t*  vint = (UInt_t*)(vcht);   // array of UInt_t  (4 byte)
       fin.seekg (0, ios::beg);
       fin.read((char*)&vcht[0], fSize);
       fin.close();
       vector<UChar_t> vch(vcht[0],vcht[fSize]);

       fTifHeader.byte0_1 = sdu::GetUShort(vcht,0);
       if(IsThisLittleEndian()) {
    	   if(fDebug>=1) {
       		   cout<<"\n\t\t <I> This is littile-endian byte order, fSwithcByte "<<fTifHeader.switchByte;
   		       cout<<" byte(0-1) "<<vcht[0]<<vcht[1]<<endl;
    	   }
        } else {
    	   // 8_22_c11.tiff
    	   fTifHeader.switchByte = kTRUE;
    	   if(fDebug>=1) {
    		   cout<<"\n\t\t <I> This is big-endian byte order, fSwithcByte "<<fTifHeader.switchByte;
    		   cout<<" byte(0-1) "<<vcht[0]<<vcht[1]<<endl;
    	   }
       }
       if(fDebug>1) cout<<"File: "<<fname<<" fSize "<<fSize<<" fTifHeader.switchByte  "<<fTifHeader.switchByte<<endl;
       fTifHeader.byte2_3 = sdu::GetUShort(vcht,2, fTifHeader.switchByte);
       fTifHeader.byte4_7 = sdu::GetUInt(vcht,4, fTifHeader.switchByte);
       if(fDebug>=1) {
    	   cout<<dec<<fTifHeader<<endl;
	       sdu::PrintBlock(vcht,0,8);
       }
	   if(fDebug>=1) cout<<" vint[1](dec) "<<dec<<vint[1]<<endl;

	   if(fTifHeader.byte2_3 != 42) {
		   if(fDebug>=1) cout<<"<E> This is not tif file ! memblock[1] != 42 : ";
		   if(fDebug>=1) cout<<" fTifHeader.byte2_3 "<<hex<<fTifHeader.byte2_3<<endl;
    	   return kFALSE;
       }
//	   fTifHeader.switchByte = kFALSE; // for testing
       fTifHeader.numberOfDirectoryEntries = sdu::GetUShort(vcht,fTifHeader.byte4_7, fTifHeader.switchByte);
       //
//       UShort_t specTag = 292;
//       UShort_t specTag = 0x866C; // $866C in Delphi notation
//       UShort_t specTag = 0x10F;  // $10F in Delphi notation
    	  //  unknown field with tag 292 (0x124) encountered
       if(fDebug>=1) cout << " fTifHeader.numberOfDirectoryEntries " << fTifHeader.numberOfDirectoryEntries
				<< endl;
		for (UInt_t i = 1; i <= fTifHeader.numberOfDirectoryEntries; ++i) {
			UInt_t DirEntryOffset = fTifHeader.byte4_7 + 2 + (i - 1) * 12;
			UShort_t tag1 = sdu::GetUShort(vcht, DirEntryOffset, fTifHeader.switchByte);
			UShort_t TestWord = sdu::GetUShort(vcht, DirEntryOffset + 2,
					fTifHeader.switchByte);
			UInt_t   Count1 = sdu::GetUInt(vcht,   DirEntryOffset + 4, fTifHeader.switchByte);
			UInt_t   value  = sdu::GetUInt(vcht,   DirEntryOffset + 8, fTifHeader.switchByte);
			UShort_t svalue = sdu::GetUShort(vcht, DirEntryOffset + 8, fTifHeader.switchByte);
			UInt_t offset1 = value; // For tag 0x866C
			if(fDebug>=1) cout << " i " << dec << i << " DirEntryOffset " << DirEntryOffset;
			if(fDebug>=1) cout << " tag1(hex) " << hex << tag1 << " TestWord " << dec
					<< TestWord;
			if(fDebug>=1) cout << " Count1 " << Count1;
			if(fDebug>=1) cout << " value " << dec << value << "(dec) : svalue "<<svalue;
			if (tag1 == 0x100) {
				fTifHeader.imageWidth = svalue;
				if(fDebug>=1) cout << " <- image width";
			} else if (tag1 == 0x101) {
				fTifHeader.imageLength = svalue;
				if(fDebug>=1) cout << " <- image length";
			} else if (tag1 == 0x115) {   // Mar 3,2013
				fTifHeader.samplesPerPixel = svalue; // TIFFTAG_SAMPLESPERPIXEL
				if(fDebug>=1) cout << " <- sample per pixel";
			} else if (tag1 == 0x866c) {
				//	  Single1:=1000*ReadSingle(offset1+$24);
				fTag866c.timeScale = 1000 * sdu::GetFloat(vcht, offset1 + 0x24,
						fTifHeader.switchByte); // ms
				fTag866c.spaceScale = sdu::GetFloat(vcht, offset1 + 0x20,
						fTifHeader.switchByte); // um
				if(fDebug>=1) {
					cout << endl << fTag866c;
				    sdu::PrintBlock(vcht, offset1 + 0x120, 16);
				}
				// Put scale values to configuration
				parsClusterFinder* cfpars = sdu::GetParsClusterFinder(this);
				if (cfpars) {
					cfpars->timescale = fTag866c.timeScale;
					cfpars->spacescale = fTag866c.spaceScale;
				}
			}
			if(fDebug>=1) cout<< endl;
		}
    }

    return ReadTiffFile();
}

Bool_t SDTiffFileInfo::ReadTiffFile()
{
    CheckTags(); // Problem with big-endian
    const char* fname = GetTitle();

	Int_t readDirectly = 0;
    TString system(gSystem->GetName());
    if(system.Contains("Win",TString::kIgnoreCase)) readDirectly = 1; // May 22,2012

	UInt_t* argbArr = 0;
	if(readDirectly==0) {
	  if(fDebug>=1) cout<<" SDTiffFileInfo::ReadTiffFile "<<fname<<endl;
	  fImage = TImage::Open(fname, TImage::kTiff);
	  argbArr = fImage->GetArgbArray(); // pointer to internal array[width x height] of double values
	  if(fDebug>=1) sdu::PrintPallete(&fImage->GetPalette()); // For testing
	}

	if(argbArr==0 ) {
		ReadTiffFileDirectly(fname); // My TIFF decoder
		if(fImage) argbArr = fImage->GetArgbArray();
		assert(argbArr);
	}
    return kTRUE;
}

Bool_t SDTiffFileInfo::Fill2DHists()
{
	// 2013: Mar 30 - seperate method
	// Created the SDImageAnalysis objects for valid image(s) too.
	UInt_t* argbArr = 0;
	if(fImage) argbArr = fImage->GetArgbArray();

	if(argbArr) {
        SDManager *man = GetSDManager();
		parsInputOutput parsIn =
				man->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsInput();
		parsFillHists pfh =
				man->GetConfigurationParser()->GetLastConfigurationInfo()->GetParsFillHists();

		TList *l = 0;
		sdCommon* sdc = sdCommon::GetSdCommon();
		SDImagesPool *pool = CreateImagesPool("");
		Add(pool);
		for (UInt_t image = 0; image < fTifHeader.samplesPerPixel; ++image) {
			imageStatus imgStat;
			if(fTifHeader.samplesPerPixel>1) imgStat.color = sdc->tiffRgb[image].c_str();
			else                   imgStat.color = "Grey";
			l = BookHistsIn(gkNameHistInput, imgStat.color.Data());
			GetList()->Add(l);

			FillHistsInTwo(fImage, l, image);

			TH2 *hin = GetTifAs2DHist(imgStat.color);
			TList *lproj = sdu::BookAndFill1DHistsFromClusterHist(hin, hin->GetName(), 1);
			lproj->SetName(Form("%s Proj", gkNameHistInput));
			l->Add(lproj);

			CheckRealityOfPicture(lproj, imgStat);
			fImagesStatus.push_back(imgStat);
			if(imgStat.status == kAvailable){
				SDImageAnalysis *img = new SDImageAnalysis(imgStat);
				pool->Add(img);
				SDPedestals *ped = new SDPedestals(Form("%s %s", sd::GetSdCommon()->leadingPedName.Data(), imgStat.color.Data()));
				img->Add(ped);
			}
		} // cycle on pictures
	    PrintPictsStatus(fImagesStatus);
		return kTRUE;
	}
	else       return kFALSE;
}

Bool_t SDTiffFileInfo::ReadTiffFileDirectly(const char* file)
{
// #define TIFF_HIST_CONTROL
	// 2013 : Feb 3;
	// http://www.libtiff.org/libtiff.html
	// Mar 8; check on big-endian
	fTiff = TIFFOpen(file, "r");

	if (fTiff) {
		ReadTagsByLibTiff(fTiff);

		char slash = '/'; //sdCommon::GetSdCommon()->fSlash;
		TString snam(file);
		uint32 ind = snam.Last(slash), len = snam.Length() - ind + 1;
		snam = snam(ind+1,len).Data();
		TASImage *img = new TASImage(fTifHeader.imageWidth, fTifHeader.imageLength);
		img->SetName(snam.Data());

		uint32 npixels = fTifHeader.imageWidth*fTifHeader.imageLength;
		Double_t* imgArr = new Double_t[npixels];

		fTifHeader.scanLine = TIFFScanlineSize(fTiff);
		UInt_t elementSize =  fTifHeader.samplesPerPixel==1?1:4; // one or charArr
		unsigned char* buf = new unsigned char[fTifHeader.imageWidth*elementSize];
		TH1::AddDirectory(kFALSE);
#ifdef TIFF_HIST_CONTROL
//		TH2* ht = new TH2F("htest", "test of tif decoding ", fTifHeader.imageWidth, -0.5,
//				fTifHeader.imageWidth - 0.5, fTifHeader.imageLength, -0.5, fTifHeader.imageLength - 0.5);
		TH2* ht = new TH2F("htest", "test of tif decoding ", 256, -0.5,
				256. - 0.5, 3, -0.5, 2.5);
//		ht->SetOption("colz");
		GetList()->Add(ht);
#endif
		for (uint32 row = 0; row < fTifHeader.imageLength; ++row) {
			int n = TIFFReadScanline(fTiff, buf, row, 0);
			uint32 rowNew = fTifHeader.imageLength - 1 - row;
//			uint32 rowNew = row;
			if (n == -1) {
				printf("Error");
				return 0;
			}
			// cout<<" row "<<row<<endl;
			for (uint32 col = 0; col < fTifHeader.imageWidth; ++col) {
				// printf("%4i buf %4i \n", col, buf[col]);
				uint32 indScanLine = col*fTifHeader.samplesPerPixel;  // first bit on sample
				uint32 ind  = fTifHeader.imageWidth * rowNew + col;
				imgArr[ind] = Double_t(buf[indScanLine]); // !!
				if(fTifHeader.maxSampleValue < UInt_t(buf[col])) fTifHeader.maxSampleValue=Double_t(buf[col]);
#ifdef TIFF_HIST_CONTROL
//				float x = float(col);
//				float y = float(rowNew);
//				ht->Fill(x, y, imgArr[ind]);
				for(UInt_t i=0; i<fTifHeader.samplesPerPixel; ++i) {
					//cout<<Int_t(buf[indScanLine+i])<<" ";
					ht->Fill(Double_t(buf[indScanLine+i]), Double_t(i));
				}
				//cout<<endl;
#endif
			}
		}
#ifdef TIFF_HIST_CONTROL
		if(fTifHeader.maxSampleValue>0) ht->Scale(1./Double_t(fTifHeader.maxSampleValue));
#endif
//		_TIFFfree(buf);
		TIFFClose(fTiff);

        Double_t dmax = Double_t(fTifHeader.maxSampleValue);
		for(uint32 ind=0; ind<npixels; ++ind) {
			imgArr[ind] /= dmax;
		}
		Int_t pri = 0;
		TImagePalette *pallete = sdu::GetGrayPallete(256, 8, pri);
		img->SetImage(imgArr, fTifHeader.imageWidth, fTifHeader.imageLength, pallete);
		fImage = img;
		fTiffReader = "PAI Tiff Reader";
		return true;
	}
	return false;
}

void   SDTiffFileInfo::ReadTagsByLibTiff(tiff *tiffInput)
{
	// Mar 9; separate method
	if(tiffInput==0) {
        cerr<<"<W> SDTiffFileInfo::ReadTagsByLibTiff : tiffInput " << tiffInput << endl;
		return;
	}

	TIFFGetField(tiffInput, TIFFTAG_IMAGEWIDTH, &fTifHeader2.imageWidth);
	TIFFGetField(tiffInput, TIFFTAG_IMAGELENGTH, &fTifHeader2.imageLength);

	// TIFFTAG_SAMPLESPERPIXEL - the number of components per pixel.
	// SamplesPerPixel is usually 1 for bilevel, grayscale, and palette-color images.
	// SamplesPerPixel is usually 3 for RGB images. If this value is higher,
	// ExtraSamples should give an indication of the meaning of the additional channels.
    TIFFGetField(tiffInput, TIFFTAG_SAMPLESPERPIXEL, &fTifHeader2.samplesPerPixel);


	// TIFFTAG_BITSPERSAMPLE - number of bits per component.
	TIFFGetField(tiffInput, TIFFTAG_BITSPERSAMPLE, &fTifHeader2.bitsPerSample);

	// TIFFTAG_SAMPLEFORMAT - Specifies how to interpret each data sample in a pixel.
	// LibTiff does not support different BitsPerSample values for different components.
	// 1 = unsigned integer data
	// 2 = two's complement signed integer data
	// 3 = IEEE floating point data
	// 4 = undefined data format
	TIFFGetField(tiffInput, TIFFTAG_SAMPLEFORMAT,   &fTifHeader2.fmt);

	TIFFGetField(tiffInput, TIFFTAG_MAXSAMPLEVALUE, &fTifHeader2.maxSampleValue);
}

TList* SDTiffFileInfo::BookHistsIn(const char* name, const char* titRgb) {
    // List 1 contains initial tif image  as histogram.
	//
	// Time is going from left to right:   1 pixel = 2.09 (ms/line),
	// Y scale is space (along scan line): 1 pixel = 0.05 (um/pixel)
    //    Image index vs hists
	// binx = col + 1; biny = row + 1;
	//
	cout<<"<I> SDManagerst::BookHistsIn is started : "<<name;
	TH1::AddDirectory(1);

	Int_t nx=2048, ny=512;
	double xmi=-0.5, xma = Double_t(nx) + xmi, ymi=-0.5, yma = Double_t(ny) + ymi;
	TString snam("00_img"), stit;
	if(strlen(titRgb)) {
		cout<<" titRgb "<<titRgb;
		snam += titRgb[0]; // First char from string
		stit = Form("Image from %s color",titRgb);
	} else {
		stit = "Grayscale image ";
	}
	TString stitAdd = Form("; %s %s                 time bin; space bin", fImage->GetName(), titRgb);
	stit += stitAdd;
	TH2F* h2 = new TH2F(snam.Data(), stit.Data(), nx,xmi,xma, ny,ymi,yma);
	h2->SetOption("colz");

	snam = name;
	snam += " ";
	snam += titRgb;
	TList *l = sda::moveHistsToList(snam);
	TH1::AddDirectory(0);

	printf("   ... done \n");

	return l;
}

void SDTiffFileInfo::FillHistsInOne(TImage *img, TList *l, Int_t pict)
{
	// Fill hists: 2048x512
	// 2013: Mar 1; Obsolete - not used now

    cout<<"<I> SDTiffFileInfo::FillHistsInOne is started : img "<<img<<endl;
	if(fTifHeader.bitsPerSample!=1) {
		cerr<<" fTifHeader.bitsPerSample = "<<fTifHeader.bitsPerSample<<" should equal 1 \n";
		return;
	}
	int w = img->GetWidth(), h = img->GetHeight();
	TArrayD* imgArr = img->GetArray(0, 0, 0);
	for (int row = 0; row < h; ++row) {
		Double_t y = Double_t(row);
		for (int col = 0; col < w; ++col) {
			Double_t x = Double_t(col);
			int ind = w * row + col;
			Double_t amp = (*imgArr)[ind];
			sda::FillH2(l, 0, x, y, amp); // Main hist
		} // col
	} // row
	delete imgArr;
}

void SDTiffFileInfo::FillHistsInTwo(TImage *img, TList *l, Int_t pict)
{
	// Jul 13,2012;
	// Fill hists from raw image 1024x1024
	// TIFFTAG_BITSPERSAMPLE = 1 or 3; // 2?

	int w = img->GetWidth(), h = img->GetHeight();
    cout<<"<I> SDTiffFileInfo::FillHistsInTwo is started : img "<<img<<
    		" w "<<w<<" h "<<h<<endl;
	pix32 pixel;
	UInt_t* argbArr = 0;
	TArrayD* imgArr = 0;
	if(fTifHeader.samplesPerPixel==1) {
	    imgArr = img->GetArray(0, 0, 0);
	} else {
		argbArr = img->GetArgbArray();
	/*	 UInt_t  ff1f1e00
		 blue   0 charArr[0]  0
		 green 1e charArr[1] 1e
		 red   1f charArr[2] 1f
		 alpha ff charArr[3] ff */

	}
	Int_t colnew=-1, rownew=-1;
	Double_t a = 0.0;
	for (int row = 0; row < h; ++row) {
		for (int col = 0; col < w; ++col) {
			UInt_t ind = w * row + col;
			if(fTifHeader.samplesPerPixel==1) {
			    a = Double_t((*imgArr)[ind]);
			} else {
				// UInt_t pictn = 2 - pict;    // March 20,2012
				pixel.ui32 = argbArr[ind];
				a = Double_t(pixel.charArr[pict]);
				// if(ind<3) pixel.Print();
			}

		//	GetNewColRowIfLeftPartIsFirst(col,row,colnew,rownew);
			if (w == 2048 && h == 512) {
				colnew = col; rownew = row;
			} else {
			    //GetNewColRowIfRightPartIsFirst(col,row,colnew,rownew);
			    fTransform->Transform(col,row,colnew,rownew);
			}

			Double_t x = Double_t(colnew);
			Double_t y = Double_t(rownew);
			sda::FillH2(l, 0, x, y, a); // Main hist
		} // col
	} // row
	if(imgArr) delete imgArr;
}

void SDTiffFileInfo::GetNewColRowIfLeftPartIsFirst(Int_t col,Int_t row, Int_t &colnew, Int_t &rownew)
{
	if(col<512) {
		colnew = row;
		rownew = 511 - col; // change direction
	} else {
		colnew = 1024 + row;
		rownew = 1023 - col; // change direction
	}
}

void SDTiffFileInfo::GetNewColRowIfRightPartIsFirst(Int_t col,Int_t row, Int_t &colnew, Int_t &rownew)
{
	//
	// Jul 14,2012 - this is our case
	//
	if(col<512) { // left part - second part of image
		colnew = 1024 + row;
		rownew = 511  - col;
	} else { // Right part - first part of image
		colnew = row;
		rownew = 1023 - col;
	}
}

void SDTiffFileInfo::CheckRealityOfPicture(TList *lproj, imageStatus &pictRec)
{
    //  Mar 6: picture could have not useful information.
    //  When fTifHeader.samplesPerPixel=3 we could have up to three pictures,
	//  But several(all) color bytes could be empty.
	//  In this case amplitude distribution is flat.
	TH1* hamp = sda::GetH1(lproj, 2);
	if(hamp==0 ) return;
	if(hamp->GetEntries()>1000000 && hamp->GetRMS()>1.e-5) pictRec.status = kAvailable;
	return;
}

void SDTiffFileInfo::PrintPictsStatus(imgsStatusVector &pv)
{
	cout<<" Pictures status : #picts "<<pv.size()<<endl;
	for(UInt_t i=0; i<pv.size(); ++i) cout<<pv[i]<<endl;
	return;
}

void SDTiffFileInfo::CheckTags()
{
	// 2013: for big-endian case
	// Mar 8; Mar 27;
    UInt_t arrSize = fTifHeader.imageWidth * fTifHeader.imageLength;
	if(arrSize != 1024*1024) {
		PrintTags("Before Checking");
		fTifHeader.imageWidth  = 2048;
		fTifHeader.imageLength = 512;
		fTifHeader.samplesPerPixel = 1; // ??
		PrintTags("After  Checking");
	}
}

void SDTiffFileInfo::PrintTags(const char* tit)
{
	// 2013: Mar 4;
	if(strlen(tit)) cout <<"      "<<tit<<endl;
	cout<<"\n\t\t Tiff file  "<<GetTitle()<<" size "<<fSize<<"bytes"<<endl;
	cout<<fTifHeader<<endl;
	cout<<" fTifHeader.numberOfDirectoryEntries "<<fTifHeader.numberOfDirectoryEntries<<endl;
	cout<<" fTifHeader.imageWidth               "<<fTifHeader.imageWidth<<endl;
	cout<<" fTifHeader.imageLength              "<<fTifHeader.imageLength<<endl;
	cout<<" fTifHeader.samplesPerPixel          "<<fTifHeader.samplesPerPixel<<endl;
	cout<<" fTifHeader.bitsPerSample            "<<fTifHeader.bitsPerSample<<endl;
	cout<<" fTifHeader.scanLine                 "<<fTifHeader.scanLine<<endl;
	cout<<" fTifHeader.fmt                      "<<fTifHeader.fmt<<endl;
	cout<<" fTifHeader.maxSampleValue           "<<fTifHeader.maxSampleValue<<endl;
	cout<<fTag866c<<endl;
}

void SDTiffFileInfo::DrawProjsNofilterVsFilter()
{
	parsPedestals* ped = sdu::GetParsPedestals(this);
	Int_t nx=2, ny=2, col1=1, col2=2;
	const char *tit1="no filter";
	TString gtit(" No filter vs ");
	gtit += ped->smooth;

	sda::DrawTwoListsOfHists(GetListHists(1), GetListHists(2), nx,ny, tit1,ped->smooth.Data(),
			col1,col2, gtit.Data());
}

void SDTiffFileInfo::TestUnionpix32()
{
	// Mar 3,2013
	pix32 p;
	p.red   = 'r';
	p.blue  = 'b';
	p.green = 'g';
	p.alpha = 0;
	p.Print();
}

void SDTiffFileInfo::TestRaster(UInt_t n, UInt_t start)
{
    TString tnam("$HOME/wrk/tiff/2013/FEB/3_ca50.tif");
    gSystem->ExpandPathName(tnam);
	TIFF* tif = TIFFOpen(tnam, "r");
	pix32 p;
	if (tif) {
		uint32 w, h;
		size_t npixels;
		uint32* raster;

		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		npixels = w * h;
		raster = (uint32*) _TIFFmalloc(npixels * sizeof(uint32));
		if (raster != NULL) {
			if (TIFFReadRGBAImage(tif, w, h, raster, 0)) {
				for(UInt_t ind=start; ind<start+n; ++ind) {
					p.ui32 = raster[ind];
					p.Print();
					cout<<endl;
				}
			}
			_TIFFfree(raster);
		}
		TIFFClose(tif);
	}
	printf(" raster = (uint32*) _TIFFmalloc(npixels * sizeof(uint32)) - reading\n");
	printf(" pix32 p = raster[ind] \n");
}

void SDTiffFileInfo::Browse(TBrowser* b)
{
//	if(fTiff) b->Add(fTiff, "tiff");
	if(fImage) b->Add(fImage);
	TObjectSet::Browse(b);
}
