/*
 * SDClusterFinder2DTwo.cpp
 *
 *  Created on: July 26, 2012
 *      Author: pavlinov
 */

#include "SDClusterFinder2DTwo.h"

ClassImp(SDClusterFinder2DTwo)

SDClusterFinder2DTwo::SDClusterFinder2DTwo() {
	// for reading only
}

SDClusterFinder2DTwo::SDClusterFinder2DTwo(const char* name) : SDClusterFinder2DOne(name) {
	// TODO Auto-generated constructor stub

}

SDClusterFinder2DTwo::~SDClusterFinder2DTwo() {
	// TODO Auto-generated destructor stub
}

Bool_t SDClusterFinder2DTwo::ArePixelsNeighbours(const pixel & p1, const pixel & p2)
{
	// Only one difference with STClusterFineder2dOne -
	// Neighbors should have common side vs common vertex
	// cout<<" SDClusterFinder2DTwo::ArePixelsNeighbours : p1 "<<p1<<endl;
	Short_t dif = p1.row-p2.row;
	if(p1.col==p2.col && TMath::Abs(dif)==1) return kTRUE;
	dif = p1.col-p2.col;
	if(p1.row==p2.row && TMath::Abs(dif)==1) return kTRUE;
	return kFALSE;
}


