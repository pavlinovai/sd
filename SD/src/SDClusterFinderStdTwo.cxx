/*
 * SDClusterFinderStdTwo.cpp
 *
 *  Created on: May 20, 2012
 *      Author: pavlinov
 */

#include "SDClusterFinderStdTwo.h"

ClassImp(SDClusterFinderStdTwo)

SDClusterFinderStdTwo::SDClusterFinderStdTwo() {
	// TODO Auto-generated constructor stub

}

SDClusterFinderStdTwo::SDClusterFinderStdTwo(const char* name) : SDClusterFinderStdOne(name)
{ }

SDClusterFinderStdTwo::~SDClusterFinderStdTwo() {
	// TODO Auto-generated destructor stub
}

Bool_t SDClusterFinderStdTwo::ArePixelsNeighbours(const pixel &p1, const pixel &p2)
{
	// Only one difference with STClusterFinederStdOne -
	// Neighbors should have common side vs common vertex
	// cout<<" SDClusterFinderStdTwo::ArePixelsNeighbours : p1 "<<p1<<endl;
	Int_t dif = p1.row-p2.row;
	if(p1.col==p2.col && TMath::Abs(dif)==1) return kTRUE;
	dif = p1.col-p2.col;
	if(p1.row==p2.row && TMath::Abs(dif)==1) return kTRUE;
	return kFALSE;
}
