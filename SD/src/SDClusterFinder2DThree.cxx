/*
 * SDClusterFinder2DThree.cpp
 *
 *  Created on: Jul 26, 2012
 *      Author: pavlinov
 */

#include "SDClusterFinder2DThree.h"
#include "SDManager.h"
#include "SDPedestals.h"
#include "SDClusterInfo.h"
#include "SDCluster.h"
#include "SDAliases.h"

#include <TStyle.h>
#include <TMath.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TClass.h>

#include "iostream"
using namespace std;

ClassImp(SDClusterFinder2DThree)

SDClusterFinder2DThree::SDClusterFinder2DThree() {
	// for reading only
}

SDClusterFinder2DThree::SDClusterFinder2DThree(const char* name) : SDClusterFinder2DOne(name)
{
   printf("<I> %s(%s) is created \n", this->Class()->GetName(), name);
}

SDClusterFinder2DThree::~SDClusterFinder2DThree() {
	// TODO Auto-generated destructor stub
}

Bool_t SDClusterFinder2DThree::ArePixelsNeighbours(const pixel & p1, const pixel & p2)
{
	// May 25,2013 - pixel from cluster (p2) should have at least 3 neighbors
	cout<<" p1 "<<p1<<endl;
    cout<<" p2 "<<p2<<" * from cluster "<<endl;
#ifdef NEIGHBORS
	static Short_t minNeighbors = 8;
	if(p2.neighbors < minNeighbors) return kFALSE;
#endif
	// Neighbors should have common side vs common vertex
	Short_t dif = p1.row-p2.row;
	if(p1.col==p2.col && TMath::Abs(dif)==1) return kTRUE;
	dif = p1.col-p2.col;
	if(p1.row==p2.row && TMath::Abs(dif)==1) return kTRUE;

	return kFALSE;
}

void* SDClusterFinder2DThree::FindClustersStatic(void* arg) // static
{  // Jun 2 => copy of SDClusterFinder2DOne::FindClustersStatic(void* arg)
	SDClusterFinder2DOne *clf = (SDClusterFinder2DOne*)arg;

	TThread::Printf(" SDClusterFinder2DOne::FindClustersStatic : name %s", clf->GetName());
	Int_t deb = clf->GetDebug();
	TH2* hin       = sda::GetH2(clf->GetMainHists(),0);
    TH2* h2Remnant = sda::GetH2(clf->GetMainHists(),1);
	if(hin==0 || h2Remnant==0) {
        cout<<" hin or h2Remnant is zero! \n";
		return 0;
	}
	TCanvas*  c = 0;
	if(clf->GetOnlineDrawing()) c = clf->DrawInputHistInCanvas(hin, clf->GetCanvas());

	TDataSet* clusts = clf->GetClusters();
	clusts->Delete();

	Int_t nused=0, binx=0, biny=0, binz=0;
	// Int_t resetPixel = 1;
    Int_t key2Dtwo = 0;
	pixel pixel;
    UShort_t binxmin,binxmax, binymin,binymax;
	if(deb>=1) {
		cout<<"\n\t SDClusterFinder::FindClusters2DOne()  : "<<hin->GetEntries()<<"(h)"<<endl;
		cout<<"name "<<hin->GetName()<<"\n tit "<<hin->GetTitle()<<" seed "<<clf->GetSeed()<<endl;
		cout<<" Call from class "<<clf->ClassName()<<endl;
	}

    UShort_t hbinxmin = 1, hbinymin = 1;
	Double_t maxIn = hin->GetMaximum();
    if(hin->GetXaxis()->GetFirst()>0) hbinxmin = hin->GetXaxis()->GetFirst();
    if(hin->GetYaxis()->GetFirst()>0) hbinymin = hin->GetYaxis()->GetFirst();
    // shift between (col, binx) and (row, biny) -> Apr 20,2013
    UInt_t colShift = hbinxmin - static_cast<UShort_t>(hin->GetXaxis()->GetBinCenter(hbinxmin));
    UInt_t rowShift = hbinymin - static_cast<UShort_t>(hin->GetYaxis()->GetBinCenter(hbinymin));

    UShort_t hbinxmax = hin->GetNbinsX(), hbinymax = hin->GetNbinsY();
    if(hin->GetXaxis()->GetLast()>0) hbinxmax = hin->GetXaxis()->GetLast();
    if(hin->GetYaxis()->GetLast()>0) hbinymax = hin->GetYaxis()->GetLast();
	if (deb >= 1) {
		TThread::Printf("<I> x range %i->%i  : y range %i -> %i : colShift %i ", hbinxmin,
				hbinxmax, hbinymin, hbinymax, colShift);
		TThread::Printf(" First maximum value %6.4f seed %6.4f : rowShift %i ",
				h2Remnant->GetMaximum(), clf->GetSeed(), rowShift);
	}

    parsClusterFinder pars = clf->GetPars();
    Int_t keyNewClusterMax = 2; // Nov 13,2012
    // Infinite cycle on clusters
    while(1)
	{
       NEWCLUSTER:
        Int_t keyNewCluster = key2Dtwo = 0;

		pixel.amp = static_cast<Float_t>(h2Remnant->GetMaximum());
	    if(pixel.amp < pars.seed) break; // end of cluster finder

#ifdef NEIGHBORS
	    pixel.neighbors = 0;
#endif
	    // Return location of bin with maximum value in the range
	    h2Remnant->GetMaximumBin(binx,biny,binz);
	    SDUtils::FillPixel(hin, binx,biny,pixel, 0);
		h2Remnant->SetBinContent(binx, biny, 0.0); // Reset bin
	    ++nused;

	    SDClusterInfo *clstInfo = new SDClusterInfo(clusts->GetListSize());
	    SDClusterBase *clst = clstInfo->GetCluster();
	    clst->AddPixel(pixel);
 	    if(deb >= 4) {cout<<"** "<<clst->GetClusterSize()<<" "; cout<<pixel;}

	    UPDATECLUSTER:
	    // Cycle on neighbors only : Apr 20,2013
	    binxmin = hin->GetXaxis()->FindFixBin(clstInfo->GetClstCharFromF0().colmin) - 1;
	    binxmax = hin->GetXaxis()->FindFixBin(clstInfo->GetClstCharFromF0().colmax) + 1;
//	    binxmin = sdu::GetBinFromColRow(clstInfo->GetClstCharFromF0().colmin,colShift) - 1;
//	    binxmax = sdu::GetBinFromColRow(clstInfo->GetClstCharFromF0().colmax,colShift) + 1;
	    binxmin = binxmin>hbinxmin?binxmin:hbinxmin; // Check boundaries
	    binxmax = binxmax<hbinxmax?binxmax:hbinxmax;

	    binymin = hin->GetYaxis()->FindFixBin(clstInfo->GetClstCharFromF0().rowmin) - 1;
	    binymax = hin->GetYaxis()->FindFixBin(clstInfo->GetClstCharFromF0().rowmax) + 1;
	    binymax = binymax<hbinymax?binymax:hbinymax; // Check boundaries
	    binymin = binymin>hbinymin?binymin:hbinymin;

	    Int_t nadded  = 0; // number of added around box
        // July 26,2012
	    CYCLES:
	    for (UShort_t binx = binxmin; binx <= binxmax; ++binx) {
			UInt_t step = 1; // for left(right) column
			if (binx > binxmin && binx < binxmax) step = binymax - binymin;
			if (key2Dtwo) step = 1; // Force searching inside all rectangular
			for (UShort_t biny = binymin; biny <= binymax; biny += step) {
				pixel.amp = static_cast<Float_t>(h2Remnant->GetBinContent(binx,biny));
				if (pixel.amp > 0.0001) { // skip empty bin
#ifdef NEIGHBORS
				    pixel.neighbors = 0;
#endif
					SDUtils::FillPixel(hin, binx, biny, pixel, 0);
					if (clf->IsPixelNeighbourToCluster(pixel, clst)) { // cycle on cluster pixels is here
						h2Remnant->SetBinContent(binx, biny, 0.0);
						clst->AddPixel(pixel);
						++nadded;
						if (deb >= 4) cout << clst->GetClusterSize() << " "<< pixel;
						goto CYCLES;
					}
				}
			}
		}
	    if(nadded==0 && keyNewCluster>=keyNewClusterMax) {
		      if(deb>=2) {
		    	  TThread::Printf(" %i cluster : %i remains ",clusts->GetListSize(), (int)(hin->GetEntries() - nused));
		    	  TThread::Printf("%i is used", nused);
		    	  TThread::Printf(" : clst size %i\n",clst->GetClusterSize());
		      }
		      if(clf->IsThisClusterReal(*clstInfo, &pars)) { // Accept cluster
			    clusts->Add(clstInfo);
			    Int_t ic = static_cast<Int_t>(clusts->GetListSize());
				if(clf->GetOnlineDrawing()) clf->DrawClusterBox(c, clstInfo, ic);
		        if(clf->GetOnlineDrawing() && c) c->Update();
		      } else {                                   // Discard cluster
		    	  delete clstInfo;
		      }
	 	      goto NEWCLUSTER;
	    } else {
		  if(nadded>0) {
			  nused += nadded;
		  } else {
			 key2Dtwo = 1;
		    ++keyNewCluster;
		  }
	      goto UPDATECLUSTER;
	    }
	} // End of pixel cycle

	Int_t clstsReal = clusts->GetListSize();

	if(clf->GetOnlineDrawing()) {
	   h2Remnant->SetMaximum(maxIn); // For drawing
	   Int_t mode=1; // Int_t pri = mode / 10;
	   gStyle->SetOptDate();
	   clf->SortAndRenameClusters(mode);
	   clf->DrawInputHistInCanvasWithClusters(1);
	   clf->DrawFinalMessage(c, clstsReal, clusts->GetListSize());
	}

	return (void*)clstsReal;
}


