/*
 * SdF0Base.cxx
 *
 *  Created on: Jan 4, 2013
 *      Author: Alexei Pavlinov
 */
#include <SDF0Base.h>

ClassImp(SDF0Base)

SDF0Base::SDF0Base() : SDObjectSet("SDF0Base") {;}

SDF0Base::SDF0Base(const Char_t* name ) : SDObjectSet(name) {;}
