#include "SDAliases.h"

#include <math.h>
// see $STAR/StRoot/St_base/Stiostream.h in dev - 10-nov-03
#include "Riostream.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <cassert>
// 
#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TKey.h>
#include <TFile.h>
#include <TList.h>
#include <TNtuple.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>
#include <THnSparse.h>
#include <TF1.h>
#include <TF2.h>
#include <TFormula.h>
#include <TCanvas.h>
#include <TPaveText.h>
#include <TMinuit.h>
#include <TPad.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TArrayD.h>
#include <TControlBar.h>
#include <TDatabasePDG.h>
#include <TParticlePDG.h>
#include <TRegexp.h>
#include <TObjString.h>
#include <TMap.h>
#include <TDirectory.h>
#include <TGFileDialog.h>

ClassImp(SDAliases)

extern "C" void sgpdge_(Int_t &pdg, Int_t &gid); // 27-mar-2003
extern "C" void dedx_(double &P,int &IP,double &Z,double &A,double &RO, double &DE); // Jul 27,2006

using namespace std; // Oct 21, 2008

SDAliases::SDAliases(const Char_t *name,const Char_t *title) : TNamed(name,title)
{
}

void SDAliases::stat(const int istat)
{
  //  gStyle->SetOptStat(TMath::Abs(istat));
  if(istat<0) printf(" Opt. Stat %i (gStyle->SetOptStat(istat)) \n", 
  gStyle->GetOptStat());
}

void SDAliases::GetEnvironment(const Char_t* envName, TString &sEnvValue, int pri)
{  // May 4,2013
   TString stmp = gSystem->Getenv(envName);
   if(stmp.Length()) sEnvValue = gSystem->ExpandPathName(stmp.Data());
   if(pri) cout<<"\t"<<envName<<" -> "<<sEnvValue<<endl; 
}

TList* SDAliases::FindList(TList *l, const char* name)
{
	if(l==0) return 0;
	else     return dynamic_cast<TList *>(l->FindObject(name));
}

TList* SDAliases::GetList(TList *l, UInt_t ind)
{
	if(l==0) return 0;
	else     return dynamic_cast<TList *>(l->At(ind));
}

TH1* SDAliases::GetH1(TList *l, UInt_t ind)
{
	if(l==0) return 0;
	else     return dynamic_cast<TH1 *>(l->At(ind));
}

TH2* SDAliases::GetH2(TList *l, UInt_t ind)
{
	if(l==0) return 0;
	else     return dynamic_cast<TH2 *>(l->At(ind));
}

TF1* SDAliases::GetF1(TList *l, UInt_t ind)
{
	if(l==0) return 0;
	else     return dynamic_cast<TF1 *>(l->At(ind));
}

TF2* SDAliases::GetF2(TList *l, UInt_t ind)
{
	if(l==0) return 0;
	else     return dynamic_cast<TF2 *>(l->At(ind));
}

TProfile* SDAliases::GetProfile(TList *l, UInt_t ind)
{
	if(l==0) return 0;
	else     return dynamic_cast<TProfile *>(l->At(ind));
}

void SDAliases::ResetListOfHists(TList *l)
{
  if(l) {
	  TList *ll=0;
	  TH1 *h=0;
	 for(int i=0; i<l->GetSize(); ++i) {
		 TObject *obj = l->At(i);
		 if((ll = dynamic_cast<TList *>(obj))) {
			 ResetListOfHists(ll);
		 } else if((h=dynamic_cast<TH1 *>(obj))){
			 h->Reset();
		 }
	 }
  }
}

void SDAliases::ClearListOfHists(TList *l)
{
  if(l) {
	  TList *ll=0;
	  TH1 *h=0;
	 for(int i=0; i<l->GetSize(); ++i) {
		 TObject *obj = l->At(i);
		 if((ll = dynamic_cast<TList *>(obj))) {
			 ClearListOfHists(ll);
		 } else if((h=dynamic_cast<TH1 *>(obj))){
			 h->Reset();
		 }
	 }
  }
}

TList *SDAliases::ReadAll(const char *fname, const char *fnl, int pri)
{ // Read hists.; list of hits or list of list of hists - 6-aug-04
  if (!fname) {printf(" \n <W> Define the name of file !!!! \n \n"); return 0;}
  TKey *key; TObject *obj;
  TString nf(fname);
  if(nf.Contains(".root") == kFALSE) nf += ".root";
  if(pri) printf("<I> SDAlias::readAll => file name %s \n",nf.Data());

  TFile f(nf.Data());
  TList *l = new TList;
  if(strlen(fnl)) l->SetName(fnl);
  TH1::AddDirectory(kFALSE);
  if(f.IsOpen()){
    printFileRootVer(&f);
    TIter nextkey(f.GetListOfKeys());
    int nKeys = f.GetListOfKeys()->GetSize();
    if(pri) printf(" #Keys %i \n", nKeys);
    int kw=0;
    while ((key=(TKey*)nextkey())) {
      obj = key->ReadObj();
      l->Add(obj);
      if(pri) printf("Key %i name %s", kw++, obj->GetName());
    }
  } else {
    printf(" => wrong name -> %s !!!\n",fname);
    return 0;
  }
  if(l&&pri>0) gROOT->GetListOfBrowsables()->Add(l);
  return l;
}

TObject* SDAliases::ReadFirstObject(const char *fname)
{
  // Jul 03, 2009
  // 2013; Jan 8;
  if (!fname) {printf(" \n <W> Define the name of file !!!! \n \n"); return 0;}
  TString nf(fname);
  if(nf.Contains(".root") == kFALSE) nf += ".root";
  printf("<I> SDAlias::readFirstObject => file name %s \n",nf.Data());

  TObject *obj = 0;
  TFile f(nf.Data());
  TH1::AddDirectory(kFALSE);
  if(f.IsOpen()){
    printFileRootVer(&f);
    TIter nextkey(f.GetListOfKeys());
    int nKeys = f.GetListOfKeys()->GetSize();
    printf("\t Keys %i \n", nKeys);
READAGAIN:
    if(nKeys) {
      TKey *key=(TKey*)nextkey();
      obj = key->ReadObj(); // read first object
      obj->Dump();
      if(strcmp(obj->ClassName(),"TProcessID")==0) goto READAGAIN;
    }
  }
  return obj;
}

TObject* SDAliases::ReadFirstObjectFromDirectory(const char* dir)
{
	TString sdir(dir);
	if(dir==0) sdir += gSystem->Getenv("SDOUTPUT");
	cout<<"\n\t ReadFileFromDirectory("<<sdir<<")\n";

	const char *openTypes[] = { "root files",   "*.root",
	                            "All files",    "*",
	                            0,              0 };

    TGFileInfo fi;
    fi.fFileTypes = openTypes;
    fi.fIniDir    = StrDup(sdir.Data());
    new TGFileDialog(gClient->GetDefaultRoot(), 0, kFDOpen,&fi);
    if (!fi.fFilename) return 0;

    // fi.fIniDir does not change after dialog.
    // fi.fFileName will keep absolute file name.
    TObject *obj = sda::ReadFirstObject(fi.fFilename);
    return obj;
}

Int_t SDAliases::FillH2byTF2(TH2* h2, TF2 *f2, Double_t th, Double_t epsil)
{
	// Dec 31, 2012;
	// 2013, Jan 12;
	// Return number of entries (pixels)
	// epsil<=0 : simple filling by function
	// epsil >0 : fill by integrall value with relative accuracy = epsil
	// Common algoritmh is not so effective if function equal zero on most of area
	int pri=0;
	if(pri) cout<<" SDAliases::FillH2byTF2 : th "<<th<<" epsil "<<epsil<<endl;
	if(h2==0 || f2==0) return 0;
    Int_t numPixeles = 0;

	TAxis* ax = h2->GetXaxis(), *ay = h2->GetYaxis();
	Double_t fun = 0.0;
	for(Int_t binx=ax->GetFirst(); binx<=ax->GetLast(); ++binx){
		if(pri) cout<<" binx "<<binx<<endl;
		for(Int_t biny=ay->GetFirst(); biny<=ay->GetLast(); ++biny) {
            if(epsil<=0.0) {
			  fun = f2->Eval(ax->GetBinCenter(binx),ay->GetBinCenter(biny));
            } else {
              Double_t x1=ax->GetBinLowEdge(binx), x2 = ax->GetBinUpEdge(binx), xw=ax->GetBinWidth(binx);
              Double_t y1=ay->GetBinLowEdge(biny), y2 = ay->GetBinUpEdge(biny), yw=ay->GetBinWidth(biny);
              fun = f2->Integral(x1,x2, y1,y2, epsil)/(xw*yw);
            }
			if(fun>th) {
				h2->SetBinContent(binx,biny, fun);
				++numPixeles;
				if(pri) cout<<" biny "<<biny<<" fun "<<fun<<endl;
			}
		}
	}
	return numPixeles;
}

TList* SDAliases::moveHistsToList(const char* name, const bool putToBrowser, int pri)
{ // 22-Aug-2001 - move hists from gROOT directory to new TList
  // see StMaker::InitMain()
  // Aug 22, 2006 - added TTree
  gROOT->cd();
  TIter nextHist(gDirectory->GetList());
  TList *l = new TList;
  l->SetName(name);
  TObject *objHist;
  while((objHist=nextHist())){
    if (objHist->InheritsFrom("TH1")) {
      TH1* hid = (TH1*)objHist;
      hid->SetDirectory(0); // Remove from gROOT
      l->Add(objHist);
      if(pri>0)
      printf(" Hist %s(%s) add to the list %s\n", hid->GetName(),hid->GetTitle(),l->GetName());
    } else if (objHist->InheritsFrom("TH1") || objHist->InheritsFrom("TTree")) {
      // not so simple 
      // TTree* tr = (TTree*)objHist;
      // tr->SetDirectory(0); // Remove from gROOT
      // l->Add(objHist);
      // if(pri>0)
      // printf(" TTree %s(%s) add to the list %s\n", tr->GetName(),tr->GetTitle(),l->GetName());
    }
  }
  if(putToBrowser) gROOT->GetListOfBrowsables()->Add((TObject*)l);
  return l;
}

void SDAliases::Help()
{// 10-jul-2002 - see TPaveText::ReadFile(..)
   const int linesize = 255;
   char currentline[linesize];
   TString curLine;
   const char* skipLine[]={"//", "#","class"};

   ifstream file("/home/pavlinov/macros/SDAlias.h",ios::in);

   while (1) {
   READ:
      file.getline(currentline, linesize);
      if (file.eof()) break;
      curLine = currentline;
      for(int i=0; i<3; i++){
         if(curLine.Contains(skipLine[i])) goto READ;
      }
      printf("%s\n",curLine.Data()); 
   }
}

TH1* SDAliases::hid(const char *fname, const char *nid)
{
  TString nf(fname); 
  if(nf.Contains(".root") == kFALSE) nf += ".root";
  TFile f(nf.Data());
  if(f.IsOpen()){
    TH1 *hist = (TH1*)f.Get(nid);
    if(hist){
      hist->SetDirectory(gROOT);
      printf("Histogram  %s Read from file %s \n", nid, nf.Data());
    }
    else{
      printf("<W> Histogram %s did not exist \n", nid);
      return 0;
    }

    f.Close();
    return (TH1*)gROOT->FindObject((char*)nid);
  }
  else{
    printf("<W> File did not exist : %s \n", fname);
    return 0;
  }
}

TH1* SDAliases::hid(const char *fname, const char *nid, const char *adName)
{// Add the adName to name of hist
  TH1* htmp = hid(fname, nid);
  if(htmp && adName){
    if(strlen(adName)) {
      TString tmp = htmp->GetName(); 
      tmp += adName; 
      htmp->SetName(tmp.Data());
    }
  }
  return htmp;
}

void SDAliases::rf(const char *fname, const char *nid)
{
  TH1 *htmp = hid(fname,nid); // ?? - 2-jan-2003
  if(htmp) return;
} 

void SDAliases::rf(const char *fname, const int id){
  char tmp[20];
  sprintf(tmp,"h%i", id);
  rf(fname, &tmp[0]); // Did'n work properly => eat first symbol h ??
} 

TNtuple *SDAliases::readNtuple(const char *fname, const char *fnuple)
{
  TFile f(fname,"READ");
  TObject *obj = f.FindObject(fnuple);
  if(obj) { 
    TNtuple *ntu = (TNtuple*)obj;
    ntu->SetDirectory(gROOT);
    f.Close();
    return ntu;
  } else    return 0;
}

int SDAliases::hrin(const char* file, const char *opt, Int_t pri)
{// have to check opt ??
  if (!file) {printf(" \n <W> Define the name of file !!!! \n \n"); return kFALSE;}
  TString OPT(opt);  TKey *key; TObject *obj; TH1 *hid;
  TString nf(file); 
  if(nf.Contains(".root") == kFALSE) nf += ".root";
  printf("<I> SDAlias::hrin => file name %s : opt %s : pri %i \n", nf.Data(), opt, pri);

  TFile f(nf.Data());

  int nKeys=0, nHists=0, nSkip=0; 
  if(f.IsOpen()){
    printFileRootVer(&f);
    TIter nextkey(f.GetListOfKeys());
    nKeys = f.GetListOfKeys()->GetSize();
    printf("Keys %i \n", nKeys);

    while ((key=(TKey*)nextkey())) {
      obj = key->ReadObj();
      if (obj->InheritsFrom("TH1")) {
         hid=(TH1*)obj;
         hid->SetDirectory(gROOT);
         if(pri) printf(" %4i : %s :  %s -> %s \n ", 
         nHists, obj->ClassName(), hid->GetName(), hid->GetTitle());
         nHists++;
      }  else {
	if(OPT.Contains("hist",TString::kIgnoreCase)){
	  if(pri) printf("%i skipped : class %s\n", nSkip, obj->ClassName());
          nSkip++;
	} else {
          obj = key->ReadObj();
          if(pri) printf(" read object %s \n ", obj->GetName()); 
	}
      }
    }
  } else {
     printf(" => wrong name -> %s !!!",file);
     return kFALSE; 
  }
  printf("%i hists read from file: %i objects skipped: %i objects (%i)\n",
  nHists, nSkip, nKeys, (nHists+nSkip)-nKeys );
  f.Close();
  printf("\n");
  return kTRUE;
}

TList* SDAliases::hrinToList(const char* file, const char *ln, const bool putToBrowser, Int_t pri)
{ // 28-aug-2003
  hrin(file);
  TList *l =  moveHistsToList(ln, putToBrowser, pri);
  if(l) printf("<I>  SDAlias::hrinToList : list size %i \n", l->GetSize());
  return  l;
}

int SDAliases::saveListOfHists(TList *list, const char* name, Bool_t kSingleKey, const char* opt)
{
  printf(" Name of out file |%s| : list name %s : opt %s \n", name, list->GetName(), opt); 
  int save = 0;
  if(list && list->GetSize() && strlen(name)){
    TString nf(name); 
    if(nf.Contains(".root") == kFALSE) nf += ".root";
    TFile file(nf.Data(),opt);
    TIter nextHist(list);
    TObject* objHist=0;
    int nh=0;
    if(kSingleKey) {
       file.cd();
       list->Write(list->GetName(),TObject::kSingleKey);
       list->ls();
       save = 1;
    } else {
      while((objHist=nextHist())) { // loop over list 
        if(objHist->InheritsFrom("TH1")) {
          TH1* hid = (TH1*)objHist;
          file.cd();
          hid->Write();
          nh++;
          printf("Save hist. %s \n",hid ->GetName());
        }
      }
      printf("%i hists. save to file -> %s\n", nh,file.GetName());
      if(nh>0) save = 1;
    }
    file.Close();
  } else {
    printf("SDAlias::saveListOfHists : N O  S A V I N G : ");
    if(list==0) cout << "List of object 0 : " << list<<endl;
    else printf("Size of list %i \n", list->GetSize());
  }
  return save;
}

TF1* SDAliases::GetFunction(TH1 *h, const int ind)
{
	if(h==0) return 0;
	return dynamic_cast<TF1 *> (h->GetListOfFunctions()->At(ind));
}

void SDAliases::printFileRootVer(const TFile *f)
{// 22-sep-03 - for convenience
  if(f==0) {
     printf(" No file !\n");
     return;
  }
  printf("    Was written by ROOT %i : %s <- Current version \n", 
  f->GetVersion(), gROOT->GetVersion());
}

void SDAliases::DrawHist(TH1* hid,int lineWidth,int lineColor,const char* opt, int lineStyle)
{
  static TString OPT;
  OPT = opt;
  if(!hid) return;
  printf(" Hist. %s : option |%s| \n", hid->GetName(), opt);
  if(hid && hid->GetEntries()>=1.) {
    if(lineWidth) hid->SetLineWidth(lineWidth);
    if(lineColor) hid->SetLineColor(lineColor);
    if(lineStyle) hid->SetLineStyle(lineStyle);
    if(OPT.Contains("stat",TString::kIgnoreCase)) hid->SetStats(kTRUE);
    hid->Draw(opt);
  } else {
    if   (strcmp(opt,"empty")==0) hid->Draw();
    else printf(" has fewer entries %i or hid is zero\n", (int) hid->GetEntries());
  }
}

TCanvas *SDAliases::DrawListOfHist(TList *list, int nx, int ny, int nmax, int istart)
{ // 14-nov-2001 from StCF 
  // 12-dec-2002 - skip empty histogram
  TCanvas *c1=0;
  TH1 *hid;
  Int_t idraw=0;
  if(list || nx==0 || ny == 0){
    int nh = list->GetSize();
    printf("List %s has %i hists : will be drawn %i or %i : istart %i\n",
	   list->GetName(), nh, nx*ny, nmax, istart);
    if(nh>=nmax && nmax!=0) nh = nmax;
    if(nx*ny> nmax){
      ny = nmax/nx;
      if(nmax == 1) nx=ny=1;
    }
    if(nh) {
      c1 = Canvas(list->GetName());
      c1->Divide(nx,ny);
      for(int i=istart; i<istart+nh; i++){                                
        hid = (TH1*)list->At(i);
        if(hid->Integral()){
	   idraw++;
           c1->cd(idraw);
           DrawHist(hid,2); 
        } else {
          printf("Hist %s is empty - skipped \n", hid->GetName());
        }
        if(idraw == nx*ny) break;
      }
    } else printf("<W> SDAlias::drawListOfHist -> list of hist is empty !!\n");
  } else printf("<W> SDAlias::drawListOfHist -> pointer is zero \n");
  return c1; 
}

void SDAliases::DrawTwoListsOfHists(TList *li1, TList *li2, int nx, int ny,
		const char *tit1, const char *tit2, int col1, int col2, const char *globalTitle,
		Int_t sameMax) {
	// 14-nov-2001 from StCF
	// 12-dec-2002 - skip empty histogram
	TCanvas *c1 = 0;
	TPad *p1 = 0;
	TH1 *hid1=0, *hid2=0;
	Int_t idraw = 0;
	Double_t integ1 = 0.0, integ2 = 0.0;
	if ((li1&&li2) || nx == 0 || ny == 0) {
		int nh = li1->GetSize();
		printf("List %s has %i hists : will be drawn %i\n", li1->GetName(), nh,
				nx * ny);
		if (nh) {
			c1 = Canvas(li1->GetName());
			p1 = pad(globalTitle, c1);
			p1->Divide(nx, ny);

			for (int i = 0; i < nh; i++) {
				hid1 = (TH1*) li1->At(i);
				integ1 = hid1->Integral();
				hid2 = (TH1*) li2->At(i);
				integ2 = hid2->Integral();

				if (integ1 || integ2)
					idraw++;
				TString opt;
				if (integ1) {
					p1->cd(idraw);
					DrawHist(hid1, 2, col1);
					opt = "same";
				} else
					printf("Hist %s is empty - skipped(li1) \n",
							hid1->GetName());
				if (integ2) {
					p1->cd(idraw);
					DrawHist(hid2, 2, col2, opt.Data(), 2);
					if (hid1->GetMaximum() < hid2->GetMaximum())
						hid1->SetMaximum(hid2->GetMaximum() * 1.1);
				} else
					printf("Hist %s is empty - skipped(li1) \n",
							hid1->GetName());
				if (idraw == 1 && strlen(tit1) && strlen(tit2)) {
					TAxis *xax = hid1->GetXaxis();
					double xm = (xax->GetXmax() + xax->GetXmin()) / 2.;
					double ym = hid1->GetMaximum() * 0.9;
					double dx = (xax->GetXmax() - xax->GetXmin()) * 0.03;
					double dxShift = (xax->GetXmax() - xax->GetXmin()) * 0.04;
					dispLine(xm, ym, col1, tit1, dx, 1, 2, dxShift);
					ym = hid1->GetMaximum() * 0.7;
					dispLine(xm, ym, col2, tit2, dx, 2, 2, dxShift);
					gPad->Update();
				}
				if (idraw == nx * ny)
					break;
			}
			c1->Update();

		}
	}
	printf( "<W> SDAlias::drawListOfHist -> li1 %p li2 %p nx %i ny %i\n",
			li1, li2, nx, ny);
}

void SDAliases::dispLine(double xm, double ym, Short_t color, const char* text,
double dx, int lineStyle, int lineWidth, double dxShift, double dyShift, float txtSize)
{// see /home/pavlinov/cosmic/pythia/geant/ocu.C
  double y1=ym, y2 = ym + dyShift;
  if(gPad->GetLogy()) {
     y1 = TMath::Log10(y1);
     y2 = TMath::Log10(y2);
  }
  TLine *l = new TLine(xm,y1, xm+dx, y1);
  l->SetLineColor(color);
  l->SetLineWidth(lineWidth);
  l->SetLineStyle(lineStyle);
  l->Draw();
  TLatex *lat = new TLatex(xm + dxShift, y2, text);
  lat->SetTextSize(txtSize);
  lat->SetTextColor(color);
  lat->Draw();
  printf("xm %f y1 %f color %i dx %f text %s\n", xm,y1, color, dx, text);
}

void SDAliases::Titles(TH1 *hid, const char *titx,const char *tity)
{
  if(hid) {
    hid->SetXTitle(titx);
    hid->SetYTitle(tity);
  } else {
    printf("<W> SDAlias::titles() -> hid is 0 !\n");
  }
}

void SDAliases::PrintHist(TH1 *hid, int n1, int n2)
{
  if(hid==0) return;
  if(n1>=n2) { // who;e histogramm
    n1 = 1;
    n2 = hid->GetNbinsX();
  }
  if(n1<=1) n1 = 1;
  if(n2>=1) n2 = hid->GetNbinsX();

  printf(" Name | %s | Title |%s|\n", hid->GetName(), hid->GetTitle());
  for(int i=n1; i<=n2; i++){
    printf(" %4i (%9.3f) y %f \n", i, hid->GetBinCenter(i), hid->GetBinContent(i));    
  }
}

TH1* SDAliases::drawF(TF1* f,int lineWidth,int lineColor,const char* opt, int lineStyle)
{ // 29-sep-2003
  if(f) {
    if(lineWidth) f->SetLineWidth(lineWidth);
    if(lineColor) f->SetLineColor(lineColor);
    if(lineStyle) f->SetLineStyle(lineStyle);
    f->Draw(opt);
    TH1* hid = f->GetHistogram();
    printf(" Function. %s : option |%s|\n", f->GetName(), opt);
    printf("lineWidth %i | lineColor %i |lineStyle %i |\n",
    lineWidth,lineColor,lineStyle);
    return hid;
  } else return 0;
}

void SDAliases::DrawProjectionX(TH2F *hid, const char *tit, int ix1, int ix2, int nx, int ny, TPad *p)
{
  if(nx||ny) {};
  TCanvas *c = 0;
  if(p==0) {
    c = Canvas("c");
    c->SetTitle(tit);
    p = pad(tit, c);
    p->cd(1);
  }
  //  p->Divide(2,2);
  //gStyle->SetOptStat(1100);

  TString opt;
  TH1D *hd;
  int lcolor=0;
  int lwidth = 3;;
  for(int ix=ix1; ix<=ix2; ix++){
    TString nam("prox"); nam += ix;
    hd = hid->ProjectionX(nam.Data(),ix,ix);

    //    hd->SetStats(kFALSE);
    if(hd->Integral() > 0.0) {
      DrawHist(hd, lwidth, lcolor,opt.Data());
      lcolor++;
      if(lwidth>1) lwidth--;
      opt="same";
    } else {
      printf(" Projection %s has not statistics \n", nam.Data());
    }
  }
  if(c>0) c->Update();
}

TList* SDAliases::CreateProjection(TH2 *h2, const char *name, const char* opt)
{
  // Jun 07,2011
  if(h2==0) return 0;
  TString OPT(opt); OPT.ToUpper();
  Int_t i1=1, i2=h2->GetNbinsY();
  TAxis *axis = h2->GetXaxis();
  if(OPT.Contains("Y")) {
    i2   = h2->GetNbinsX();
    axis = h2->GetXaxis();
  }

  TList *lp = new TList;
  TProfile *hp = 0;
  if(strlen(name)) lp->SetName(Form("%sPro%s",name,OPT.Data()));
  hp = h2->ProfileX(Form("%s ProfX", h2->GetName()));
  lp->Add(hp);
  hp = h2->ProfileY(Form("%s ProfY", h2->GetName()));
  lp->Add(hp);
  if(OPT.Contains("ONLY")) return lp;

  TH1D *hd = 0;
  TString stmp(h2->GetName());
  Int_t ind = stmp.Index("_")+1, length = stmp.Length()-ind;
  stmp = stmp(ind,length);
  for(int i=i1; i<=i2; i++){
    TString nam(Form("%2.2i %sPro%s", i, stmp.Data(),OPT.Data()));
    TString tit(Form("%2.2i %s %6.2f<%s<%6.2f \n", i, h2->GetTitle(), axis->GetBinLowEdge(i), OPT.Data(), axis->GetBinUpEdge(i)));
    if(OPT.Contains("X")) {
      hd = h2->ProjectionX(nam.Data(),i,i);
    } else {
      hd = h2->ProjectionY(nam.Data(),i,i);
    }
    hd->SetTitle(tit.Data());
    lp->Add(hd);
    //printf(" i %i %s \n", i, nam.Data());
  }
  return lp;
}

void SDAliases::FillProfiles(TList *l, const Int_t ind)
{
  if(l==0) return;
  TH2* h2 = dynamic_cast<TH2 *>(l->At(ind));
  if(h2==0) return;

  TString snam(h2->GetName()), stmp=snam(3,snam.Length()-3);
  TProfile* hprof = h2->ProfileX(Form("%iProfX%s",l->GetSize(),stmp.Data()));
  l->Add(hprof);
  hprof = h2->ProfileY(Form("%iProfY%s",l->GetSize(),stmp.Data()));
  l->Add(hprof);
}

// comparing with drawing - 16-mar-04
TCanvas* SDAliases::compareWithDrawing(TH1F* h1, TH1F* h2, char *tit)
{
  if(h1==0 || h2==0) return 0;
  TCanvas *c = Canvas("COMP");
  c->SetTitle(tit);

  TPad *p = pad(tit, c);
  p->Divide(1,2);

  p->cd(1);
  DrawHist(h1,2,1);
  DrawHist(h2,2,2,"same");

  p->cd(2);
  TH1F* hrat = (TH1F*)h1->Clone("Normalised Ratio (h1/h2)"); 
  //  hrat->Divide(h2, h1->Integral()/h2->Integral());
  hrat->Divide(h2);
  DrawHist(hrat, 2);
  
  c->Update();
  return c;
}

TCanvas* SDAliases::Canvas(const char *name,const char *tit,
  int wtopx, int wtopy, int ww, int wh,int clear)
{
  // Jul 18,09 : gROOT->FindObject(name)
  if(name==0) name="canvas";
  if(tit==0)  tit="CANVAS";

  TCanvas* c = 0;

  TObject *cObj = gROOT->FindObject(name);
  if(cObj) {
    c = dynamic_cast<TCanvas *>(cObj);
  } else {
    c = new TCanvas(name,tit,wtopx,wtopy,ww,wh); 
  }
  if(clear) c->Clear();
  return c;
}

TCanvas* SDAliases::c() {return Canvas();} // Alias

TPad* SDAliases::pad(const char *name, TCanvas *c, double ymax)
{
  // Canvas c will include one TPad for title (include date) and TPad pad
  if(c) {
    c->cd();
    TString tit(name); 
    if(tit.Length()) {
       TPad *padTit = new TPad("TitPad","Title Pad",0.02,ymax,0.98,1.0);
       padTit->SetFillColor(18); // See class TAttFill 
       padTit->Draw();
       padTit->cd();

      TDatime HistTime;
      tit += "    "; 
      TString sd(HistTime.AsString());
      tit += sd(0,10);
      tit += "/";
      tit += sd(sd.Length()-2,2);
      TLatex *latex = new TLatex;
      latex->SetName("textForTitle");
      latex->SetTextAlign(22);
      latex->SetTextSize(Float_t(0.4)); 
      latex->DrawLatex(0.5, 0.5, tit.Data());
    } else {ymax = 1.;}

    c->cd();
    TString spad(c->GetName()); spad += "_pad";
    TPad *pad = new TPad(spad.Data(),"MainPad",0.0,0.00,1.00,ymax);
    pad->Draw();
    c->Update();
    return pad;
  }
  else return 0;
}

TPad* SDAliases::getMainPad(TCanvas *c)
{ // don't change title for MainPad - 4-jun-04
  if(c==0) return 0;
  TIter next(c->GetListOfPrimitives());                           
  TPad *p=0;
  while (TObject *obj = next()){
    if(obj->InheritsFrom("TPad")) {
      TString t(obj->GetTitle());
      if(t.Contains("MainPad")) {
        p = (TPad*)obj;
        cout<<" Find TPad with name "<<obj->GetName()<<" p "<<p<<endl;
      }
    }
  }
  return p;
}

TPad* SDAliases::getTitPad(TCanvas *c)
{
  if(c==0) return 0;
  TObject* obj = c->FindObject("TitPad");
  TPad *p=0;
  if(obj) {
    printf(" Find TPad with name %s\n", obj->GetName());
    p = (TPad*)obj;
  }
  return p;
}

TLatex *SDAliases::getTitText(TCanvas *c)
{
  TPad *p =SDAliases::getTitPad(c);
  p->ls();
  if(p==0) return 0; 
  TObject *obj = p->FindObject("textForTitle");
  if(obj==0) return 0;
  return (TLatex*)obj;
}

TLegend* SDAliases::getLegend(TCanvas *c)
{
  if(c==0) return 0;
  TPad *p=getMainPad(c);
  if(p==0) return 0;
  TIter next(p->GetListOfPrimitives());                           
  TLegend *leg=0;
  while (TObject *obj = next()){
    if(obj->InheritsFrom("TLegend")) {
      leg = (TLegend*)obj;
      printf(" Find TLegend with name %s\n", leg->GetName());
      return leg; 
    }
  }
  return leg;
}

void SDAliases::PrintList(TList *l)
{
	if(l==0) {
		cout<<" l is zero \n";
		return;
	}
	cout<<" list name "<<l->GetName()<<" size "<<l->GetSize()<<endl;
	l->ls("*");
}

void SDAliases::savePict(const int save, TCanvas* c, const char *dir)
{
  TString opt;
  if(save==0 || c==0) {
    printf("<I> savePict() => save=0 or canvas c=0 \n");
    printf(" save = 1 :  ps\n");
    printf("      = 2 : eps\n");
    printf("      = 3 : gif(default)\n");
    return;
  }
  switch (save){
  case 1: opt="ps"; break;
  case 2: opt="eps"; break;
  case 3: opt="gif"; break;
  default: opt="gif";
  }

  TString name(dir);
  name += c->GetTitle();
  name += ".";
  name += opt;
  printf("Picture was made for canvas name->%s  and title->%s\n", c->GetName(), c->GetTitle());
  c->Print(name.Data(),opt.Data());
}

TGraph *SDAliases::drawGraph(Int_t n, Double_t *x, Double_t *y, Int_t markerColor,
Int_t markerStyle, const char* opt, const char* tit, const char* xTit,const char* yTit, Int_t ifun,  
const char *optFit, const char *fun)
{
  /* Drawing options 
  chopt='L' :  A simple polyline between every points is drawn
  chopt='F' :  A fill area is drawn ('CF' draw a smooth fill area)
  chopt='A' :  Axis are drawn around the graph
  chopt='C' :  A smooth Curve is drawn
  chopt='*' :  A Star is plotted at each point
  chopt='P' :  Idem with the current marker
  chopt='B' :  A Bar chart is drawn at each point
  chopt='1' :  ylow=rwymin
  chopt='X+' : The X-axis is drawn on the top side of the plot.
  chopt='Y+' : The Y-axis is drawn on the right side of the plot.

    Fitting options
   The list of fit options is given in parameter option.
      option = "W"  Set all errors to 1
             = "U" Use a User specified fitting algorithm (via SetFCN)
             = "Q" Quiet mode (minimum printing)
             = "V" Verbose mode (default is between Q and V)
             = "B" Use this option when you want to fix one or more parameters
                   and the fitting function is like "gaus","expo","poln","landau".
             = "R" Use the Range specified in the function range
             = "N" Do not store the graphics function, do not draw
             = "0" Do not plot the result of the fit. By default the fitted function
                   is drawn unless the option"N" above is specified.
             = "+" Add this new fitted function to the list of fitted functions
                   (by default, any previous function is deleted)
             = "C" In case of linear fitting, not calculate the chisquare
                    (saves time)
             = "F" If fitting a polN, switch to minuit fitter
             = "ROB" In case of linear fitting, compute the LTS regression
                     coefficients (robust(resistant) regression), using
                     the default fraction of good points
               "ROB=0.x" - compute the LTS regression coefficients, using
                           0.x as a fraction of good points

  */
  printf("SDAlias::drawGraph started \n");
  printf("Drawing opt |%s| : Fitting opt |%s|\n", opt, optFit);

    TGraph *gr = new TGraph(n, x, y);
    gr->SetMarkerColor(markerColor);
    gr->SetLineColor(markerColor);
    gr->SetMarkerStyle(markerStyle);
    gr->SetTitle(tit);
    gr->Draw(opt);

    TString ctmp(opt);
    if(ctmp.Contains("A")) {
       gr->GetHistogram()->SetXTitle(xTit);
       gr->GetHistogram()->SetYTitle(yTit);
    }
    ctmp = optFit; 
    if(ifun>=0) {
      TString sf("pol"); sf += ifun;
      gr->Fit(sf.Data(),optFit);
      printf("\n ** Fit by Polynomial of degree %i : %s **\n", ifun, sf.Data());
    } else if(!ctmp.Contains("no",TString::kIgnoreCase)){
      printf("\n ** ifun %i : %s **\n", ifun, fun);
      gr->Fit(fun, optFit);
    }

    return gr;
}

TGraphErrors *SDAliases::drawGraphErrors(const Int_t n,Double_t *x,Double_t *y,Double_t *ex,
Double_t *ey, Int_t markerColor,  Int_t markerStyle, const char* opt, const char* tit, 
const char* xTit,const char* yTit, Int_t ifun, const char *optFit, const char *fun)
{
  /*
  */

  printf("SDAlias::drawGraphErrors started \n");
  printf("Drawing opt |%s| : ifun %i: Fitting opt |%s|, fun |%s|\n", 
	 opt, ifun, optFit, fun);
  //  print(n, x,y, ex,ey, tit);

  TGraphErrors *gr = new TGraphErrors(n, x,y,ex,ey);
  gr->SetMarkerColor(markerColor);
  gr->SetLineColor(markerColor);
  gr->SetMarkerStyle(markerStyle);
  if(tit&&strlen(tit)>0) gr->SetTitle(tit);
  gr->Draw(opt);

  TString ctmp(opt);
  if(ctmp.Contains("A")) {
     gr->GetHistogram()->SetXTitle(xTit);
     gr->GetHistogram()->SetYTitle(yTit);
  }
  if(ifun>=0) {
    if(ifun != 999) {
      TString sf("pol"); sf += ifun;
      gr->Fit(sf.Data(),optFit);
      printf("\n ** Fit by Polynomial of degree %i : %s **\n", ifun, sf.Data());
    } else {
      gr->Fit(fun, optFit);
      printf("\n ** Fit by %s **\n", fun);
    }
  } else {
    printf("\n ** ifun %i : %s **\n", ifun, fun);
    gr->Fit(fun, optFit);
  }

  return gr;
}

void SDAliases::print(Int_t n,Double_t *x,Double_t *y,Double_t *ex,Double_t *ey, const char* tit)
{
  printf("\n **\n  Number points %i : tit %s\n", n, tit);
  if(n<=0) return;
  for(Int_t i=0; i<n; i++) {
    printf(" x %f +/- %f ",  x[i], ex[i]);
    printf(" y %f +/- %f \n",y[i], ey[i]);
  }
  printf(" ** \n");
}

TH1F *SDAliases::create(TH1F* hid, const Char_t *nam, const Char_t *tit)
{
  TH1F *hout = 0;
  if(hid==0) return 0;

  TString name=nam;
  if(name.Length()==0) name  = hid->GetName();
  TString title=tit;
  if(title.Length()==0) title = hid->GetTitle();

  TAxis *xax = hid->GetXaxis();
  if(xax->GetXbins()->GetSize()==0) { // uniform step
    hout = new TH1F(name.Data(),title.Data(), hid->GetNbinsX(),
    xax->GetXmin(),xax->GetXmax()); 
    printf("TH1F *SDAlias::create hist : %s : bin %i xmin %f xmax %f\n",
    hout->GetName(), hout->GetNbinsX(),
    hout->GetXaxis()->GetXmin(), hout->GetXaxis()->GetXmax());
  } else {
    hout = new TH1F(name.Data(),title.Data(), hid->GetNbinsX(),
    hid->GetXaxis()->GetXbins()->GetArray()); 
  }

  return hout;
}

TH1F *SDAliases::copy(TH1F* hid, const Char_t *nam, const Char_t *tit,Int_t modeCopy)
{
   TH1F *hout = create(hid,nam,tit);
   if(hout==0) return 0;

   printf("TH1F *SDAlias::copy hist : %s \n", hid->GetName());

   for(Int_t i=1; i<=hid->GetNbinsX(); i++){
      hout->SetBinContent(i, hid->GetBinContent(i));
      if(modeCopy>0) {
         hout->SetBinError(i, hid->GetBinError(i));
      }
   }
   return hout;
}

TH1D *SDAliases::create(TH1D* hid, const Char_t *nam, const Char_t *tit)
{
  TH1D *hout = 0;
  if(hid==0) return 0;

  TString name=nam;
  if(name.Length()==0) name  = hid->GetName();
  TString title=tit;
  if(title.Length()==0) title = hid->GetTitle();

  TAxis *xax = hid->GetXaxis();
  if(xax->GetXbins()->GetSize()==0) { // uniform step
    hout = new TH1D(name.Data(),title.Data(), hid->GetNbinsX(),
    xax->GetXmin(),xax->GetXmax()); 
  } else {
    hout = new TH1D(name.Data(),title.Data(), hid->GetNbinsX(),
    hid->GetXaxis()->GetXbins()->GetArray()); 
  }

  return hout;
}

TH1D *SDAliases::copy(TH1D* hid, const Char_t *nam, const Char_t *tit,Int_t modeCopy)
{
   TH1D *hout = create(hid,nam,tit);
   if(hout==0) return 0;
   for(Int_t i=1; i<=hid->GetNbinsX(); i++){
      hout->SetBinContent(i, hid->GetBinContent(i));
      if(modeCopy>0) {
         hout->SetBinError(i, hid->GetBinError(i));
      }
   }
   return hout;
}

void SDAliases::FillH1(TList *l, int ind, double x, double w, double error)
{ // 29-aug-2003; May 27, 2006
  static TH1* hid=0;
  static int  bin=0;
  if(l && ind < l->GetSize()){
    hid = (TH1*)l->At(ind);
    if(error <= 0.0) { // standard way
      hid->Fill(x,w);
    } else{
      bin = hid->FindBin(x);
      hid->SetBinContent(bin,w);
      hid->SetBinError(bin,error);
    }
  }
}

void SDAliases::FillH2(TList *l, int ind, double x, double y, double w)
{ // 29-oct-2003
  static TH2* hid=0;
  if(l && ind < l->GetSize()){
    hid = (TH2*)l->At(ind);
    hid->Fill(x,y,w);
  }
}

void SDAliases::FillHprof(TList *l, int ind, double x, double y, double w)
{ // Feb 4,2011
  static TProfile* hid=0;
  if(l && ind < l->GetSize()){
    hid = dynamic_cast<TProfile *>(l->At(ind));
    if(hid) hid->Fill(x,y,w);
  }
}

void SDAliases::FillHnSparse(TList *l, Int_t ind, Double_t* x, Double_t w)
{
  // Nov 02,2010: fill THnSparse hist
  static THnSparse* hsp=0;
  //printf(" l %p ind %i x[0] %f w %f : l->GetSize() %i \n", l,ind,x[0],w,l->GetSize());
  if(l==0 || x==0) return;
  if(ind>=0 && ind < l->GetSize()){
    hsp = dynamic_cast<THnSparse *>(l->At(ind));
    if(hsp) hsp->Fill(x,w);
  }
}

// get method - Jun 16, 2006
TObject* SDAliases::FindObjectByPartName(TSeqCollection *col,const char* partName)
{
  if(col==0 || strlen(partName)==0) return 0;

  TObject *obj = 0;
  TString sn;

  for(int i=0; i<col->GetSize(); i++) {
    obj = col->At(i);
    sn  = obj->GetName();
    if(sn.Contains(partName, TString::kIgnoreCase)) return obj;
  }
  return 0;
}

TH1* SDAliases::findHistByName(TList *list, const char* name)
{
  //
  // Jan 10,2007 - recursive function
  //
  TH1 *h=0;

  if(list == 0)            return 0;
  if(list->GetSize() == 0) return 0;
  printf(" <I>  SDAlias::findHistByName : %s(%i) -> |%s| \n",
	 list->GetName(), list->GetSize(), name);
  for(int i=0; i<list->GetSize(); i++) {
    h = 0;
    TObject *o = list->At(i);
    TString className(o->ClassName());
    if(className.Contains("TH1")||className.Contains("TH2")||className.Contains("TH3")) {
      // printf("%i \t: %s : %s\n", i, o->GetName(), className.Data()); 
      TString histName(o->GetName());
      if(histName == name) {
        printf(" <I> found histogram with name %s : i=%i\n", name, i);
        return (TH1*)o;
      } else {
        // printf(" ** no histogram with name |%s| != |%s|\n", histName.Data(), name);
      }
    } else {
      if(className == "TList") h = SDAliases::findHistByName((TList*)o, name);
      if(h) return h;
    }
  }
  //  printf(" <W> no histogram with name %s \n", name);
  return 0;
}

TMap* SDAliases::getFileMap(const char* fn, const char *dir, const int pri)
{ // May 23, 2007; see rHits.C, method getFileMap(const char* fn, const char *dir)
  TString fls=gSystem->ExpandPathName(dir); 
  fls += fn;
  ifstream in;
  in.open(fls.Data());
  if (!in) {
    cout << "Input file |%s|cannot be opened.\n" << fls.Data();
    return 0;
  }
  if(pri>0) cout << "File with list of file : " << fls.Data() << endl << endl;

  TMap *mapTmp = new TMap;
  mapTmp->SetName("fileMaps");
  gROOT->GetListOfBrowsables()->Add(mapTmp);

  char str[200];
  TString ins; // input string
  TString DIR, F;
  TObjString *key, *value;
  while (!in.getline(str,200).eof()) { // Nov 21,2010 from AliCaloMipPict
    if(!in.good()) break;
    TString ins(str); // input string
    ins.ReplaceAll("\t"," "); // replace tabulator to blank space
    if(pri) printf(" ins |%s|\n", ins.Data());
    if       (ins.Contains("//")) { // comments
      if(pri) cout << ins << endl;
    } else if(ins[0]=='/' || ins[0]=='$') {
      DIR = ins;
      DIR.ReplaceAll(" ","");
      if(pri) printf("Directory |%s|\n",DIR.Data());
    } else {
      Int_t ind1 = ins.Index(" "), i=0;
      TString stmp = ins(0,ind1);
      stmp.ReplaceAll(" ","");
      key   = new TObjString(stmp);
      for(i=ind1+1; i<ins.Length(); i++) if(ins[i] != ' ') break;

      stmp = DIR + ins(i,ins.Length()-i+1);
      stmp.ReplaceAll(" ","");
      value = new TObjString(stmp);
      if(pri) printf("|%s| -> |%s| \n",key->GetString().Data(),value->GetString().Data());
      mapTmp->Add(key, value);
    }
    if(ins.Contains("END")) break;
  }
  in.close();

  return mapTmp;
}

TString SDAliases::getFileName(const TMap *MAP, const int var)
{ // May 23, 2007
  TString fn(""); 
  if(var>=0 && MAP) {
    TString key(Form("%3.3i",var)); 

    TObject *obj = MAP->GetValue(key.Data()); // FindObject return a TPair* pointers 
    if(obj) {
      TObjString* ofn = (TObjString*)(obj);
      fn = ofn->GetString();      
    } else {
      fn="";
      printf(" No file for var %i !!! \n", var);
    }
  }
  return fn;
}

Double_t* SDAliases::ArrLogAxis(const Double_t xmax, const Int_t nbin, const Double_t xminLog, const Int_t pri)
{
  // Feb 27,2011
  Double_t xmaxLog=TMath::Log10(xmax);
  Double_t step = (xmaxLog-xminLog)/nbin;
  Double_t* xArrLog = new Double_t[nbin+1]; // should be destroy after using
  if(pri>0) printf(" nbin %i xmax %9.3f xminLog %12.5e xmaxLog %12.5e  step %12.5e \n",nbin,xmax, xminLog,xmaxLog,step);
  xArrLog[0] = 0.0; // start from zero
  for(Int_t i=1; i<=nbin; i++) {
    Double_t dexp = xminLog + step*i;
    xArrLog[i] = pow(10.,dexp);
    if(pri>0) printf(" %4i dexp %8.4f x %e \n", i, dexp, xArrLog[i]);
  }
  return xArrLog;
}


TMap* SDAliases::initFilesMap(const char* fn, const Int_t pri)
{
  // Sep 26,2010 - comments could be any text now
  // Oct 05: more accurate treating of input string
  TString fls=gSystem->ExpandPathName(fn);
  ifstream in;
  in.open(fls);
  if (!in) {
    cout << "Input file |%s|cannot be opened.\n" << fls.Data();
    return 0;
  }
  cout << "File with list of file : " << fls.Data();

  TMap *mapTmp = new TMap;

  char str[200];
  TObjString *key=0, *value=0;
  TString DIR;

  while(!in.getline(str,200).eof()) {
    if(!in.good()) break;
    TString ins(str); // input string
    ins.ReplaceAll("\t"," "); // replace tabulator to blank space
    if(pri) printf(" ins |%s|\n", ins.Data());
    if       (ins.Contains("//")) { // comments
      if(pri) cout << ins << endl;
    } else if(ins[0]=='/' || ins[0]=='$') {
      DIR = ins;
      DIR.ReplaceAll(" ","");
      if(pri) printf("Directory |%s|\n",DIR.Data());
    } else {
      Int_t ind1 = ins.Index(" "), i=0;
      TString stmp = ins(0,ind1);
      stmp.ReplaceAll(" ","");
      key   = new TObjString(stmp);
      for(i=ind1+1; i<ins.Length(); i++) if(ins[i] != ' ') break;

      stmp = DIR + ins(i,ins.Length()-i+1);
      stmp.ReplaceAll(" ","");
      value = new TObjString(stmp);
      if(pri) printf("|%s| -> |%s| \n",key->GetString().Data(),value->GetString().Data());
      mapTmp->Add(key, value);
    }
    if(ins.Contains("END")) break;
  }
  in.close();
  cout << " Files : " << mapTmp->GetSize() << endl;

  return mapTmp;
}

TH1F* SDAliases::efficiencyFactor(TH1F* hid, TString opt, Int_t norm)
{// 6-jan-2003; 13-nov-05 - reverse case
  if(!(hid && hid->Integral())) return 0;
  printf("/n SDAlias::efficiencyFactor : opt %s ", opt.Data());
  if(norm) printf(" norm %i", norm);
  printf("\n");
  TH1F* hEff = (TH1F*)hid->Clone();
  AddToName(hEff, "Eff");
  hEff->Reset();
  hEff->Sumw2();
  Int_t m=0, n=Int_t(hid->GetEntries());
  if(norm>0) {
    printf(" Will be normalised on #events %i : %i \n", norm, n);
    n = norm;
  }
  Double_t prob=0.0, errProb=0.0;
  for(Int_t i=1; i<=hid->GetNbinsX(); i++){
     if(!opt.Contains("r",TString::kIgnoreCase)) m = Int_t(hid->Integral(1,i)); // eff < th
     else                         m = Int_t(hid->Integral(i, hid->GetNbinsX()));// eff > th
     probAndErr(m, n, prob, errProb, 0);
     hEff->SetBinContent(i, prob);
     hEff->SetBinError(i, errProb);
  }
  return hEff;
}

TH1F* SDAliases::suppressionFactor(TH1F* hid, TString opt, Int_t norm)
{// 6-jan-2003; 13-nov-05 - reverse case
  if(!(hid && hid->Integral())) return 0;
  printf("SDAlias::suppressionFactor : opt %s : norm %i\n", opt.Data(),norm);
  TH1F* hSup = (TH1F*)hid->Clone();
  AddToName(hSup, "Supp");
  hSup->Reset();
  hSup->Sumw2();
  Int_t m=0, n=Int_t(hid->GetEntries());
  if(norm>0) {
    printf("Will be normalised on #events %i : %i \n", norm, n);
    n = norm;
  }
  Double_t prob=0.0, errProb=0.0, sup=0.0, errSup;
  for(Int_t i=1; i<=hid->GetNbinsX(); i++){
     if(!opt.Contains("r",TString::kIgnoreCase)) m = Int_t(hid->Integral(1,i));
     else                                        m = Int_t(hid->Integral(i, hid->GetNbinsX()));
     probAndErr(m, n, prob, errProb, 0);
     if(m>0) {
        sup    = 1./prob;
        errSup = sup*(errProb/prob); // relative error is the same
        hSup->SetBinContent(i, sup);
        hSup->SetBinError(i, errSup);
     }
  }
  return hSup;
}

void SDAliases::probAndErr(int m, int n, double &prob, double& errProb, int print)
{
  // Calculattion for B.D.
  double dispProb;
  if(m<=n && n>0) {
    prob     = double(m)  / double(n);
    dispProb = prob*(1.-prob) / double(n); // D = p*(1.-p)
    errProb     = sqrt(dispProb);
    if(print) printf(" m %i n %i prob %f+/-%f(%5.2f%%)\n",
    m,n, prob, errProb, 100.*errProb/prob);
  }
  else printf("m=%i n=%i => m must be <= n !! \n", m, n); 
}

void SDAliases::suppressionAndErr(int m, int n, double &sup, double& errSup, int print)
{
  // Nov 21, 2007
  double prob, errProb;
  probAndErr(m, n, prob, errProb, 0);
  if(m>0) {
    sup    = 1./prob;
    errSup = sup*(errProb/prob); // relative error is the same
  } else {
    sup = 0.0;
    errSup = 0.0;
  }
  if(print) printf("m=%i n=%i => sup %f +/- %f \n", m, n, sup, errSup); 
}

// --
void SDAliases::divide(TArrayD *ar, const double c)
{
  if(c==0 || ar==0) return;
  for(int i=0; i<ar->GetSize(); i++) (*ar)[i] /= c;
}

void SDAliases::multiply(TArrayD *ar, const double c)
{
  if(ar==0) return;
  for(int i=0; i<ar->GetSize(); i++) (*ar)[i] *= c;
}

void SDAliases::multiply(int n, double *ar, const double c)
{
  if(n==0 || ar==0) return;
  for(int i=0; i<n; i++) ar[i] *= c;
}

void SDAliases::AddToName(TH1* hid, const char *addText)
{
  if(!(hid && strlen(addText))) return;
  TString name(hid->GetName());
  name += addText;
  hid->SetName(name.Data());
}

void SDAliases::AddToTitle(TH1* hid, const char *addText)
{
  if(!(hid && strlen(addText))) return;
  TString title(hid->GetTitle());
  title += addText;
  hid->SetTitle(title.Data());
}

void SDAliases::AddToNameAndTitle(TH1 *h, const char *name, const char *title)
{ // Mar 18, 2007; Feb 18,2011
  if(h==0) return;
  if(name  && strlen(name))  h->SetName(Form("%s%s",h->GetName(),name));
  if(title && strlen(title)) h->SetTitle(Form("%s %s",title,h->GetTitle()));
}

void SDAliases::AddToNameAndTitleToList(TList *l,const char *name,const char *title)
{ // Mar 18, 2007
  if(l==0) return;
  if(name || title) {
    for(int i=0; i<l->GetSize(); i++) {
      TObject *o = l->At(i);
      if(o->InheritsFrom("TH1")) {
        TH1 *h = (TH1*)o;
        AddToNameAndTitle(h, name, title);
      }
    }
  }
}

TList* SDAliases::SumHists(const char* ln, const char* lfs, const char* nout, const char* opt)
{ // 05-06-04 -> see SDAliases::Help()
  const int linesize = 255;
  char currentline[linesize];
  ifstream file(lfs,ios::in); // file with files names

  int iFile=0, iGoodFile=0;
  TList *lOfLAll=0, *lOfL=0;
  
  TH1::AddDirectory(kFALSE);
  while (1) { // cycle on files
    //    READ:
    file.getline(currentline, linesize);
    if (file.eof()) break;
    if(currentline[0] == '#') continue; //
    iFile++;
    printf("%3i th files will be opened -> %s \n", iFile, currentline);

    TFile f(currentline,"READ");
    if(!f.IsOpen()) continue;

    lOfL = (TList*)f.Get(ln);
    if(!lOfL)       continue;
    iGoodFile++;
    if(iGoodFile==1) {
      lOfLAll = (TList*)lOfL->Clone(lOfL->GetName());
      gROOT->GetListOfBrowsables()->Add(lOfLAll);
    } else {
      SDAliases::add(lOfLAll, lOfL);
    }
  }

  printf("########### %i opened files : %i good files ######## \n", iFile, iGoodFile);
  if(lOfLAll) SDAliases::saveListOfHists(lOfLAll,nout, kTRUE, opt);
  printf(" ########################################### \n");
  return lOfLAll;
}


int SDAliases::add(TList *l1, TList *l2)
{ 
  // Apr 11, 2009; recursive option
  // l1 and l2 - list of list
  if(l1==0 || l2==0) return 1;
  if(l1->GetSize() != l2->GetSize()) {
    l1->ls(); l2->ls(); return 2;
  }
  TH1 *h1=0, *h2=0;
  TObject *obj1=0, *obj2=0;
  for(int i=0; i<l1->GetSize(); i++) {
    obj1 = l1->At(i);
    obj2 = l2->At(i);
    if(obj1->InheritsFrom("TH1")&&obj1->InheritsFrom("TH1")) { // hists
      h1 = (TH1*)obj1;
      h2 = (TH1*)obj2;

      CompareHistsParameters(h1,h2);

      h1->Add(h2);
    } else if(obj1->InheritsFrom("TList")&&obj2->InheritsFrom("TList")){
      // Jan 14, 2009 - recursive possibility
      add(dynamic_cast<TList *>(obj1),dynamic_cast<TList *>(obj2));
    }
  }
  return 0;
}

int SDAliases::addOld(TList *l1, TList *l2)
{ // l1 and l2 - list of list
  if(l1==0 || l2==0) return 1;
  if(l1->GetSize() != l2->GetSize()) {
    l1->ls(); l2->ls(); return 2;
  }
  TH1 *h1=0, *h2=0;
  TObject *obj1=0, *obj2=0;
  for(int i=0; i<l1->GetSize(); i++) {
    obj1 = l1->At(i);
    obj2 = l2->At(i);
    if(obj1->InheritsFrom("TH1")&&obj1->InheritsFrom("TH1")) { // hists
      h1 = (TH1*)obj1;
      h2 = (TH1*)obj2;

      CompareHistsParameters(h1,h2);

      h1->Add(h2);
    }
    /*
    //    TList *lc1 = (TList*)l1->At(i);
    //    TList *lc2 = (TList*)l2->At(i);
    if(lc1==0 || lc2==0) continue;

    for(int ih=0; ih<lc1->GetSize(); ih++) {
      h1 = (TH1*)lc1->At(ih);
      h2 = (TH1*)lc2->FindObject(h1->GetName());
      h1->Add(h2);
    }
    */
  }
  l2->Clear();
  return 0;
}

void SDAliases::CompareHistsParameters(TH1 *h1, TH1 *h2)
{
  // May 10, 2007
  int keyErr=0;
  if(h1==0 || h2==0) return;
  if(h1->GetNbinsX() != h2->GetNbinsX()) {
    printf("<W> different number of bins %i -> %i \n", h1->GetNbinsX(), h2->GetNbinsX());
    keyErr++;
  }
  TAxis *xax1 = h1->GetXaxis(), *xax2 = h2->GetXaxis();
  if(xax1->GetXmin() != xax2->GetXmin()){
    printf("<W> different xmin %f : %f \n", xax1->GetXmin(),xax2->GetXmin());
    keyErr++;
  }
  if(xax1->GetXmax() != xax2->GetXmax()){
    printf("<W> different xmax %f : %f \n", xax1->GetXmax(),xax2->GetXmax());
    keyErr++;
  }
  if(keyErr) printf(" **** %s|%s : kerErr %i **** \n", h1->GetName(), h2->GetName(), keyErr);
}

TLatex *SDAliases::lat(const char *text, Float_t x,Float_t y, Int_t align, Float_t tsize, short tcolor)
{ // 12-mar-2003; 21-jan-05 - no correction for Logy
  double y1=y;
  //  if(gPad->GetLogy()) { // 1-jul - as in drawline
  //   y1 = TMath::Log10(y1);
  //   printf("Log scale %f -> %f\n", y, y1);
  //}
  TLatex *latex = new TLatex;
  latex->SetTextAlign(align);
  latex->SetTextSize(tsize);
  latex->SetTextColor(tcolor);
  latex->DrawLatex(x, y1, text);
  printf("<I> SDAlias::lat() -> x %f y %f tsize %f \n %s gPad->GetLogy() %i\n",
	 x,y,tsize, text, gPad->GetLogy());
  return latex;
}

TF1* SDAliases::expo(const char *addName, double xmi, double xma)
{
	TString name("expo");
	name += addName;
	TF1 *fun = new TF1(name.Data(), "[0]*TMath::Exp(-[1]*x)", xmi, xma);
	fun->SetLineColor(kRed);
	fun->SetParName(0,"A");
	fun->SetParName(1,"B");
	return fun;
}

TF1* SDAliases::gausi(const char *addName,double xmi,double xma,double N,double mean,double sig,double width)
{ // fit by gaus where first parameter is the number of events under gaus - see rHits.C on PDSF
  //printf("gausi : xmi %f -> xma %f \n", xmi,xma);
  //printf ("N %f :mean %f :sig %f : \n", N,mean,sig);
  TString name("gi");
  name += addName;
  TF1 *F = new TF1(name.Data(), gi, xmi, xma, 4); 
  F->SetParNames("INTEGRAL","MEAN","#sigma","WIDTH");

  F->SetParameter(0,N);
  F->SetParameter(1,mean);
  F->SetParameter(2,sig);

  F->FixParameter(3,width); // width of histogramm bin
  F->SetLineColor(kRed);
  return F;
}

TF1* SDAliases::gausi(const char *addName, double xmi, double xma, TH1 *h)
{
  if(h) return gausi(addName, xmi, xma, h->Integral(), h->GetMean(),h->GetRMS(), h->GetBinWidth(1));
  else  return 0; 
}

Double_t SDAliases::gi(Double_t *x, Double_t *par)
{ //  see rHits.C on PDSF
  // gaus(0) is a substitute for [0]*exp(-0.5*((x-[1])/[2])**2)

  static Double_t C = TMath::Sqrt(2.*TMath::Pi()), y=0.0, f=0.0; // sqrt(2.*pi)

  y  = (x[0]-par[1])/par[2];
  f  = par[0]*par[3]/(par[2]*C) * TMath::Exp(-0.5*y*y);

  return f;
}

TF1* SDAliases::fitLandauWithGaus(TH1 *h, const char *addName, double xmi, double xma,
double width, double MP, double area, double gSigma, double binWidth, int setParLimits)
{ // Apr 21, 2006
  /*
      The list of fit options is given in parameter option.
         option = "W"  Set all weights to 1 for non empty bins; ignore error bars
                = "WW" Set all weights to 1 including empty bins; ignore error bars
                = "I"  Use integral of function in bin instead of value at bin center
                = "L"  Use Loglikelihood method (default is chisquare method)
                = "LL" Use Loglikelihood method and bin contents are not integers)
                = "U"  Use a User specified fitting algorithm (via SetFCN)
                = "Q"  Quiet mode (minimum printing)
                = "V"  Verbose mode (default is between Q and V)
                = "E"  Perform better Errors estimation using Minos technique
                = "B"  Use this option when you want to fix one or more parameters
                       and the fitting function is like "gaus","expo","poln","landau".
                = "M"  More. Improve fit results
                = "R"  Use the Range specified in the function range
                = "N"  Do not store the graphics function, do not draw
                = "0"  Do not plot the result of the fit. By default the fitted function
                       is drawn unless the option"N" above is specified.
                = "+"  Add this new fitted function to the list of fitted functions
                       (by default, any previous function is deleted)
                = "C"  In case of linear fitting, don't calculate the chisquare
                       (saves time)
                = "F"  If fitting a polN, switch to minuit fitter

   */
  cout<<"fitLandauWithGaus : hist is "<<h<<" : "<<addName<<": x "<<xmi<<" -> "<<xma<<endl;
  if(h == 0) return 0;

  binWidth =  h->GetBinWidth(1);
  TF1 *f = SDAliases::landauWithGaus(addName,xmi,xma, width,MP,area,gSigma,binWidth, setParLimits);
  // 
  h->Fit(f, "N", "", xmi, xma);  // Simple fit
  h->Fit(f, "E+", "", xmi, xma); // Perform better Errors estimation using Minos technique

  return f;
}

TF1* SDAliases::landauWithGaus(const char *addName, double xmi, double xma,
double width, double MP, double area, double gSigma, double binWidth, int setParLimits)
{
  TString name("langaufun");
  if(addName) name += addName;
  int npar = 5;
  //  if(binWidth > 0.0) npar=5; 
  TF1 *fun = new TF1(name.Data(),langaufun, xmi, xma,npar);

  fun->SetParNames("Width","MP","Area","GSigma","binWidth");

  fun->SetParameter(0,width);
  fun->SetParameter(1,MP);
  fun->SetParameter(2,area);
  fun->SetParameter(3,gSigma);

  fun->FixParameter(4,binWidth);   // width of histogramm bin
 
  printf(" Initial paratmeters : width %f : MP %f : area : %f : gSigma %f binWidth %f\n",
	 width,MP,area, gSigma, binWidth);

  if(setParLimits==1) {
    fun->SetParLimits(0, width*0.5, width*1.5);
    fun->SetParLimits(1, MP*0.5, MP*1.5);
    fun->SetParLimits(2, area*0.3, area*2.);
    fun->SetParLimits(3, gSigma*0.1, MP*3.);
  } else if(setParLimits==2){ // all parameters shoul de positive
    for(int i=0; i<4; i++) fun->SetParLimits(i, 1.e-4, 1.+9);
  }
  fun->SetLineColor(kRed);
  return fun;
}

Double_t SDAliases::langaufun(Double_t *x, Double_t *par)
{
   // http://root.cern.ch/root/html/examples/langaus.C.html
   //Fit parameters:
   //par[0]=Width (scale) parameter of Landau density
   //par[1]=Most Probable (MP, location) parameter of Landau density
   //par[2]=Total area (integral -inf to inf, normalization constant)
   //par[3]=Width (sigma) of convoluted Gaussian function
   //par[4]=bin width 
   //
   //In the Landau distribution (represented by the CERNLIB approximation), 
   //the maximum is located at x=-0.22278298 with the location parameter=0.
   //This shift is corrected within this function, so that the actual
   //maximum is identical to the MP parameter.

      // Numeric constants
   static Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
   static Double_t mpshift  = -0.22278298;       // Landau maximum location

      // Control constants
   static Double_t np = 100.0;      // number of convolution steps
   static Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

      // Variables
   static Double_t xx;
   static Double_t mpc;
   static Double_t fland;
   static Double_t xlow,xupp;
   static Double_t step;
   static Double_t i;
   static Double_t sum = 0.0;

      // MP shift correction
      mpc = par[1] - mpshift * par[0]; 

      // Range of convolution integral
      xlow = x[0] - sc * par[3];
      xupp = x[0] + sc * par[3];

      step = (xupp-xlow) / np;

      // Convolution integral of Landau and Gaussian by sum
      sum = 0.0;
      for(i=1.0; i<=np/2; i++) {
         xx = xlow + (i-.5) * step;
         fland = TMath::Landau(xx,mpc,par[0]) / par[0];
         if(xx>0 && fland>0.0) { // Dec 11, 2007
           sum += fland * TMath::Gaus(x[0],xx,par[3]);
	 }
         xx = xupp - (i-.5) * step;
         fland = TMath::Landau(xx,mpc,par[0]) / par[0];
         if(xx>0 && fland>0.0) { // Dec 11, 2007
           sum += fland * TMath::Gaus(x[0],xx,par[3]);
	 }
      }

  return (par[2] * step * sum * invsq2pi / par[3])*par[4];
}

TF1* SDAliases::landauWithGausBg(const char *addName, double xmi, double xma,
				 double width, double MP, double area, double gSigma, 
                                 double binWidth, int setParLimits, int keyBg)
{ // Bg3 or 4 - Jun 16, 2006
  TString name("langauBG3");
  if(keyBg == 4)  name = "langauBG4";
  if(addName) name += addName;
  TF1 *fun = 0; 
  if(keyBg == 4) {
    fun = new TF1(name.Data(),langaufunPlusBg4, xmi, xma, 9);
  } else {
    fun = new TF1(name.Data(),langaufunPlusBg3, xmi, xma, 9);
  }
  //  fun->SetParNames("Width","MP","Area","GSigma","binWidth","expc","expx","expx2");
  fun->SetParNames("Width","MP","Area","GSigma","binWidth","a","b","c","d","e");

  fun->SetParameter(0,width);
  fun->SetParameter(1,MP);
  fun->SetParameter(2,area);
  fun->SetParameter(3,gSigma);

  fun->FixParameter(4,binWidth);   // width of histogramm bin
  // bg
  fun->SetParameter(5,0.1);
  fun->SetParameter(6,0.1);
  fun->SetParameter(7,-10.);

  printf(" Initial paratmeters : width %f : MP %f : area : %f : gSigma %f \n",
	 width,MP,area, gSigma);

  if(setParLimits) {
    fun->SetParLimits(0, width*0.5, width*1.5);
    fun->SetParLimits(1, MP*0.5, MP*1.5);
    fun->SetParLimits(2, area*0.3, area*2.);
    fun->SetParLimits(3, gSigma*0.1, MP*3.);
  }
  return fun;
}

Double_t SDAliases::langaufunPlusBg3(Double_t *x, Double_t *par)
{ // Mar 25, 2006; Jun 16, 2006
  // pars 0->4 are for langaufun; 5-9 for bg3 
  static double bg=0.0, langau=0.0;
  //  y      = x[0] - 5; // for adc
  langau = langaufun(x,par);
  bg     = bg3(x, (par+5));
  return (langau+bg);
}

Double_t SDAliases::langaufunPlusBg4(Double_t *x, Double_t *par)
{ // Mar 25, 2006; Jun 16, 2006
  // pars 0->4 are for langaufun; 5-9 for bg4 
  static double bg=0.0, langau=0.0;
  //  y      = x[0] - 5; // for adc
  langau = langaufun(x,par);
  bg     = bg4(x, (par+5));
  return (langau+bg);
}

TF1* SDAliases::landauWithGausBgGaus(const char *addName, double xmi, double xma, double width, double MP,
                                     double area, double gSigma, double binWidth, double pbeam, double sig,
                                     int setParLimits)
{
  TString name("langauBg3Gaus");
  if(addName) name += addName;
  TF1 *fun = new TF1(name.Data(),langaufunPlusBgPlusGaus, xmi, xma, 11);

  fun->SetParNames("Width","MP","Area","GSigma","binWidth","expc","expx","expx2", "N", "pbeam", "sig");

  fun->SetParameter(0,width);
  fun->SetParameter(1,MP);
  fun->SetParameter(2,area);
  fun->SetParameter(3,gSigma);

  fun->FixParameter(4,binWidth);   // width of histogramm bin
  // bg
  fun->SetParameter(5,1.);
  fun->SetParameter(6,1.);
  fun->SetParameter(7,1.);
  //gaus
  fun->SetParameter(8,1000.);
  fun->SetParameter(9,pbeam);
  fun->SetParameter(10,sig);

  printf(" Initial paratmeters : width %f : MP %f : area : %f : gSigma %f : pbeam %f : sig %f\n",
	 width,MP,area, gSigma, pbeam, sig);

  if(setParLimits) {
    fun->SetParLimits(0, width*0.5, width*1.5);
    fun->SetParLimits(1, MP*0.5, MP*1.5);
    fun->SetParLimits(2, area*0.3, area*2.);
    fun->SetParLimits(3, gSigma*0.1, MP*3.);
  }
  return fun;
}

Double_t SDAliases::langaufunPlusBgPlusGaus(Double_t *x, Double_t *par)
{ // Mar 20, 2006
  // pars 0->4 are for langaufun; 5-7 for bg ; 8-10 for gaus  
  static double bg=0.0, langau=0.0, gaus=0.0;

  langau = langaufun(x,par);

  bg     = exp(par[5] + par[6]*x[0] + par[7]*x[0]*x[0]);

  gaus   = par[8]*TMath::Gaus(x[0], par[9], par[10], kTRUE); 

  return (langau + bg + gaus);
}

// Back ground function - May 25, 2006
Double_t SDAliases::bg1(Double_t *x, Double_t *par)
{ // 3 pars; pol2(0)
  static double bg1=0.0;
  bg1 = par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
  return bg1;  
}

Double_t SDAliases::bg2(Double_t *x, Double_t *par)
{ // 3 pars; exp(pol2(0))
  static double bg2=0.0;
  bg2 = exp(par[0] + par[1]*x[0] + par[2]*x[0]*x[0]);
  return bg2;  
}

Double_t SDAliases::bg3(Double_t *x, Double_t *par)
{ // 5 pars;   pol2(0) * expo(3)
  static double bg3=0.0;
  bg3 = (par[0] + par[1]*x[0] + par[2]*x[0]*x[0]) * exp(par[3]*x[0]);
  return bg3;  
}

Double_t SDAliases::bg4(Double_t *x, Double_t *par)
{ // 5 pars;   expo(pol3)
  static double bg4=0.0;
  bg4 = exp(par[0] + par[1]*x[0] + par[2]*x[0]*x[0] + par[3]*x[0]*x[0]*x[0]);
  return bg4;  
}

TF1* SDAliases::fbg2(const char *addName, double xmi, double xma)
{
  TString name("bg2");
  name += addName;
  TF1 *F = new TF1(name.Data(), gi, xmi, xma, 5); 
  F->SetParNames("a0","a1","expb0","expb1","expb2");

  return F;
}

// load libraries
void SDAliases::loadPythiaStaff()
{// 12-jul-2002 - /home/pavlinov/cosmic/pythia/starfast/jetAna.C
  if(gSystem->Load("$PYTHIA/libPythia6.so") < 0){
     gSystem->Load("/home/local/ROOT/pythia6220/libPythia6.so");
  }
  gSystem->Load("libEG.so");
  gSystem->Load("libEGPythia6.so");
}

void SDAliases::loadLibraries(const char *opt, const char *libList, const char* sep)
{// see tInfo.C - 25-jul-2002
  TString ll(libList), lib;
  int startIndex=0, lenLL = ll.Length(), index=0, iLib=0;
  bool endLoop = kFALSE;
  if(lenLL==0 || strlen(sep)==0) goto END;

 NEXT:
  index = ll.Index(sep, startIndex);
  if (index>startIndex && index<lenLL) {
     lib = ll(startIndex, index - startIndex);
     iLib++;
     startIndex = index + 1; // !!!
  } else if (index==-1) { // single or lasrary 
     lib = ll(startIndex, lenLL - startIndex + 1);
     iLib++;
     endLoop = kTRUE;
  }
  if (lib.Length()){
     lib.ReplaceAll(" ","");   // delete space
     gSystem->ExpandPathName(lib);
     if (lib.Contains(".so")) { // will be loaded .so  
        printf(" %i library  %s  will be loaded \n", iLib, lib.Data());
	gSystem->Load(lib.Data());
     }  else { 
        if (!lib.Contains(".cxx") && !lib.Contains(".C")) { // check on full names 
	   lib += ".cxx";
        }
        printf(" %i library will be compiled from -> %s\n", iLib, lib.Data());
        TString cmd(".L ");
        cmd += lib;
        cmd += "+";
        if(strlen(opt)) cmd += "+"; // obligatory recompiling
        gROOT->ProcessLine(cmd.Data());
     }
     if(!endLoop) goto NEXT;
  }
 END:
  if (iLib==0) {
    printf(" No libraries : libList %s : sep %s\n", libList, sep);
  }
}

void SDAliases::loadAliceAnalysisLibraries()
{
  // Mar 30,2010 - analysis of ALICe data (LHC got 7 TeV beam)
  if(strcmp(gProgName,"aliroot")) {
    printf("<E> It is not aliroot!\n");
    return;
  }
  TString libs("libANALYSIS.so,libANALYSISalice.so,libANALYSIScalib.so,libPAIANA.so"); // $ALICE_ROOT/ANALYSIS"
  loadLibraries("", libs.Data());
}


//void SDAliases::

TVector3* SDAliases::vecFromMagEtaPhi(TVector3* v, double mag, double eta, double phi)
{ // 30-jan-2002 => see vec3 in cosmic/pythia/jetResolution.C
  // 12-jmay-2003 for old version of root
  static double px, py, pz, pr;
  TVector3 *vw = 0;

  if(!v) vw = new TVector3();
  else   vw = v;

  pz = mag * TMath::TanH(eta);  // tanh(eta) = cos(theta)
  pr = sqrt(mag*mag - pz*pz);
  px = pr * TMath::Cos(phi);
  py = pr * TMath::Sin(phi);

  vw->SetXYZ(px, py, pz);
  return vw; 
}

double  SDAliases::thetaToEta(double theta, int mode)
{ // mode%10=0 , theta in radian; mode%10=1 , theta in degree
  // mode >10 - print
  // theta change from 0 to pi now !!
  static double eta=0., thetaDegree=0.;
  static int kpri=0;

  kpri = mode/10;

  if(mode%10 == 1) {
    thetaDegree = theta; 
    theta      /= (180./TMath::Pi()); 
  } else {
    thetaDegree = theta*(180./TMath::Pi()); 
  }
  if(theta>1.e-10 && theta <TMath::Pi()-1.e-10) {
    eta = -TMath::Log(TMath::Tan(theta/2.));
  } else {
    eta  = 1.e+10; // may be include sign
    kpri = -1;
  }

  if(kpri) {
    if(kpri<0) printf("Something wrong with theta !!!\n");
    printf(" eta = -TMath::Log(TMath::Tan(theta/2.)) \n");
    printf(" theta %9.6f(%7.2f) -> eta %9.6f\n", 
	   theta, thetaDegree, eta);
  }
  return eta;
}

int SDAliases::ParseString(const TString &tChain, TObjArray &Opt)
{ // Feb 06, 2006
  Ssiz_t begin, index, end, end2;
  begin = index = end = end2 = 0;
  TRegexp separator("[^ ;,\\t\\s/]+");
  while ( (begin < tChain.Length()) && (index != kNPOS) ) {
    // loop over given Chain options
    index = tChain.Index(separator,&end,begin);
    if (index >= 0 && end >= 1) {
      TString substring(tChain(index,end));
      Opt.Add(new TObjString(substring.Data()));
    }
    begin += end+1;
  }
  return Opt.GetEntries();  
}

void SDAliases::DrawKickAngle()
{
  // p = 0.03*B*r; B in kgs, r in m, p = gev/c
  // alpha = 2.*arcsin(remc/(2.*r)), where r=radius of track in constant
  // magnetic field.
  double B=5., remc=4.25;

  TFormula r("r",Form("x/(0.03*%3.1f)",B)); // x is momentum 
  TF1 *rr    = new TF1("rr", "r", 0.0, 10.);
  TF1 *ralpha = new TF1("alpha",Form("2.*asin(%4.2f/(2.*r))*TMath::RadToDeg()",remc),
  0.15*remc/2.-0.001,10.);
  ralpha->SetNpx(500);
  TF1 *beta  = new TF1("beta",Form("90.-asin(%4.2f/(2.*r))*TMath::RadToDeg()",remc), 
  0.5,10.5);
  TF1 *sbeta  = new TF1("sbeta",Form("cos(asin(%4.2f/(2.*r)))",remc), 
  0.5,10.5);

  TCanvas *c = new TCanvas("cKick","cKick",10,10, 800, 600);
  c->Divide(2,2);

  c->cd(1);
  rr->Draw();
  TH1 *hrr = rr->GetHistogram();
  hrr->SetXTitle(" p GeV/C   ");
  hrr->SetYTitle(" r m   ");

  c->cd(2);
  ralpha->Draw();
  TH1 *hralpha = ralpha->GetHistogram();
  hralpha->SetXTitle(" p GeV/C   ");
  hralpha->SetYTitle(" #alpha   ");

  c->cd(3);
  beta->Draw();
  TH1 *hbeta = beta->GetHistogram();
  hbeta->SetXTitle(" p GeV/C   ");
  hbeta->SetYTitle(" #beta   ");

  c->cd(4);
  sbeta->Draw();
  TH1 *hsbeta = sbeta->GetHistogram();
  hsbeta->SetXTitle(" p GeV/C   ");
  hsbeta->SetYTitle(" sin(#beta)   ");

  c->Update();
}

void SDAliases::DrawElectronPositronAngle()
{
  TF1* fangle    = new TF1("fangle","0.511/x",1.,11.); // in mrad, x-momentum in GeV
  TF1* fangleDeg = new TF1("fangleDeg","0.511/(1000.*x)*TMath::RadToDeg()",1.,11.); // in degree, x-momentum in GeV
  TCanvas *c = new TCanvas("cKick","cKick",10,10, 800, 600);
  c->Divide(1,2);
 
  c->cd(1);
  fangle->Draw();
  TH1* h = fangle->GetHistogram();
  h->GetXaxis()->SetTitle(" P GeV");
  h->GetYaxis()->SetTitle(" Angle mrad");

  c->cd(2);
  fangleDeg->Draw();
  h = fangleDeg->GetHistogram();
  h->GetXaxis()->SetTitle(" P GeV");
  h->GetYaxis()->SetTitle(" Angle degree");

  c->Update();

  for(Int_t i=0; i<=10; i++){
    Double_t p = 1. + 1.*i;
    printf(" p %5.1f(GeV) angle %5.4f(mrad) %6.5f(degree)\n",
	   p, fangle->Eval(p), fangleDeg->Eval(p));
  }
}

double  SDAliases::etaToTheta(double eta, int mode)
{
  static double theta, thetaDegree;
  static int kpri;

  kpri = mode/10;

  if(TMath::Abs(eta) < 1.e+10) {
     theta = 2.*TMath::ATan(TMath::Exp(-eta));
  } else {
     kpri  = -1;
     theta = -1000.;
  }
  thetaDegree = theta*(180./TMath::Pi()); 

  if(kpri) {
    if(kpri<0) printf("Something wrong with eta !!!\n");
    printf(" theta = 2.*TMath::ATan(TMath::exp(-eta)) \n");
    printf(" eta %9.6f -> theta %9.6f(%9.2f)\n", 
	     eta, theta, thetaDegree);
  }
  if(mode%10==1) return thetaDegree;
  else           return theta;
}

void SDAliases::cbar()
{
  static TControlBar *mybar=0;
  if(!mybar) {
    mybar = new TControlBar("vertical","PAI CBar (SDAlias)");

    mybar->AddButton(" Standard browser ","new TBrowser(\"browser\",\"StandardBr\")",
    		     "new TBrowser(\"browser\",\"StandardBr\",300,200)");

    mybar->AddButton(" Small browser ","new TBrowser(\"browser\",\"SmallBr\",150,150)",
		     "new TBrowser(\"browser\",\"SmallBr\",150,150)");
    // Mar 6, 2007
    mybar->AddButton("TCanvas 1","new TCanvas(\"Canvas1\",\"Canvas1\",50,50,400,300)",
		     "new TCanvas(\"Canvas1\",\"Canvas1\",50,50,400,300)");

    //mybar->AddButton(" Load  mylib.so ","gSystem.Load(\"mylib.so\")",
    //"gSystem.Load(\"mylib.so\")");

    mybar->AddButton("  P r i n t  E n v i r o n m e n t ","gEnv->Print()",
    "gEnv->Print()");

    mybar->AddButton("L i s t  of gROOT ","gROOT->ls()","gROOT->ls()");

    mybar->AddButton("List  of  Objects","gObjectTable->Print(); printf(\" \\n gObjectTable->Print() \\n \");",
    "See ROOT Overview => p 53 ");

    mybar->AddButton("List  of  Classes","gClassTable->Print()",
    "See ROOT Overview => p 53 ");

    mybar->AddButton("List  of Libraries",
    "gSystem->ListLibraries(); printf(\"gSystem->ListLibraries() \\n \"); ",
    "List of load libraries ");

    mybar->AddButton(" Create canvas "," SDAlias::canvas(\"c1\",\"CPAI1\")",
    "SDAlias::canvas(\"c1\",\"CPAI1\")");

    mybar->AddButton("Hists to gROOT",
    "gROOT->cd(); TH1::AddDirectory(kTRUE); printf(\"TH1::AddDirectory(kTRUE) \\n \");",
    "Put new hists to ROOT memory");
    // 19-oct-05
    mybar->AddButton("gStyle->SetOptStat(111111)",
    "gStyle->SetOptStat(1111111); printf(\"gStyle->SetOptStat(111111) \\n \");",
    "Change default behavior of OptStat");
    // 20-nov-05
    mybar->AddButton("gStyle->SetOptFit(111111)",
    "gStyle->SetOptFit(111); printf(\"gStyle->SetOptFit(111) \\n \");",
    "Change default behavior of OptFit");

    // for ALICE only
    if(strcmp(gProgName,"aliroot")==0) {
      // Mar 30, 2010 - new staff
      if(1) {
      mybar->AddButton("loadAliceAnalysisLibraries()", "SDAlias::loadAliceAnalysisLibraries()",
      "SDAlias::loadAliceAnalysisLibraries()");
      }
      if(0) {
      mybar->AddButton("G3GUI", ".x $ALICEC/geant3/TGeant3/G3GUI.C",
      ".x $ALICEC/geant3/TGeant3/G3GUI.C");
      // Apr 27, 2007
      mybar->AddButton(".L rHits.C+", ".L /home/pavlinov/ALICE/SHISHKEBAB/rHits.C+",
      ".L /home/pavlinov/ALICE/SHISHKEBAB/rHits.C+");
      mybar->AddButton(".L clust.C", ".L /home/pavlinov/ALICE/SHISHKEBAB/clust.C",
      ".L /home/pavlinov/ALICE/SHISHKEBAB/clust.C");
      mybar->AddButton("readAllLists()", "readAllLists();", "readAllLists();");
      // May 11, 2007
      mybar->AddButton(".x sumOfRpHists.C", ".x sumOfRpHists.C", 
      "see $HOME/macros/ALICE");
      }
      // Apr 2009 - IHEP,Russia
      if(0) {
        TString cmd = "gROOT->ProcessLine(\".L /home/pavlinov/PHOS/LED/classLed.cxx+\");";
        cmd += "gROOT->ProcessLine(\".L /home/pavlinov/PHOS/FIT/testFit.cxx+\");";
        cmd += "gROOT->ProcessLine(\".L /home/pavlinov/PHOS/LED/LEDN.C+\")";
        mybar->AddButton("Load all libs for FAF", cmd.Data(),"All libs for FAF");
      //

        mybar->AddButton(".L classLed.cxx+", ".L /home/pavlinov/PHOS/LED/classLed.cxx+",
        ".L /home/pavlinov/PHOS/LED/classLed.cxx+");
        mybar->AddButton(".L testFit.cxx+", ".L /home/pavlinov/PHOS/FIT/testFit.cxx+",
        ".L /home/pavlinov/PHOS/FIT/testFit.cxx+");
        mybar->AddButton(".L LEDN.C+", ".L /home/pavlinov/PHOS/LED/LEDN.C+",
        ".L /home/pavlinov/PHOS/LED/LEDN.C+");
      }
      //      mybar->AddButton("Load libPAIANA.so", "gROOT->GetListOfBrowsables()->Add(new LEDN(\"LEDN\"))",
      //      "load libPAIANA.so");
    } else { // introduce dependence from alice libraries
      //AliCaloMipPict *pict = new AliCaloMipPict("CaloPict");
      //new TBrowser;
    }

    //    mybar->AddButton(" Quit  from  R O O T ",".q",".q");

    mybar->Show();
  }
  // Feb 12, 2007
  SDAliases *pai = new SDAliases;
  gROOT->GetListOfBrowsables()->Add(pai);
  // Dec 12, 05 - predefined some function
  printf("<I> Pavlinov's Control Bar was started \n");
  TF1 *f[3];
  f[0] = new TF1("gc", "gaus(0)+pol0(3)", 0., 50.);
  f[1] = new TF1("gp1","gaus(0)+pol1(3)", 0., 50.);
  f[2] = new TF1("gp2","gaus(0)+pol2(3)", 0., 50.);
  for(int i=0;i<3;i++) {
    f[i]->SetParameter(0,1000.);
    f[i]->SetParameter(1,0.303);
    f[i]->SetParameter(2,0.0334);

    f[i]->SetParLimits(0,10.,1.e+8);
    f[i]->SetParLimits(2, 0.0001, 50.);
  }
//  Dec 23, 2006
// have to tune parameters
  TF1 *gi = SDAliases::gausi("", 0.2, 0.5, 1., 0.25, 0.007, 0.001);
  if(gi) {};  
}

void SDAliases::setGraphAttr(TGraph *gr,int lCol,int lWidth,int mkStyle, int mkCol,int mkSize)
{
  if(!gr) {
    cout<<"<W> SDAlias::setGraphAttr : gr undefined " << gr << cout;
    return;
  }
  gr->SetLineColor(lCol);
  gr->SetLineWidth(lWidth);
  gr->SetMarkerStyle(mkStyle);
  gr->SetMarkerColor(mkCol);
  gr->SetMarkerSize(Size_t(mkSize));
}

Int_t SDAliases::getDateStamp(int day, int year)
{
  int dm[12]={31,28,31,30,31,30,31,31,30,31,30,31}, maxDay=365; 
  int month=0, dInMonth=0, date=0;

  if(year%4==0) {dm[1]=29; maxDay=366;}
  if(day<1 || day>maxDay) {
    printf("<E> day(%i) must be positive and less then %i for year %i/n", day, maxDay, year);
    return 0;
  }

  dInMonth = day;
  for(int i=0; i<12; i++){
    if(dInMonth>dm[i]) dInMonth -= dm[i];
    else {
      month = i + 1;
      break;
    }
  }
  date = year*10000 + month*100 + dInMonth;
  printf(" day %i year %i : month %i dInMonth %i : date %i\n", day,year, month,dInMonth, date);
  return date;
}

Double_t SDAliases::EnergyCorrectionPol2(Double_t eRec)
{ 
  // Correction to rec.energy - Jul 15, 2007
  // E(gamma) = corr * E(rec); corr = p0 + p1*eRec + p2*eRec*eRec
  /*
    Fitting results:
    Parameters:
    NO.             VALUE           ERROR
    0       9.918060e-01    3.126907e-04
    1       4.121968e-04    1.819830e-05
    2       -2.131218e-06   1.694250e-07
  */
  static Double_t p0=9.918060e-01, p1=4.121968e-04, p2=-2.131218e-06;
  static Double_t corr = 0.0;
  corr = 1.0;
  if(eRec>=0.0 && eRec <=105.) {
    corr = p0 + p1*eRec + p2*eRec*eRec;
  }
  return corr;
} 

//
// ========================= HTML =======================================================
//
void SDAliases::AddTextFromFunction(TString &text, TF1 *f, const Char_t* name)
{
  if(f==0) return;
  text += Form("<br> &nbsp; N(%s)      %7.0f +/- %5.0f\n", name, f->GetParameter(0), f->GetParError(0));
  text += Form("<br> &nbsp; %s    %7.3f +/- %5.3f MeV\n",  name, f->GetParameter(1), f->GetParError(1));
  text += Form("<br> &nbsp; sig    %7.3f +/- %5.3f MeV\n", f->GetParameter(2), f->GetParError(2));
}

void SDAliases::CreateHtmlHead(FILE *io, const Char_t *title)
{
 fprintf(io, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html>\n");
 fprintf(io, " <head>\n");
 fprintf(io, " <link rel=\"shortcut icon\" href=\"http://rhic.physics.wayne.edu/~pavlinov/favicon.ico\"/>\n");
 fprintf(io, "  <link rel=\"stylesheet\" type=\"text/css\" href=\"http://rhic.physics.wayne.edu/~pavlinov/CSS/pai.css\" /> \n");
 fprintf(io, "<title>%s</title>\n",title); 
 fprintf(io, "</head>\n");

 fprintf(io, " <body class=border> \n");
 fprintf(io, " <h1>%s</h1> \n", title);
}

void SDAliases::CreateHtmlEnd(FILE *io)
{
  fprintf(io,"\n\n<hr>\n");
  //  fprintf(io,"  <address><a href=\"mailto:pavlinov@ihep.ru\"> &copy; &nbsp; Alexei Pavlinov</a></address>\n");
  fprintf(io,"  <address><a href=\"mailto:pavlinovalexei@gmail.com\"> &copy; &nbsp; Alexei Pavlinov</a></address>\n");
  TDatime dt;
  fprintf(io,"<!-- Created: %s -->\n", dt.AsString());
  fprintf(io,"<!-- hhmts start -->\n");
  fprintf(io,"Last modified: %s \n",dt.AsString());
  fprintf(io,"<!-- hhmts end -->\n");
  fprintf(io," </body>\n");
  fprintf(io,"</html>\n");
}

void SDAliases::CreateLink(FILE *io, const Char_t *name, const Char_t *text, const int br)
{
  // Jul 09, 09
  if(io && strlen(name)) {
    fprintf(io, "<a href=\"%s\">%s</a> <br> \n",name, text);
    if(br>0) {
      for(Int_t i=0; i<br;i++) fprintf(io, "<br>");
      fprintf(io, "\n");
    }
  }
}

void SDAliases::CreatePictureLink(FILE *io, const Char_t *pictName, const Char_t *pictText)
{
  // Jul 09, 09
  if(io && strlen(pictName)) {
    fprintf(io, "<a href=\"%s\">\n",pictName);
    fprintf(io, " <img SRC=\"%s\" Height=120 width=130></a>\n", pictName);
    if(pictText) {
      fprintf(io, " <br>\n %s <br><br>", pictText);
    }
  }
}
//
// Table
//
void SDAliases::CreateTableHead(FILE *io, const Char_t* textAlign)
{
  // Jul 31,09
  fprintf(io,"\n\n<hr>\n");
  fprintf(io,"<table style=\"text-align: %s;\" border=\"1\"",textAlign);
  fprintf(io,"cellpadding=\"2\" cellspacing=\"2\"> \n");
  fprintf(io,"<tbody> \n <tr> \n");
}

void SDAliases::CreateTableEnd(FILE *io, const Char_t* textFoot)
{
  fprintf(io," </tr> \n</tbody> \n");
  if(strlen(textFoot)) {
    fprintf(io,"<TFOOT>\n");
    fprintf(io,"<TR> <TH ALIGN=CENTER >%s</TH> </TR>\n",textFoot);
    fprintf(io,"</TFOOT>\n");
  }
  fprintf(io,"</table> \n");
}

void  SDAliases::CreateTableTd(FILE *io, const Char_t* textAlign,
			     const Char_t* href, const Char_t* text)
{
  fprintf(io,"<td style=\"text-align: %s; vertical-align: top;\">\n",  textAlign);
  fprintf(io,"<big>\n");
  TString sref(href);
  if(sref.Contains(".gif")) {
    CreatePictureLink(io,href, text);
  } else {
    fprintf(io,"<a href=\"%s\">%s</a>\n", href, text);
  }
  fprintf(io,"</big>\n");
  fprintf(io,"</td>\n");
}
