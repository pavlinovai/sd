/*
 * SDClusterFinderBase.cpp
 *
 *  Created on: May 17, July 24, 2012;
 *      Author: pavlinov
 */

#include "SDClusterFinderBase.h"
#include "sdpixel.h"
#include "SDCluster.h"
#include "SDCluster2D.h"

ClassImp(SDClusterFinderBase)

Int_t SDClusterFinderBase::FindClsutersWithThreads()
{
   cout<<"<I> You should implement FindClsutersWithThreads() : Apr 14,2013\n";
   return 0;
}

#ifndef WIN32
Int_t SDClusterFinderBase::FindClsutersWithPosixThreads()
{
   cout<<"<I> You should implement FindClsutersWithPosixThreads() : Apr 20,2013\n";
   return 0;
}
#endif

Bool_t SDClusterFinderBase::ArePixelsNeighbours(const pixel & p1, const pixel & p2)
{
	// June 1, 2013 -> more fast version
	// Default: pixels have common vertex
	// p2 is from cluster, p1 - candidate to cluster
	// cout<<" SDClusterFinderBase::ArePixelsNeighbours p1 "<<p1<<" p2 "<<p2<<endl;
	Int_t dif = p1.col - p2.col;
	if(TMath::Abs(dif)>1) return kFALSE;
	dif = p1.row - p2.row;
	if(TMath::Abs(dif)>1) return kFALSE;
	return kTRUE;
}

Bool_t SDClusterFinderBase::IsPixelNeighbourToCluster(const pixel &p, SDClusterBase* clb)
{
	//	  for(Int_t i=0; i<cl.GetNRows(); ++i) {
	// Cycle in reverse directions - best speeding performance
	SDCluster *cl = dynamic_cast<SDCluster *> (clb);
	if (cl) {
		for (Int_t i = (cl->GetClusterSize() - 1); i >= 0; --i) {
			if (ArePixelsNeighbours(p, *(clb->GetPixel(i))))
				return kTRUE;
		}
		return kFALSE;
	} else {
		SDCluster2D *cl2d = dynamic_cast<SDCluster2D *> (clb);
		vrowpx& rows = cl2d->GetRows();
		UInt_t row1 = p.row-1, row2 = p.row+1, rmax = rows.size()-1;
		if(row1<0) {
			row1 = 0;
			row2 = 1;
		} else if (row2>rmax){
			row2 = rmax;
			row1 = rmax - 1;
		}
		for(UInt_t r=row1; r<=row2; ++r){
			setpx* spx   = rows[r];
			if(spx ==0 ) continue;

			UInt_t nspx = spx->size();
			if(ArePixelsNeighbours(p, *spx->begin())) return kTRUE;
			else if (nspx==1)                        continue; // Just one pixel
			if(ArePixelsNeighbours(p, *spx->rbegin()))   return kTRUE;
			else if (nspx==2)                        continue; // Just two pixels
            setpx::iterator it, it1 = ++(spx->begin()), it2 = --(spx->end());
            for(it=it1; it!=it2; ++it) {
            	if(ArePixelsNeighbours(p, *it)) return kTRUE;
            }
		}
		return kFALSE;
	}
}
