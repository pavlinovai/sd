/*
 * SDCluster2D.cxx
 *
 *  Created on: Nov 15, 2012
 *      Author: Alexei Pavlinov
 */

#include "SDCluster2D.h"
#include "sdClusterChar.h"

#include <TArrayD.h>
#include <TPolyLine.h>

ClassImp(SDCluster2D)

SDCluster2D::SDCluster2D() : TDataSet(), fNClusterSize(0), fRows(), fPolyLine(0), fDebug(0)
{ }

SDCluster2D::SDCluster2D(const char* name, const UInt_t nrow, const char* tit) : TDataSet(name,tit),
		fNClusterSize(0), fRows(), fPolyLine(0), fDebug(0)
{
	fRows.resize(nrow);
	vrowpx::iterator it;
	for(it=fRows.begin(); it!= fRows.end(); it++) *it = 0;
}

SDCluster2D::~SDCluster2D()
{
	// 2013: Feb 24;
	delete fPolyLine;
}

SDClusterInfo* SDCluster2D::GetClusterInfo() const
{
	return dynamic_cast<SDClusterInfo *> (GetParent());
}

void SDCluster2D::AddPixel(pixel &p)
{
	sdClusterChar& clstChar = GetClusterInfo()->GetClstCharFromF0();

	if (GetClusterSize() == 0) {
		clstChar.colmin = clstChar.colmax = p.col;
		clstChar.rowmin = clstChar.rowmax = p.row;
		clstChar.maxamp = p.amp;
	} else {
		clstChar.colmin = clstChar.colmin > p.col ? p.col : clstChar.colmin;
		clstChar.colmax = clstChar.colmax < p.col ? p.col : clstChar.colmax;
		clstChar.rowmin = clstChar.rowmin > p.row ? p.row : clstChar.rowmin;
		clstChar.rowmax = clstChar.rowmax < p.row ? p.row : clstChar.rowmax;
		if(clstChar.maxamp < p.amp) clstChar.maxamp = p.amp;
	}
    if(fRows[p.row]==0) fRows[p.row] = new setpx;
	fRows[p.row]->insert(p);
	++fNClusterSize;
	clstChar.numpixels = GetClusterSize();
}

pixel*  SDCluster2D::GetPixel(UInt_t ind)
{
    if (ind > fNClusterSize - 1)
		return 0; // out of array

	UInt_t indLocal = ind;
	setpx::iterator it;
	for (UInt_t row = 0; row < fRows.size(); ++row) {
		setpx* s = fRows[row];
		if (s == 0) continue;

		if (indLocal < s->size()) {
			it = s->begin();
			if (indLocal) for (UInt_t i = 0; i < indLocal; ++i) ++it;
			return const_cast<pixel *> (&(*it));
		} else {
			indLocal -= s->size();
		}
	}
	return 0;
}

pixel* SDCluster2D::GetPixel(UInt_t col, UInt_t row)
{
   setpx* s = fRows[row];
   if(s==0) return 0;

   setpx::iterator it;
   for(it=s->begin(); it!=s->end(); ++it){
	   if(it->col == col) return const_cast<pixel *>(&(*it));
   }
   return 0;
}

void SDCluster2D::PrintClusterInfo(const int pri) const
{
  // Nov 16,2012
	cout << GetName() << " fNClusterSize " << fNClusterSize << " fRows.size() "<<fRows.size()<<endl;
	cout<<" clch "<<GetClusterInfo()->GetClstCharFromF0()<<endl;
	if (fNClusterSize == 0 || pri == 0)
		return;

	setpx::iterator it;
	UInt_t nCheck=0;
	cout.precision(2);
	for (UInt_t row = 0; row < fRows.size(); ++row) {
		setpx* s = fRows[row];
		if(s==0) continue;
		if(pri>=2) cout << row << " s " << s <<endl;

		if(pri>=1) cout << " **\t  Row " << row << " #pxls " << s->size() << endl;
	    nCheck += s->size();
		UInt_t ic = 0;
		if (pri <= 1) continue;

		for (it = s->begin(); it != s->end(); ++it) {
			if(pri>=2) {
				cout << " c " << (*it).col << " a " << cout.width(4)<<(*it).amp;
#ifdef NEIGHBORS
				if(pri>=3) cout<<" n "<<(*it).neighbors;
#endif
			   ++ic;
			   if (ic == 5) {
				  ic = 0;
				  if(pri>=2)cout << endl;
			   }
			}
		}
		if (ic > 0 && pri>=2) cout << endl;
	}
	cout << GetName() << " fNClusterSize " << fNClusterSize << " nCheck "<< nCheck<<endl;
}

Int_t SDCluster2D::Compare(const SDClusterBase *clb) const
{
	// Method using for sorting in TList only
	// clb should be SDClusterBase type
	// Nov 16,2012
	const SDClusterBase *clst = dynamic_cast<const SDClusterBase *> (clb);
	int diff = this->GetClusterSize() - clst->GetClusterSize();
	diff = diff < 0 ? -1 : diff;
	diff = diff > 0 ? 1 : 0;
	return diff;
}

Bool_t SDCluster2D::IsNormalCluster() const
{
	return GetClusterInfo()->GetClstCharFromF0().IsNormalCluster();
}

TPolyLine* SDCluster2D::CreatePolyline()
{
	// Real Polyline : Nov 17,2012
	sdClusterChar clch = GetClusterInfo()->GetClstCharFromF0();
	if(fDebug) {
		cout<<" clch "<<clch<<endl;
		PrintClusterInfo(fDebug);
	}
	UInt_t rowstep = 8, offset = 2;
	UInt_t lineWidth = 2;
    UInt_t ndiff = (clch.rowmax - clch.rowmin), nrow=1;
    if      (ndiff==0)      nrow = 1;
    else if (ndiff<rowstep) nrow = 2;
    else                    nrow = ndiff/rowstep + 2;
    UInt_t np = 2*nrow + 2; // number of ponts in polyline
	TArrayD x(np), y(np);

    Int_t ip=0, row=0;
    setpx* rpx=0;
    // left side of cluster from bottom to top
    for(UInt_t i=0; i<nrow; ++i) {
	   row = clch.rowmin + rowstep*i;
	   row = UInt_t(row)>clch.rowmax?clch.rowmax:row;
	   rpx = GetRows()[row];
	   if(rpx==0) continue;

	   x[ip]  = rpx->begin()->col;
	   x[ip] -= offset;
	   y[ip]  = row;
	   ++ip;
	}
    // top segment
    y[ip-1] += 0.5; // offset;
    x[ip] = rpx->rbegin()->col+offset;
    y[ip] = row + 0.5; // +offset;
    ++ip;
    // right side of cluster from top to bottom
    for(UInt_t i=0; i<nrow; ++i) {
	   row = clch.rowmax - rowstep*i;
	   row = row<Int_t(clch.rowmin)?clch.rowmin:row;
	   rpx = GetRows()[row];
	   if(rpx==0) continue;

       if(fDebug>1) {
	     cout<<" i "<<i<<" row "<<row<<" rpx "<<rpx<<endl;
	     cout<<" rpx->size() "<<rpx->size()<<endl;
       }
       setpx::iterator it = rpx->begin();
	   //<<" rpx->begin()"<< *it <<endl;
	   if(rpx->size()==1) x[ip] = rpx->begin()->col;
	   else              x[ip] = rpx->rbegin()->col;
	   x[ip] += offset;
	   y[ip]  = row;
	   ++ip;
	}
//    if(y[ip-1]>offset) y[ip-1] -= offset;
//    else               y[ip-1] = 0;
    y[ip-1] -= 0.5;
    // bottom segment
    x[0] = x[0]>x[1]?x[1]:x[0];
//    if(y[ip-1]>offset) y[0] -= offset;
//    else               y[0] = 0;
    x[ip] = x[0];
    y[0]  -= 0.5;
    y[ip] = y[0];
    ++ip;
    if(fDebug) cout<<" SDCluster2D::CreatePolyline() np "<<np<<" ip "<<ip<<endl;

    if(fPolyLine) delete fPolyLine;

	fPolyLine = new TPolyLine(ip, x.GetArray(), y.GetArray());
	fPolyLine->SetFillColor(0);
	fPolyLine->SetLineColor(1);
	fPolyLine->SetLineWidth(lineWidth);
	return fPolyLine;
}

void SDCluster2D::Browse(TBrowser *b)
{

}
