/*
 * sdCommon.cpp
 *
 *    Created on: Jan 5, 2013
 * Copyright (C) 2012-2013, Alexei Pavlinov
 * All rights reserved.
 */
//#ifdef WIN32
//#include "vld.h"
//#endif

#include "sdCommon.h"
#include <TSystem.h>
#include <TString.h>

#include <iostream>
using namespace std;

sdCommon* sdCommon::fgSdCommon = 0;

sdCommon* sdCommon::GetSdCommon(){
	if(fgSdCommon == 0) fgSdCommon = new sdCommon();
	return fgSdCommon;
}

Bool_t sdCommon::IsThisWindows()
{
	TString stmp(gSystem->GetBuildArch());
	return stmp.Contains("win", TString::kIgnoreCase);
}

sdCommon::sdCommon()
{
  rcsid="$Id: sdCommon.cxx,v 1.8 2013/03/15 21:21:29 pavlinov Exp $";
// Name of implemented pedestals(F0) reconstruction
  const Char_t* t[] = {"ped(f0)"};
  Int_t n = sizeof(t)/sizeof(const Char_t*);
  for(Int_t i=0; i<n; ++i) nameOfPeds.push_back(t[i]);

  // Name of implemented cluster finder
  const Char_t* t1[]={"stdone","stdtwo","2done","2dtwo", "2dthree"};
  n = sizeof(t1)/sizeof(const Char_t*);
  for(Int_t i=0; i<n; ++i) nameOfClfs.push_back(t1[i]);

  // Name of implemented response function
  vecStr ellPars;
  const char* parNames[] = { "h", "x_{0}", "a", "y_{0}", "b" };
  n = sizeof(parNames) / sizeof(char*);
  for (int i = 0; i < n; ++i) ellPars.push_back(parNames[i]);
  funPair ell("ellipsoid",ellPars);
  nameOfFuns.push_back(ell);

  // Name of implemented F0(pedestals) function
  const Char_t* t3[]={"f0uniform"};
  n = sizeof(t3)/sizeof(const Char_t*);
  for(Int_t i=0; i<n; ++i) nameOfF0.push_back(t3[i]);
  // Copyright string for ROOT Canvas, ..
  CPAI  = "#color[2]{#ocopyright PAI}";
  CPAI2 = "Copyright (C) 2012-2013, Alexei Pavlinov, All rights reserved.";
  //
  sdTrial = "TrialVersion";
  // Tiff staff : Mar 3;
  leadingTiffName = "TifFile";
  leadingPedName = "Ped";
  leadingPCFName = "CF";

  leadingNameSplitAlgo1 = "Split1";
  nameCLFContainer      = "CLFsPool";

  leadNameScan = "ClstScan";

  const Char_t* t4[]={"Blue", "Green", "Red"}; // look to definition of union pix32
//  const Char_t* t4[]={"Blue", "Red", "Green"}; // for coincidence with NIH - Mar 20;
  n = sizeof(t4)/sizeof(const Char_t*);
  for(Int_t i=0; i<n; ++i) tiffRgb.push_back(t4[i]);
  // May 4,2013
#ifdef WIN32
  fSlash = '\\';
#else 
  fSlash = '/';
#endif
}

void sdCommon::PrintNameOfAnaFuns()
{
  cout<<" Name of available analytical function \n";
  for(UInt_t i=0; i<nameOfFuns.size(); ++i) {
	  funPair fp = nameOfFuns[i];
	  cout<<" i "<<fp.first<<endl;
	  vecStr vs = fp.second;
	  for(UInt_t j=0; j<vs.size(); ++j) cout<<vs[j]<<" \n";
  }
}
