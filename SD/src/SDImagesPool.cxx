/*
 * SDImagesPool.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: Alexei Pavlinov
 */

#include "SDImagesPool.h"
#include "SDAliases.h"
#include "SDImageAnalysis.h"

// Images pool(container) for tif file - could be up to 3 images

SDImagesPool::SDImagesPool() : TDataSet()
{
	// For reading
}

SDImagesPool::SDImagesPool(const char* name) : TDataSet(name)
{

}

SDImagesPool::~SDImagesPool() {
	// TODO Auto-generated destructor stub
}
//
SDImageAnalysis* SDImagesPool::GetSDImageAnalysis(const char* partName)
{
	// Mar 10,2013 : partName is color of image
	return dynamic_cast<SDImageAnalysis *>(sda::FindObjectByPartName(fList, partName));
}
