#include "SDTrialVersion.h"

#include <TGMsgBox.h>

#include <iostream>
#include <assert.h>

// Root file with SDTrialVersion object
const char* SDTrialVersion::gkFileName="LCond.root";

ClassImp(SDTrialVersion)

SDTrialVersion::SDTrialVersion(void) : TDataSet(),
fStartDate(), fEndDate(), fSignature(), fDebug(0)
{}

SDTrialVersion::SDTrialVersion(const char* name, const char* title, Int_t year, Int_t month, Int_t day, Int_t nm, Int_t nd) : TDataSet(name, title),
	fStartDate(), fEndDate(year,month,day, 0,0,0), fSignature(""), fDebug(0)
{
   time_t endSec   = fEndDate.GetSec();
   Int_t daySec    =  3600*24; // Seconds in day
   Int_t days      = 31*nm + nd;
   Int_t startSec  = startSec + daySec * days;
   fStartDate.SetSec(endSec);
 }

SDTrialVersion::~SDTrialVersion(void)
{
}
// --
void SDTrialVersion::CreateSignature()
{
	fSignature.Form(" Trial version for  %s\n", GetName());
	fSignature += TString::Format(" From  %s to %s \n", fStartDate.AsString(), fEndDate.AsString());
    if(fDebug>0) {
	  std::cout<<fSignature<<std::endl;
      fStartDate.Print();
      fEndDate.Print();
   }
}

Bool_t SDTrialVersion::CheckValidation()
{
	TTimeStamp dtCurrent;
	time_t iend = fEndDate.GetSec(), icur = dtCurrent.GetSec();
	if(iend > icur) {
		TTimeStamp dif((time_t)(iend-icur),0);
		if(fDebug) dif.Print();
		Bool_t inUTC = kTRUE;
		Int_t secOffset = 0; 
		UInt_t year, month, day;
		dif.GetDate(inUTC, secOffset, &year, &month, &day);
		UInt_t hour, min, sec;
		dif.GetTime(inUTC, secOffset, &hour, &min, &sec);
		std::cout<<"You can use SD ";
		UInt_t m = month -1;
		if(m>0) std::cout<<m<<" month ";
		std::cout<<day<<" days "<<hour<<" hours "<<std::endl;
		return kTRUE;
	} else {
		TString stmp;
		stmp.Form("<I> Trial Version was expired at %s ", fEndDate.AsString("s"));
		std::cout<<stmp<<std::endl;
		new TGMsgBox(gClient->GetRoot(), 0, "  T r i a l    V e r s i o n  ", stmp.Data()
			,kMBIconExclamation, 0);
        assert(0);
        return kFALSE;
	}
}
