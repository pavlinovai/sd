/*
 * SDManager.cpp
 *
 *  Created on: Apr 12, 2012
 *              2013 :  Feb 2; Jun 2;
 *      Author: pavlinov
 */

#include "SDManager.h"
#include "SDConfigurationParser.h"
#include "SDConfigurationInfo.h"
#include "SDPedestals.h"
#include "SDAliases.h"
#include "SDClusterFinderStdOne.h"
#include "SDClusterFinderStdTwo.h"
#include "SDClusterFinder2DOne.h"
#include "SDClusterInfo.h"
#include "SDControlBar.h"
#include "SDTiffFileInfo.h"
#include "sdCommon.h"
#include "SDTrialVersion.h"
//#include "versionUpdate.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TFile.h>
#include <TImage.h>
#include <TBrowser.h>
#include <TCanvas.h>
#include <TH2.h>
#include <TF1.h>
#include <TF2.h>
#include <TArrayD.h>
#include <TBrowser.h>
#include <TPolyMarker.h>
#include <TDatime.h>
#include <TDataSet.h>
#include <TGFileDialog.h>

#include <assert.h>
#include <algorithm>
#include <vector>
#include <iostream>
#include <memory>
#include <fstream>
#include <string>

using namespace std;

const char* SDManager::gkNameHistInForCF = "Hists for CF";
const char* SDManager::gkPeds = "Pedestals";
const char* SDManager::gkConfParser = "SD config.Parser";
const char *SDManager::gkBarName = "SD bar";

TString OS(gSystem->Getenv("OS"));

SDManager::SDManager() : SDObjectSet(),
		fConfigDir(), fConfigName(), fConfigNameDefault(),
		fStatus(-1), fDateStart(0), fDateStop(0), fOutputFileName()
{
	// Default constructor for reading
}

SDManager::SDManager(const char* name) : SDObjectSet(name),
		fConfigDir(), fConfigName(), fConfigNameDefault(),
    fStatus(-1),fDateStart(new TDatime), fDateStop(0), fOutputFileName("")
{
	TString snam = name;
    if(snam.Length()==0) SetName("sdman");
    gROOT->GetListOfBrowsables()->Add(this);
    fStatus = kBegin;
    // gErrorIgnoreLevel = 0; // Default is -1
}

SDManager::~SDManager() {
	// TODO Auto-generated destructor stub
}

void SDManager::ClearEverything()
{
	// 2013: Feb 24;
	// Delete everything for preparing to process new tiff picture
	if(fDateStart) {delete fDateStart; fDateStart = 0;}
	if(fDateStop)  {delete fDateStop;  fDateStop = 0;}
	// Clear Finders
	TDataSet *ds = FindByName("ClusterFinders");
	ds->Delete("all");
	Remove(ds);
}

TH2*  SDManager::GetRawInputHist() {
    Int_t ndiv = 0;
    if(GetConfigurationParser()) {
	  parsFillHists pfh =
			GetConfigurationParser()->GetLastConfigurationInfo()->GetParsFillHists();
	  ndiv = pfh.ndiv;
    }
	return (TH2*)(GetListHistsInput()->At(ndiv));
}

TList* SDManager::GetListHistsInput()
{
	return GetTiffFileInfo()->GetListHistsInput();
}

SDConfigurationParser*     SDManager::GetConfigurationParser() const {
	return dynamic_cast<SDConfigurationParser *>(FindByName(gkConfParser));
}

SDPedestals* SDManager::GetPedestals() const {
	return dynamic_cast<SDPedestals *>(FindByName(gkPeds));
}

SDClusterFinderBase* SDManager::GetClusterFinder(const int ind) const
{
   TDataSet *ds = FindByName("ClusterFinders");
   if(ds==0) return 0;
   else      return dynamic_cast<SDClusterFinderBase *>(ds->At(ind));
}

SDControlBar*  SDManager::GetControlBar()
{
	return dynamic_cast<SDControlBar *>(FindByName(gkBarName));
}

SDTiffFileInfo* SDManager::GetTiffFileInfo()
{
	return dynamic_cast<SDTiffFileInfo *>(FindByName(SDTiffFileInfo::gkName));
}

TH2 *SDManager::GetImageHistForCF(const int ind)
{
	return dynamic_cast<TH2 *>(GetListHistsInput()->At(ind));
}

void SDManager::InitMain()
{
	// double scale = 10000.; // double(0xFFFFFF)
	if(fStatus>=kInitMain) {
	   cout<<" SDManager::Init() was done already : fStatus "<<fStatus<<endl;
	   return;
	}
   TString stmp, ROOTSYS, SDMAIN, SDXML, SDTIFF;
   sda::GetEnvironment("ROOTSYS", ROOTSYS, 1);
   sda::GetEnvironment("SDMAIN",  SDMAIN,  1);
   sda::GetEnvironment("SDXML",   SDXML,   1);
   sda::GetEnvironment("SDTIFF",  SDTIFF,  1);
   if (ROOTSYS.Contains("QT",TString::kIgnoreCase) && 0) {
		gSystem->Load("libXMLParser");
		gSystem->Load("libGQt");
		gSystem->Load("libQtRootGui");
	}
	if(SDXML.Length()==0) { // No Default value
		cout<<"<W> SDXML environment variable is absent! Try ${SDMAIN}/data .. ";
		if(SDMAIN.Length()) {
		   SDXML += "/data";
		   cout<<" yes!"; // ??
		} else {
		  cout<<" Define SDXML !\n";
		  assert(0);
		}
	}
	TString stmpName = SDXML + "/DefaultConf.xml"; // Absolute name (Unix style)
    if(OS.Contains("_NT")) { // MS Windows style ??
    	SDXML.ReplaceAll("\\","/");
    	stmpName.ReplaceAll("\\","/");
    }
	fConfigDir.SetString(SDXML.Data());
	fConfigNameDefault.SetString(stmpName.Data());
	fConfigName.SetString(stmpName.Data());
	InitDefaultConfiguration();

#ifdef TRIAL
	InitTrial();
#endif

//	if(gROOT->IsBatch()) {
//		cout<<" Batch job: no control bar "<<endl;
//	} else {
//		InitControlBar();
//	}
//	sdu::PrintCvsInfo();
    fStatus = kInitMain;
}

void SDManager::InitControlBar()
{
	SDControlBar *bar = new SDControlBar(gkBarName);
 	Add(bar);
	bar->Init();
}

void SDManager::InitDefaultConfiguration()
{
	SDConfigurationParser *conf = new SDConfigurationParser(gkConfParser);
	Add(conf);
	// First is default configuration
	conf->ParseFile(fConfigNameDefault.GetString().Data()); // Default configuration
	conf->GetConfigurationInfo(0)->SetName("Default Config");
}

void SDManager::InitConfiguration()
{
    SDConfigurationParser *parser = GetConfigurationParser();
    if(parser->GetListSize()==1) {
	  parser->ParseFile(fConfigName.GetString().Data());
    } else {
    	if(fDebug) cout<<"<I> SDManager::InitConfiguration() is done already !\n";
    }
    fStatus = kInitConfiguration;
}

#ifdef TRIAL
Bool_t SDManager::InitTrial()
{   // June 2,2013
	// End of trial day is defined at sdOpt.h
	TString stmp(sd::GetSdCommon()->sdTrial);
	SDTrialVersion* trial = new SDTrialVersion(stmp.Data()," SD Trial Version ", t_year, t_month, t_day);
	trial->CreateSignature();
	Bool_t key = trial->CheckValidation();
	if(key==kFALSE) delete trial;
	else           Add(trial);
	return key;
}

SDTrialVersion* SDManager::GetTrialVersion()
{
	return dynamic_cast<SDTrialVersion *>(sda::FindObjectByPartName(fList, sd::GetSdCommon()->sdTrial.Data()));
}
#endif

Bool_t SDManager::SelectXMLFileDialog()
{
	const char *openTypes[] = { "XML files",   "*.xml",
	                            "All files",    "*",
	                            0,              0 };
	if(fDebug) cout<<"  SDManger::SelectXMLFileDialog() \n";

    TGFileInfo fi;
    fi.fFileTypes = openTypes;
    fi.fIniDir    = StrDup(GetConfigDir().Data());
    new TGFileDialog(gClient->GetDefaultRoot(), 0, kFDOpen,&fi);
    if (!fi.fFilename) return kFALSE; // No xml file selection

    // fi.fIniDir does not change after dialog.
    // fi.fFileName will keep absolute file name.
    SetConfigName(StrDup(fi.fFilename));
    fStatus = kSelectXmlFile;
    return kTRUE;
}

Bool_t SDManager::SelectTifFileDialog()
{
	const char *openTypes[] = { "Tif files",   "*.tif*",
	                            "All files",    "*",
	                            0,              0 };
	if(fDebug) cout<<"  SDManger::SelectTifFileDialog() \n";

    TGFileInfo fi;
    fi.fFileTypes = openTypes;
    // Select tiff directory
	TString dir(gSystem->Getenv("SDTIFF"));
	if(dir.Length()==0) dir = GetConfigurationParser()->GetDefaultConfigurationInfo()->GetParsInput().dir;
    fi.fIniDir = StrDup(dir.Data());

    // fi.fIniDir does not change after dialog.
    // fi.fFileName will keep absolute file name.
    new TGFileDialog(gClient->GetDefaultRoot(), 0, kFDOpen,&fi);
    if (!fi.fFilename) return kFALSE; // No tif file selection

    //  fi.fFilename keeps absolute file name
    if(fDebug) cout<<" fi.fIniDir "<<fi.fIniDir<<endl;
    if(fDebug) cout<<" fi.fFilename "<<fi.fFilename<<endl;
//  //  SDConfigurationInfo* conf = new SDConfigurationInfo(*(GetConfigurationParser()->GetDefaultConfigurationInfo()));
    // Feb 2; after xml file selection keep the values of parameters from xml file;
    //        After tiff file selectiorn (if you want) these pars will be used
    SDConfigurationInfo* conf = new SDConfigurationInfo(*(GetConfigurationParser()->GetLastConfigurationInfo()));
    if(fDebug) cout<<"<I> SDManager::SelectTifFileDialog() : fFilename "<<fi.fFilename<<endl;

    TString tifFile = StrDup(fi.fFilename);
    Ssiz_t ind = tifFile.Last('/'), length = ind + 1;
    conf->fParsInput.dir  = tifFile(0,length);
    conf->fParsInput.name = tifFile(ind+1, tifFile.Length()-length);
    conf->SetName(conf->fParsInput.name.Data());
    if(fDebug) cout<<" fParsInput "<<conf->fParsInput<<endl;

    // Get output name from tif file name
    TString snam = conf->fParsInput.name;
    snam = snam(0, snam.Index(".tif")); // Discard extension : ".tif" or ".tiff"
    snam += ".root";
    conf->fParsOutput.name = snam;

    GetConfigurationParser()->Add(conf); // Sep 20,2012
    fStatus = kSelectXmlFile;
    return kTRUE;
}

SDTiffFileInfo* SDManager::ReadTif()
{
	// 2013: 19 Jan;
	SDConfigurationInfo *conf = GetConfigurationParser()->GetLastConfigurationInfo();
	cout<<"\t\t<I> Current configuration "<<conf<<endl;
	parsInputOutput parsIn = conf->GetParsInput();
	parsFillHists   pfh    = conf->GetParsFillHists();

	TString stmp = parsIn.dir + parsIn.name;
	gSystem->ExpandPathName(stmp);
	// Get info about a file. Info is returned in the form of a FileStat_t
    // structure (see TSystem.h).
    // The function returns 0 in case of success and 1 if the file could
    // not be stat'ed.
	FileStat_t buf;
	int key = gSystem->GetPathInfo(stmp.Data(), buf);
	if(key == 1) {
		cout<<"<E> tif file does not exist "<<stmp.Data()<<endl;
		assert(0);
	}
	SDTiffFileInfo *tifInfo = new SDTiffFileInfo(SDTiffFileInfo::gkName, stmp.Data());
	Add(tifInfo);
	if(tifInfo->DecodeTiffFile()) {
		cout<<"<I> Tif image was read from "<<stmp<<endl;
	}

	TImage* img = GetTiffFileInfo()->GetImage();
	if(img) {
	  UpdateName(parsIn.name);
	  fStatus = kReadTiff;
	} else {
	  cout<<"\n\t<E> problem with reading tif image \n";
	  assert(0);
	}
	return tifInfo;
}


void SDManager::UpdateName(TString stmp)
{
    // Name of manager after definition of tif file
	// stmp is absolute name of tif file
	int ind2 = stmp.Index(".tif")-1, ind1=ind2;
	for(int i=ind1; i>=0; --i){
		ind1 = stmp.Index("/",i);
		if(ind1>0) break;
	}
	++ind1;
	TString snam = GetName();
	snam += " ";
	snam += stmp(ind1, ind2-ind1+1);
	SetName(snam.Data());
}

TList* SDManager::BookAndFillHistsIn(TImage* img, const char* name, Float_t scale, Int_t ndiv) {
    // List 1 contains initial tif image  as histogram.
	// Also (if ndiv > 0) create ndiv histograms as part of
	// initial one (for testing).
	//
	// Time is going from left to right: 1 pixel = 2.09 ms,
	// Y scale is space (along scan line): 1 pixel = 0.05 micro meter
    // Omage index vs hists
	// binx = col + 1; biny = row + 1;
	int divMax=16;
	cout<<"<I> SDManagerst::BookAndFillHistsIn is started : "<<name;
	cout<<" img "<<img<<endl;
	TH1::AddDirectory(1);

	int w = img->GetWidth(), h = img->GetHeight();
	TArrayD* imgArr = img->GetArray(0, 0, 0);
	Double_t sw = w / divMax;
	printf(" w %i h %i : %i sw %f : %i \n", w, h,
			w * h, sw, int(sw));
	if(imgArr) cout<<" imgArr size "<<imgArr->GetSize();
	else       assert(0);

	//	int wh=w/ndiv, hh=h;
	double xmi=-0.5, xma = w-0.5, ymi=-0.5, yma = h-0.5;
	TString stit(img->GetName());
	TString stitAdd = Form("; %s                     time bin; space bin", img->GetName());
	stit += stitAdd;
	TH2F* h2 = new TH2F("00_img", stit.Data(), w,xmi,xma, h, ymi,yma);
	h2->SetOption("colz");

	if(ndiv>=1) {
	int ih = 0;
	  for (int idiv = 0; idiv < divMax; ++idiv) {
		TString snam(Form("%2.2i_part%2i ", ++ih, idiv));
		stit = Form("Part %i ", idiv);
		stit += stitAdd;
		Double_t w1 = sw * idiv-0.5, w2 = w1 + sw;
		TH2* htmp = new TH2F(snam.Data(), stit.Data(), int(sw), w1, w2, h,ymi,yma);
		htmp->SetOption("colz");
	  }
    }

	TList *l = sda::moveHistsToList(name);
	sda::AddToNameAndTitleToList(l, "1", " Img 1");
	TH1::AddDirectory(0);
	// Fill hists
	for (int row = 0; row < h; ++row) {
		Double_t y = double(row);
		for (int col = 0; col < w; ++col) {
			int ind = w * row + col;
			Double_t a = (*imgArr)[ind];
			Double_t b = a * scale;
			h2->SetBinContent(col + 1, row + 1, b);

			if (ndiv >= 1) {
				double x = double(col);
				int idiv = col / int(sw) + 1;
				sda::FillH2(l, idiv, x, y, b);
				//printf(" (%4i,%3i) idiv %i a %f b %f \n",row,col,idiv, a,b);
			}
		} // col
	} // row
	printf(" ... done \n");

	return l;
}

void SDManager::InitPedestals()
{
    cout<<" \n\t SDManager::InitPedestals() is started \n";
	SDPedestals *ped = new SDPedestals("");
	Add(ped);

	parsPedestals pp =
			GetConfigurationParser()->GetLastConfigurationInfo()->GetParsPedestals();
	ped->SetPedestalsPars(pp);
	cout<<pp<<endl;

//	parsFillHists pfh = GetConfigurationParser()->GetLastConfigurationInfo()->GetParsFillHists();
//	TList *l1 = GetListHistsInput();
	TH2* h2 = GetTiffFileInfo()->GetTifAs2DHist();
	ped->SetInput2DHist(h2/*, pp.set*/);

	cout<<"\n\t End of SDManager::InitPedestals() \n";
	sda::PrintList(GetPedestals()->GetListWorkHists());

    fStatus = kInitPedestals;
}

void SDManager::DoPedestals()
{
	GetPedestals()->DoAll();
    fStatus = kDoPedestals;
}

void SDManager::InitClustersFinder(){
	//
	// Cluster finders : could be several ones
	//
	TDataSet *ds = new TDataSet("ClusterFinders");
	Add(ds);

	TList* lInCF = CreateListHistsInputForCF();
	GetList()->Add(lInCF);
	TH2 *h2th = GetPedestals()->GetHistForClusterFinder();
    lInCF->Add(h2th);
	cout<<" 2 "<<endl;
	sda::PrintList(GetPedestals()->GetListWorkHists());

    parsClusterFinder pcf = GetConfigurationParser()->GetLastConfigurationInfo()->GetParsClusterFinder();
	SDClusterFinderBase *clf = sdu::CreateClusterFinder(pcf);
	ds->Add(dynamic_cast<SDClusterFinderStdOne *>(clf)); // Keep is here
	clf->SetCFPars(pcf);

	clf->Init();
    clf->SetInputHist(h2th);
    // Transition to F0 units
    clf->SetSeed(clf->GetSeed()*GetPedestals()->GetSigF0Unit());
	fStatus = kInitClusterFunding;
}

TList* SDManager::CreateListHistsInputForCF()
{
	TList* lInCF = new TList;
	lInCF->SetName(gkNameHistInForCF);
	GetList()->Add(lInCF);
	return lInCF;
}

//Int_t SDManager::DoClusterFinder() // obsolete
//{
//    SDClusterFinderBase* clf = GetClusterFinder(0); // now only one
//    if(clf==0) {
//      cout<<" SDManager::DoClustersFinding : clf is "<<clf;
//      return -1;
//    }
//
//	Int_t nclust = clf->FindClusters();
//// 	GetListHistsInputForCF()->Add(clf->GetMainHists()->At(1)); // Add remnant hists
//
//    fStatus = kDoClusterFunding;
//    return nclust;
//}

Int_t SDManager::DoAll()
{
    InitConfiguration();
	ReadTif();

	InitPedestals();
    DoPedestals();

	InitClustersFinder();
	Int_t nclust = 0;
//	DoClusterFinder();

	return nclust;
}

void SDManager::DeleteControlBar()
{
	// We don't need to keep control bar before saving
	SDControlBar* bar = GetControlBar();
    if(bar==0) return;
	Remove(bar);
	delete bar;
}

TString SDManager::GetFileNameWithoutExtension(Int_t pri)
{
	parsInputOutput* outPars = sdu::GetParsOutput(this);
    parsInputOutput* inPars = sdu::GetParsInput(this);
    TString s, stmp = outPars->name;
    stmp.ToLower();
	if(stmp.Contains("nosave")) return s;

	if(fOutputFileName.Length()==0) {
	  if(fDateStop==0) fDateStop = new TDatime;
	  TString dirsd;

	  if(stmp.Length() && stmp.Contains("auto")==0) {
		  s = outPars->name;
		  if(s.Contains(".root")) s = s(0,s.Index(".root"));
	  } else {

        s = inPars->name;
	    s.ToLower();
	    s = s(0,s.Index(".tif")); // name of tif file without extension (".tif..")

	  // F0 (pedstals parameters)
	    parsPedestals* peds = sdu::GetParsPedestals(this);
	    s += Form("_Set%iNsig%3.1f", peds->set, peds->nsig);

	  // Clusters Finder pars
	    parsClusterFinder* cfPars = sdu::GetParsClusterFinder(this);
	    s += Form("_Seed%3.1f_",cfPars->seed);

		s += fDateStop->GetDate();
	  }
	  if(strlen(outPars->dir)) {
		  dirsd = outPars->dir;
	  } else {
        dirsd = gSystem->Getenv("SDOUTPUT");
	  }
	  if(dirsd.Length()) s.Prepend(dirsd);
	  fOutputFileName = s;
	}
	gSystem->ExpandPathName(fOutputFileName);
	if (pri) {
		if (pri > 1) {
			cout << (*inPars) << endl;
			cout << (*outPars) << endl;
		}
		cout << " fOutputFileName " << fOutputFileName << endl;
	}
	return fOutputFileName;
}

void SDManager::SetFileNameWithoutExtension(TString nf)
{
	// Do I need this?
	if(nf.Length()) fOutputFileName = nf;
}

void SDManager::SaveSD(Int_t pri)
{
  if(GetConfigurationParser()->SaveToRootFile() == kFALSE) return;
  DeleteControlBar();

  TString snam = GetFileNameWithoutExtension();
  if(snam.Contains(".root")==0) snam += ".root";
  TFile f(snam.Data(), "RECREATE");
  if(f.IsOpen()){
    this->Write();
    f.Close();
    if(pri) printf("<I> SDManager::SaveSD() : %s was saved \n to file %s \n", GetName(), snam.Data());
  } else {
    if(pri) printf("<E> DIDN'T SAVE |%s| to file |%s|\n", GetName(), snam.Data());
  }
}

TString SDManager::SaveTxtFileAsCSV(char symbol)
{
	// 2013: Feb 23;
	// Save text file in CSV format for reading by Excell
	//
	TString snam = GetFileNameWithoutExtension() + "CSV.txt";
	ofstream fout(snam.Data());
	if (fout.is_open()) {
		SDClusterFinderBase* cf = GetClusterFinder(0);
		if (cf == 0) {
			fout << "No cluster : fStatus " << fStatus << endl;
			return "";
		}
        if(0) {
		  parsClusterFinder* pcf = sdu::GetParsClusterFinder(this);
		  fout << "spacescale" << pcf->spacescale<<" um";
		  fout << "  timescale  " << pcf->timescale << " ms" << endl;
		  fout << "clusts    Ave_amp       Peak    size(um)    Duration(ms)\n";
        }

        // Create the name of columns
		const char* nameOfColumns[]={"nclusts","npixels","ave_amp","peak","size_um",
				                      "duration_ms","Position_pxls"};
        Int_t nc = sizeof(nameOfColumns)/sizeof(char*);
        for(int i=0; i<nc-1; ++i) fout<<nameOfColumns[i]<<symbol;
        fout<<endl;
        //
		TDataSet *cls = cf->GetClusters();
		for (Int_t i = 0; i < cls->GetListSize(); i++) {
			SDClusterInfo* cl = cf->GetClusterInfo(i);
			sdClusterChar clch = cl->GetClstCharFromZero(); // March 15,2013
			fout << i+1 <<symbol;
			fout << clch.numpixels << symbol;
			fout << clch.aveamp << symbol;
			fout << clch.maxamp << symbol;
			fout << clch.size   << symbol;
			fout << clch.duration << symbol <<endl;
		}
		if(fDebug>=0) cout<<"<I> Clusters info was saved to CSV text file "<<snam<<endl;
	} else {
		cout<<"<E> DIDN'T Save "<<GetName()<<" to CSV text file "<<snam<<endl;
	}
	return snam;
}

TString SDManager::SaveTxtFileTwo() {
	if(fDebug>=1) cout<<"<I> SaveTxtFileTwo is started \n";
	TString snam = GetFileNameWithoutExtension() + ".txt";
	FILE* fout = fopen(snam.Data(), "w");
	if (fout) {
		TString stmp(Form("User: %s ",gSystem->Getenv("USER")));
		stmp += Form("HOST %s ", gSystem->HostName());
		TDatime dt;
		stmp += dt.AsString();
		fprintf(fout,"%s \n",stmp.Data());
		SDClusterFinderBase* cf = GetClusterFinder(0);
		if (cf == 0) {
			cout<<"<W> No cluster : fStatus "<<fStatus<<endl;
			fprintf(fout, "No cluster : fStatus %i ", fStatus);
			return "";
		}
        parsInputOutput *pin = sdu::GetParsInput(this);
        fprintf(fout, "%s %s \n", pin->dir.Data(), pin->name.Data());

        //parsFillHists *pf = sdu::GetParsFillHists(this);

        parsPedestals* peds = sdu::GetParsPedestals(this);
		fprintf(fout,"algorithm\t\t%s\n", peds->algo.Data());
		fprintf(fout,"type of smoothing(filter)\t%s\n", peds->smooth.Data());
		fprintf(fout,"chi2cut %6.4f sigcut %6.4f set %i nsig %6.4f \n",
				peds->chi2cut,peds->sigcut,peds->set, peds->nsig);

		parsClusterFinder* pcf = sdu::GetParsClusterFinder(this);
		fprintf(fout, "clf %s seed %5.3f ", pcf->name.Data(), pcf->seed);
		fprintf(fout, "spacescale %8.6f(um) ", pcf->spacescale);
		fprintf(fout, " timescale %8.6f(ms) \n", pcf->timescale);

		TDataSet *cls = cf->GetClusters();
		fprintf(fout," Number of clusters (sparks) %i\n", cls->GetListSize());

		fprintf(fout,
				"#clusts    #pixels Ave_amp     Peak    Size(um)    Duration(ms)      Position(pxls)\n");
		for (Int_t i = 0; i < cls->GetListSize(); i++) {
			SDClusterInfo* cl = cf->GetClusterInfo(i);
			sdClusterChar clch = cl->GetClstCharFromZero();
			Int_t ifort = i + 1;
			fprintf(fout, "%4i     %6i   ", ifort, clch.numpixels);
			fprintf(fout, "%6.3f %9.3f ", clch.aveamp, clch.maxamp);
			fprintf(fout, "%10.3f   %10.3f ", clch.size, clch.duration);
			fprintf(fout, "      c:%4i-%4i r:%4i-%4i",clch.colmin,clch.colmax, clch.rowmin,clch.rowmax);
			fprintf(fout,"\n");
		}
		fclose(fout);
		if(fDebug>=1) cout<<"<I> Clusters info was saved to text file "<<snam<<endl;
	} else {
		if(fDebug>=1) cout<<"<E> DIDN'T SAVE "<<GetName()<<" to file "<<snam<<endl;
	}
	if(fDebug>=0) cout<<"<I> SaveTxtFileTwo is   ended \n";
	if(fDebug>3) {
	   TString cmd(".!more ");
	   cmd += snam;
	   gROOT->ProcessLine(cmd.Data());
	}
	return snam;
}

void SDManager::TestOne() {
	// Attached is an example of our line-scan recording (tif file).
	// Time is going from left to right: 1 pixel = 2.09 ms,
	// Y scale is space (along scan line): 1 pixel = 0.05 mikrometer

	TCanvas* c1 = new TCanvas("ctif", "examples of image manipulations", 2048,
			512);
	//   c1->Divide(2, 3);
	c1->cd(1);
	// fImg->Draw("xxx");
//	fImg->SetEditable(kTRUE);
	c1->Update();
	//
	//	int w = fImg->GetWidth(), h =fImg->GetHeight(), ind = 0, ndiv=16;
	//    TArrayD* imgArr = fImg->GetArray(0,0,0);
	//	for (int row = 0; row < hh; ++row) {
	//		for (int col = 0; col < wh; ++col) {
	//		  ind = w*row + col;
	//		  Double_t a = (*imgArr)[ind];
	//		  Double_t b = a*double(0xFFFFFF);
	//		  h2->SetBinContent(col+1,row+1, b);
	////		  printf(" (%i4,%i3) a %f b %f \n",row,col, a,b);
	//		}
	//	}
	//	printf(" last ind %i 0xFFFFFF = %i\n", ind, 0xFFFFFF);
	//	TCanvas* c2 = new TCanvas("ch2", "examples of image manipulations", 2048, 512);
	//	c2->cd(1);
	//
	////	TF2* hf2 = new TF2("hf2","[0]+[1]*x+[2]*y",0.0,wh, 0.0,hh);
	////  a TF2 must be a function of x,y, even if constant, so instead of - from Rene Brun
	//	TF2* hf2 = new TF2("hf2","[0]+0*x*y",0.0,wh, 0.0,hh);
	//	hf2->SetParameter(0,1.e+6);
	//	h2->Fit(hf2,"","");
	//
	//	h2->Draw("lego");
	//	c2->Update();
	//	delete imgArr;
}

void SDManager::TestTwo() {

}

void SDManager::PrintInfo()
{
   cout<<" SDManager "<<GetName()<<" fStatus : "<<fStatus;
   cout<<"\n Configuration  dir "<<fConfigDir.GetString();
   cout<<"\n            Default "<<fConfigNameDefault.GetString();
   cout<<"\n            Current "<<fConfigName.GetString()<<endl;
   PrintVersion(stdout);
}

void SDManager::PrintVersion(FILE* fout)
{
	// 2013: Mar 18;
    TDatime dt;
	fprintf(fout, "Build date     : %s ", dt.AsSQLString());
	fprintf(fout, "Version number : %s\n", sdu::GetVersion().Data());
#ifdef TRIAL
	fprintf(fout, "Trial version : end at  %s\n", GetTrialVersion()->GetEndDate().AsString());
#endif
}

void SDManager::Browse(TBrowser* b)
{
//	if() b->Add(, );
//	if(fImg) b->Add(fImg);
	b->Add(&fConfigDir,"ConfDir");
	b->Add(&fConfigName,"ConfName");
	b->Add(&fConfigNameDefault,"Default ConfName");
	TObjectSet::Browse(b);
}

ClassImp(SDManager)
