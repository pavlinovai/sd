/*
 * SDConfigurationInfo.cpp
 *
 *  Created on: May 26, 2012
 *      Author: pavlinov
 */

#include "SDConfigurationInfo.h"
#include "SDUtils.h"
#include  "SDConfigurationParser.h"
#include "SDTransformation.h"

#include "TGWindow.h"
#include "TGLabel.h"
#include <TGFrame.h>
#include <TGLayout.h>
#include <TGGC.h>
#include <TGTextView.h>
#include <TGButton.h>

#include "iostream"

using namespace std;

parsInputOutput::parsInputOutput() : dir("$(SDTIFF)"), name("8_22_c11.tiff")
	{;}

ClassImp(parsFillHists)
parsFillHists::parsFillHists() : scale(1.), ndiv(0) {;}

ClassImp(parsPedestals)
parsPedestals::parsPedestals() :  algo("idl"),smooth("m2"), chi2cut(1.5),sigcut(2.0),set(2),nsig(3.0) {;}

ClassImp(parsClusterFinder)
parsClusterFinder::parsClusterFinder() : name("2done"), seed(4), timescale(2.09),spacescale(0.05),
minpixels(20),minsize(0.25),minduration(6.27), drawing(0), minsizeInPxs(5), mindurationInPxs(3)
{
	CalculateSizesInPixels();
}

ClassImp(parsSdScan)
parsSdScan::parsSdScan() : fNpScan(0), fMinStep(0.1), fMaxRelAmp(0.8), fMaxNClusts(5) {;}

void parsClusterFinder::CalculateSizesInPixels()
{
	minsizeInPxs     =  Int_t(minsize / spacescale);
	mindurationInPxs =  Int_t(minduration/timescale);
//	minduration = mindurationInPxs * timescale;
//	minsize     = minsizeInPxs     * spacescale;
}

ClassImp(parsSdThreads)
parsSdThreads::parsSdThreads() : fMaxThreads(2), fKeep(0), fNcpus(1), fSysInfo()
{
	// 2013: 14 Apr; 22 May
	gSystem->GetSysInfo(&fSysInfo);
	fNcpus = fSysInfo.fCpus;
}

Bool_t parsSdThreads::IsMultiThreadingPossible()
{
   if(fMaxThreads>1) return kTRUE;
   else              return kFALSE;
}

ClassImp(SDConfigurationInfo)
SDConfigurationInfo::SDConfigurationInfo(const char* name) : TDataSet(name),
  fParsInput(),fParsFillHists(),fParsPedestals(), fParsClusterFinder(), fParsOutput(),
  fParsThreads(), fParsScan(),
  fMain(0), fGuiInput(0)
{
}

SDConfigurationInfo::SDConfigurationInfo(const SDConfigurationInfo& conf) : TDataSet(conf.GetName()),
		fParsInput(conf.fParsInput), fParsFillHists(conf.fParsFillHists),
		fParsPedestals(conf.fParsPedestals),fParsClusterFinder(conf.fParsClusterFinder),
		fParsOutput(conf.fParsOutput), fParsThreads(conf.fParsThreads), fParsScan(conf.fParsScan),
		fMain(conf.fMain), fGuiInput(conf.fGuiInput)
{}

SDConfigurationInfo::~SDConfigurationInfo() {}

SDManager*  SDConfigurationInfo::GetManager()
{
	return sdu::GetParentTempl<SDManager>(this);
}

SDConfigurationParser* SDConfigurationInfo::GetConfigurationParser()
{
	return sdu::GetParentTempl<SDConfigurationParser>(this);
}


void SDConfigurationInfo::SetConfigurationStd(const char* indir)
{
  fParsInput.dir  = indir;
}

void SDConfigurationInfo::SetConfiguration()
{
	const char* windowName="SD configuration";
	fMain = new TGMainFrame(gClient->GetRoot(), 10, 10, kVerticalFrame);
	   // recusively delete all subframes on exit
	   fMain->SetCleanup(kDeepCleanup);

	   //fGuiInput = new SDGuiTwoStrings(fMain, GetName(), GetName());
	   //fMain->AddFrame(fGuiInput, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5));

	   InputDialog(fMain);

	   // terminate ROOT session when window is closed
	   //fMain->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
	   //fMain->DontCallClose();

	   fMain->MapSubwindows();
	   fMain->Resize();

	   // set minimum width, height
	   fMain->SetWMSizeHints(fMain->GetDefaultWidth(), fMain->GetDefaultHeight(),
	                         1000, 1000, 0, 0);
	   fMain->SetWindowName(windowName);
	   fMain->MapRaised();
}

void SDConfigurationInfo::InputDialog(TGMainFrame *mf)
{
   if(mf==0) return;

   TGGroupFrame* gf = new TGGroupFrame(mf, "TIFF file dialog");
   mf->AddFrame(gf, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5));

   TGHorizontalFrame *horz = new TGHorizontalFrame(gf);
   gf->AddFrame(horz, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));

   // First vertical frame
   TGVerticalFrame *vert = new TGVerticalFrame(horz);
   horz->AddFrame(vert, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
   TGLabel *label = new TGLabel(vert, "Tiff file name");
   vert->AddFrame(label, new TGLayoutHints(kLHintsLeft | kLHintsCenterY));

   Pixel_t backpxl;
   gClient->GetColorByName("#c0c0c0", backpxl);
   TString stmp = GetParsInput().GetAbsoluteName();
   TGTextView* fTextView = new TGTextView(vert, 400, 30, stmp.Data(), kFixedWidth | kFixedHeight);
   fTextView->SetBackground(backpxl);
   vert->AddFrame(fTextView, new TGLayoutHints(kLHintsExpandX));
   fTextView->ShowBottom();

   // Second vertical frame
   TGVerticalFrame *vert2 = new TGVerticalFrame(horz);
   horz->AddFrame(vert2, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
	// Select tif file
   TString cmd("SDControlBar::SelectTifFileDialog()");
   TGTextButton* select = new TGTextButton(vert2, "Select tif file",cmd.Data());
   vert2->AddFrame(select, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 3, 2, 2, 2));

//   TGLabel *label2 = new TGLabel(vert2, "Test");
//   vert2->AddFrame(label2, new TGLayoutHints(kLHintsLeft | kLHintsCenterY));
}

void SDConfigurationInfo::PrintInfo()
{
	cout<<" Configuration information : "<<GetName()<<endl;
	cout<<(*this);
}

ostream& operator<<(ostream &os, const parsInputOutput &pio)
{
	return os << "dir " << pio.dir << " name " << pio.name;
}

ostream& operator<<(ostream &os, const parsFillHists &pfh)
{
	return os << "scale "<<pfh.scale<<" ndiv " << pfh.ndiv;
}

ostream& operator<<(ostream &os, const parsPedestals &peds)
{
	return os << " algo " << peds.algo << " smooth " << peds.smooth << " set "
			<< peds.set << " nsig " << peds.nsig << " chi2cut " << peds.chi2cut
			<< " sigcut " << peds.sigcut;
}
ostream& operator<<(ostream &os, const parsClusterFinder &cf)
{
	return os << "name " << cf.name << " seed " << cf.seed << " spacescale "
			<< cf.spacescale << " um/px  timescale " << cf.timescale<<" ms/px "<<endl
			<< " minpixels    " << cf.minpixels<<endl
			<< " min size     " << cf.minsize<<" (pxs) "<<cf.minsizeInPxs<<" (Y)\n"
			<< " min duration " << cf.minduration<<" (pxs) "<<cf.mindurationInPxs<<" (X)";
}

ostream& operator<<(ostream &os, const parsSdThreads &th)
{
	return os<<" max threads "<<th.GetMaxThreads()
			 <<" fKeep "<<th.GetKeep()<<" #cpus "<<th.GetNcpus();
}

ostream& operator<<(ostream &os, const parsSdScan &sc)
{
	return os<<" Np Scan "<<sc.fNpScan<<" Min Step "<<sc.fMinStep;
}

ostream& operator<<(ostream &os, const parsTransformation& trans)
{
	return os<<"fRotate90 "<<trans.GetRotate90()<<" fFlipVert "<<trans.GetFlipVert()
			<<" fCombine "<<trans.GetCombine();
}

ostream& operator<<(ostream &os, const SDConfigurationInfo &ci)
{
	os<<ci.fParsInput<<endl;
	os<<ci.fParsFillHists<<endl;
	os<<ci.fParsTransformation;
	os<<ci.fParsThreads<<endl;
	os<<ci.fParsPedestals<<endl;
	os<<ci.fParsClusterFinder<<endl;
	os<<ci.fParsOutput<<endl;
	os<<ci.fParsScan<<endl;
	return os;
}

