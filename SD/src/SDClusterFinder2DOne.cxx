/*
 * SDClusterFinder2DOne.cpp
 *
 *  Created on: May 20, 2012
 *      Author: pavlinov
 */

#include "SDManager.h"
#include "SDPedestals.h"
#include "SDClusterFinder2DOne.h"
#include "SDClusterFinder2DTwo.h"
#include "SDClusterInfo.h"
#include "SDCluster.h"
#include "SDAliases.h"

#include <TStyle.h>
#include <TMath.h>
#include <TH2.h>
#include <TCanvas.h>
//#include <TThread.h>
// #include <pthread.h>

#include "iostream"

const char* SDClusterFinder2DOne::gkCLFPool="CLF Pool";     // name of cluster finder pool

using namespace std;

ClassImp(SDClusterFinder2DOne)

SDClusterFinder2DOne::SDClusterFinder2DOne() {
// for reading only
}

SDClusterFinder2DOne::SDClusterFinder2DOne(const char* name) : SDClusterFinderStdOne(name) {
	TList *lsum = BookSummaryHists();
	GetList()->Add(lsum);
//	lsum->ls();
}

SDClusterFinder2DOne::~SDClusterFinder2DOne() {
	// TODO Auto-generated destructor stub
}

SDClusterFinderStdOne* SDClusterFinder2DOne::GetCLF(Int_t ind)
{
	TDataSet* ds = GetCLFPool();
	if(ds) return dynamic_cast<SDClusterFinderStdOne *>(ds->At(ind));
	else   return 0;
}

Int_t SDClusterFinder2DOne::FindClusters()
{
	// 2012 : May 20
	//      : Oct 23 - keyNewCluster
	//      : Nov 13 - key2Dtwo
	//      : Nov 20 - selection of histogramm range for testing
	// Cluster finder on 2D histograms. It is significantly faster
	// due to taking into account the 2D picture of pixels.
	//
	FindClustersStatic(dynamic_cast<SDClusterFinder2DOne *>(this));
	return GetClusters()->GetListSize();
}

Int_t SDClusterFinder2DOne::FindClsutersWithThreads()
{
	// Apr 14-21, 2013
	TThread::Printf("\t\t SDClusterFinder2DOne::FindClustersWithThreads() is started");
	//
	TList* lin = GetImageAnalysis()->GetPedestals()->GetSplittedHists();
	if(lin==0 || lin->GetSize()==0) {
		cout<<"<W> lin or linGetSize() == 0 : lin "<<lin<<endl;
		return 0; // Just for sure
	}

    TDataSet *ds = new TDataSet(gkCLFPool);
    Add(ds);

    // Create bunch of cluster finders
    SDClusterFinder2DOne *clf = 0;
    TString snam;
    vector<TThread *> vthreads;
    Int_t i1 = 0, i2 = lin->GetSize();

   // i2 = 1;
    for(Int_t i=i1; i<i2; ++i) {
   	    snam.Form("%s %i",GetName(),i);
    	if(dynamic_cast<SDClusterFinder2DTwo *>(this)) {
    	   clf = new SDClusterFinder2DTwo(snam.Data());
    	} else {
     	   clf = new SDClusterFinder2DOne(snam.Data());;
    	}
    	ds->Add(clf);
    	// Look to SDClusterFinderStdOne* SDImageAnalysis::InitClusterFinder()
    	clf->SetCFPars(GetPars());
    	clf->Init();
    	TH2 *h2th = sda::GetH2(lin,i);
    	clf->SetInputHist(h2th);
    	clf->SetSeed(GetSeed());
    	clf->SetOnlineDrawing(0);
    	// clf->SetThreadsStatus(kTThreads);
    	// clf->SetDebug(2);
    	//
    	snam.Prepend(TString::Format("Th%i",i));
    	clf->SetThreadsStatus(kTThreads);
    	void* arg = (void*)clf;
    	TThread *th = new TThread(snam.Data(), SDClusterFinder2DOne::FindClustersStatic, arg);
    	th->Run();
    	vthreads.push_back(th);
    }
    // TThread::Ps();
    for(UInt_t i=0; i<vthreads.size(); ++i) vthreads[i]->Join();
    // TThread::Ps();
    for(UInt_t i=0; i<vthreads.size(); ++i) vthreads[i]->Delete();

    Int_t nclsts = MergeClusterFinders(ds);

	TThread::Printf("\t\t SDClusterFinder2DOne::FindClustersWithThreads() is ended\n");
	return nclsts;
}

Int_t SDClusterFinder2DOne::FindClsutersWithPosixThreads()
{
	// Apr 14, 2013
	TThread::Printf("\t\t SDClusterFinder2DOne::FindClustersWithPosixThreads() is started");
#ifndef WIN32
	size_t newStackSize = 4*1024*1024;
	sdu::SetPThreadStacksize(newStackSize);
#endif
	//
	TList* lin = GetImageAnalysis()->GetPedestals()->GetSplittedHists();
	if(lin==0 || lin->GetSize()==0) return 0; // Just for sure

    TDataSet *ds = new TDataSet(gkCLFPool);
    Add(ds);

    // Create bunch of cluster finders
    SDClusterFinder2DOne *clf = 0;
    TString snam;
    UInt_t i1=0, i2 = lin->GetSize();
    pthread_attr_t vattr[2]; // temporary
    pthread_t      vpthreads[2];

 // i1=0, i2 = 1;
    for(UInt_t i=i1; i<i2; ++i) {
   	    snam.Form("%s %i",GetName(),i);
    	if(dynamic_cast<SDClusterFinder2DTwo *>(this)) {
    	   clf = new SDClusterFinder2DTwo(snam.Data());
    	} else {
     	   clf = new SDClusterFinder2DOne(snam.Data());;
    	}
    	ds->Add(clf);
    	// Look to SDClusterFinderStdOne* SDImageAnalysis::InitClusterFinder()
    	clf->SetCFPars(this->GetPars());
    	clf->Init();
    	TH2 *h2th = sda::GetH2(lin,i);
    	clf->SetInputHist(h2th);
    	clf->SetSeed(this->GetSeed());
    	clf->SetOnlineDrawing(0);
    	// clf->SetThreadsStatus(kPosixThreads);
    	// clf->SetDebug(2);
    	//
    	pthread_attr_init(&vattr[i]);
	    Int_t returnVal = pthread_attr_setdetachstate(&vattr[i], PTHREAD_CREATE_JOINABLE);
	    assert(!returnVal);

	   	clf->SetThreadsStatus(kPosixThreads);
    	void* arg = (void*)clf;
 		int threadError = pthread_create(&vpthreads[i], &vattr[i], SDClusterFinder2DOne::FindClustersStatic, arg);
		assert(!threadError);

//    	vattr.push_back(&attr);
//    	vpthreads.push_back(&posixThread);
    	TThread::Printf("\t\t %i Posix thread is started : %p : theaderror %i",
    			i, &vpthreads[i], threadError);
    }
    for(UInt_t i=i1; i<i2; ++i) {
    	void *status;
    	Int_t rc = pthread_join(vpthreads[i], &status);
        TThread::Printf("%i threads: ERROR return code from pthread_join() is %d : thread %p :status %ld",
        		i, rc, &vpthreads[i], (long)status);
        if (rc) assert(0);
    }

	Int_t nclsts = MergeClusterFinders(ds);

//	pthread_exit(NULL);
	TThread::Printf("\t\t SDClusterFinder2DOne::FindClustersWithPosixThreads() is ended\n");

	return nclsts;
}

void* SDClusterFinder2DOne::FindClustersStatic(void* arg) // static
{
	SDClusterFinder2DOne *clf = (SDClusterFinder2DOne*)arg;

	// TThread::Printf(" SDClusterFinder2DOne::FindClustersStatic : name %s", clf->GetName());
	Int_t deb = clf->GetDebug();
	TH2* hin       = sda::GetH2(clf->GetMainHists(),0);
    TH2* h2Remnant = sda::GetH2(clf->GetMainHists(),1);
	if(hin==0 || h2Remnant==0) {
        cout<<" hin or h2Remnant is zero! \n";
		return 0;
	}
	TCanvas*  c = 0;
	if(clf->GetOnlineDrawing()>0) c = clf->DrawInputHistInCanvas(hin, clf->GetCanvas());

	TDataSet* clusts = clf->GetClusters();
	clusts->Delete();

	Int_t nused=0, binx=0, biny=0, binz=0;
	Int_t resetPixel = 1;
    Int_t key2Dtwo = 0;
	pixel pixel;
    UShort_t binxmin,binxmax, binymin,binymax;
	if(deb>=1) {
		cout<<"\n\t SDClusterFinder::FindClusters2DOne()  : "<<hin->GetEntries()<<"(h)"<<endl;
		cout<<"name "<<hin->GetName()<<"\n tit "<<hin->GetTitle()<<" seed "<<clf->GetSeed()<<endl;
		cout<<" Call from class "<<clf->ClassName()<<endl;
	}

    UShort_t hbinxmin = 1, hbinymin = 1;
	Double_t maxIn = hin->GetMaximum();
    if(hin->GetXaxis()->GetFirst()>0) hbinxmin = hin->GetXaxis()->GetFirst();
    if(hin->GetYaxis()->GetFirst()>0) hbinymin = hin->GetYaxis()->GetFirst();
    // shift between (col, binx) and (row, biny) -> Apr 20,2013
    UInt_t colShift = hbinxmin - static_cast<UShort_t>(hin->GetXaxis()->GetBinCenter(hbinxmin));
    UInt_t rowShift = hbinymin - static_cast<UShort_t>(hin->GetYaxis()->GetBinCenter(hbinymin));

    UShort_t hbinxmax = hin->GetNbinsX(), hbinymax = hin->GetNbinsY();
    if(hin->GetXaxis()->GetLast()>0) hbinxmax = hin->GetXaxis()->GetLast();
    if(hin->GetYaxis()->GetLast()>0) hbinymax = hin->GetYaxis()->GetLast();
	if (deb >= 1) {
		TThread::Printf("<I> x range %i->%i  : y range %i -> %i : colShift %i ", hbinxmin,
				hbinxmax, hbinymin, hbinymax, colShift);
		TThread::Printf(" First maximum value %6.4f seed %6.4f : rowShift %i ",
				h2Remnant->GetMaximum(), clf->GetSeed(), rowShift);
	}

    parsClusterFinder pars = clf->GetPars();
    Int_t keyNewClusterMax = 2; // Nov 13,2012
    // Infinite cycle on clusters
    while(1)
	{
       NEWCLUSTER:
        Int_t keyNewCluster = key2Dtwo = 0;

		pixel.amp = static_cast<Float_t>(h2Remnant->GetMaximum());
	    if(pixel.amp < pars.seed) break; // end of cluster finder

	    // Return location of bin with maximum value in the range
	    h2Remnant->GetMaximumBin(binx,biny,binz);
	    SDUtils::FillPixel(h2Remnant, binx,biny,pixel, resetPixel);
	    ++nused;

	    SDClusterInfo *clstInfo = new SDClusterInfo(clusts->GetListSize());
	    SDClusterBase *clst = clstInfo->GetCluster();
	    clst->AddPixel(pixel);
 	    if(deb >= 4) {cout<<"** "<<clst->GetClusterSize()<<" "; cout<<pixel;}

	    UPDATECLUSTER:
	    // Cycle on neighbors only : Apr 20,2013
	    binxmin = hin->GetXaxis()->FindFixBin(clstInfo->GetClstCharFromF0().colmin) - 1;
	    binxmax = hin->GetXaxis()->FindFixBin(clstInfo->GetClstCharFromF0().colmax) + 1;
//	    binxmin = sdu::GetBinFromColRow(clstInfo->GetClstCharFromF0().colmin,colShift) - 1;
//	    binxmax = sdu::GetBinFromColRow(clstInfo->GetClstCharFromF0().colmax,colShift) + 1;
	    binxmin = binxmin>hbinxmin?binxmin:hbinxmin; // Check boundaries
	    binxmax = binxmax<hbinxmax?binxmax:hbinxmax;

	    binymin = hin->GetYaxis()->FindFixBin(clstInfo->GetClstCharFromF0().rowmin) - 1;
	    binymax = hin->GetYaxis()->FindFixBin(clstInfo->GetClstCharFromF0().rowmax) + 1;
	    binymax = binymax<hbinymax?binymax:hbinymax; // Check boundaries
	    binymin = binymin>hbinymin?binymin:hbinymin;

	    Int_t nadded  = 0; // number of added around box
        // July 26,2012
	    CYCLES:
	    for (UShort_t binx = binxmin; binx <= binxmax; ++binx) {
			UInt_t step = 1; // for left(right) column
			if (binx > binxmin && binx < binxmax) step = binymax - binymin;
			if (key2Dtwo) step = 1; // Force searching inside all rectangular
			for (UShort_t biny = binymin; biny <= binymax; biny += step) {
				pixel.amp = static_cast<Float_t>(h2Remnant->GetBinContent(binx,biny));
				if (pixel.amp > 0.0001) { // skip empty bin
					SDUtils::FillPixel(h2Remnant, binx, biny, pixel, 0);
					if (clf->IsPixelNeighbourToCluster(pixel, clst)) { // cycle on cluster pixels is here
						h2Remnant->SetBinContent(binx, biny, 0.0);
						clst->AddPixel(pixel);
						++nadded;
						if (deb >= 4) cout << clst->GetClusterSize() << " "<< pixel;
						goto CYCLES;
					}
				}
			}
		}
	    if(nadded==0 && keyNewCluster>=keyNewClusterMax) {
		      if(deb>=2) {
		    	  TThread::Printf(" %i cluster : %i remains ",clusts->GetListSize(), (int)(hin->GetEntries() - nused));
		    	  TThread::Printf("%i is used", nused);
		    	  TThread::Printf(" : clst size %i\n",clst->GetClusterSize());
		      }
		      if(clf->IsThisClusterReal(*clstInfo, &pars)) { // Accept cluster
			    clusts->Add(clstInfo);
			    Int_t ic = static_cast<Int_t>(clusts->GetListSize());
				if(clf->GetOnlineDrawing()) clf->DrawClusterBox(c, clstInfo, ic);
		        if(clf->GetOnlineDrawing() && c) c->Update();
		      } else {                                   // Discard cluster
		    	  delete clstInfo;
		      }
	 	      goto NEWCLUSTER;
	    } else {
		  if(nadded>0) {
			  nused += nadded;
		  } else {
			 key2Dtwo = 1;
		    ++keyNewCluster;
		  }
	      goto UPDATECLUSTER;
	    }
	} // End of pixel cycle

	Int_t clstsReal = clusts->GetListSize();

	if (clf->GetOnlineDrawing() >= 0 && !clf->IsThreadsAvailable()) {
        // Should execute when no threads
		h2Remnant->SetMaximum(maxIn); // For drawing
		Int_t mode = 1; // Int_t pri = mode / 10;
		gStyle->SetOptDate();
		clf->SortAndRenameClusters(mode);
		if (clf->GetOnlineDrawing() > 0) {
			clf->DrawInputHistInCanvasWithClusters(1);
			clf->DrawFinalMessage(c, clstsReal, clusts->GetListSize());
		}
	}

	return (void*)clstsReal;
}
