/*
 * SDEllipsoid.cpp
 *
 *  Created on: Dec 29, 2012;
 *      2013 : 30 Jan;
 *      Author: Alexei Pavlinov
 */

#include "SDEllipsoid.h"
#include "sdCommon.h"

#include <iostream>
#include <cassert>

using namespace std;

/*       Ellipsoid      F o r m u l a e
 * f(x,y) = [0]*sqrt(1.-(x-[1])^2/[2]^2-(y-[3])/[4]^2)
 * a,b,h - semi-principal axes
 * [0]     - h, maximum value
 * [1],[2] - x(mean) and a
 * [3],[4] - y(mean) and b
 * f(x) = (pi/2)*h*b(1-x^2/a^2) : projection on X axis
 * FWHMX(duration) = sqrt(2)*a;
 * f(y) = (pi/2)*h*a(1-x^2/b^2) : projection on X axis
 * FWHMX(space)    = sqrt(2)*b;
 */

SDEllipsoid::SDEllipsoid() : SimResponseFunction()
{
  // For reading only
}

SDEllipsoid::SDEllipsoid(const char* name, Double_t par0,Double_t par1,Double_t par2,Double_t par3,Double_t par4)
: SimResponseFunction(name)
{
	// x(y) dimension in pixels
	// fNpar = 5;

	fTF2 = new TF2("ell", ellipsoid, 0., fgXmax, 0., fgYmax, 5);
	fTF2->SetParameters(par0,par1,par2,par3,par4);
    InitEllipsoid();
	// Init(); Should call separately
}

SDEllipsoid::SDEllipsoid(const char* name, Double_t par0,Double_t par2, Double_t par4)
: SimResponseFunction(name)
{
    Double_t par1=30., par3=30.;
	fTF2 = new TF2("ell", ellipsoid, 0., fgXmax, 0., fgYmax, 5);
	fTF2->SetParameters(par0,par1,par2,par3,par4);
    InitEllipsoid();
}

SDEllipsoid::SDEllipsoid(const char* name, const Double_t* pars, const Int_t N)
: SimResponseFunction(name)
{
	TArrayD arr(5);
	if(N==3) {
	  arr[0] = pars[0];
	  arr[1] = 1023.;
	  arr[2] = pars[1];
	  arr[3] = 255.;
	  arr[4] = pars[2];
	} else if (N==5) {
	  for(Int_t i=0; i<5; ++i) arr[i] = pars[i];
	} else {
		cout<<" N = "<<N<<" dhould be 3 or 5 \n";
		assert(0);
	}
    fTF2 = new TF2(name, ellipsoid, 0., fgXmax, 0., fgYmax, 5);
    fTF2->SetParameters(arr.GetArray());
    InitEllipsoid();
}

void SDEllipsoid::InitEllipsoid()
{
	//	TFormula zs("zs", "(1.-pow((x-[1])/[2],2)-pow((y-[3])/[4],2))");
	sdCommon *sdc = sdCommon::GetSdCommon();
	funPair fp = sdc->nameOfFuns[0];

	SetName(fp.first.c_str());
	TString stmp(GetName());
	stmp.Prepend("f");
	fTF2->SetName(stmp.Data());
	fTF2->SetNpx(Int_t(fgXmax));
	fTF2->SetNpy(Int_t(fgYmax));

	for (UInt_t i = 0; i < fp.second.size(); ++i)
		fTF2->SetParName(i, (fp.second)[i].c_str());
    // Bits
	SetBit(i0,   kTRUE);
	SetBit(i0+1, kTRUE);
}

SDEllipsoid::~SDEllipsoid() {
	// TODO Auto-generated destructor stub
}

void SDEllipsoid::DefineAnaliticalSignature()
{
	// 30 Jan,2013;
    TF2* f = GetFunction();
    sdCommon *sdc = sdCommon::GetSdCommon();
	funPair fp = sdc->nameOfFuns[0];
    fSignature = "";
    for(UInt_t i=0; i<(fp.second).size(); i+=2)
    fSignature += Form("%s%3.1f", (fp.second)[i].c_str(), f->GetParameter(i));
}

void SDEllipsoid::CalculateAnalyticalSolution()
{
	fAnaPars.maxamp   = fTF2->GetParameter(0);
	fAnaPars.aveamp   = (2./3.)*fTF2->GetParameter(0);
	fAnaPars.duration = TMath::Sqrt(2.)*fTF2->GetParameter(2); // FWHMX
	fAnaPars.size     = TMath::Sqrt(2.)*fTF2->GetParameter(4); // FWHMY
	fAnaPars.numpixels = (UInt_t)(TMath::Pi()*fTF2->GetParameter(2)*fTF2->GetParameter(4)); // Square of ellipse
	// Just for completeness
	fAnaPars.xhm1     = fTF2->GetParameter(1) - fTF2->GetParameter(2)/TMath::Sqrt(2.);
	fAnaPars.xhm2     = fTF2->GetParameter(1) + fTF2->GetParameter(2)/TMath::Sqrt(2.);
	fAnaPars.yhm1     = fTF2->GetParameter(3) - fTF2->GetParameter(4)/TMath::Sqrt(2.);
	fAnaPars.yhm2     = fTF2->GetParameter(3) + fTF2->GetParameter(4)/TMath::Sqrt(2.);
}

Double_t ellipsoid(Double_t* var, Double_t* par)
{
/*   * f(x,y) = [0]*sqrt(1.-(x-[1])^2/[2]^2-(y-[3])/[4]^2)
	 * a,b,h - semi-principal axes
	 * [0]     - h, maximum value
	 * [1],[2] - x0 and a
	 * [3],[4] - y0 and b
*/
	Double_t x  = var[0], y  = var[1], z = 0.0;

	Double_t h  = par[0];
	Double_t x0 = par[1];
	Double_t a  = par[2];
	Double_t y0 = par[3];
	Double_t b  = par[4];

	Double_t zs = (1.-pow((x-x0)/a,2)-pow((y-y0)/b,2));
	if(zs>=0) z = h*TMath::Sqrt(zs);
	return z;
}

ClassImp(SDEllipsoid)
