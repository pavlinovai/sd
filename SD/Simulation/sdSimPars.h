/*
 * sdSimPars.h
 *
 *  Created on: Dec 22, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDSIMPARS_H_
#define SDSIMPARS_H_

#include <TString.h>
#include <TArrayD.h>

class sdSimPars {
public:
	sdSimPars();
	sdSimPars(int var);
	virtual ~sdSimPars();
	// Elipsoid
	void InitEllipsoid(int var);
	//
	Int_t   nev;        // numeber of events
	//
	TString simfun;     // Name of function
	TArrayD parsfun;    // simfun pars
	TString f0Name;     // Name of F0 class
	TArrayD parsF0;     // pars of F0

	ClassDef(sdSimPars,1)
};

#endif /* SDSIMPARS_H_ */
