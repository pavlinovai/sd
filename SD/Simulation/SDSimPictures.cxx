/*
 * SDSimPictures.cpp
 *
 *  Created on: Jan 13, 2013
 *      Author: Alexei Pavlinov
 */

#include "SDSimPictures.h"
//#include "SDPictures.h"
#include "SDAliases.h"
#include "SDUtils.h"
#include "SDSimulationManager.h"
#include "SimResponseFunction.h"
#include "SDConfigurationParser.h"
#include "SDConfigurationInfo.h"
#include "SDPedestals.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TFile.h>
#include <TBrowser.h>
#include <TCanvas.h>
#include <TH2.h>
#include <TF1.h>
#include <TF2.h>
#include <TText.h>
#include <TLine.h>
#include <TList.h>
#include <TBox.h>

ClassImp(SDSimPictures)

SDSimPictures::SDSimPictures(const Char_t* dir) : SDObjectSet("SDSimPictures"),
fDir("/Users/pavlinov/wrk/sdoutput/SIMULATION/"), fSimPool(new TDataSet("SimPool")), fSimMan(0)
{
    gROOT->GetListOfBrowsables()->Add(this);
	Add(fSimPool);
	if(strlen(dir)) fDir=dir;
}

SDSimPictures::~SDSimPictures() {
	// TODO Auto-generated destructor stub
}

SDSimulationManager* SDSimPictures::ReadSimObject()
{
    // Users/pavlinov/wrk/sdoutput/NOV2012/4_ca100_red_channel
	fSimMan = dynamic_cast<SDSimulationManager *>(sda::ReadFirstObjectFromDirectory(fDir.Data()));
	fSimPool->Add(fSimMan);
//	TString st = fSimMan->;
//	SetName(Form("%s %s", GetName(), st.Data()));
	return fSimMan;
}

TCanvas* SDSimPictures::DrawAnaVsTH2()
{
	// Pict.1 - demonstrate the analytical vs TH2
//	TCanvas* c = sda::Canvas("cf0p1","cf0p1",20,20,600,400);
//	c->Divide(3,2);

	TList* l = fSimMan->GetFunction()->GetAnaVsTH2Hists();
	TCanvas* c = sda::DrawListOfHist(l,3,2, 6,0);
	c->SetWindowSize(600, 400);
	c->Update();

	// Prepared pad for test output
	c->cd(6);
	TBox* box = new TBox(0.,0., 1.,1.);
	box->Draw();

	c->Update();

	return c;
}
