/*
 * SimSimpleGauss.cpp
 *
 *  Created on: Dec 15, 2012
 *      Author: Alexei Pavlinov
 */

#include "SimSimpleGauss.h"
#include "SDF0Base.h"

#include <TH2.h>
#include <TIterator.h>

#include <iostream>
using namespace std;;

/*       F o r m u l a s
 * f(x,y) = [0]*exp(-0.5*((x-[1])/[2])**2) * exp(-0.5*((y-[3])/[4])**2)
 * [0]     - A, function normalization
 * [1],[2] - x(mean) and sigma(x)
 * [3],[4] - y(mean) and sigma(y)
 * f(x) = [0]*exp(-0.5*((x-[1])/[2])**2) : projection on X axis
 * f(y) = [0]*exp(-0.5*((y-[3])/[4])**2) : projection on Y axis
 * FWHM = 2*sqrt(2*ln2)*sig = 2.3548*sig
 */

const char* snam="SimpleGuass";
const char* sfun="[0]*exp(-0.5*((x-[1])/[2])**2)*exp(-0.5*((y-[3])/[4])**2)";

SimSimpleGauss::SimSimpleGauss()
{
   // x(y) dimension in pixels
   // fNpar = 5;
   // Default parameters
   fSDF0 = 0;
   SetName(snam);
   TString stmp(snam);
   stmp.Prepend("f");
   TF2* f2 = new TF2(stmp.Data(),sfun,0.,fgXmax,0.,fgYmax);
   GetList()->AddAt(f2, 0);
   f2->SetParameters(1.,50.,10., 50.,10.);
   f2->SetNpx(Int_t(fgXmax));
   f2->SetNpy(Int_t(fgYmax));
   const char* parNames[] = {"A","x_{0}","#sigma_{x}", "y_{0}","#sigma_{y}"};
   int n = sizeof(parNames)/sizeof(char*);
   for(int i=0; i<n; ++i) f2->SetParName(i, parNames[i]);
}

SimSimpleGauss::~SimSimpleGauss()
{
//  delete fSDF0;
}

void SimSimpleGauss::ClearHists()
{
   // Delete all histogramms from list
   if (GetList()->GetSize() == 1) return;

   TIter next1(GetList(), kIterBackward);
   TObject* obj = 0;
   while ((obj = next1())) {
      if (GetList()->GetSize() > 1) {
         GetList()->Remove(obj);
         delete obj;
      }
   }
}

void SimSimpleGauss::SetPars(Double_t a0, Double_t sigx, Double_t sigy)
{
	GetFunction()->SetParameter(0,  a0);
	GetFunction()->SetParameter(2,sigx);
	GetFunction()->SetParameter(4,sigy);
}

TH2* SimSimpleGauss::CreateTH2()
{
  TH1::AddDirectory(kFALSE); // Should be here

  TString snam(GetName()), stit(GetName());
  snam += "AsTH2";
  stit += " : ";
  TF2* f = GetFunction();
  for(Int_t i=0; i<f->GetNpar(); ++i) stit += Form("%s %4.2f, ",f->GetParName(i), f->GetParameter(i));

//  fTH2 = new TH2F(snam, stit, GetNpx(),0.0,GetXmax(), GetNpy(),0.0,GetYmax());
  TH2* h = dynamic_cast<TH2 *>(f->CreateHistogram());
  if(h) {
    h->SetName(snam);
    h->SetTitle(stit);
    h->SetOption("colz");
    GetList()->AddAt(h,1);
  }
  return h;
}

TH2* SimSimpleGauss::CreateTH2plusF0()
{
    TH2* hin = GetFunctionAsTH2();
	TString snam(Form("%s%s",hin->GetName(),fSDF0->GetName()));
	TH2* hout = (TH2*)hin->Clone(snam);
	TString stit(Form("%s + %s",hin->GetTitle(),fSDF0->GetName()));
	hout->SetTitle(stit);
	for(Int_t binx=1; binx<=hout->GetNbinsX(); ++binx){
		for(Int_t biny=1; biny<=hout->GetNbinsY(); ++biny){
//			Double_t x    = fTH2PlusF0->GetXaxis()->GetBinCenter(binx);
//			Double_t y    = fTH2PlusF0->GetYaxis()->GetBinCenter(biny);
			Double_t f0   = 1; // on definition
			Double_t fun  = hin->GetBinContent(binx,biny);
			hout->SetBinContent(binx, biny, f0 + fun);
			Double_t ey = 0.;
			if(ey>0.0) hout->SetBinError(binx, biny, ey);
		}
	}
	hout->SetOption("colz");
    GetList()->AddAt(hout,2);
	return hout;
}

void SimSimpleGauss::CutTH2plusF0(TH2* h2in, Double_t th)
{
	// Prepared hist for cluster finding
	if (th < 0.0 || h2in==0) return;

	for (Int_t binx = 1; binx <= h2in->GetNbinsX(); ++binx) {
       for	(Int_t biny=1; biny<=h2in->GetNbinsY(); ++biny) {
		  Double_t f = h2in->GetBinContent(binx,biny) - 1.;
		  if(f<th) h2in->SetBinContent(binx,biny, 0.0);
		  else     h2in->SetBinContent(binx,biny, f);
//		  cout<<" bin "<<binx<<":"<<biny<<" f-1."<<f<<endl;
	   }
    }
}

TH2* SimSimpleGauss::CreateTH2plusF0Raw()
{
	return 0;
}

ClassImp(SimSimpleGauss)
