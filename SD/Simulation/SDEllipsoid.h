/*
 * SDEllipsoid.h
 *
 *  Created on: Dec 29, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDELLIPSOID_H_
#define SDELLIPSOID_H_

#include "SimResponseFunction.h"

class SDEllipsoid: public SimResponseFunction {
public:
	SDEllipsoid();
	SDEllipsoid(const char* name, Double_t par0,Double_t par1,Double_t par2,Double_t par3,Double_t par4);
	SDEllipsoid(const char* name, Double_t par0, Double_t par2, Double_t par4);
	SDEllipsoid(const char* name, const Double_t* pars, const Int_t N);
	virtual ~SDEllipsoid();
	void InitEllipsoid();

	virtual void DefineAnaliticalSignature();

	virtual void CalculateAnalyticalSolution();
	//
	// (x-x0)**2/a**a+ (y-y0)**2/b**2 + z**2/h**2=1
	//
    // void SetPars(Double_t h=1., Double_t x0=30., Double_t a=10., Double_t y0=30., Double_t b=10.); // *MENU*

    ClassDef(SDEllipsoid,1)
};
// Posiive ellipsoid function
Double_t ellipsoid(Double_t* var, Double_t* par);

#endif /* SDELLIPSOID_H_ */
