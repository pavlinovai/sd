/*
 * SDSimulationManager.cpp
 *
 *  Created on: Dec 22, 2012
 *      Author: Alexei Pavlinov
 */

#include "SDSimulationManager.h"
#include "SDF0Uniform.h"
#include "SimSimpleGauss.h"
#include "SDEllipsoid.h"
#include "SDCluster2D.h"
#include "SDClusterFinderBase.h"
#include "SDClusterFinder2DOne.h"
#include "SDPedestals.h"
#include "SDConfigurationInfo.h"
#include "SDUtils.h"
#include "sdCommon.h"

#include <TROOT.h>
#include <TH2.h>
#include <TFile.h>
#include <TRandom1.h>
#include <TGProgressBar.h>

#include <cassert>
#include <memory>

ClassImp(SDSimulationManager)

SDSimulationManager::SDSimulationManager() : SDObjectSet("SDSimulationManager"),
fSimPars(), fXmin(-1.),fXmax(-1.),fXstep(-1.), fYmin(-1.),fYmax(-1.),fYstep(-1.)
{}

SDSimulationManager::SDSimulationManager(Int_t nev, const Char_t* funName, Double_t* pars, Int_t npar) :
	SDObjectSet("SDSimulationManager"), fSimPars(), fXmin(-1.),fXmax(-1.),fXstep(-1.),
	fYmin(-1.),fYmax(-1.),fYstep(-1.)
{
	fSimPars.nev    = nev;
	fSimPars.simfun = funName;
	fSimPars.parsfun.Set(npar, pars);
	//
	Init();
}

SDSimulationManager::SDSimulationManager(sdSimPars& simPars) :
			SDObjectSet("SDSimulationManager"), fSimPars(simPars), fXmin(-1.),fXmax(-1.),fXstep(-1.),
			fYmin(-1.),fYmax(-1.),fYstep(-1.)
{
	Init();
}

SDSimulationManager::~SDSimulationManager() {
	// TODO Auto-generated destructor stub
}

void SDSimulationManager::Init()
{
	SimResponseFunction* fsim = GetAnalyticalFunction(fSimPars.simfun, fSimPars.parsfun);
	Add(fsim);
	gROOT->GetListOfBrowsables()->Add(this);

	SDF0Base* f0 = new SDF0Uniform("");
	//Double_t f0sig = 0.001;
	//fF0->SetSigF0Unit(f0sig);
	f0->Init();
	fsim->SetF0(f0);
	fsim->Init();
    //
	// Reconstruction
	//
	InitClusterFinder();
}

SDClusterFinderStdOne* SDSimulationManager::InitClusterFinder()
{
    parsClusterFinder pcf;
    pcf.drawing = 0;
    // For comparing in pixels : Apr 2,2013
    pcf.spacescale = 1.;
    pcf.timescale  = 1.;
    SDClusterFinderStdOne* clf = sdu::InitClusterFinder(&pcf, 0);
	Add(clf);
	return clf;
}

SimResponseFunction* SDSimulationManager::GetFunction()
{
  return dynamic_cast<SimResponseFunction *>(At(0));
}

SDClusterFinderStdOne* SDSimulationManager::GetClusterFinder()
{
	return dynamic_cast<SDClusterFinderStdOne *>(At(1));
}

SDPedestals* SDSimulationManager::GetPedestals()
{
	return dynamic_cast<SDPedestals *>(At(2));
}

Int_t SDSimulationManager::RunOne()
{
    auto_ptr<TGProgressBar> pb(sdu::GetHProgressBar(400));
    sdu::UpdateProgressBar(pb.get(),0,fSimPars.nev);

    SimResponseFunction *fsim = GetFunction();
    SDPedestals *peds = GetPedestals();
    SDClusterFinderStdOne *clf = GetClusterFinder();
    TString clfname(clf->GetName());

    TRandom1 rndm;
    TF2* f = fsim->GetFunction();
    fXmin = f->GetParameter(2)+0.5, fXmax = 2048. - fXmin, fXstep = fXmax - fXmin;
    fYmin = f->GetParameter(4)+0.5, fYmax =  512. - fYmin, fYstep = fYmax - fYmin;

    Int_t i = 0, iupdate=fSimPars.nev/100;
    iupdate = iupdate>0?iupdate:1;
    Double_t th=0.001;
//    return 0;
    TH2 *hthcut = 0;
	for(i=0; i<fSimPars.nev; ++i){
	   clf->SetName(clfname);
       Double_t x = fXmin + fXstep*rndm.Rndm();
       Double_t y = fYmin + fYstep*rndm.Rndm();
       f->SetParameter(1,x);
       f->SetParameter(3,y);

       TString opt("");
       if(peds) {
           fsim->FillAll(th, i, opt);
       } else {
    	  opt = "mean1x8";
    	  th = 0.4 / TMath::Sqrt(3.*25) * 3;
          fsim->FillAll(th, i, opt);

    	  TH2* h2PlusF0 = fsim->GetFunctionPlusF0AsTH2();
    	  if(h2PlusF0 && h2PlusF0->Integral()>1000.) {
    	    TH2* hsmooth  = fsim->GetFunctionPlusF0AsTH2Smooth();
    	    sdu::Smooth(h2PlusF0, opt, hsmooth);

    	    hthcut = fsim->GetFunctionPlusF0AsTH2Threshold();
            SDPedestals::FillFOverF0MinusOneWithCut(hsmooth, th, hthcut);

    	    clf->SetInputHist(hthcut);
    	  }
       }
       if(hthcut) {
          clf->FindClusters();

          if(clf->GetClusters()->GetListSize()==1) { // only one cluster now
    	     sdClusterChar clCh = clf->GetClusterInfo(0)->GetClstCharFromF0();
             sdu::FillHistsPars1VsPars2(fsim->GetTH2VsClfOneHists(), fsim->GetPars(), clCh);
          }
       }

       if(i%iupdate==0 || i+1==fSimPars.nev) sdu::UpdateProgressBar(pb.get(),i+1,fSimPars.nev);
 	}
	if(peds) printf(" peds |%s| \n", peds->GetName());
	else     printf("<I> No pedestals : %p \n", peds);
	return i;
}

void SDSimulationManager::SaveSim(const Char_t* addName)
{
	// Jan 8,2013 - first variant
	TString dir = "$HOME/wrk/sdoutput/SIMULATION/";
	TString sname = dir + GetFunction()->GetAnaliticalSignature();
	if(strlen(addName)) sname += addName;
	sname += ".root";
	TFile f(sname.Data(), "RECREATE");
	if (f.IsOpen()) {
		this->Write();
		f.ls();
		f.Close();
		printf("<I> %s was saved \n to file %s \n", GetName(), sname.Data());
	} else {
		printf("<E> DIDN'T SAVE |%s| to file |%s|\n", GetName(), sname.Data());
	}
}
//
// Static methods for simulation manager
//
SimResponseFunction* SDSimulationManager::GetAnalyticalFunction(const Char_t* funName, const Double_t* pars, const Int_t npar)
{
		// 2013; Jan 12;
	sdCommon *sdc = sdCommon::GetSdCommon();
	SimResponseFunction* simFun = 0;
	if(strcmp(funName,sdc->nameOfFuns[0].first.c_str())==0) {
		simFun = new SDEllipsoid(funName, pars, npar);
	} else {
		cout<<"<E> Analytical function with name |"<<funName<<"| does not exist\n";
		sdc->PrintNameOfAnaFuns();
		assert(0);
	}
	return simFun;
}

SimResponseFunction* SDSimulationManager::GetAnalyticalFunction(const Char_t* funName, const TArrayD &arr)
{
	return GetAnalyticalFunction(funName, arr.GetArray(), arr.GetSize());
}

