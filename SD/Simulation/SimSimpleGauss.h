/*
 * SimSimpleGauss.h
 *
 *  Created on: Dec 15, 2012
 *      Author: Alexei Pavlinov
 *      Should be revised due to problem with mean value - unlimited area of Gaus
 */

#ifndef SIMSIMPLEGAUSS_H_
#define SIMSIMPLEGAUSS_H_

#include "SimResponseFunction.h"
#include <TMath.h>
#include "SDAliases.h"

const Double_t c=2.*TMath::Sqrt(2.*TMath::Log(2.)); // 2*sqrt(2*ln2) = 2.3548

class SimSimpleGauss : public SimResponseFunction {
public:
	SimSimpleGauss();
	virtual ~SimSimpleGauss();
	//
    void SetPars(Double_t a0=1., Double_t sigx=10., Double_t sigy=10.); // *MENU*

    virtual TH2* GetFunctionAsTH2() {return sda::GetH2(GetList(),0);}
	virtual TH2* GetFunctionPlusF0AsTH2() {return sda::GetH2(GetList(),1);}
	virtual TH2* GetFunctionPlusF0AsTH2Raw() {return sda::GetH2(GetList(),2);}
	virtual TH2* CreateTH2();
	virtual TH2* CreateTH2plusF0();
	virtual void CutTH2plusF0(TH2* h2in, Double_t th=0.0);
	virtual TH2* CreateTH2plusF0Raw();
	virtual void ClearHists(); // Delete working histogramm
	//
	virtual Double_t GetFWHMX() {return c*GetFunction()->GetParameter(2);}
	virtual Double_t GetFWHMY() {return c*GetFunction()->GetParameter(4);}
	virtual Double_t GetDuration() {return GetFWHMX();}
	virtual Double_t GetSpace()    {return GetFWHMY();}

    ClassDef(SimSimpleGauss,1)
};

#endif /* SIMSIMPLEGAUSS_H_ */
