/*
 * SimResponseFunction.cpp
 *
 *  Created on: Dec 30, 2012
 *      Author: Alexei Pavlinov
 */
#include "SimResponseFunction.h"
#include "SDF0Base.h"

#include <TH2.h>
#include <TBrowser.h>

ClassImp(SimResponseFunction)

Double_t SimResponseFunction::fgXmax=2048.;
Double_t SimResponseFunction::fgYmax=512.;

const Char_t* SimResponseFunction::fgNamHist0="funHists";      // Hist of function
const Char_t* SimResponseFunction::fgNamHist1="AnaVsTH2Hists"; // Hist of comparing analytical vs TH2 parameters calculation
const Char_t* SimResponseFunction::fgNamProj="ProjInitialTH2";  // projection of initial TH2

const Char_t* SimResponseFunction::fgNamHist2="TH2vsClfOneHists";   // Hist of comparing TH2 pars vs cluster finder one
const Char_t* SimResponseFunction::fgNamHist3="TH2vsClfTwoHists";  // Hist of comparing TH2 pars vs cluster finder two

SimResponseFunction::SimResponseFunction() : SDObjectSet(),
fTF2(0), fSDF0(0)
{
	// Default constructor for reading only
}

SimResponseFunction::SimResponseFunction(const Char_t* name) : SDObjectSet("name"),
		fTF2(0), fSDF0(0)
{
   TList *l = new TList; // Obligatory - have to be here
   l->SetName(fgNamHist0);
   GetList()->Add(l);
   // On default : only TH2 function
   SetBit(i0,   kFALSE);
   SetBit(i0+1, kFALSE);
   SetBit(i0+2, kTRUE);
}

SimResponseFunction::~SimResponseFunction()
{
}

void SimResponseFunction::SetF0(SDF0Base* var)
{
	fSDF0 = var;
	Add(fSDF0);
}

void SimResponseFunction::Init()
{
	InitNamesAndTitles();

	if(IsThisTF2Function()) {
		CreateTH2();
		if(fSDF0) {
			CreateTH2plusF0();
			CreateTH2plusF0Raw();
			CreateTH2Smooth();
			CreateTH2SmoothAndThreshold();
		}
	}

	Int_t pri=0;
	if(IsThisTF2Function() && ExistAnalyticalSolution()) {
		TList *l = BookHistsPars1VsPars2(fgNamHist1, "AnaVsTH2", pri);
		GetList()->Add(l);
		CalculateAnalyticalSolution();
	}
	TList *l = BookHistsPars1VsPars2(fgNamHist2, "TH2VsClfOne", (pri=1));
	GetList()->Add(l);
}

void SimResponseFunction::InitNamesAndTitles()
{
	   // Names & Titles
	   fHistNames.push_back(Form("00_%sAsTH2",GetName()));             // TF2 itself
	   fHistTitles.push_back(Form("%s : ",GetName()));

	   fHistNames.push_back(Form("01_AsTH2+%s",fSDF0->GetName()));     // TF2 + F0
	   fHistTitles.push_back(Form("%s+%s",GetName(),fSDF0->GetName()));

	   fHistNames.push_back(Form("02_AsTH2+%sRaw",fSDF0->GetName()));  // Raw Image -> input for LCR analysis
	   fHistTitles.push_back(Form("%s, Raw(tiff)", fHistTitles[1].c_str()));

	   fHistNames.push_back(Form("03_AsTH2+%s",fSDF0->GetName())); // TF2 + F0 after smoothing (filtering)
	   fHistTitles.push_back(Form("%s, ", fHistTitles[1].c_str()));

	   fHistNames.push_back(Form("04_AsTH2+%s",fSDF0->GetName())); // TF2 + F0 after smoothing (filtering)
	   fHistTitles.push_back(Form("%s, ", fHistTitles[1].c_str()));
/*
	   fHistNames.push_back(Form());
	   fHistTitles.push_back(Form());
*/
}

void SimResponseFunction::ResetFunctionHists(TList *l)
{
   // Reset all histogramms from list
   // TList* l = GetListHists(fgNamHist0);
   if (l == 0) return;

   TList* lpro = GetProjHists();
   if(lpro) {
	   GetList()->Remove(lpro);
	   lpro->Delete("slow");
	   delete lpro;
   }

   TIter next1(l);
   TObject* obj = 0;
   while ((obj = next1())) {
       TList* l = dynamic_cast<TList *>(obj);
       if(l==0) {
	     TH1* h = dynamic_cast<TH1 *>(obj);
	     if(h) h->Reset();
       } else {
    	 ResetFunctionHists(l);
       }
   }
}

TH2* SimResponseFunction::CreateTH2()
{
  cout<<"\t\t<I> SimResponseFunction::CreateTH2() is started \n";
	TH1::AddDirectory(kFALSE); // Obligatory

  TF2* f = GetFunction();
  DefineAnaliticalSignature();
  TString stit(fHistTitles[0].c_str());
  stit += GetAnaliticalSignature();

  TH2* h = dynamic_cast<TH2 *>(f->CreateHistogram());
  h = (TH2*)(h->Clone(fHistNames[0].c_str()));
  if(h) {
    h->SetTitle(stit);
    h->SetOption("colz");
    GetListHists(fgNamHist0)->Add(h);
  }
  cout<<"\t\t<I> SimResponseFunction::CreateTH2() is ended \n";
  return h;
}

TH2* SimResponseFunction::CreateTH2plusF0()
{
	cout<<"\t\t<I> SimResponseFunction::CreateTH2plusF0() is started \n";
    TH2* hin = GetFunctionAsTH2();
	TH2* hout = (TH2*)hin->Clone(fHistNames[1].c_str());
	TString stit(fHistTitles[1].c_str());
	stit += GetAnaliticalSignature();
	hout->SetTitle(stit);

	hout->SetOption("colz");
    GetListHists(fgNamHist0)->Add(hout);
	  cout<<"\t\t<I> SimResponseFunction::CreateTH2plusF0() is endeded : hout-> Intergral() "<<
			  hout->Integral()<<endl;
	return hout;
}

void SimResponseFunction::CutTH2plusF0(TH2* h2in, Double_t th)
{
	// Prepared hist before filtering
	if (th < 0.0 || h2in==0) return;

	for (Int_t binx = 1; binx <= h2in->GetNbinsX(); ++binx) {
       for	(Int_t biny=1; biny<=h2in->GetNbinsY(); ++biny) {
		  Double_t f = h2in->GetBinContent(binx,biny) - 1.;
		  if(f<th) h2in->SetBinContent(binx,biny, 0.0);
		  else     h2in->SetBinContent(binx,biny, f);
	   }
    }
}

TH2* SimResponseFunction::CreateTH2plusF0Raw()
{
	cout << "\t\t<I> SimResponseFunction::CreateTH2plusF0Raw() is started \n";
	TH2* hin = GetFunctionAsTH2();
	TH2* hout = (TH2*) hin->Clone(fHistNames[2].c_str());
	TString stit(fHistTitles[2].c_str());
	stit += GetAnaliticalSignature();
	hout->SetTitle(stit);

	hout->SetOption("colz");
	GetListHists(fgNamHist0)->Add(hout);
	cout
			<< "\t\t<I> SimResponseFunction::CreateTH2plusF0Raw() is ended : hout-> Intergral() "
			<< hout->Integral() << endl;
	return hout;
}

TH2* SimResponseFunction::CreateTH2Smooth()
{
	cout << "\t\t<I> SimResponseFunction::CreateTH2Smooth() is started \n";
	TH2* hin = GetFunctionAsTH2();
	TH2* hout = (TH2*)hin->Clone(fHistNames[3].c_str());
	TString stit(fHistTitles[3].c_str());
	stit += GetAnaliticalSignature();
	hout->SetTitle(stit);

	hout->SetOption("colz");
	GetListHists(fgNamHist0)->Add(hout);
	cout << "\t\t<I> SimResponseFunction::CreateTH2Smooth() is ended \n";
	return hout;
}

TH2* SimResponseFunction::CreateTH2SmoothAndThreshold()
{
	cout << "\t\t<I> SimResponseFunction::CreateTH2SmoothAndThreshold() is started \n";
	TH2* hin = GetFunctionAsTH2();
	TH2* hout = (TH2*)hin->Clone(fHistNames[4].c_str());
	TString stit(fHistTitles[4].c_str());
	stit += GetAnaliticalSignature();
	hout->SetTitle(stit);

	hout->SetOption("colz");
	GetListHists(fgNamHist0)->Add(hout);
	cout << "\t\t<I> SimResponseFunction::CreateTH2SmoothAndThreshold() is ended \n";
	return hout;
}

void SimResponseFunction::FillTH2(TH2* h, TF2* f, sdClusterChar& par, Double_t th, Int_t nev)
{
	// Fill TH2 histogram and calculate pars
	if(h==0 || f==0) return;
	// Update hist name&title
	h->SetName(Form("%sN%i", fHistNames[0].c_str(), nev));
	TString stit(Form("Nev %i %s ",nev,fHistTitles[0].c_str()));
	stit += GetAnaliticalSignature();
    h->SetTitle(stit);

	Double_t epsil = 0.0; // fill by function
//	epsil = 0.001; // accuracy of integrall calculation
//	par.numpixels = sda::FillH2byTF2(h, f, th, epsil);
	par.numpixels = FillH2byEllipsoid(h, f, th, epsil); // Should be put to the SDElipsoid
	par.maxamp = h->GetMaximum();

	TH1::AddDirectory(kFALSE);
	// hamp used here for axis definition only
	TH1 *hamp = new TH1F("AmpOfTF2","amplitude distribution of TF2", 100, 0.0, h->GetMaximum()*1.1);
	TList* lpro = sdu::BookAndFill1DHistsFromClusterHist(h,"TF2", 0, hamp);
	lpro->SetName(fgNamProj);

	delete hamp;
	hamp = sda::GetH1(lpro,2);
	par.aveamp = hamp->GetMean();

	sdu::GetFWHMs(sda::GetH1(lpro,0), sda::GetH1(lpro,1), par);

	GetList()->Add(lpro);
}

void SimResponseFunction::FillTH2plusF0(Int_t nev)
{
    TH2* hin  = GetFunctionAsTH2();
    TH2* hout = GetFunctionPlusF0AsTH2();
    // Update name&title
	hout->SetName(Form("%sN%i", fHistNames[1].c_str(), nev));
	TString stit(Form("Nev %i %s ",nev,fHistTitles[1].c_str()));
	stit += GetAnaliticalSignature();
    hout->SetTitle(stit);

    Double_t f0 = 1; // on definition
	Double_t fun=0.0, ey=0.0, fsum = 0.0;
	for(Int_t binx=1; binx<=hout->GetNbinsX(); ++binx){
		for(Int_t biny=1; biny<=hout->GetNbinsY(); ++biny){
			fun  = hin->GetBinContent(binx,biny);
			ey   = fSDF0->GetErrorHist()->GetRandom(); // Error should be calculated more carefully
			fsum = f0 + fun + ey;
			// Take into account digitization
			fsum = Int_t(fsum/fSDF0->GetDigitizationScale())*fSDF0->GetDigitizationScale();
			hout->SetBinContent(binx, biny, fsum);
		}
	}
	return;
}

void SimResponseFunction::FillTH2plusF0Raw(Int_t nev)
{
    TH2* hin  = GetFunctionPlusF0AsTH2();
    TH2* hout = GetFunctionPlusF0AsTH2Raw();
    // Update name&title
	hout->SetName(Form("%sN%i", fHistNames[2].c_str(), nev));
	TString stit(Form("Nev %i %s ",nev,fHistTitles[2].c_str()));
	stit += GetAnaliticalSignature();
	stit += "; time ; space";
    hout->SetTitle(stit);

	Double_t fin = 0.0, fout = 0.0, f0x = 0.0, f0y = 0.0;
	TAxis *ax = hout->GetXaxis(), *ay=hout->GetYaxis();
	for(Int_t binx=1; binx<=hout->GetNbinsX(); ++binx){
		f0x = fSDF0->GetF0X(ax->GetBinCenter(binx));
		for(Int_t biny=1; biny<=hout->GetNbinsY(); ++biny){
			f0y = fSDF0->GetF0Y(ay->GetBinCenter(biny));
			fin  = hin->GetBinContent(binx,biny);
			fout = fin * f0x * f0y;
			hout->SetBinContent(binx, biny, fout);
			// hout->SetBinError(binx, biny, ey);
		}
	}
}

void SimResponseFunction::FillAll(Double_t th, Int_t nev, TString opt)
{
    TList *l = GetListHists(fgNamHist0);
	ResetFunctionHists(l);
    DefineAnaliticalSignature();

	TH2* h = GetFunctionAsTH2();
    TF2* f = GetFunction();
    FillTH2(h, f, fPars, th, nev);
    FillHistsPars1VsPars2(GetListHists(fgNamHist1), fAnaPars, fPars);
    // return;
    //
    FillTH2plusF0(nev);
    //
    FillTH2plusF0Raw(nev);
    if(opt.Length()) {
        TH2* hout = GetFunctionPlusF0AsTH2Smooth();
        // Update name&title
        TString snam = fHistNames[3].c_str() + opt;
        snam += Form("N%i",nev);
    	hout->SetName(snam);
    	TString stit(Form("Nev %i %s %s",nev,fHistTitles[3].c_str(), opt.Data()));
        stit.ReplaceAll("Filter",opt.Data());
    	stit += GetAnaliticalSignature();
        hout->SetTitle(stit);
        //
        hout  = GetFunctionPlusF0AsTH2Threshold();
        snam  = fHistNames[4].c_str() + opt;
        snam += Form("Th=%4.2f, n=%i", th, nev);
    	hout->SetName(snam);
        stit = Form("%s, %s,Th=%4.2f,Nev %i ", fHistTitles[1].c_str(), opt.Data(),th,nev);
     	stit += GetAnaliticalSignature();
     	stit += "; ; ";
        hout->SetTitle(stit);
   }
}

void SimResponseFunction::DefineAnaliticalSignature()
{
    TF2* f = GetFunction();
    fSignature = "";
	for(Int_t i=0; i<f->GetNpar(); ++i) fSignature += Form("%s %4.1f, ",f->GetParName(i), f->GetParameter(i));
}

// Ellipsoid specific : speed up the process calculation
Int_t SimResponseFunction::FillH2byEllipsoid(TH2* h2, TF2 *f2, Double_t th, Double_t epsil)
{
	// 2013, Jan 12;
	// Return number of entries (pixels)
	// epsil<=0 : simple filling by function
	// epsil >0 : fill by integral value with relative accuracy = epsil
	// Common algorithm is not so effective if function equal zero on most of area
	int pri=0;
	if(pri) cout<<" SDAliases::FillH2byEllipsoid : th "<<th<<" epsil "<<epsil<<endl;
	if(h2==0 || f2==0) return 0;
    Int_t numPixeles = 0;

	TAxis* ax = h2->GetXaxis(), *ay = h2->GetYaxis();
    Double_t xmin = f2->GetParameter(1) - f2->GetParameter(2);
    Double_t xmax = f2->GetParameter(1) + f2->GetParameter(2);
    Int_t binxMin = ax->FindBin(xmin), binxMax = ax->FindBin(xmax);
    Double_t ymin = f2->GetParameter(3) - f2->GetParameter(4);
    Double_t ymax = f2->GetParameter(3) + f2->GetParameter(4);
    Int_t binyMin = ay->FindBin(ymin), binyMax = ay->FindBin(ymax);

	Double_t fun = 0.0;
	for(Int_t binx=binxMin; binx<=binxMax; ++binx){
		if(pri) cout<<" binx "<<binx<<endl;
		for(Int_t biny=binyMin; biny<=binyMax; ++biny) {
            if(epsil<=0.0) {
			  fun = f2->Eval(ax->GetBinCenter(binx),ay->GetBinCenter(biny));
            } else {
              Double_t x1=ax->GetBinLowEdge(binx), x2 = ax->GetBinUpEdge(binx), xw=ax->GetBinWidth(binx);
              Double_t y1=ay->GetBinLowEdge(biny), y2 = ay->GetBinUpEdge(biny), yw=ay->GetBinWidth(biny);
              fun = f2->Integral(x1,x2, y1,y2, epsil)/(xw*yw);
            }
			if(fun>th) {
				h2->SetBinContent(binx,biny, fun);
				++numPixeles;
				if(pri) cout<<" biny "<<biny<<" fun "<<fun<<endl;
			}
		}
	}
	return numPixeles;
}
//
//  Hists
//
void SimResponseFunction::Browse(TBrowser *b)
{
   if(fTF2) b->Add(fTF2);
   SDObjectSet::Browse(b);
//   	   if(fSDF0) b->Add(fSDF0, "f0");
}
