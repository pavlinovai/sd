/*
 * sdSimPars.cpp
 *
 *  Created on: Dec 22, 2012
 *      Author: Alexei Pavlinov
 */

#include "sdSimPars.h"
#include "sdCommon.h"

#include "iostream"
#include "cassert"

ClassImp(sdSimPars)

sdCommon *sdc = sdCommon::GetSdCommon();
sdSimPars::sdSimPars() : nev(0), simfun(""), parsfun(0), f0Name(""), parsF0(){
	// for reading only
}

sdSimPars::sdSimPars(int var) {
	// for reading only
	InitEllipsoid(var);
}

sdSimPars::~sdSimPars() {
	// TODO Auto-generated destructor stub
}

void sdSimPars::InitEllipsoid(int var)
{
	funPair fp = sdc->nameOfFuns[0];

	nev    = 1000;
	simfun = fp.first.c_str();;
	parsfun.Set(3);
	f0Name = sdc->nameOfF0[0].c_str();
	switch(var){
	case 1: // Big cluster
		parsfun[0]  = 4.;
		parsfun[1] = 100.;
		parsfun[2] = 100.;
		break;
	default:
		std::cout<<" Ellipsoid variant "<<var<<" is not available \n";
		assert(0);
	}
}
