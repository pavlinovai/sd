/*
 * SimResponseFunction.h
 *
 *  Created on: Dec 15, 2012
 *      Author: Alexei Pavlinov
 *
 */

#ifndef SIMRESPONSEFUNCTION_H_
#define SIMRESPONSEFUNCTION_H_

#include "SDObjectSet.h"
#include "sdCommon.h"
#include "SDAliases.h"
#include "SDUtils.h"
#include <sdClusterChar.h>

#include <TMath.h>
#include <TList.h>
#include <TF2.h>

class TH2;
class TBrowser;
class SDF0Base;

class SimResponseFunction : public SDObjectSet {
public:
    static const Int_t i0=14;
	enum EFunStatusBits {
	      kAsTF2              = BIT(i0),   // Function is TF2
	      kAnalyticalSolution = BIT(i0+1), // Can calculate pars analytically
	      kAsTH2              = BIT(i0+2)  // Function can be as TH2 only
	};

    SimResponseFunction();
    SimResponseFunction(const Char_t* name);
    virtual ~SimResponseFunction();
    virtual void Init();
    virtual void InitNamesAndTitles();
	virtual void CalculateAnalyticalSolution() {;} // Empty
	// Set methods
	void SetHist(TH2* h2) {GetList()->Add((TObject*)h2);}
	void SetF0(SDF0Base* var);

	Bool_t IsThisTF2Function() {return TestBit(i0);}
	Bool_t ExistAnalyticalSolution() {return TestBit(i0+1);}
	Bool_t IsThisTH2Function() {return TestBit(i0+2);}

	// Virtual function
	virtual TF2* GetFunction() {return fTF2;}
	virtual TList* GetFunHists() {return GetListHists(fgNamHist0);}
	virtual TList* GetAnaVsTH2Hists() {return GetListHists(fgNamHist1);}
    virtual TH2* GetFunctionAsTH2() {return sda::GetH2(GetFunHists(),0);}
	virtual TH2* GetFunctionPlusF0AsTH2() {return sda::GetH2(GetFunHists(),1);}
	virtual TH2* GetFunctionPlusF0AsTH2Raw() {return sda::GetH2(GetFunHists(),2);}
	virtual TH2* GetFunctionPlusF0AsTH2Smooth() {return sda::GetH2(GetFunHists(),3);}
	virtual TH2* GetFunctionPlusF0AsTH2Threshold() {return sda::GetH2(GetFunHists(),4);}
	virtual TList* GetProjHists() {return GetListHists(fgNamProj);}
	virtual TList* GetTH2VsClfOneHists() {return GetListHists(fgNamHist2);}
	virtual TList* GetTH2VsClfTwoHists() {return GetListHists(fgNamHist3);}

	virtual TH2* CreateTH2();
	virtual TH2* CreateTH2plusF0();
	virtual void CutTH2plusF0(TH2* h2in, Double_t th=0.0);
	virtual TH2* CreateTH2plusF0Raw();
	virtual TH2* CreateTH2Smooth();
	virtual TH2* CreateTH2SmoothAndThreshold();

	virtual void ResetFunctionHists(TList *l);
	virtual void FillTH2(TH2* h, TF2* f, sdClusterChar& par, Double_t th, Int_t nev);
	virtual void FillTH2plusF0(Int_t nev);
	virtual void FillTH2plusF0Raw(Int_t nev);
	virtual void FillAll(Double_t th, Int_t nev, TString opt);
	virtual void DefineAnaliticalSignature();
	virtual TString& GetAnaliticalSignature() {return fSignature;}
	// Analytical signature
	// nonvirtual function
	Int_t FillH2byEllipsoid(TH2* h2, TF2 *f2, Double_t th=0.0, Double_t epsil=0.0); // Return number of entries (pixels)
	//  Hists
	virtual TList* BookHistsPars1VsPars2(const char* name, const char* opt="Ana vs TH2", Int_t pri=0)
	{return sdu::BookHistsPars1VsPars2(name, opt, pri);}
	virtual void FillHistsPars1VsPars2(TList* l, const sdClusterChar& par1, const sdClusterChar& par2)
	{sdu::FillHistsPars1VsPars2(l,par1,par2);}
	//
    virtual sdClusterChar GetPars()    {return fPars;}
    virtual sdClusterChar GetAnaPars() {return fAnaPars;}

	virtual Double_t GetMax()    {return fPars.maxamp;}
    virtual Double_t GetMean()   {return fPars.aveamp;}
	virtual Double_t GetFWHMX()  {return fPars.duration;}
	virtual Double_t GetFWHMY()  {return fPars.size;}
    //
    virtual Double_t GetAnaMax()    {return fAnaPars.maxamp;}
    virtual Double_t GetAnaMean()   {return fAnaPars.aveamp;}
	virtual Double_t GetAnaFWHMX()  {return fAnaPars.duration;}
	virtual Double_t GetAnaFWHMY()  {return fAnaPars.size;}

protected:
	virtual Bool_t IsFolder() const {return kTRUE;}
    virtual void   Browse(TBrowser *b);

    TF2*          fTF2;      // Should be defined in derived constructor
	SDF0Base*     fSDF0;     // Pointer to F0
	sdClusterChar fPars;     // From hist
	sdClusterChar fAnaPars;  // Analytical calculation (if available)
public:
	static Double_t fgXmax;
    static Double_t fgYmax;
    //
    TString fSignature; //!
private:
    // Name&Title
    vecStr fHistNames;  //!
    vecStr fHistTitles; //!
    //
    static const Char_t* fgNamHist0; // Hist of function
    static const Char_t* fgNamHist1; // Hist of comparing analytical vs TH2 parameters calculation
    static const Char_t* fgNamHist2; // Hist of comparing TH2 pars vs cluster finder one
    static const Char_t* fgNamHist3; // Hist of comparing TH2 pars vs cluster finder two
    static const Char_t* fgNamProj;  // projection of initial TH2

ClassDef(SimResponseFunction,1)
};

#endif /* SIMRESPONSEFUNCTION_H_ */
