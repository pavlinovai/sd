/*
 * SDSimulationManager.h
 *
 *  Created on: Dec 22, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDSIMULATIONMANAGER_H_
#define SDSIMULATIONMANAGER_H_

#include "SDObjectSet.h"
#include "sdSimPars.h"

class SimResponseFunction;
class SDF0Base;
class SDClusterFinderStdOne;
class SDPedestals;
class sdSimPars;

class TArrayD;

static Double_t parsEllipsoidDefault[3]={1.,10.,10.};
static Int_t nparsDefault = sizeof(parsEllipsoidDefault)/sizeof(Double_t*);

class SDSimulationManager: public SDObjectSet {
public:
	SDSimulationManager();
	SDSimulationManager(Int_t nev, const Char_t* funName="ellipsoid",
			Double_t* pars=parsEllipsoidDefault, Int_t npar=nparsDefault);
	SDSimulationManager(sdSimPars& simPars);

	virtual ~SDSimulationManager();
	// To do: hists for x0,y0 distributions
    void   Init();
    SDClusterFinderStdOne* InitClusterFinder();
    //
	SimResponseFunction*   GetFunction();
    SDClusterFinderStdOne* GetClusterFinder();
    SDPedestals*           GetPedestals();

	// RunOne simulation
	Int_t RunOne();
	// Save : Should be revised later - Jan 8,2013
    void SaveSim(const Char_t* addName=""); // *MENU*
    // Static methods
	static SimResponseFunction* GetAnalyticalFunction(const Char_t* funName, const Double_t* pars, const Int_t npar);
	static SimResponseFunction* GetAnalyticalFunction(const Char_t* funName, const TArrayD& arr);
protected:
	sdSimPars fSimPars;
	// Working variable
	Double_t fXmin, fXmax, fXstep; // Random simulation of x0
	Double_t fYmin, fYmax, fYstep; // Random simulation of y0

	ClassDef(SDSimulationManager,1)
};

#endif /* SDSIMULATIONMANAGER_H_ */
