/*
 * SDSimPictures.h
 *
 *  Created on: Jan 13, 2013
 *      Author: Alexei Pavlinov
 */

#ifndef SDSIMPICTURES_H_
#define SDSIMPICTURES_H_

#include "SDObjectSet.h"

class SDSimulationManager;
class TDataSet;
class TCanvas;

class SDSimPictures: public SDObjectSet {
public:
	SDSimPictures(const Char_t* dir);
	virtual ~SDSimPictures();
	//
	void SetDir(const char* dir) {fDir = dir;}
	SDSimulationManager* GetSimMan() const {return fSimMan;}
	TDataSet*  GetPool()  const {return fSimPool;}
    //
	SDSimulationManager* ReadSimObject();  // *MENU*
	//
    TCanvas*   DrawAnaVsTH2(); // *MENU*
//    TCanvas*   DrawF0PictTwo(); // *MENU*
//    TCanvas*   DrawF0PictThree(); // *MENU*
protected:
	TString    fDir;
	TDataSet*  fSimPool;
	SDSimulationManager* fSimMan;

	ClassDef(SDSimPictures,0)
};

#endif /* SDSIMPICTURES_H_ */
