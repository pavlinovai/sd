/*
 * SDF0Uniform.h
 *
 *  Created on: Dec 16, 2012
 *      Author: Alexei Pavlinov
 */

#ifndef SDF0UNIFORM_H_
#define SDF0UNIFORM_H_

#include <Rtypes.h>
#include "SDF0Base.h"

// This number has come from analysis of exp.data
// Look to picture No_events_red_1tet2.tif
const Double_t f0yOne = 0.2774;
const Double_t sigOne = 0.399; // F0 unit
const Double_t xmin = -0.88;   //
const Double_t xmax =  2.00;   //

class SDF0Uniform: public SDF0Base {
public:
	SDF0Uniform();
	SDF0Uniform(const Char_t* name);
	virtual ~SDF0Uniform();
	virtual void Init();

    void SetF0Y(Double_t var) {fF0Y=var;}
    void setF0Sig(Double_t fF0Sig)
    {this->fF0Sig = fF0Sig;}
    void setXmax(Double_t fXmax)
    {this->fXmax = fXmax;}
    void setXmin(Double_t fXmin)
    {this->fXmin = fXmin;}
	void SetPars(Double_t f0y=f0yOne, Double_t f0Sig=sigOne); // *MENU*

	virtual Double_t GetF0X(Double_t x) const {return 1.;} // no time dependence
	virtual Double_t GetF0Y(Double_t y) const {return fF0Y;}
	// F0 unit
	virtual void SetSigF0Unit(Double_t f0Sig=sigOne) {fF0Sig = f0Sig;}// *MENU*
	virtual Double_t GetSigF0Unit() const {return fF0Sig;}
	virtual TH1*     GetErrorHist();
protected:
	Double_t fF0Y;
	Double_t fF0Sig;
	Double_t fXmin;
	Double_t fXmax;

	ClassDef(SDF0Uniform,1)

};

#endif /* SDF0UNIFORM_H_ */
