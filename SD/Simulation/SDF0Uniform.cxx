/*
 * SDF0Uniform.cpp
 *
 *  Created on: Dec 16, 2012
 *      Author: Alexei Pavlinov
 */

#include "SDF0Uniform.h"
#include "SDAliases.h"
#include "SDUtils.h"
#include "sdCommon.h"

#include <TF1.h>
#include <TH1.h>

ClassImp(SDF0Uniform)

SDF0Uniform::SDF0Uniform() :  fF0Y(0), fF0Sig(0), fXmin(0), fXmax(0) {
	// For reading only
}

SDF0Uniform::SDF0Uniform(const Char_t* name) :  fF0Y(f0yOne), fF0Sig(sigOne), fXmin(xmin), fXmax(xmax) {
    vecStr nf0 = sd::GetSdCommon()->nameOfF0;
	TString stmp(nf0[0].c_str());
	if(strlen(name)) stmp += name;
	SetName(stmp);
}

SDF0Uniform::~SDF0Uniform() {
	// TODO Auto-generated destructor
}

void SDF0Uniform::Init()
{
    TH1::AddDirectory(kFALSE);
	TF1 gaus("gaus","exp(-0.5*(x/[0])**2)", fXmin, TMath::Abs(fXmin));
	gaus.SetNpx(200);
	gaus.SetParameter(0,fF0Sig);
	TH1* h = (TH1*)gaus.GetHistogram()->Clone("GausSig0.4");
	TString stit(Form("Gaus dist., x_{0}=0, #sigma=%4.3f, xmin=%5.2f xmax=%5.2f",fF0Sig,fXmin, TMath::Abs(fXmin)));
	h->SetTitle(stit);
	GetList()->Add(h);
}

void SDF0Uniform::SetPars(Double_t f0y, Double_t f0Sig)
{
	SetF0Y(f0y);
	SetSigF0Unit(f0Sig);
}

TH1* SDF0Uniform::GetErrorHist()
{
	return sda::GetH1(GetList(),0);
}

